$(document).ready(function() {
	$('#form_upload').on('submit', function(event){
		event.preventDefault();
		var formData = new FormData($('#form_upload')[0]);

		$('.msg').hide();
		$('.progress').show();
		
		$.ajax({
			xhr : function() {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener('progress', function(e){
					if(e.lengthComputable){
						console.log('Bytes Loaded : ' + e.loaded);
						console.log('Total Size : ' + e.total);
						console.log('Persen : ' + (e.loaded / e.total));
						
						var percent = Math.round((e.loaded / e.total) * 100);
						
						$('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
					}
				});
				return xhr;
			},
			
			type : 'POST',
			url : 'upload.php',
			data : formData,
			processData : false,
			contentType : false,
			success : function(response){
				$('#form_upload')[0].reset();
				$('#btnBar').hide();
				$('.progress').hide();
				$('.msg').show();
				if(response == ""){
					alert('File Gagal Upload');
				}else{
					var msg = 'File Berhasil Upload. <br>Nama File : ' + response;
					$('.msg').html(msg);
				}
			}
		});
	});
});