<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 8;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeprs = $cari['KodePerson']; $kodeprs2 = $cari['KodePersonManual']; $namaprs = $cari['NamaPerson']; $tlahir = $cari['TempatLahir']; $tgllahir = $cari['TanggalLahir']; 
	$noktp = $cari['NoIdentitas']; $alamat = $cari['Alamat']; $kontakprs = $cari['ContactPerson']; $emailprs = $cari['Email']; $ketprs = $cari['Keterangan']; 
	$jenisk = $cari['JenisKelamin']; $fotoprs = $cari['FotoProfil']; $prov = $cari['Provinsi']; $kab = $cari['Kabupaten']; $kec = $cari['Kecamatan']; $desa = $cari['Desa'];
	$bankprs = $cari['NamaBankPerson']; $norekprs = $cari['NoRekPerson']; $banknama = $cari['AtasNamaBank']; $userprs = $cari['UserName'];
	$fbprs = $cari['Facebook']; $igprs = $cari['Instagram'];
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Supplier
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-supplier.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-supplier.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
					<?php if($id == null AND $page == null){ ?>
					<ul class="nav nav-tabs">
                        <li class="active"><a href="mst-supplier.php">Supplier</a></li>
                        <li><a href="mst-maindist.php">Master Distributor</a></li>
                        <li><a href="mst-dist.php">Distributor</a></li>
                        <li><a href="mst-agen.php">Agen</a></li>
                        <li><a href="mst-reseller.php">Reseller</a></li>
                        <li><a href="mst-member.php">User</a></li>
                    </ul><br>
					<?php } ?>
					
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <!-- <th width="15%">Gambar</th> -->
                                        <th width="45%">Nama Supplier</th>
                                        <th width="15%">Set Aktif</th>
                                        <th width="10%">Status</th>
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where IsSupplier = 'TRUE' AND StatusPerson = 'SUPPLIER' order by IsVerified ASC, NamaPerson ASC ", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".strtoupper($cari['NamaPerson'])."</strong><br>Kode Supplier : ".ucwords($cari['KodePersonManual'])."<br>UserName : ".ucwords($cari['UserName']); ?></td>
										<td><?php if($cari['IsAktif'] == TRUE){ echo '<a href="mst-supplier.php?page='.htmlspecialchars(base64_encode("nonaktif")).'&id='.htmlspecialchars(base64_encode($cari["KodePerson"])).'" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='mst-supplier.php?page=".htmlspecialchars(base64_encode('aktif'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-danger btn-sm'>Non Aktif</a>"; } ?></td>
										<td><?php if($cari['IsVerified'] === '1'){ echo '<a href="#" class="btn btn-success btn-sm">Verified</a>'; } 
										else { echo "<a href='#' class='btn btn-danger btn-sm'>Not Verified</a>"; } ?>
										</td>
                                        <td>
											<a href="mst-supplier.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-supplier.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<a href="mst-supplier.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<!-- <div class="form-group">
                                            <label>Kode Supplier</label>
                                            <input class="form-control" type="text" name="_kodesupplier" placeholder="ex : Kode Supplier Seperti MMMM" autocomplete="off" required>
                                        </div> -->
										<div class="form-group">
                                            <label>Nama Supplier</label>
                                            <input class="form-control" type="text" name="_namasupplier" placeholder="ex : Nama Supplier" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" rows="8" cols="40" name="_alamatsup" placeholder="ex : Ini Adalah Alamat Supplier" autocomplete="off"></textarea>
                                        </div>
										<hr>
										<div class="form-group">
                                            <label>Username</label>
                                            <input class="form-control" type="text" name="_username" placeholder="ex : username" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <span>* Password Supplier Default adalah '123456'</span>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-input-sup">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-sup">Reset</button>
									</div>
								</form>
									
									<?php
										include "../connections/config.php";
										$_namasupplier = @htmlspecialchars($_POST['_namasupplier']); 
										$_alamatsup = @htmlspecialchars($_POST['_alamatsup']); $_username = @htmlspecialchars($_POST['_username']); $_pass = md5('123456'); 
										if(isset($_POST['_submit-input-sup'])){
											// membuat id otomatis
											$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePerson,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePerson,' ',''),8)='PRS-".date('Y')."'") or die( print_r( sqlsrv_errors(), true)); 
											$nums = @sqlsrv_num_rows($sql); 
											while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											// membuat kode user
											$bikin_kode = str_pad($kode, 10, "0", STR_PAD_LEFT);
											$kode_jadi = "PRS-".date('Y')."-".$bikin_kode;
											
											// membuat id otomatis 2
											$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode2 FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='SGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true)); 
											$nums2 = @sqlsrv_num_rows($sql2); 
											while($data2 = @sqlsrv_fetch_array($sql2, SQLSRV_FETCH_ASSOC)){
												if($nums2 === 0){ $kode2 = 1; }else{ $kode2 = $data2['kode2'] + 1; }
											}
											// membuat kode user 2
											$bikin_kode2 = str_pad($kode2, 10, "0", STR_PAD_LEFT);
											$kode_jadi2 = "SGM-".date('Y')."-".$bikin_kode2;
									
											/*$cek = @sqlsrv_query($dbconnect, "select * from MasterPerson where replace(KodePersonManual,' ','') = replace('$_kodesupplier',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$num = @sqlsrv_num_rows($cek);
											if($num === 0){*/
												$cek2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where replace(UserName,' ','') = replace('$_username',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num2 = @sqlsrv_num_rows($cek2);
												if($num2 === 0){
													$query = @sqlsrv_query($dbconnect, "INSERT into MasterPerson(KodePerson,KodePersonManual,MemberID,NamaPerson,StatusPerson,IsAktif,
													Level,IsVerified,UserName,Password,FotoProfil,KodeCabang,IsSupplier,IsCustomer,IsSales,IsOther,PasswordTransaksi,Alamat,MemberSponsor,Referral,Tgl_Reg,MemberSponsorAsal)
													values('$kode_jadi','$kode_jadi2','$kode_jadi2','$_namasupplier','SUPPLIER','1','USER','0','$_username','$_pass','round_account.png','CAB-0001',
													'1','0','0','0','$_pass','$_alamatsup','$id_tree_aktif','https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($kode_jadi))."','".date('Y-m-d H:i:s')."','$id_tree_aktif')") or die( print_r( sqlsrv_errors(), true)); 	 
														if($query){
															echo '<script type="text/javascript">
															sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
															function () { window.location.href = "mst-supplier.php"; });
															</script>';
														}
														else{
															echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
														}
												}else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Username Sudah Terpakai ", "error"); </script>';
												}
											/*}else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Supplier Sudah Ada ", "error"); </script>';
											}*/
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Kode Supplier</label>
                                            <input class="form-control" type="text" value="<?php echo $kodeprs2; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama</label>
                                            <input class="form-control" type="text" name="_namasupplier2" value="<?php echo $namaprs; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
											<label>Jenis Kelamin</label><br/>
											<?php if($jenisk === 'Laki-laki'){
												echo '
												<label class="radio-inline">
													<input type="radio" name="_jenis_kelamin2" value="Laki-Laki" checked required>Laki-laki
												</label>
												<label class="radio-inline">
													<input type="radio" name="_jenis_kelamin2" value="Perempuan" required>Perempuan
												</label>
												';
											}elseif($jenisk === 'Perempuan'){
												echo '
												<label class="radio-inline">
													<input type="radio" name="_jenis_kelamin2" value="Laki-Laki" required>Laki-laki
												</label>
												<label class="radio-inline">
													<input type="radio" name="_jenis_kelamin2" value="Perempuan" checked required>Perempuan
												</label>
												';
											}else{
												echo '
												<label class="radio-inline">
													<input type="radio" name="_jenis_kelamin2" value="Laki-Laki" required>Laki-laki
												</label>
												<label class="radio-inline">
													<input type="radio" name="_jenis_kelamin2" value="Perempuan" required>Perempuan
												</label>
												';
											}
											?>
										</div>
										<div class="form-group">
                                            <label>No Identitas</label>
                                            <input class="form-control" type="text" name="_noktp2" value="<?php echo $noktp; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Kontak Person</label>
                                            <input class="form-control" type="text" name="_kontak2" value="<?php echo $kontakprs; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="email" name="_email2" value="<?php echo $emailprs; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Facebook</label>
                                            <input class="form-control" type="text" name="_fb2" value="<?php echo $fbprs; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Instagram</label>
                                            <input class="form-control" type="text" name="_ig2" value="<?php echo $igprs; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" rows="6" cols="40" name="_alamat2" autocomplete="off"><?php echo $alamat; ?></textarea>
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" rows="6" cols="40" name="_ketsup2" autocomplete="off"><?php echo $ketprs; ?></textarea>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-edit-sup">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-sup">Reset</button>
									</div>
								
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Provinsi</label>
                                            <select class="form-control" id="prov" name="_prov2" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Provinsi --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM provinces ORDER BY province_id") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list)){
														if($prov !== NULL){
															if($daftar['province_id'] === $prov){
																echo "<option value=\"".$daftar['province_id']."\" selected='selected'>".$daftar['provinsi']."</option>\n";
															}else{
																echo "<option value=\"".$daftar['province_id']."\" >".$daftar['provinsi']."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['province_id']."\" >".$daftar['provinsi']."</option>\n";
														}
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Kabupaten</label>
                                            <select class="form-control" id="kab" name="_kab2" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Kabupaten --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM regencies where province_id = '".$prov."' ORDER BY regency_id") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list)){
														if($kab !== NULL){
															if($daftar['regency_id'] === $kab){
																echo "<option value=\"".$daftar['regency_id']."\" selected='selected'>".$daftar['kabupaten']."</option>\n";
															}else{
																echo "<option value=\"".$daftar['regency_id']."\" >".$daftar['kabupaten']."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['regency_id']."\" >".$daftar['kabupaten']."</option>\n";
														}
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Kecamatan</label>
                                            <select class="form-control" id="kec" name="_kec2" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Kecamatan --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM districts where province_id = '".$prov."' AND regency_id = '".$kab."' ORDER BY district_id") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list)){
														if($kec !== NULL){
															if($daftar['district_id'] === $kec){
																echo "<option value=\"".$daftar['district_id']."\" selected='selected'>".$daftar['kecamatan']."</option>\n";
															}else{
																echo "<option value=\"".$daftar['district_id']."\" >".$daftar['kecamatan']."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['district_id']."\" >".$daftar['kecamatan']."</option>\n";
														}
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Desa</label>
                                            <select class="form-control" id="desa" name="_desa2" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Desa --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM villages where province_id = '".$prov."' AND regency_id = '".$kab."' AND district_id = '".$kec."' ORDER BY village_id") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list)){
														if($desa !== NULL){
															if($daftar['village_id'] === $desa){
																echo "<option value=\"".$daftar['village_id']."\" selected='selected'>".$daftar['desa']."</option>\n";
															}else{
																echo "<option value=\"".$daftar['village_id']."\" >".$daftar['desa']."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['village_id']."\" >".$daftar['desa']."</option>\n";
														}
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
											<label>Bank Person</label>
											<select class="form-control" id="desa" name="_bank2" autocomplete="off" required>
												<?php echo '<option value="">-- Pilih Bank --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM mstbank ORDER BY KodeBank") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list)){
														if($bankprs !== NULL){
															if($daftar['KodeBank'] === $bankprs){
																echo "<option value=\"".$daftar['KodeBank']."\" selected='selected'>".$daftar['NamaBank']."</option>\n";
															}else{
																echo "<option value=\"".$daftar['KodeBank']."\" >".$daftar['NamaBank']."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['KodeBank']."\" >".$daftar['NamaBank']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group">
                                            <label>No Rekening</label>
                                            <input class="form-control" type="text" name="_norek2" value="<?php echo $norekprs; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Nama Rekening</label>
                                            <input class="form-control" type="text" name="_namarek2" value="<?php echo $banknama; ?>" autocomplete="off" required>
                                        </div>
									</div>
									
										<?php
										include "../connections/config.php";
										$_namasup2 = @htmlspecialchars($_POST['_namasupplier2']); $_noktp2 = @htmlspecialchars($_POST['_noktp2']); $_kontak2 = @htmlspecialchars($_POST['_kontak2']);
										$_email2 = @htmlspecialchars($_POST['_email2']); $_alamat2 = @htmlspecialchars($_POST['_alamat2']); $_ketsup2 = @htmlspecialchars($_POST['_ketsup2']);
										$_prov2 = @htmlspecialchars($_POST['_prov2']); $_kab2 = @htmlspecialchars($_POST['_kab2']); $_kec2 = @htmlspecialchars($_POST['_kec2']);
										$_desa2 = @htmlspecialchars($_POST['_desa2']); $_bank2 = @htmlspecialchars($_POST['_bank2']); $_atasnama2 = @htmlspecialchars($_POST['_namarek2']);
										$_norek2 = @htmlspecialchars($_POST['_norek2']); $_jenis2 = @htmlspecialchars($_POST['_jenis_kelamin2']);
										$_fb2 = @htmlspecialchars($_POST['_fb2']); $_ig2 = @htmlspecialchars($_POST['_ig2']);
										if(isset($_POST['_submit-edit-sup'])){
											if($emailprs === $_email2){
												$query = @sqlsrv_query($dbconnect, "update MasterPerson set NamaPerson = '$_namasup2', NoIdentitas = '$_noktp2', ContactPerson = '$_kontak2', HandphoneCP = '$_kontak2', 
												TelpCP = '$_kontak2', Email = '$_email2', Alamat = '$_alamat2', Keterangan = '$_ketsup2', Provinsi = '$_prov2', Kabupaten = '$_kab2', Kecamatan = '$_kec2', Desa = '$_desa2', 
												NamaBankPerson = '$_bank2', NoRekPerson = '$_norek2', AtasNamaBank = '$_atasnama2', JenisKelamin = '$_jenis2', Facebook = '$_fb2', Instagram = '$_ig2' where KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-supplier.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
											}else{
												$cek3 = @sqlsrv_query($dbconnect, "select * from MasterPerson where replace(Email,' ','') = replace('$_email2',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num3 = @sqlsrv_num_rows($cek3);
												if($num3 > 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Email Sudah Terpakai ", "error"); </script>';
												}else{
													$query = @sqlsrv_query($dbconnect, "update MasterPerson set NamaPerson = '$_namasup2', NoIdentitas = '$_noktp2', ContactPerson = '$_kontak2', HandphoneCP = '$_kontak2', 
													TelpCP = '$_kontak2', Email = '$_email2', Alamat = '$_alamat2', Keterangan = '$_ketsup2', Provinsi = '$_prov2', Kabupaten = '$_kab2', Kecamatan = '$_kec2', Desa = '$_desa2', 
													NamaBankPerson = '$_bank2', NoRekPerson = '$_norek2', AtasNamaBank = '$_atasnama2', JenisKelamin = '$_jenis2', Facebook = '$_fb2', Instagram = '$_ig2' where KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true)); 	 
														if($query){
															echo '<script type="text/javascript">
															sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
															function () { window.location.href = "mst-supplier.php"; });
															</script>';
														}
														else{
															echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
														}
												}
												
											}
										}
										@sqlsrv_close;

										?>
                                </form>   
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-3">
									<div class="form-group">
                                        <label>Foto Profil</label>
                                            <?php if($fotoprs == null OR $fotoprs === '' OR $fotoprs === 'round_account.png'){
												echo '<dd><img class="imgl img-thumbnail" src="../android_grosirmart/img/fotoprofil/round_account.png" width="100%" /></dd>'; 
											}else{ 
												echo '<dd><img class="imgl img-thumbnail" src="../android_grosirmart/img/fotoprofil/'.$fotoprs.'" width="100%" /></dd>';
											} ?>
                                    </div>
								</div>
								
								<div class="col-lg-4">
									<div class="form-group">
                                        <label>Nama Supplier</label>
                                            <dd><?php echo $kodeprs2." : ".ucwords($namaprs)."<br>Username : ".$userprs; ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Alamat</label>
                                            <dd><?php echo ucwords($alamat)."<br>";
											if($kontakprs === null OR $kontakprs === ""){ echo "Kontak Person : -"; } else { echo "Kontak Person : ".ucwords($kontakprs); }; echo "<br>"; 
											if($emailprs === null OR $emailprs === ""){ echo "Email : -"; } else { echo "Email : ".$emailprs; }; echo "<br>"; 
											if($fbprs === null OR $fbprs === ""){ echo "Facebook : -"; } else { echo "Facebook : ".$fbprs; }; echo "<br>"; 
											if($igprs === null OR $igprs === ""){ echo "Instagram : -"; } else { echo "Instagram : ".$igprs; };
											?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Provinsi</label>
                                            <dd><?php $edit = @sqlsrv_query($dbconnect, "select * from provinces where province_id = '$prov'");
												$row = @sqlsrv_fetch_array($edit);
												if($prov === null OR $prov === ""){ echo "-"; } else { echo ucwords($row['provinsi']); } ?>
											</dd>
                                    </div>
									<div class="form-group">
                                        <label>Kabupaten</label>
                                            <dd><?php $edit = @sqlsrv_query($dbconnect, "select * from regencies where regency_id = '$kab'");
												$row = @sqlsrv_fetch_array($edit);
												if($kab === null OR $kab === ""){ echo "-"; } else { echo ucwords($row['kabupaten']); } ?>
											</dd>
                                    </div>
									<div class="form-group">
                                        <label>Kecamatan</label>
                                            <dd><?php $edit = @sqlsrv_query($dbconnect, "select * from districts where district_id = '$kec'");
												$row = @sqlsrv_fetch_array($edit);
												if($kec === null OR $kec === ""){ echo "-"; } else { echo ucwords($row['kecamatan']); } ?>
											</dd>
                                    </div>
									<div class="form-group">
                                        <label>Desa</label>
                                            <dd><?php $edit = @sqlsrv_query($dbconnect, "select * from villages where village_id = '$desa'");
												$row = @sqlsrv_fetch_array($edit);
												if($desa === null OR $desa === ""){ echo "-"; } else { echo ucwords($row['desa']); } ?>
											</dd>
                                    </div>
								</div>
								
								<div class="col-lg-4">
									<div class="form-group">
                                        <label>Data Bank</label>
                                            <dd><?php $edit = @sqlsrv_query($dbconnect, "select * from mstbank where KodeBank = '$bankprs'");
												$row = @sqlsrv_fetch_array($edit);
												if($bankprs === null OR $bankprs === ""){ echo "-"; } else { echo ucwords($row['NamaBank']); } ?>
											</dd>
                                    </div>
									<div class="form-group">
                                        <label>Atas Nama</label>
                                            <dd><?php if($banknama === null OR $banknama === ""){ echo "-"; } else { echo ucwords($banknama); } ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>No Rekening</label>
                                            <dd><?php if($norekprs === null OR $norekprs === ""){ echo "-"; } else { echo ucwords($norekprs); } ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Keterangan</label>
                                            <dd><?php if($ketprs === null OR $ketprs === ""){ echo "-"; } else { echo ucwords($ketprs); } ?></dd><br>
                                    </div>
								</div>
							</div>
						</div>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$file_path = "../android_wantrast_sqlsrv/img/fotoprofil/".$fotoprs; $file_path2 = "../android_wantrast_sqlsrv/img/fotoprofil/thumb_".$fotoprs;
							$cek = @sqlsrv_query($dbconnect, "select * from MasterBarang where replace(KodePerson,' ','') = replace('$kodeprs',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Supplier Memiliki Master Barang ", type: "error" },
								function () { window.location.href = "mst-supplier.php"; }); </script>';
							}else{
								$cek2 = @sqlsrv_query($dbconnect, "select * from TrPembelian where replace(KodePerson,' ','') = replace('$kodeprs',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$num2 = @sqlsrv_num_rows($cek2);
								if($num2 > 0){
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Supplier Dipakai Transaksi ", type: "error" },
									function () { window.location.href = "mst-supplier.php"; }); </script>';
								}else{
									$cek3 = @sqlsrv_query($dbconnect, "select * from MasterPerson where replace(KodePerson,' ','') = replace('$kodeprs',' ','') AND IsAktif = 'True'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
									$num3 = @sqlsrv_num_rows($cek3);
									if($num3 > 0){
										echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Non Aktifkan Person Terlebih Dahulu ", type: "error" },
										function () { window.location.href = "mst-supplier.php"; }); </script>';
									}else{
										// hapus data 
										if(file_exists($file_path)){ unlink($file_path); }
										if(file_exists($file_path2)){ unlink($file_path2); }
										$delete = @sqlsrv_query($dbconnect, "DELETE from MasterPerson WHERE KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true));
										echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
										function () { window.location.href = "mst-supplier.php"; }); </script>';
									}
								}
							}
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// non aktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterPerson set IsAktif = 'False' WHERE KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-supplier.php"; }); </script>';
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterPerson set IsAktif = 'True' WHERE KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-supplier.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script>
	$(document).ready(function() {
		$('#datepicker-example1').Zebra_DatePicker();
	});
	</script>
	
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<!-- membuat dropdown bertingkat tab I -->
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		  $("#prov").change(function(){
			var KodeProv = $("#prov option:selected").val();
			$.ajax({ url: "../get-data/get-kab.php", data: "get_prov="+KodeProv, cache: false, success: function(msg){ $("#kab").html(msg); } });
		  });
		  $("#kab").change(function(){
			var KodeProv = $("#prov option:selected").val();
			var KodeKab = $("#kab option:selected").val();
			$.ajax({ url: "../get-data/get-kec.php", data: "get_prov="+KodeProv+"&get_kab="+KodeKab, cache: false, success: function(msg){ $("#kec").html(msg); } });
		  });
		  $("#kec").change(function(){
			var KodeProv = $("#prov option:selected").val();
			var KodeKab = $("#kab option:selected").val();
			var KodeKec = $("#kec option:selected").val();
			$.ajax({ url: "../get-data/get-desa.php", data: "get_prov="+KodeProv+"&get_kab="+KodeKab+"&get_kec="+KodeKec, cache: false, success: function(msg){ $("#desa").html(msg); } });
		  });
		});
	</script>

</body>

</html>
