<?php
include "akses.php"; include "../connections/config.php"; include "tgl-indo.php";
$no = 1; $min_date = @$_POST["start_date"].' 00:00:00'; $max_date = @$_POST["end_date"].' 23:59:59';
$columns = array(0 => 'KodeLog', 1 => 'KodePerson', 2 => 'Tanggal', 3 => 'Action', 4 => 'Deskripsi');

if(@$_POST["is_date_search"] == "yes"){
	$query = 'SELECT * FROM ServerLog WHERE DateTimeLog BETWEEN "'.$min_date.'" AND "'.$max_date.'" AND ';
}elseif(@$_POST["is_date_search"] == "no"){
	$query = 'SELECT * FROM ServerLog WHERE DateTimeLog LIKE "%'.date("Y-m-d").'%" AND ';
}

if(isset($_POST["search"]["value"])){
 $query .= ' Deskripsi LIKE "%'.$_POST["search"]["value"].'%" ';
}

if(isset($_POST["order"])){
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' ';
}
else{
 $query .= 'ORDER BY DateTimeLog DESC ';
}

$query1 = '';

if($_POST["length"] != -1){
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = sqlsrv_num_rows(sqlsrv_query($dbconnect, $query));
$result = sqlsrv_query($dbconnect, $query . $query1);
$data = array();

while($row = sqlsrv_fetch_array($result)){
 $sub_array = array();
 $sub_array[] = $no++;
 $sub_array[] = '<strong>'.ucwords($row[3]).'</strong><br>'.TanggalIndo($row[2]).'&nbsp;'.DATE('H:i', strtotime($row[2])); 
 $sub_array[] = ucwords($row[5]); 
 $sub_array[] = ucwords($row[4]); 
 $data[] = $sub_array;
}

function get_all_data($dbconnect){
 $query = "SELECT * FROM logserver";
 $result = sqlsrv_query($dbconnect, $query);
 return sqlsrv_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($dbconnect),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>