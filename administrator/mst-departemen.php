<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 3;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from MasterDepartemen where KodeDepartemen = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodedpr = $cari['KodeDepartemen']; $kodedpr2 = $cari['KodeDepartemenManual']; $departemen = $cari['NamaDepartemen']; $fotodpr = $cari['GambarDepartemen']; $ketdpr = $cari['Keterangan'];
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Kategori
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-departemen.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-departemen.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="15%">Gambar</th>
                                        <th width="45%">Nama Kategori</th>
                                        <th width="10%">Status</th>
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select * from MasterDepartemen order by KodeDepartemen", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php if($cari['GambarDepartemen'] !== null){
												echo '<img class="imgl img-thumbnail" src="../img/foto-departemen/thumb_'.$cari['GambarDepartemen'].'" width="100%" />';
											}else{ 
												echo '<img class="imgl img-thumbnail" src="../img/foto-departemen/no-image.png" width="100%" />'; 
											} ?>
										</td>
                                        <td><?php echo "<strong>".ucwords($cari['NamaDepartemen'])."</strong><br>".$cari['KodeDepartemenManual']; ?></td>
                                        <td><?php if($cari['IsAktif'] == TRUE){ echo '<a href="mst-departemen.php?page='.htmlspecialchars(base64_encode("nonaktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeDepartemen"])).'" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='mst-departemen.php?page=".htmlspecialchars(base64_encode('aktif'))."&id=".htmlspecialchars(base64_encode($cari['KodeDepartemen']))."' class='btn btn-danger btn-sm'>Non Aktif</a>"; } ?></td>
                                        <td>
											<?php if($cari['KodeDepartemen'] === 'DPT-0000001'){
												echo '';
											} else { ?>
											<a href="mst-departemen.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeDepartemen'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-departemen.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['KodeDepartemen'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<a href="mst-departemen.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeDepartemen'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>
											<?php } ?>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<?php
									// membuat id otomatis
									include "../connections/config.php";
									$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeDepartemen,7)) AS kode FROM MasterDepartemen WHERE LEFT(replace(KodeDepartemen,' ',''),4)='DPT-'") or die( print_r( sqlsrv_errors(), true)); 
									$nums = @sqlsrv_num_rows($sql); 
									while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
										if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
									}
									$bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
									$kode_jadi = "DPT-".$bikin_kode;
									
								?>
									<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Masukkan Gambar Kategori Terlebih Dahulu</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kode_jadi);?>&type=<?php echo base64_encode('_dept');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									</div>
									  
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Kode Kategori Manual</label>
                                            <input class="form-control" type="text" name="_kodedpr" placeholder="ex : 000001" autocomplete="off" required>
                                            <input class="form-control" type="hidden" name="_iddpr" value="<?php echo $kode_jadi; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama Kategori</label>
                                            <input class="form-control" type="text" name="_namadpr" placeholder="ex : Nama Kategori" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <select class="form-control" name="_ketdpr" autocomplete="off" required>
												<option value="">-- Pilih Jenis Kategori --</option>
												<option value="PRODUK">Produk atau Barang</option>
												<option value="LAYANAN">Layanan atau Jasa</option>
										    </select>
                                        </div>
											
										<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>
										<button type="reset" class="btn btn-default" name="_submit-input-dpr">Reset</button>	
                                      
									</div>
								</form>
									
									<?php
									include "../connections/config.php";
									$_iddpr = @htmlspecialchars($_POST['_iddpr']); $_kode = @htmlspecialchars($_POST['_kodedpr']); $_nama = @htmlspecialchars($_POST['_namadpr']); $_ket = @htmlspecialchars($_POST['_ketdpr']); 
									if(isset($_POST['_submit-input-dpr'])){
										$cek = @sqlsrv_query($dbconnect, "select * from MasterDepartemen where replace(KodeDepartemen,' ','') = replace('$_iddpr',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										$num = @sqlsrv_num_rows($cek);
										if($num > 0){
											$cek2 = @sqlsrv_query($dbconnect, "select * from MasterDepartemen where replace(KodeDepartemenManual,' ','') = replace('$_kode',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$num2 = @sqlsrv_num_rows($cek2);
											if($num2 > 0){
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Kategori Manual Sudah Ada ", "error"); </script>';
											}else{
											$query = @sqlsrv_query($dbconnect, "update MasterDepartemen set KodeDepartemenManual = '$_kode', NamaDepartemen = '$_nama', Keterangan = '$_ket', 
											IsAktif = '1', IsDapatKupon = '0', IsDapatPoint = '1' where KodeDepartemen = '".$_iddpr."'") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-departemen.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
											}
										}else{
											echo '<script type="text/javascript">sweetAlert("Maaf!", " Anda Belum Upload Icon Gambar ", "error"); </script>';
										}
									}
									@sqlsrv_close;

									?>
                                    
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Ganti Icon Gambar</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kodedpr);?>&img=<?php echo base64_encode($fotodpr);?>&type=<?php echo base64_encode('_dept');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
											</div>
											<div class="form-group">
												<img class="imgl img-thumbnail" src="../img/foto-departemen/thumb_<?php echo $fotodpr; ?>" id="_oldimg" width="50%" /><br>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									</div>
									
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Kode Kategori</label>
                                            <input class="form-control" type="text" value="<?php echo $kodedpr; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Kode Kategori Manual</label>
                                            <input class="form-control" type="text" name="_kodedpr2" value="<?php echo $kodedpr2; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Nama Kategori</label>
                                            <input class="form-control" type="text" name="_dpr2" value="<?php echo $departemen; ?>" autocomplete="off" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <select class="form-control" name="_ketdpr2" autocomplete="off" required>
                                            <?php echo '<option value="">-- Pilih Kategori --</option>';
											if($ketdpr === 'PRODUK'){
												echo '<option value="PRODUK" selected="selected">Produk atau Barang</option>';
												echo '<option value="LAYANAN">Layanan atau Jasa</option>';	
											}elseif($ketdpr === 'LAYANAN'){
												echo '<option value="PRODUK">Produk atau Barang</option>';
												echo '<option value="LAYANAN" selected="selected">Layanan atau Jasa</option>';	
											}elseif($ketdpr === 'TERLARIS'){
												echo '<option value="TERLARIS" selected="selected">Terlaris</option>';
											}else{
												echo '<option value="PRODUK">Produk atau Barang</option>';
												echo '<option value="LAYANAN">Layanan atau Jasa</option>';
											}
											
											?>
											</select>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-edit-dpr">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-dpr">Reset</button>
									</div>
								</form>
										
										<?php
										include "../connections/config.php";
										$_kode2 = @htmlspecialchars($_POST['_kodedpr2']); $_nama2 = @htmlspecialchars($_POST['_dpr2']); $_ket2 = @htmlspecialchars($_POST['_ketdpr2']); 
										if(isset($_POST['_submit-edit-dpr'])){
											if($kodedpr2 === $_kode2){
												$query = @sqlsrv_query($dbconnect, "update MasterDepartemen set KodeDepartemenManual = '$_kode2', NamaDepartemen = '$_nama2', Keterangan = '$_ket2' 
												where KodeDepartemen = '".$kodedpr."'") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
														function () { window.location.href = "mst-departemen.php"; });
														</script>';
													}
													else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
													}
											}else{
												$cek = @sqlsrv_query($dbconnect, "select * from MasterDepartemen where replace(KodeDepartemenManual,' ','') = replace('$_kode2',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num = @sqlsrv_num_rows($cek);
												if($num > 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Kategori Manual Sudah Ada ", "error"); </script>';
												}else{
												$query = @sqlsrv_query($dbconnect, "update MasterDepartemen set KodeDepartemenManual = '$_kode2', NamaDepartemen = '$_nama2', Keterangan = '$_ket2' 
												where KodeDepartemen = '".$kodedpr."'") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
														function () { window.location.href = "mst-departemen.php"; });
														</script>';
													}
													else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
													}
												}
											}
										}
										@sqlsrv_close;

										?>
                                
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Icon Kategori</label>
                                            <?php if($fotodpr !== null){
												echo '<dd><img class="imgl img-thumbnail" src="../img/foto-departemen/'.$fotodpr.'" width="30%" /></dd>';
											}else{ 
												echo '<dd><img class="imgl img-thumbnail" src="../img/foto-departemen/no-image.png" width="30%" /></dd>'; 
											} ?>
                                    </div>
									<div class="form-group">
                                        <label>Kode kategori</label>
                                            <dd><?php echo $kodedpr2; ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Nama Kategori</label>
                                            <dd><?php echo ucwords($departemen); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Keterangan</label>
                                            <dd><?php echo ucwords($ketdpr); ?></dd><br>
                                    </div>
										
								</div>
							</div>
						</div>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$file_path = "../img/foto-departemen/".$fotodpr; $file_path2 = "../img/foto-departemen/thumb_".$fotodpr;
							$cek = @sqlsrv_query($dbconnect, "select * from MasterSubDepartemen where replace(KodeDepartemen,' ','') = replace('$kodedpr',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Departemen Dipakai Transaksi ", type: "error" },
								function () { window.location.href = "mst-departemen.php"; }); </script>';
							}else{
								// hapus data
								if(file_exists($file_path)){ unlink($file_path); }
								if(file_exists($file_path2)){ unlink($file_path2); }
								$delete = @sqlsrv_query($dbconnect, "DELETE from MasterDepartemen WHERE KodeDepartemen = '".$kodedpr."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-departemen.php"; }); </script>';
								
							}
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// non aktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterDepartemen set IsAktif = 'False' WHERE KodeDepartemen = '".$kodedpr."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-departemen.php"; }); </script>';
						}  elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterDepartemen set IsAktif = 'True' WHERE KodeDepartemen = '".$kodedpr."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-departemen.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>


</body>

</html>
