<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
/* $fitur_id = 5; */

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
$menu = isset($_GET['menu']) ? base64_decode($_GET['menu']) : 0 ;

$query = @mysqli_query($con, "select * from mstbankpenerima where KodeBank = '".$id."'") or die(mysqli_error($query));
while($cari = @mysqli_fetch_array($query)){ 
	$kodebank = $cari['KodeBank']; $bank = $cari['NamaBank']; $atasnama = $cari['AtasNama']; $norek = $cari['Rekening'];
}
@mysqli_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php /* include "lock-menu.php"; */ ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Bank Perusahaan
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-bank-penerima.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-bank-penerima.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
					<?php /* if($id == null AND $page == null){ ?>
					<ul class="nav nav-tabs">
                        <li><a href="mst-bank.php">Master Bank</a></li>
                        <li class="active"><a href="mst-bank-penerima.php">Bank Perusahaan</a></li>
                    </ul><br>
					<?php } */ ?>
					
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="70%">Nama Bank</th>
                                        <!-- <th width="15%">No Rekening</th> -->
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @mysqli_query($con, "select * from mstbankpenerima order by AtasNama") or die(mysqli_error($query));
								$no = 1;
								while($cari = @mysqli_fetch_array($query)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".ucwords($cari['Rekening'])." : ".ucwords($cari['AtasNama'])."</strong><br>Bank : ".ucwords($cari['NamaBank']); ?></td>
                                        <!-- <td><?php /* $query22 = @mysqli_query($con, "select * from mstbankpenerima where Peruntukan LIKE '%REGISTRASI%' AND KodeBank = '".$cari['KodeBank']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										$cari22 = @sqlsrv_num_rows($query22); if($cari22 === 1){ echo '<a href="" class="btn btn-default btn-sm">REGISTRASI</a>&nbsp;'; } ?>
										<?php $query33 = @mysqli_query($con, "select * from mstbankpenerima where Peruntukan LIKE '%GARANSI%' AND KodeBank = '".$cari['KodeBank']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										$cari33 = @sqlsrv_num_rows($query33); if($cari33 === 1){ echo '<a href="" class="btn btn-default btn-sm">GARANSI</a>&nbsp;'; } ?>
										<?php $query44 = @mysqli_query($con, "select * from mstbankpenerima where Peruntukan LIKE '%DOMPET%' AND KodeBank = '".$cari['KodeBank']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										$cari44 = @sqlsrv_num_rows($query44); if($cari44 === 1){ echo '<a href="" class="btn btn-default btn-sm">DOMPET</a>&nbsp;'; } */ ?>
										</td> -->
										<td>
											<a href="mst-bank-penerima.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeBank'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-bank-penerima.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['KodeBank'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<a href="mst-bank-penerima.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeBank'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@mysqli_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Nama Bank</label>
                                            <input class="form-control" type="text" name="_namabank" placeholder="ex : Nama Bank" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Atas Nama</label>
                                            <input class="form-control" type="text" name="_atasnama" placeholder="ex : Nama Nasabah" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Nomor Rekening</label>
                                            <input class="form-control" type="text" name="_norek" placeholder="ex : Nomor Rekening" autocomplete="off" required>
                                        </div><hr>
										<!-- <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" rows="10" cols="40" name="_ket" placeholder="ex : Keterangan" autocomplete="off"></textarea>
                                        </div><hr> -->
										<button type="submit" class="btn btn-default" name="_submit-input-bank">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-bank">Reset</button>
										<br><br>
									</div>
								</form>
									
									<?php
										include "../connections/config.php";
										$_nama = @htmlspecialchars($_POST['_namabank']); 
										$_atasnama = @htmlspecialchars($_POST['_atasnama']); 
										$_norek = @htmlspecialchars($_POST['_norek']);
										if(isset($_POST['_submit-input-bank'])){
											// membuat id otomatis
											$sql = @mysqli_query($con, "SELECT MAX(RIGHT(KodeBank,3)) AS kode FROM mstbankpenerima") or die(mysqli_error($sql)); 
											$nums = @mysqli_num_rows($sql); 
											while($data = @mysqli_fetch_array($sql)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											// membuat kode user
											$bikin_kode = str_pad($kode, 3, "0", STR_PAD_LEFT);
											$kode_jadi = $bikin_kode;
									
											$query = @mysqli_query($con, "INSERT into mstbankpenerima(KodeBank,NamaBank,AtasNama,Rekening)values('$kode_jadi','$_nama','$_atasnama','$_norek')") or die(mysqli_error($query)); 	 
											if($query){
												echo '<script type="text/javascript">
												sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
												function () { window.location.href = "mst-bank-penerima.php"; });
												</script>';
											}else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
											}
										}
										@mysqli_close;

										?>
                                   
							</div>
						</div>
						
						<!-- /.panel-heading -->
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Nama Bank</label>
                                            <input class="form-control" type="text" name="_nama2" value="<?php echo $bank; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Atas Nama</label>
                                            <input class="form-control" type="text" name="_atasnama2" value="<?php echo $atasnama; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nomor Rekening</label>
                                            <input class="form-control" type="text" name="_norek2" value="<?php echo $norek; ?>" autocomplete="off" required>
                                        </div><hr>
										<button type="submit" class="btn btn-default" name="_submit-edit-rek">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-rek">Reset</button>
										<br><br>
									</div>
										
										<?php
										include "../connections/config.php";
										$_nama2 = @htmlspecialchars($_POST['_nama2']); $_norek2 = @htmlspecialchars($_POST['_norek2']); 
										$_atasnama2 = @htmlspecialchars($_POST['_atasnama2']);
										if(isset($_POST['_submit-edit-rek'])){
											$query = @mysqli_query($con, "update mstbankpenerima set NamaBank = '$_nama2', Rekening = '$_norek2', AtasNama = '$_atasnama2' where KodeBank = '".$id."'") or die(mysqli_error($query)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-bank-penerima.php"; });
													</script>';
												}
										}
										@mysqli_close;

										?>
                                    </form> 
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Nomor Rekening</label>
                                            <dd><?php echo ucwords($norek); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Atas Nama</label>
                                            <dd><?php echo ucwords($atasnama); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Nama Bank</label>
                                            <dd><?php echo ucwords($bank); ?></dd>
                                    </div>
									
									
								</div>
							</div>
						</div>
						
						<?php /* } elseif($id != null AND $page == 'nonaktif'){ 
							$file_path = "../android_wantrast_sqlsrv/img/fotoprofil/".$fotoprs; $file_path2 = "../android_wantrast_sqlsrv/img/fotoprofil/thumb_".$fotoprs;
							$cek = @mysqli_query($con, "select * from TrPembelian where replace(KodePerson,' ','') = replace('$kodeprs',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Supplier Dipakai Transaksi ", type: "error" },
								function () { window.location.href = "mst-supplier.php"; }); </script>';
							}else{
								// hapus data 
								/*if(file_exists($file_path)){ unlink($file_path); }
								if(file_exists($file_path2)){ unlink($file_path2); }
								$delete = @mysqli_query($con, "DELETE from MasterPerson WHERE id_support = '".$kodespons."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-sponsor.php"; }); </script>';
								
								// inaktif data
								$delete = @mysqli_query($con, "update MasterPerson set IsAktif = 'False' WHERE KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-supplier.php"; }); </script>';
							}
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @mysqli_query($con, "update MasterPerson set IsAktif = 'True' WHERE KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-supplier.php"; }); </script>'; */
							
						} elseif($id != null AND $page == 'delete'){ 
							// delete data
							$cek2 = @mysqli_query($con, "select * from tabunganuser where replace(TransferKeRek,' ','') = replace('$kodebank',' ','')") or die(mysqli_error($cek2));
							$num2 = @mysqli_num_rows($cek2);
								if($num2 > 0){
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Data Bank Dipakai Transaksi Tabungan ", type: "error" },
									function () { window.location.href = "mst-bank-penerima.php"; }); </script>';
								} else {
									$delete = @mysqli_query($con, "delete from mstbankpenerima WHERE KodeBank = '".$id."'") or die(mysqli_error($delete));
									echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Bank Perusahaan Berhasil ", type: "success" },
									function () { window.location.href = "mst-bank-penerima.php"; }); </script>'; 
								}
							
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script>
	$(document).ready(function() {
		$('#datepicker-example1').Zebra_DatePicker();
	});
	</script>
	
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<!-- membuat dropdown bertingkat tab I -->
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		  $("#prov").change(function(){
			var KodeProv = $("#prov option:selected").val();
			$.ajax({ url: "../get-data/get-kab.php", data: "get_prov="+KodeProv, cache: false, success: function(msg){ $("#kab").html(msg); } });
		  });
		  $("#kab").change(function(){
			var KodeProv = $("#prov option:selected").val();
			var KodeKab = $("#kab option:selected").val();
			$.ajax({ url: "../get-data/get-kec.php", data: "get_prov="+KodeProv+"&get_kab="+KodeKab, cache: false, success: function(msg){ $("#kec").html(msg); } });
		  });
		  $("#kec").change(function(){
			var KodeProv = $("#prov option:selected").val();
			var KodeKab = $("#kab option:selected").val();
			var KodeKec = $("#kec option:selected").val();
			$.ajax({ url: "../get-data/get-desa.php", data: "get_prov="+KodeProv+"&get_kab="+KodeKab+"&get_kec="+KodeKec, cache: false, success: function(msg){ $("#desa").html(msg); } });
		  });
		});
	</script>

</body>

</html>
