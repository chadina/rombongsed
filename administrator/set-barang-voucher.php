<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 27;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
$gbr = isset($_GET['gbr']) ? base64_decode($_GET['gbr']) : 0 ;
$sup = isset($_GET['sup']) ? base64_decode($_GET['sup']) : 0 ;
$hj = isset($_GET['hj']) ? base64_decode($_GET['hj']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select MasterBarang.*, HargaJualSatuan.NilaiVoucher, HargaJualSatuan.KodeSatuan, HargaJualSatuan.KonversiSatuan, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi, HargaJualSatuan.HargaJual1, HargaJualSatuan.HargaJual2, HargaJualSatuan.HargaJual3, HargaJualSatuan.HargaJual4,
HargaJualSatuan.HargaJual5, MasterDepartemen.NamaDepartemen, MasterSubDepartemen.NamaSubDepartemen, MasterPerson.NamaPerson from MasterBarang 
inner join MasterPerson on MasterPerson.KodePerson = MasterBarang.KodePerson
inner join MasterDepartemen on MasterDepartemen.KodeDepartemen = MasterBarang.KodeDepartemen
inner join MasterSubDepartemen on MasterSubDepartemen.KodeSubDepartemen = MasterBarang.KodeSubDepartemen
inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang where MasterBarang.KodeBarang = '".$id."'  AND HargaJualSatuan.KodeSatuan = 'SAT-0000001'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodebrg = $cari['KodeBarang']; $kodebrg2 = $cari['KodeBarangManual']; $barang = $cari['NamaBarang']; $fotobrg = $cari['Gambar']; $ketbrg = $cari['Keterangan'];
	$deptbrg = $cari['KodeDepartemen']; $subbrg = $cari['KodeSubDepartemen']; $katbrg = $cari['KodeKategory']; $hargabrg = $cari['HargaBeli']; $harga1 = $cari['HargaJual1'];
	$harga2 = $cari['HargaJual2']; $harga3 = $cari['HargaJual3']; $harga4 = $cari['HargaJual4']; $harga5 = $cari['HargaJual5']; $hargabrg2 = $cari['HargaEceranTertinggi'];
	$namadept = $cari['NamaDepartemen']; $namasubdept = $cari['NamaSubDepartemen']; $kodeprs = $cari['KodePerson']; $namaprs = $cari['NamaPerson'];
	$hargajualbrg = $cari['HargaJual']; $fotobrg2 = $cari['Gambar2']; $fotobrg3 = $cari['Gambar3']; $satuan = $cari['KodeSatuan']; $konversi = $cari['KonversiSatuan']; 
	$voucher = $cari['NilaiVoucher'];
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- Slide Galeri -->
	<link href="../web/css/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="../web/css/jcarousel.css" rel="stylesheet" />
	<link href="../web/css/flexslider.css" rel="stylesheet" />
	<link href="../web/css/style.css" rel="stylesheet" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Set Voucher Barang 
					<?php if($id == null AND $page == null){ 
						echo "<a href='set-barang-voucher.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					} elseif($id == null AND $page == "tambah" ){ 
						echo "<a href='set-barang-voucher.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					} elseif($id !== null AND ($page == "edit" OR $page == "detail" OR $page == "set_gambar" OR $page == "set-harga")){ 
						echo "<a href='set-barang-voucher.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="15%">Gambar</th>
                                        <th width="45%">Nama Barang</th>
                                        <th width="20%">Kategori</th>
                                        <th width="10%">Status</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select MasterBarang.IsDapatVoucher, MasterBarang.KodeDepartemen, HargaJualSatuan.NilaiVoucher, MasterBarang.KodeBarang, MasterBarang.KodeBarangManual, MasterBarang.NamaBarang, MasterBarang.Gambar, MasterBarang.IsAktif, MasterPerson.NamaPerson, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi from MasterBarang inner join MasterPerson 
								on MasterPerson.KodePerson = MasterBarang.KodePerson inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang
								where MasterBarang.IsBarangOnline = 'TRUE' AND MasterBarang.IsDapatVoucher = 'TRUE' AND HargaJualSatuan.KodeSatuan = 'SAT-0000001' order by MasterBarang.NamaBarang", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php if($cari['Gambar'] !== null){
												echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$cari['Gambar'].'" width="100%" />';
											}else{ 
												echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" width="100%" />'; 
											} ?>
										</td>
                                        <td><?php echo "<strong>".ucwords($cari['NamaBarang'])."</strong><br>".$cari['KodeBarangManual']."<br>Harga Dasar Produsen : Rp. ".number_format($cari['HargaBeli'])."<br>Harga Eceran Tertinggi : Rp. ".number_format($cari['HargaEceranTertinggi'])."<br>Harga Jual : Rp. ".number_format($cari['HargaJual'])."<br>"; ?>
										<?php echo 'Produsen : '; if($cari['NamaPerson'] === 'POS'){ echo 'ADMINISTRATOR'; } else { echo ucwords($cari['NamaPerson']); } ?></td>
                                        <!-- <td><?php /*$harga_akhir = $cari['HargaEceranTertinggi']-$cari['NilaiVoucher']; 
										if($harga_akhir <= 0){ $hg_akhir = 0; } else { $hg_akhir = $harga_akhir; } 
										echo "Nilai Voucher : <br>Rp. ".number_format($cari['NilaiVoucher']); */?></td> -->
										<td><?php $query2 = @sqlsrv_query($dbconnect, "select NamaDepartemen from MasterDepartemen Where KodeDepartemen = '".$cari['KodeDepartemen']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ 
										echo $cari2['NamaDepartemen']; } ?></td>
										<td>
										<?php /*if($cari['IsAktif'] == TRUE){ echo '<a href="#" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='#' class='btn btn-danger btn-sm'>Non Aktif</a>"; } */?>
										<?php if($cari['IsDapatVoucher'] == TRUE){ echo '<a href="set-barang-voucher.php?page='.htmlspecialchars(base64_encode("nonaktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeBarang"])).'" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='set-barang-voucher.php?page=".htmlspecialchars(base64_encode('aktif'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang']))."' class='btn btn-danger btn-sm'>Non Aktif</a>"; } ?>
										<!-- <a href='set-barang-voucher.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang'])); ?>' class='btn btn-warning btn-sm'>Edit</a> -->
										</td>
                                        
									</tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post" action="set-barang-voucher.php?page=<?php echo htmlspecialchars(base64_encode('tambah')); ?>">
										<div class="form-group">
										<label>Masukkan Kode atau Nama Barang</label>
											<input type="text" class="form-control" name="keyword" placeholder="Kode atau Nama Barang" autocomplete="off" value="<?php echo @$_REQUEST['keyword']; ?>">
										</div>
										<div class="form-group">
                                            <label>Kategori Voucher</label>
                                            <select class="form-control" name="kodedpr" autocomplete="off">
                                                <?php echo '<option value="">-- Pilih Kategori --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT KodeDepartemen FROM KategoriVoucher GROUP BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														if($daftar['KodeDepartemen'] === @$_REQUEST['kodedpr']){
															$list2 = @sqlsrv_query($dbconnect, "SELECT KodeDepartemen, NamaDepartemen FROM MasterDepartemen Where KodeDepartemen = '".$daftar['KodeDepartemen']."'") or die( print_r( sqlsrv_errors(), true)); 
															while($daftar2 = @sqlsrv_fetch_array($list2, SQLSRV_FETCH_ASSOC)){
																echo "<option value=\"".$daftar['KodeDepartemen']."\" selected='selected'>".ucwords($daftar2['NamaDepartemen'])."</option>\n";
															}
														} else {
															$list2 = @sqlsrv_query($dbconnect, "SELECT NamaDepartemen FROM MasterDepartemen Where KodeDepartemen = '".$daftar['KodeDepartemen']."'") or die( print_r( sqlsrv_errors(), true)); 
															while($daftar2 = @sqlsrv_fetch_array($list2, SQLSRV_FETCH_ASSOC)){
																echo "<option value=\"".$daftar['KodeDepartemen']."\" >".ucwords($daftar2['NamaDepartemen'])."</option>\n";
															}
														}
													}
												?>
                                            </select>
                                        </div>
										<button type="submit" name="cek" value="cek" class="btn btn-info">Cari</button><br>
											
									</form>
								</div>
							</div><hr>

							<div class="row">
								<div class="panel-body table-responsive">
											<table width="100%" class="table table-striped table-bordered table-hover" id="access-table">
												<thead>
													<tr>
														<th width="10%">No</th>
														<th width="15%">Gambar</th>
														<th width="30%">Nama Barang</th>
														<th width="20%">Kategori</th>
														<th width="5%">Cek</th>
													   
													</tr>
												</thead>
												<tbody>
												
												<form action="set-barang-voucher.php?page=<?php echo htmlspecialchars(base64_encode('tambah')); ?>" method="post">
								
												<?php include "../connections/config.php";
												$keyword = @$_REQUEST['keyword'];
												$kodedpr = @$_REQUEST['kodedpr'];
												if(($keyword == null OR $keyword === '') AND ($kodedpr == null OR $kodedpr === '')){
													$query = '';
												}elseif($keyword == null OR $keyword === ''){	
													$query = @sqlsrv_query($dbconnect, "select HargaJualSatuan.NilaiVoucher, MasterBarang.IsDapatVoucher, MasterBarang.KodeBarang, MasterBarang.KodeBarangManual, MasterBarang.NamaBarang, MasterBarang.Gambar, MasterBarang.IsAktif, MasterPerson.NamaPerson, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi, MasterBarang.KodeDepartemen from MasterBarang inner join MasterPerson 
													on MasterPerson.KodePerson = MasterBarang.KodePerson inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang
													where MasterBarang.KodeDepartemen = '".$kodedpr."' AND MasterBarang.IsDapatVoucher != 'TRUE' AND MasterBarang.IsBarangOnline = 'TRUE' AND MasterBarang.IsAktif = 'TRUE' AND HargaJualSatuan.KodeSatuan = 'SAT-0000001' order by MasterBarang.NamaBarang", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												}elseif($kodedpr == null OR $kodedpr === ''){	
													$query = @sqlsrv_query($dbconnect, "select HargaJualSatuan.NilaiVoucher, MasterBarang.IsDapatVoucher, MasterBarang.KodeBarang, MasterBarang.KodeBarangManual, MasterBarang.NamaBarang, MasterBarang.Gambar, MasterBarang.IsAktif, MasterPerson.NamaPerson, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi, MasterBarang.KodeDepartemen from MasterBarang inner join MasterPerson 
													on MasterPerson.KodePerson = MasterBarang.KodePerson inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang
													where (MasterBarang.KodeBarangManual LIKE '%".$keyword."%' OR MasterBarang.NamaBarang LIKE '%".$keyword."%') AND MasterBarang.IsDapatVoucher != 'TRUE' AND MasterBarang.IsBarangOnline = 'TRUE' AND MasterBarang.IsAktif = 'TRUE' AND HargaJualSatuan.KodeSatuan = 'SAT-0000001' order by MasterBarang.NamaBarang", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												}else{
													$query = @sqlsrv_query($dbconnect, "select HargaJualSatuan.NilaiVoucher, MasterBarang.IsDapatVoucher, MasterBarang.KodeBarang, MasterBarang.KodeBarangManual, MasterBarang.NamaBarang, MasterBarang.Gambar, MasterBarang.IsAktif, MasterPerson.NamaPerson, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi, MasterBarang.KodeDepartemen from MasterBarang inner join MasterPerson 
													on MasterPerson.KodePerson = MasterBarang.KodePerson inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang
													where MasterBarang.KodeDepartemen = '".$kodedpr."' AND (MasterBarang.KodeBarangManual LIKE '%".$keyword."%' OR MasterBarang.NamaBarang LIKE '%".$keyword."%') AND MasterBarang.IsDapatVoucher != 'TRUE' AND MasterBarang.IsBarangOnline = 'TRUE' AND MasterBarang.IsAktif = 'TRUE' AND HargaJualSatuan.KodeSatuan = 'SAT-0000001' order by MasterBarang.NamaBarang", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												}
												$no = 1;
												$hitung = @sqlsrv_num_rows($query);
												if($hitung > 0){
												while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
												?>
													<tr class="odd gradeX">
														<td><?php echo $no; ?></td>
														<td><?php if($cari['Gambar'] !== null){
																echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$cari['Gambar'].'" width="100%" />';
															}else{ 
																echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" width="100%" />'; 
															} ?>
														</td>
														<td><?php echo "<strong>".ucwords($cari['NamaBarang'])."</strong><br>".$cari['KodeBarangManual']."<br>Harga Dasar Produsen : Rp. ".number_format($cari['HargaBeli'])."<br>Harga Eceran Tertinggi : Rp. ".number_format($cari['HargaEceranTertinggi'])."<br>Harga Jual : Rp. ".number_format($cari['HargaJual'])."<br>"; ?>
														<?php echo 'Produsen : '; if($cari['NamaPerson'] === 'POS'){ echo 'ADMINISTRATOR'; } else { echo ucwords($cari['NamaPerson']); } ?></td>
														<!-- <td><?php /* if($cari['IsDapatVoucher'] == TRUE){ echo "<a href='#' class='btn btn-success btn-sm'>Aktif</a>"; } 
														else { echo "<a href='' class='btn btn-danger btn-sm'>Non Aktif</a>"; } ?></td>
														<td><?php if($cari['NilaiVoucher'] != null){
															echo '<input class="form-control" style="text-align:right" type="text" name="cekharga['.$no.']" value="'.$cari["NilaiVoucher"].'" placeholder="0" autocomplete="off" >';
														} else {	
															echo '<input class="form-control" style="text-align:right" type="text" name="cekharga['.$no.']" placeholder="0" autocomplete="off" >';
														} */ ?>	
														</td> -->
														<td><?php $query2 = @sqlsrv_query($dbconnect, "select NamaDepartemen from MasterDepartemen Where KodeDepartemen = '".$cari['KodeDepartemen']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ 
														echo $cari2['NamaDepartemen']; } ?></td>
														<td align="center">
															<!-- <input type="checkbox" id="cekbox" name="cekbox[<?php echo $no; ?>]" value="<?php echo $cari['KodeBarang']; ?>"/> -->
															<input type="checkbox" id="cekbox" name="cekbox[]" value="<?php echo $cari['KodeBarang']; ?>"/>
														</td>
													</tr>
												<?php $no++; } 
												}else{
													echo '<tr class="odd gradeX"><td colspan=6>';
													echo '<div class="alert alert-info alert-dismissable"><h4><p align="center">Tidak Ada Data<br></p></h4></div><br/>';
													echo '</td></tr>';
												}
												@sqlsrv_close(); ?> 
												
												</tbody>
												
												<input type="button" class="btn btn-md btn-warning" onclick="cek(this.form.cekbox)" value="Select All" />&nbsp;
												<input type="button" class="btn btn-md btn-danger" onclick="uncek(this.form.cekbox)" value="Clear All" />&nbsp;
												<input type="submit" class="btn btn-md btn-success" value="Simpan" name="submit" /><br><br>
												
												<?php
												// update akses
												if(isset($_POST['submit'])){
													/* $voucher = @$_POST['cekharga']; $cekbox = @$_POST['cekbox']; 
													if($cekbox) {
														foreach ($cekbox as $key => $value) {
															/*echo $value.','.$voucher[$key];*
															
															@sqlsrv_query($dbconnect, "update MasterBarang SET IsDapatVoucher = '1' where KodeBarang = '$value'") or die( print_r( sqlsrv_errors(), true));
															@sqlsrv_query($dbconnect, "update HargaJualSatuan SET NilaiVoucher = '$voucher[$key]' where KodeBarang = '$value' AND KodeSatuan = 'SAT-0000001'") or die( print_r( sqlsrv_errors(), true));

														}
														echo '<script type="text/javascript">sweetAlert({ title: "Sukses!", text: " Set Barang Voucher Sukses", type: "success" },
														function () { window.location.href = "set-barang-voucher.php"; }); </script>';
													}else {
														echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Anda Belum Barang Voucher ", type: "error" },
														function () { window.location.href = "set-barang-voucher.php?page='.htmlspecialchars(base64_encode("tambah")).'"; }); </script>';
													} */
													
													$cekbox = @$_POST['cekbox'];
													if($cekbox) {
														foreach ($cekbox as $value) {
															@sqlsrv_query($dbconnect, "update MasterBarang SET IsDapatVoucher = '1' where KodeBarang = '$value'") or die( print_r( sqlsrv_errors(), true));
														}
														echo '<script type="text/javascript">sweetAlert({ title: "Sukses!", text: " Set Barang Voucher Sukses", type: "success" },
														function () { window.location.href = "set-barang-voucher.php"; }); </script>';
													}else {
														echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Anda Belum Memilih Barang Voucher ", type: "error" },
														function () { window.location.href = "set-barang-voucher.php?page='.htmlspecialchars(base64_encode("tambah")).'"; }); </script>';
													} 
												} 
												
												?>
												
												</form>
											</table>
										</div>
									
							</div>
							<!-- /.panel -->
						</div>
						<!-- /.col-lg-12 -->
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Ubah Nilai Voucher
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Nama Barang</label>
                                            <input class="form-control" type="text" value="<?php echo $kodebrg2.' : '.strtoupper($barang); ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Voucher Saat Ini</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal" name="_bayar" value="<?php echo number_format($voucher,0,"","."); ?>" autocomplete="off">
                                        </div>
										<br>
										<button type="submit" class="btn btn-default" name="_submit-edit-bayar">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-bayar">Reset</button>
									</div>
								</form>
									
										<?php
										include "../connections/config.php";
										$_bayar = @htmlspecialchars($_POST['_bayar']); $_nilaibayar =  @str_replace(".", "", $_bayar);
										if(isset($_POST['_submit-edit-bayar'])){
											$query = @sqlsrv_query($dbconnect, "update HargaJualSatuan set NilaiVoucher = '$_nilaibayar' where KodeBarang = '$id' AND KodeSatuan = 'SAT-0000001'") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: "Update Voucher Tersimpan ", type: "success" },
														function () { window.location.href = "set-barang-voucher.php"; });
														</script>';
													}
												
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$file_path = "../img/foto-barang/".$fotobrg; $file_path2 = "../img/foto-barang/thumb_".$fotobrg;
							$cek = @sqlsrv_query($dbconnect, "select * from TrSalesOrderItem where KodeBarang = '".$kodebrg."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Data Barang Dipakai Transaksi ", type: "error" },
								function () { window.location.href = "mst-barang.php"; }); </script>';
							}else{
								$cek2 = @sqlsrv_query($dbconnect, "select * from MasterBarang where KodeBarang = '".$kodebrg."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$data2 = @sqlsrv_fetch_array($cek2);
								if($data2['IsAktif'] == TRUE){
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Non Aktifkan Data Barang Terlalu Dahulu", type: "error" },
									function () { window.location.href = "mst-barang.php"; }); </script>';
								}else{
									if(file_exists($file_path)){ unlink($file_path); }
									if(file_exists($file_path2)){ unlink($file_path2); }
									@sqlsrv_query($dbconnect, "DELETE From HargaJualSatuan WHERE KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true));
									@sqlsrv_query($dbconnect, "DELETE From MasterBarang WHERE KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true));
									echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
									function () { window.location.href = "mst-barang.php"; }); </script>';
								}
							}
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// nonaktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterBarang set IsDapatVoucher = 'False' WHERE KodeBarang = '".$id."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Voucher Berhasil ", type: "success" },
							function () { window.location.href = "set-barang-voucher.php"; }); </script>';
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterBarang set IsDapatVoucher = 'True' WHERE KodeBarang = '".$id."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Voucher Berhasil ", type: "success" },
							function () { window.location.href = "set-barang-voucher.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>
	
	<!-- membuat dropdown bertingkat klien-->
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		  $("#_kodedpr").change(function(){
			var id_departemen = $("#_kodedpr").val();
			$.ajax({
				url: "../get-data/get-departemen.php",
				data: "get_dept="+id_departemen,
				cache: false,
				success: function(msg){
					//jika data sukses diambil dari server kita tampilkan
					//di <select id=kota>
					$("#_kodesub").html(msg);
				}
			});
		  });
		  $("#_kodesub").change(function(){
			var id_subdepartemen = $("#_kodesub").val();
			$.ajax({
				url: "../get-data/get-subdepartemen.php",
				data: "get_sub="+id_subdepartemen,
				cache: false,
				success: function(msg){
					$("#_kodekat").html(msg);
				}
			});
		  });
		});
	</script>
	
	<script text="javascript">
			/* Tanpa Rupiah */
			var tanpa_rupiah = document.getElementById('nominal');
			var tanpa_rupiah2 = document.getElementById('nominal2');
			var tanpa_rupiah3 = document.getElementById('nominal3');
			var tanpa_rupiah4 = document.getElementById('nominal4');
			var tanpa_rupiah5 = document.getElementById('nominal5');
			var tanpa_rupiah6 = document.getElementById('nominal6');
			var tanpa_rupiah7 = document.getElementById('nominal7');
			var tanpa_rupiah8 = document.getElementById('nominal8');
			var tanpa_rupiah9 = document.getElementById('nominal9');
			var tanpa_rupiah10 = document.getElementById('nominal10');
			var tanpa_rupiah11 = document.getElementById('nominal11');
			
			tanpa_rupiah.addEventListener('keyup', function(e){ tanpa_rupiah.value = formatRupiah(this.value); });
			tanpa_rupiah.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah2.addEventListener('keyup', function(e){ tanpa_rupiah2.value = formatRupiah(this.value); });
			tanpa_rupiah2.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah3.addEventListener('keyup', function(e){ tanpa_rupiah3.value = formatRupiah(this.value); });
			tanpa_rupiah3.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah4.addEventListener('keyup', function(e){ tanpa_rupiah4.value = formatRupiah(this.value); });
			tanpa_rupiah4.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah5.addEventListener('keyup', function(e){ tanpa_rupiah5.value = formatRupiah(this.value); });
			tanpa_rupiah5.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah6.addEventListener('keyup', function(e){ tanpa_rupiah6.value = formatRupiah(this.value); });
			tanpa_rupiah6.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah7.addEventListener('keyup', function(e){ tanpa_rupiah7.value = formatRupiah(this.value); });
			tanpa_rupiah7.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah8.addEventListener('keyup', function(e){ tanpa_rupiah8.value = formatRupiah(this.value); });
			tanpa_rupiah8.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah9.addEventListener('keyup', function(e){ tanpa_rupiah9.value = formatRupiah(this.value); });
			tanpa_rupiah9.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah10.addEventListener('keyup', function(e){ tanpa_rupiah10.value = formatRupiah(this.value); });
			tanpa_rupiah10.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah11.addEventListener('keyup', function(e){ tanpa_rupiah11.value = formatRupiah(this.value); });
			tanpa_rupiah11.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			/* Fungsi */
			function formatRupiah(bilangan, prefix)
			{
				var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
					split	= number_string.split(','),
					sisa 	= split[0].length % 3,
					rupiah 	= split[0].substr(0, sisa),
					ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
					
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
			}
			
			function limitCharacter(event)
			{
				key = event.which || event.keyCode;
				if ( key != 188 // Comma
					 && key != 8 // Backspace
					 && key != 9 // Tab
					 && key != 116 // F5
					 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
					 && (key < 48 || key > 57) // Non digit
					 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
					) 
				{
					event.preventDefault();
					return false;
				}
			}
			</script>
	
	<!-- jQuery sticky menu -->
    <script src="../web/js/owl.carousel.min.js"></script>
    <script src="../web/js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="../web/js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="../web/js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="../web/js/bxslider.min.js"></script>
	<script type="text/javascript" src="../web/js/script.slider.js"></script>
	
	<!-- Slide -->
	<script src="../web/js/jquery.fancybox.pack.js"></script>
	<script src="../web/js/jquery.fancybox-media.js"></script>
	<script src="../web/js/google-code-prettify/prettify.js"></script>
	<script src="../web/js/portfolio/jquery.quicksand.js"></script>
	<script src="../web/js/portfolio/setting.js"></script>
	<script src="../web/js/jquery.flexslider.js"></script>
	<script src="../web/js/animate.js"></script>
	<script src="../web/js/custom.js"></script>

</body>

</html>
