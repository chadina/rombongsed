<?php
//memasukkan file config.php
include "../connections/config.php";
date_default_timezone_set('Asia/Jakarta');

//membuat id user
$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(id_file,5)) AS kode FROM mst_file WHERE LEFT(id_file,9)='FILE-".date('Y')."'") or die( print_r( sqlsrv_errors(), true)); 
$nums = @sqlsrv_num_rows($sql); 
while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
	if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
}
// membuat kode user
$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
$kode_jadi = "FILE-".date('Y')."-".$bikin_kode;

//mendefinisikan folder upload
define("UPLOAD_DIR", "../files/");

if (!empty($_FILES["media"])) {
	$media	= $_FILES["media"];
	$ext	= pathinfo($_FILES["media"]["name"], PATHINFO_EXTENSION);
	$size	= $_FILES["media"]["size"];
	$tgl	= date("Y-m-d h:i:s");

	if ($media["error"] !== UPLOAD_ERR_OK) {
		echo '<div class="alert alert-warning">Gagal Upload File.</div>';
		exit;
	}

	// filename yang aman
	$name = preg_replace("/[^A-Z0-9._-]/i", "_", $media["name"]);

	// mencegah overwrite filename
	$i = 0;
	$parts = pathinfo($name);
	while (file_exists(UPLOAD_DIR . $name)) {
		$i++;
		$name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
	}

	$success = move_uploaded_file($media["tmp_name"], UPLOAD_DIR . $name);
	if($success) { 
		@sqlsrv_query($dbconnect, "INSERT INTO mst_file(id_file, nama_file, file_size, file_type, tanggal) VALUES('$kode_jadi','$name', '$size', '$ext','$tgl')") or die( print_r( sqlsrv_errors(), true));
		$edit = @sqlsrv_query($dbconnect, "SELECT TOP (1) id_file, nama_file FROM mst_file ORDER BY id_file DESC") or die( print_r( sqlsrv_errors(), true));
		while($row = @sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){
			$id = $row['id_file']; $nama = $row['nama_file'];
		}
		echo $nama;
		exit;
	}
	chmod(UPLOAD_DIR . $name, 0644);
}
?>