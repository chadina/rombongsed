<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
// $fitur_id = 7;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
$menu = isset($_GET['menu']) ? base64_decode($_GET['menu']) : 0 ;

$query = mysqli_query($con, "select * from mstuser where id_user = '".$id."'");
while($cari = mysqli_fetch_array($query)){ 
	$kodeuser = $cari['id_user']; $nama = $cari['NamaLengkap']; $alamat = $cari['Alamat']; $telp = $cari['NoHP']; 
	$username = $cari['UserName']; $pass = $cari['Password']; $fotoadmin = $cari['foto']; $email = $cari['email'];
}
mysqli_close();

$post = $_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master User
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-user.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					} elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-user.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="55%">Nama User</th>
                                        <th width="15%">Status</th>
                                        <!-- <th width="15%">Fitur</th> -->
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = mysqli_query($con, "select * from mstuser order by NamaLengkap");
								$no = 1;
								while($cari = mysqli_fetch_array($query)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".ucwords($cari['NamaLengkap'])."</strong><br>Username : ".ucwords($cari['UserName'])."<br>No Telepon: ".$cari['NoHP']; ?></td>
                                        <td><?php if($cari['IsAktif'] == '1'){ echo '<a href="mst-user.php?page='.htmlspecialchars(base64_encode("nonaktif")).'&id='.htmlspecialchars(base64_encode($cari["id_user"])).'" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='mst-user.php?page=".htmlspecialchars(base64_encode('aktif'))."&id=".htmlspecialchars(base64_encode($cari['id_user']))."' class='btn btn-danger btn-sm'>Non Aktif</a>"; } ?></td>
                                        <!-- <td><a href="mst-user.php?page=<?php echo htmlspecialchars(base64_encode('setting'))."&id=".htmlspecialchars(base64_encode($cari['id_user'])); ?>" class="btn btn-info btn-sm">Set Fitur</a></td> -->
                                        <td>
											<a href="mst-user.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['id_user'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-user.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['id_user'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<?php if($cari['UserName'] == 'admin'){ 
												echo '';
											} else { ?>
												<a href="mst-user.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['id_user'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a><br><br>
											<!--	<a href="mst-user.php?page=<?php echo htmlspecialchars(base64_encode('set_fitur'))."&id=".htmlspecialchars(base64_encode($cari['id_user'])); ?>" class="btn btn-primary btn-sm">Set Akses</a>-->
											<?php } ?>
										</td>
                                    </tr>
								<?php $no++; } 
								mysqli_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
									<?php
									// membuat id otomatis
											$sql = mysqli_query($con, "SELECT MAX(RIGHT(id_user,5)) AS kode FROM mstuser WHERE LEFT(replace(id_user,' ',''),10)='ADM-".date('Ym')."'"); 
											$nums = mysqli_num_rows($sql); 
											while($data = mysqli_fetch_array($sql)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
											$kode_jadi = "ADM-".date('Ym')."-".$bikin_kode;
									?>
									
									<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Input Foto Profil</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kode_jadi);?>&type=<?php echo base64_encode('_admin');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									</div>
									
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Nama User</label>
                                            <input class="form-control" type="text" name="_namauser" placeholder="ex : Nama User" autocomplete="off" required>
                                            <input class="form-control" type="hidden" name="_iduser" value="<?php echo $kode_jadi; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" rows="10" cols="40" name="_alamat" placeholder="ex : Alamat" autocomplete="off"></textarea>
                                        </div>
										<div class="form-group">
                                            <label>Telepon</label>
                                            <input class="form-control" type="text" name="_telp" placeholder="ex : 888888888888" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="email" name="_email" placeholder="ex : compromise@mail.com" autocomplete="off" required>
                                        </div>
										<hr>
										<div class="form-group">
                                            <label>Username</label>
                                            <input class="form-control" type="text" name="_username" placeholder="ex : username" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" type="password" name="_password" placeholder="ex : password" autocomplete="off" required>
                                        </div>
										
										<button type="submit" class="btn btn-default" name="_submit-input-user">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-user">Reset</button>
									</div>
								</form>
								
										<?php
										include "../connections/config.php";
										$_iduser = htmlspecialchars($_POST['_iduser']); $_nama = htmlspecialchars($_POST['_namauser']); $_alamat = htmlspecialchars($_POST['_alamat']);  
										$_telp = htmlspecialchars($_POST['_telp']); $_username = htmlspecialchars($_POST['_username']); $_password = htmlspecialchars(md5($_POST['_password'])); 
										$_email = htmlspecialchars($_POST['_email']); 
										if(isset($_POST['_submit-input-user'])){
											$cek = mysqli_query($con, "select * from mstuser where replace(id_user,' ','') = replace('$_iduser',' ','')");
											$num = mysqli_num_rows($cek);
											if($num > 0){
												$cek2 = mysqli_query($con, "select * from mstuser where replace(UserName,' ','') = replace('$_username',' ','')");
												$num2 = mysqli_num_rows($cek2);
												if($num2 > 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Username Sudah Terpakai ", "error"); </script>';
												}else{
													$cek3 = mysqli_query($con, "select * from mstuser where replace(email,' ','') = replace('$_email',' ','')");
													$num3 = mysqli_num_rows($cek3);
													if($num3 > 0){
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Email Sudah Terpakai ", "error"); </script>';
													}else{
														$query = mysqli_query($con, "update mstuser set NamaLengkap = '$_nama', NoHP = '$_telp', email = '$_email', Alamat = '$_alamat',
														UserName = '$_username', Password = '$_password', level = 'ADMIN_WEB', IsAktif = '0' where id_user = '".$_iduser."'"); 	 
															if($query){
																echo '<script type="text/javascript">
																sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
																function () { window.location.href = "mst-user.php"; });
																</script>';
															}
															else{
																echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
															}
													}
												}
											}else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Anda Belum Upload Foto Profil ", "error"); </script>';
											}
										}
										mysqli_close;

										?>
                                    
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Ganti Foto Profil</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kodeuser);?>&img=<?php echo base64_encode($fotoadmin);?>&type=<?php echo base64_encode('_admin');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
											</div>
											<div class="form-group">
												<img class="imgl img-thumbnail" src="../img/foto-admin/thumb_<?php echo $fotoadmin; ?>" id="_oldimg" width="50%" /><br>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									</div>
									
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Kode User</label>
                                            <input class="form-control" type="text" value="<?php echo $kodeuser; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama User</label>
                                            <input class="form-control" type="text" name="_namauser2" value="<?php echo $nama; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" rows="10" cols="40" name="_alamat2" autocomplete="off"><?php echo $alamat; ?></textarea>
                                        </div>
										<div class="form-group">
                                            <label>Telepon</label>
                                            <input class="form-control" type="text" name="_telp2" value="<?php echo $telp; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="email" name="_email2" value="<?php echo $email; ?>" autocomplete="off" required>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-edit-user">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-user">Reset</button>
									</div>
								</form>
									
										<?php
										include "../connections/config.php";
										$_nama2 = htmlspecialchars($_POST['_namauser2']); $_alamat2 = htmlspecialchars($_POST['_alamat2']);  
										$_tlahir2 = htmlspecialchars($_POST['_tlahir2']); $_telp2 = htmlspecialchars($_POST['_telp2']); 
										$_email2 = htmlspecialchars($_POST['_email2']); $_tgl2 = htmlspecialchars($_POST['_tgl2']); 
										if(isset($_POST['_submit-edit-user'])){
											if($email === $_email2){
												$query = mysqli_query($con, "update mstuser set NamaLengkap = '$_nama2', NoHP = '$_telp2', email = '$_email2', Alamat = '$_alamat2'
												where id_user = '".$kodeuser."'"); 	 
													if($query){
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
														function () { window.location.href = "mst-user.php"; });
														</script>';
													}
											}else{
												$cek3 = mysqli_query($con, "select * from mstuser where replace(email,' ','') = replace('$_email2',' ','')");
												$num3 = mysqli_num_rows($cek3);
												if($num3 > 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Email Sudah Terpakai ", "error"); </script>';
												}else{
													$query = mysqli_query($con, "update mstuser set NamaLengkap = '$_nama2', NoHP = '$_telp2', email = '$_email2', Alamat = '$_alamat2'
													where id_user = '".$kodeuser."'"); 	 
														if($query){
															echo '<script type="text/javascript">
															sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
															function () { window.location.href = "mst-user.php"; });
															</script>';
														}
												}
												
											}
										}
										mysqli_close;

										?>
                                   
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Foto Profil</label>
                                            <?php if($fotoadmin !== null){
												echo '<dd><img class="imgl img-thumbnail" src="../img/foto-admin/'.$fotoadmin.'" width="50%" /></dd>';
											}else{ 
												echo '<dd><img class="imgl img-thumbnail" src="../img/foto-admin/no-image.png" width="50%" /></dd>'; 
											} ?>
                                    </div>
									<div class="form-group">
                                        <label>Nama User</label>
                                            <dd><?php echo ucwords($nama)." (".$username.") "; ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Alamat</label>
                                            <dd><?php echo ucwords($alamat); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Telepon & Email</label>
                                            <dd><?php echo ucwords($telp)." : ".$email; ?></dd>
                                    </div>
									
								</div>
							</div>
						</div>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$cek2 = mysqli_query($con, "select * from fitur_level where replace(id_user,' ','') = replace('$kodeuser',' ','')");
							$num2 = mysqli_num_rows($cek2);
							if($num2 > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " User Memiliki Hak Akses ", type: "error" },
								function () { window.location.href = "mst-user.php"; }); </script>';
							}else{
								/* $file_path = "../img/foto-admin/".$fotoadmin; $file_path2 = "../img/foto-admin/thumb_".$fotoadmin;
								if(file_exists($file_path)){ unlink($file_path); }
								if(file_exists($file_path2)){ unlink($file_path2); } */
								$delete = mysqli_query($con, "delete from mstuser WHERE id_user = '".$kodeuser."'");
									echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
									function () { window.location.href = "mst-user.php"; }); </script>';
							}
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							// $delete = mysqli_query($con, "update mstuser set IsAktif = '1' WHERE id_user = '".$kodeuser."'");
							$delete = mysqli_query($con, "UPDATE `mstuser` SET `IsAktif` = b'1' WHERE id_user = '".$kodeuser."'");
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-user.php"; }); </script>';
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// aktif data
							// $delete = mysqli_query($con, "update mstuser set IsAktif = '0' WHERE id_user = '".$kodeuser."'");
							$delete = mysqli_query($con, "UPDATE `mstuser` SET `IsAktif` = b'0' WHERE id_user = '".$kodeuser."'");
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-user.php"; }); </script>';
							
						}
						// elseif($id != null AND $page == 'update_akses'){ 
							// // update akses
							// $cekbox = $_POST['cekbox'];
							// if($cekbox) {
								// foreach ($cekbox as $value) {
									// mysqli_query($con, "INSERT into fitur_level (id_user,fitur_id)values('$id','$value')");
								// }
								// echo '<script type="text/javascript">sweetAlert({ title: "Sukses!", text: " Verifikasi Hak Akses Berhasil ", type: "success" },
								// function () { window.location.href = "mst-user.php?page='.htmlspecialchars(base64_encode("set_fitur")).'&id='.htmlspecialchars(base64_encode($id)).'"; }); </script>';
							// }else {
								// echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Anda Belum Memilih Menu ", type: "error" },
								// function () { window.location.href = "mst-user.php?page='.htmlspecialchars(base64_encode("set_fitur")).'&id='.htmlspecialchars(base64_encode($id)).'"; }); </script>';
							// } 
						// }  elseif($id != null AND $page == 'delete_akses'){ 
							// // delete akses
							// $delete = @mysqli_query($con, "DELETE from fitur_level WHERE id_user = '".$id."' AND fitur_id = '".$menu."'") or die( print_r( mysqli_errors(), true));
							// echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Menu Berhasil ", type: "success" },
							// function () { window.location.href = "mst-user.php?page='.htmlspecialchars(base64_encode("set_fitur")).'&id='.htmlspecialchars(base64_encode($id)).'"; }); </script>'; 
						// } 
						?>
												
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script>
	$(document).ready(function() {
		$('#datepicker-example1').Zebra_DatePicker();
	});
	</script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Non Aktifkan Menu Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>

	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>

</body>

</html>
