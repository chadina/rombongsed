<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Lokasi
					<?php /* if($id == null AND $page == null){ 
						echo "<a href='mst-jenis.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>"; 
					} */
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-tabs">
                        <li class="active"><a href="#prov" data-toggle="tab">Provinsi</a></li>
                        <li><a href="#kab" data-toggle="tab">Kabupaten</a></li>
                        <li><a href="#kec" data-toggle="tab">Kecamatan</a></li>
                        <li><a href="#desa" data-toggle="tab">Desa</a></li>
                    </ul><br>
					
                    <div class="tab-content">
                    <div class="tab-pane fade in active" id="prov">
					<div class="panel panel-default">
						<?php /* if($id == null AND $page == null){ */ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="prov_grid">
                                <thead>
                                    <tr>
                                        <th width="15%">Kode Provinsi</th>
                                        <th width="85%">Nama Provinsi</th>
                                        <!-- <th width="20%">Aksi</th> -->
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php /* } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Nama Jenis Dokumen</label>
                                            <input class="form-control" type="text" name="_dok" placeholder="ex : Ijazah" autocomplete="off" required>
                                        </div>
                                        
										<button type="submit" class="btn btn-default" name="_submit-input-dok">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-dok">Reset</button>
										
										<?php
										include "../connections/config.php";
										// membuat id
										$sql = @mysqli_query($dbconnect, 'select right(KodeJenis,5) AS kode FROM mstjenisdokumen ORDER BY KodeJenis DESC LIMIT 1'); 
										$nums = @mysqli_num_rows($sql);
										if($nums <> 0){	$data = @mysqli_fetch_array($sql); $kode = $data['kode'] + 1; }else{ $kode = 1; }
										
										// membuat kode
										$create_kode = "JNS/".str_pad($kode, 5, "0", STR_PAD_LEFT);
																
										$_nama = $_POST['_dok']; 
										if(isset($_POST['_submit-input-dok'])){
											$query = @mysqli_query($dbconnect, "insert into mstjenisdokumen(KodeJenis,NamaJenisDok)values('$create_kode','$_nama')");	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-jenis.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
										}
										@mysqli_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Kode Jenis Dokumen</label>
                                            <input class="form-control" type="text" value="<?php echo $kodejns; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama Jenis Dokumen</label>
                                            <input class="form-control" type="text" name="_dok2" value="<?php echo $jenis; ?>" autocomplete="off" required>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default" name="_submit-edit-dok">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-dok">Reset</button>
										
										<?php
										include "../connections/config.php";
										$_nama2 = $_POST['_dok2']; 
										if(isset($_POST['_submit-edit-dok'])){
											$query = @mysqli_query($dbconnect, "update mstjenisdokumen set NamaJenisDok = '$_nama2' where KodeJenis = '".$kodejns."'");	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-jenis.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
										}
										@mysqli_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<dl>
										<dt>Nama Jenis Dokumen</dt>
										<dd><?php echo $jenis; ?></dd><br>
										
									</dl>
								</div>
							</div>
						</div>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$delete = @mysqli_query($dbconnect, "DELETE from mstjenisdokumen WHERE KodeJenis = '".$kodejns."'");
							echo '<script type="text/javascript">
							sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-jenis.php"; });
							</script>';
						} */ ?>
                    </div>
					</div>
					
					<div class="tab-pane fade" id="kab">
					<div class="panel panel-default">
					    <div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="kab_grid">
                                <thead>
                                    <tr>
                                        <th width="15%">Kode Kabupaten</th>
                                        <th width="55%">Nama Kabupaten</th>
                                        <th width="30%">Nama Provinsi</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>          
					</div>
					</div>
					
					<div class="tab-pane fade" id="kec">
					<div class="panel panel-default">
					    <div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="kec_grid">
                                <thead>
                                    <tr>
                                        <th width="10%">Kode Kecamatan</th>
                                        <th width="40%">Nama Kecamatan</th>
                                        <th width="25%">Kabupaten</th>
                                        <th width="25%">Provinsi</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>          
					</div>
					</div>
					
					<div class="tab-pane fade" id="desa">
					<div class="panel panel-default">
					    <div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="desa_grid">
                                <thead>
                                    <tr>
                                        <th width="15%">Kode Desa</th>
                                        <th width="30%">Nama Desa</th>
                                        <th width="20%">Kecamatan</th>
                                        <th width="20%">Kabupaten</th>
                                        <th width="15%">Provinsi</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>          
					</div>
					</div>
					</div>
                    <!-- /.panel -->
					
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
    $( document ).ready(function() {
		$('#prov_grid').DataTable({
			// "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
			"lengthMenu": [[25, 50, 100], [25, 50, 100]],
			"bProcessing": true,
			"serverSide": true,
			"ajax":{
				url :"response-data.php?type=<?php echo base64_encode('provinsi'); ?>", // json datasource
				type: "post",  // type of method  ,GET/POST/DELETE
				error: function(){
				  $("#prov_grid_processing").css("display","none");
				}
			}
		});   
	});
    
	$( document ).ready(function() {
		$('#kab_grid').DataTable({
			"lengthMenu": [[25, 50, 100], [25, 50, 100]],
			"bProcessing": true,
			"serverSide": true,
			"ajax":{
				url :"response-data.php?type=<?php echo base64_encode('kabupaten'); ?>", // json datasource
				type: "post",  // type of method  ,GET/POST/DELETE
				error: function(){
				  $("#kab_grid_processing").css("display","none");
				}
			}
		});   
	});
	
	$( document ).ready(function() {
		$('#kec_grid').DataTable({
			"lengthMenu": [[25, 50, 100], [25, 50, 100]],
			"bProcessing": true,
			"serverSide": true,
			"ajax":{
				url :"response-data.php?type=<?php echo base64_encode('kecamatan'); ?>", // json datasource
				type: "post",  // type of method  ,GET/POST/DELETE
				error: function(){
				  $("#kec_grid_processing").css("display","none");
				}
			}
		});   
	});
	
	$( document ).ready(function() {
		$('#desa_grid').DataTable({
			"lengthMenu": [[25, 50, 100], [25, 50, 100]],
			"bProcessing": true,
			"serverSide": true,
			"ajax":{
				url :"response-data.php?type=<?php echo base64_encode('desa'); ?>", // json datasource
				type: "post",  // type of method  ,GET/POST/DELETE
				error: function(){
				  $("#desa_grid_processing").css("display","none");
				}
			}
		});   
	});
	</script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script>


</body>

</html>
