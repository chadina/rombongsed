<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 1;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
$gbr = isset($_GET['gbr']) ? base64_decode($_GET['gbr']) : 0 ;
$sup = isset($_GET['sup']) ? base64_decode($_GET['sup']) : 0 ;
$hj = isset($_GET['hj']) ? base64_decode($_GET['hj']) : 0 ;
$satbrg = isset($_GET['sat']) ? base64_decode($_GET['sat']) : 0 ;
$konvbrg = isset($_GET['konv']) ? base64_decode($_GET['konv']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select MasterBarang.*, HargaJualSatuan.KodeSatuan, HargaJualSatuan.KonversiSatuan, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi, HargaJualSatuan.HargaJual1, HargaJualSatuan.HargaJual2, HargaJualSatuan.HargaJual3, HargaJualSatuan.HargaJual4,
HargaJualSatuan.HargaJual5, MasterDepartemen.NamaDepartemen, MasterSubDepartemen.NamaSubDepartemen, MasterPerson.NamaPerson from MasterBarang 
inner join MasterPerson on MasterPerson.KodePerson = MasterBarang.KodePerson
inner join MasterDepartemen on MasterDepartemen.KodeDepartemen = MasterBarang.KodeDepartemen
inner join MasterSubDepartemen on MasterSubDepartemen.KodeSubDepartemen = MasterBarang.KodeSubDepartemen
inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang where MasterBarang.KodeBarang = '".$id."'  AND HargaJualSatuan.KodeSatuan = 'SAT-0000001'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodebrg = $cari['KodeBarang']; $kodebrg2 = $cari['KodeBarangManual']; $barang = $cari['NamaBarang']; $fotobrg = $cari['Gambar']; $ketbrg = $cari['Keterangan'];
	$deptbrg = $cari['KodeDepartemen']; $subbrg = $cari['KodeSubDepartemen']; $katbrg = $cari['KodeKategory']; $hargabrg = $cari['HargaBeli']; $harga1 = $cari['HargaJual1'];
	$harga2 = $cari['HargaJual2']; $harga3 = $cari['HargaJual3']; $harga4 = $cari['HargaJual4']; $harga5 = $cari['HargaJual5']; $hargabrg2 = $cari['HargaEceranTertinggi'];
	$namadept = $cari['NamaDepartemen']; $namasubdept = $cari['NamaSubDepartemen']; $kodeprs = $cari['KodePerson']; $namaprs = $cari['NamaPerson'];
	$hargajualbrg = $cari['HargaJual']; $fotobrg2 = $cari['Gambar2']; $fotobrg3 = $cari['Gambar3']; $satuan = $cari['KodeSatuan']; $konversi = $cari['KonversiSatuan']; 
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- Slide Galeri -->
	<link href="../web/css/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="../web/css/jcarousel.css" rel="stylesheet" />
	<link href="../web/css/flexslider.css" rel="stylesheet" />
	<link href="../web/css/style.css" rel="stylesheet" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Barang
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-barang.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					} elseif($id !== null AND ($page == "edit" OR $page == "detail" OR $page == "set_gambar" OR $page == "set-harga")){ 
						echo "<a href='mst-barang.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="15%">Gambar</th>
                                        <th width="35%">Nama Barang</th>
                                        <th width="10%">Status</th>
                                        <th width="10%">Harga</th>
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select MasterBarang.KodeBarang, MasterBarang.KodeBarangManual, MasterBarang.NamaBarang, MasterBarang.Gambar, MasterBarang.IsAktif, MasterPerson.NamaPerson, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi from MasterBarang inner join MasterPerson 
								on MasterPerson.KodePerson = MasterBarang.KodePerson inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang
								where MasterBarang.IsBarangOnline = 'TRUE' AND HargaJualSatuan.KodeSatuan = 'SAT-0000001' order by MasterBarang.NamaBarang", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php if($cari['Gambar'] !== null){
												echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$cari['Gambar'].'" width="100%" />';
											}else{ 
												echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" width="100%" />'; 
											} ?>
										</td>
                                        <td><?php echo "<strong>".ucwords($cari['NamaBarang'])."</strong><br>".$cari['KodeBarangManual']."<br>Harga Dasar Produsen : Rp. ".number_format($cari['HargaBeli'])."<br>Harga Eceran Tertinggi : Rp. ".number_format($cari['HargaEceranTertinggi'])."<br>Harga Jual : Rp. ".number_format($cari['HargaJual'])."<br>"; ?>
										<?php echo 'Produsen : '; if($cari['NamaPerson'] === 'POS'){ echo 'ADMINISTRATOR'; } else { echo ucwords($cari['NamaPerson']); } ?></td>
                                        <td><?php if($cari['IsAktif'] == TRUE){ echo '<a href="mst-barang.php?page='.htmlspecialchars(base64_encode("nonaktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeBarang"])).'" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='mst-barang.php?page=".htmlspecialchars(base64_encode('aktif'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang']))."' class='btn btn-danger btn-sm'>Non Aktif</a>"; } ?>
										<td><a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('set-harga'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang'])); ?>" class="btn btn-success btn-sm">Set Harga</a></td>
                                        <td>
											<a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a><br><br>
											<a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('set_gambar'))."&id=".htmlspecialchars(base64_encode($cari['KodeBarang'])); ?>" class="btn btn-primary btn-sm">Set Gambar</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Kode Produsen ( * Jika Dikosongi Maka Barang Ini Milik Admin )</label>
											<label>Masukkan Kode atau Username Produsen</label><br><span>( * Jika Dikosongi Maka Barang Ini Adalah Barang )</span><br><br>
											<?php
											if($sup == null){
												echo '<input type="text" class="form-control" name="cek_supplier" placeholder="Masukkan Kode atau Username Produsen" autocomplete="off"/><br>'; 
												echo '<button type="submit" name="cek" value="cek" class="btn btn-info">Cek Produsen</button>';
											}
                                            else{ 
											$query9 = sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$sup."'") or die( print_r( sqlsrv_errors(), true));
											$hasil9 = sqlsrv_fetch_array($query9);
												echo '<input type="text" class="form-control" name="nama_supplier" value="'.$hasil9['KodePersonManual'].' : '.$hasil9['NamaPerson'].'" required="required" readonly /><br>'; 
												echo '<input type="text" class="form-control" value="Status : '.$hasil9['StatusPerson'].'" required="required" readonly /><br>'; 
												echo '<a href="mst-barang.php?page='.base64_encode("tambah").'" name="cek" class="btn btn-info">Batal</a>';
											}
											?>
											
									<?php if(isset($_POST['cek'])){
										$cek_supplier = @$_POST['cek_supplier'];
										if(empty($cek_supplier)){
											echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Supplier Belum Diisi ", "error"); </script>';
										}else{
											$query = sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE (replace(KodePersonManual,' ','') = replace('$cek_supplier',' ','') OR replace(UserName,' ','') = replace('$cek_supplier',' ','')) AND (StatusPerson = 'SUPPLIER' OR StatusPerson = 'DISTRIBUTOR' OR StatusPerson = 'AGEN' OR StatusPerson = 'RESELLER')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$cari = sqlsrv_num_rows($query);
											while($hasil = sqlsrv_fetch_array($query)){ $kode = $hasil['KodePerson']; }
											if($cari === 0){
												echo '<script type="text/javascript">
												sweetAlert({ title: "Maaf!", text: " Produsen Tidak Tersedia ", type: "error" },
												function () { window.location.href = "mst-barang.php?page='.base64_encode("tambah").'"; });
												</script>';
											}else{
												echo '<script type="text/javascript">window.location.href = "mst-barang.php?page='.base64_encode("tambah").'&sup='.base64_encode($kode).'";</script>';
											}

										}	
											
									}
									?>
									</form>
										
                                        </div><br>
										
								<form role="form" method="post">
									<?php
											if($sup == null){
												echo '<input type="hidden" class="form-control" name="_kode_supplier" value="'.$id_tree_aktif.'" autocomplete="off" readonly />';
											}
                                            else{ 
											$query9 = sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$sup."'") or die( print_r( sqlsrv_errors(), true));
											$hasil9 = sqlsrv_fetch_array($query9);
												echo '<input type="hidden" class="form-control" name="_kode_supplier" autocomplete="off" value="'.$hasil9['KodePerson'].'" readonly />';
										} ?>
										<div class="form-group">
                                            <label>Kode Barang Manual</label>
                                            <input class="form-control" type="text" name="_kodebrg" placeholder="ex : 000000000001" autocomplete="off" required>
                                            <!-- <input class="form-control" type="hidden" name="_idbrg" value="<?php echo $kode_jadi; ?>" autocomplete="off" readonly> -->
                                        </div>
										<div class="form-group">
                                            <label>Nama Barang</label>
                                            <input class="form-control" type="text" name="_namabrg" placeholder="ex : Nama Barang" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Harga Dasar Produsen</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal"  name="_hargabeli" placeholder="0" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Harga Eceran Tertinggi</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal2"  name="_hargaet" placeholder="0" autocomplete="off" required>
                                        </div>
										<!-- <div class="form-group">
                                            <label>Harga Beli (Market Place)</label>
                                            <input class="form-control" type="number" name="_hargabeli" placeholder="ex : 15000" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Distributor Internal)</label>
                                            <input class="form-control" type="number" name="_harga1" placeholder="ex : 15000" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Distributor)</label>
                                            <input class="form-control" type="number" name="_harga2" placeholder="ex : 15000" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Agen)</label>
                                            <input class="form-control" type="number" name="_harga3" placeholder="ex : 15000" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Reseller)</label>
                                            <input class="form-control" type="number" name="_harga4" placeholder="ex : 15000" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Member)</label>
                                            <input class="form-control" type="number" name="_harga5" placeholder="ex : 15000" autocomplete="off">
                                        </div> -->
										<div class="form-group">
                                            <label>Kategori</label>
                                            <select class="form-control" id="_kodedpr" name="_kodedpr" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Kategori --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterDepartemen where KodeDepartemen != 'DPT-0000001' AND Keterangan = 'PRODUK' ORDER BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){	
														echo "<option value=\"".$daftar['KodeDepartemen']."\" >".ucwords($daftar['NamaDepartemen'])."</option>\n";
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Sub Kategori</label>
                                            <select class="form-control" id="_kodesub" name="_kodesub" autocomplete="off" required>
                                                <option value="">-- Pilih Sub Kategori --</option>
											</select>
                                        </div>
										<!--<div class="form-group">
                                            <label>Kategori</label>
                                            <select class="form-control" id="_kodekat" name="_kodekat" autocomplete="off" required><option value="">-- Pilih Kategori --</option></select>
                                        </div> -->
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" rows="8" cols="40" name="_ketbrg" placeholder="ex : Ini Adalah Master Barang" autocomplete="off"></textarea>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-input-brg">Submit</button>
										<button type="reset" class="btn btn-default" name="_submit-input-brg">Reset</button><br><br>	
                                      
									</div>
								</form>
									
									<?php
									include "../connections/config.php";
									$_kode = @htmlspecialchars($_POST['_kodebrg']); $_nama = @htmlspecialchars($_POST['_namabrg']); 
									$_hargabrg = @htmlspecialchars($_POST['_hargabeli']); $_hargaet = @htmlspecialchars($_POST['_hargaet']);
									$_dept = @htmlspecialchars($_POST['_kodedpr']); $_subdpr = @htmlspecialchars($_POST['_kodesub']);
									$_kat = @htmlspecialchars($_POST['_kodekat']); $_ket = @htmlspecialchars($_POST['_ketbrg']);  $_sup = @htmlspecialchars($_POST['_kode_supplier']); 
									$_nilai_beli =  @str_replace(".", "", $_hargabrg); $_nilai_et =  @str_replace(".", "", $_hargaet);
									if(isset($_POST['_submit-input-brg'])){
										/* $cek = @sqlsrv_query($dbconnect, "select * from MasterBarang where replace(KodeBarang,' ','') = replace('$_idbrg',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										$num = @sqlsrv_num_rows($cek);
										if($num > 0){ */
										
										// membuat id otomatis
										$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeBarang,16)) AS kode FROM MasterBarang WHERE LEFT(KodeBarang,2)='B-'") or die( print_r( sqlsrv_errors(), true)); 
										$nums = @sqlsrv_num_rows($sql); 
										while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
										if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
										}
										// membuat kode user
										$bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
										$kode_jadi = "B-".$bikin_kode;
									
											$cek2 = @sqlsrv_query($dbconnect, "select * from MasterBarang where replace(KodeBarangManual,' ','') = replace('$_kode',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$num2 = @sqlsrv_num_rows($cek2);
											if($num2 > 0){
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Barang Manual Sudah Ada ", "error"); </script>';
											}else{
											$query = @sqlsrv_query($dbconnect, "insert into MasterBarang (KodeBarang,KodeBarangManual,NamaBarang,HargaBeli,HargaEceran,HargaPromo,HargaUmum,Keterangan,StokMin,IsParcel,IsPromo,IsAktif,IsSentToServer,
											IsBeliXGratisY,IsDiubah,IsGalon,KodeDepartemen,KodeSubDepartemen,IsBarangDibeli,IsBarangDijual,IsPaketPinjaman,IsDapatKupon,IsDapatPoint,IsBarangOnline,BeratKg,NilaiHPP,HargaBeliSebelumnya,
											SelisihHargaBeli,JmlDibeliX,JmlGratisY,KodePerson,Gambar,IsDapatVoucher)values('$kode_jadi','$_kode','$_nama','$_nilai_beli','0','0','0','$_ket','0','False','False','True','False','False','False','False','$_dept','$_subdpr','True','True','False', 
											'False','True','True','0','0','0','0','0','0','$_sup','no-image.png','0')") or die( print_r( sqlsrv_errors(), true)); 	
												if($query){
													@sqlsrv_query($dbconnect, "INSERT into HargaJualSatuan(KodeBarang,KodeSatuan,KonversiSatuan,HargaBeli,HargaJual,HargaEceranTertinggi,HargaJual1,HargaJual2,HargaJual3,HargaJual4,HargaJual5,Diskon,KodeCabang)
													values('$kode_jadi','SAT-0000001','1','$_nilai_beli','$_nilai_et','$_nilai_et','0','0','0','0','0','0','CAB-0001')") or die( print_r( sqlsrv_errors(), true)); 
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-barang.php?page='.htmlspecialchars(base64_encode("insert_gambar")).'&id='.htmlspecialchars(base64_encode($kode_jadi)).'"; });
													</script>';
													
													
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
											}
										/*}else{
											echo '<script type="text/javascript">sweetAlert("Maaf!", " Anda Belum Upload Icon Gambar ", "error"); </script>';
										}*/
									}
									@sqlsrv_close;

									?>
                                    
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Masukkan Icon Gambar</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kodebrg);?>&img=<?php echo base64_encode($fotobrg);?>&type=<?php echo base64_encode('_brng');?>&gbr=<?php echo htmlspecialchars(base64_encode('Gambar')); ?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
											</div>
											<div class="form-group">
												<?php if($fotobrg == null){
													echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" id="_oldimg" width="50%" /><br>';
												}else{
													echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$fotobrg.'" id="_oldimg" width="50%" /><br>';
												}
												?>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									</div>
									
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Kode Barang</label>
                                            <input class="form-control" type="text" value="<?php echo $kodebrg; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Kode Barang Manual</label>
                                            <input class="form-control" type="text" name="_kodebrg2" value="<?php echo $kodebrg2; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Nama Barang</label>
                                            <input class="form-control" type="text" name="_namabrg2" value="<?php echo $barang; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Harga Dasar Produsen</label>
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal" name="_hargajl" value="<?php echo number_format($hargabrg,0,"","."); ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Harga Eceran Tertinggi</label>
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal2"  name="_hargaet" value="<?php echo number_format($hargabrg2,0,"","."); ?>" autocomplete="off" required>
                                        </div>
										<!-- <div class="form-group">
                                            <label>Harga Beli (Harga Supplier)</label>
                                            <input class="form-control" type="number" name="_hargabeli2" value="<?php echo $hargabrg; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Harga Eceran Tertinggi</label>
                                            <input class="form-control" type="number" name="_hargabeli22" value="<?php echo $hargabrg2; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Distributor Internal)</label>
                                            <input class="form-control" type="number" name="_harga12" value="<?php echo $harga1; ?>" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Distributor)</label>
                                            <input class="form-control" type="number" name="_harga22" value="<?php echo $harga2; ?>" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Agen)</label>
                                            <input class="form-control" type="number" name="_harga32" value="<?php echo $harga3; ?>" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Reseller)</label>
                                            <input class="form-control" type="number" name="_harga42" value="<?php echo $harga4; ?>" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual (Member)</label>
                                            <input class="form-control" type="number" name="_harga52" value="<?php echo $harga5; ?>" autocomplete="off">
                                        </div> -->
										<div class="form-group">
                                            <label>Kategori</label>
                                            <select class="form-control" id="_kodedpr" name="_kodedpr2" autocomplete="off">
                                                <?php echo '<option value="">-- Pilih Departemen --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterDepartemen where KodeDepartemen != 'DPT-0000001' AND Keterangan = 'PRODUK' ORDER BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														if($deptbrg !== null){
															if($daftar['KodeDepartemen'] === $deptbrg){
																echo "<option value=\"".$daftar['KodeDepartemen']."\" selected='selected'>".ucwords($daftar['NamaDepartemen'])."</option>\n";
															}else{
																echo "<option value=\"".$daftar['KodeDepartemen']."\">".ucwords($daftar['NamaDepartemen'])."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['KodeDepartemen']."\">".ucwords($daftar['NamaDepartemen'])."</option>\n";
														}
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Sub Kategori</label>
                                            <select class="form-control" id="_kodesub" name="_kodesub2" autocomplete="off">
                                                <?php echo '<option value="">-- Pilih Sub Kategori --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterSubDepartemen ORDER BY KodeSubDepartemen") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														if($subbrg !== null){
															if($daftar['KodeSubDepartemen'] === $subbrg){
																echo "<option value=\"".$daftar['KodeSubDepartemen']."\" selected='selected'>".ucwords($daftar['NamaSubDepartemen'])."</option>\n";
															}else{
																echo "<option value=\"".$daftar['KodeSubDepartemen']."\">".ucwords($daftar['NamaSubDepartemen'])."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['KodeSubDepartemen']."\">".ucwords($daftar['NamaSubDepartemen'])."</option>\n";
														}
													}
												?>
                                            </select>
                                        </div>
										<!-- <div class="form-group">
                                            <label>Kategori</label>
                                            <select class="form-control" id="_kodekat" name="_kodekat2" autocomplete="off">
                                                <?php /* echo '<option value="">-- Pilih Kategori --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterKategory ORDER BY KodeKategory") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														if($katbrg !== null){
															if($daftar['KodeKategory'] === $katbrg){
																echo "<option value=\"".$daftar['KodeKategory']."\" selected='selected'>".ucwords($daftar['NamaKategory'])."</option>\n";
															}else{
																echo "<option value=\"".$daftar['KodeKategory']."\">".ucwords($daftar['NamaKategory'])."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['KodeKategory']."\">".ucwords($daftar['NamaKategory'])."</option>\n";
														}
													}
												*/?>
                                            </select>
                                        </div> -->
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" rows="8" cols="40" name="_ketbrg2" autocomplete="off"><?php echo $ketbrg; ?></textarea>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-edit-brg">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-brg">Reset</button>
									</div>
								</form>
										
										<?php
										include "../connections/config.php";
										$_kode2 = @htmlspecialchars($_POST['_kodebrg2']); $_nama2 = @htmlspecialchars($_POST['_namabrg2']); 
										$_hargabrg2 = @htmlspecialchars($_POST['_hargajl']); $_dept2 = @htmlspecialchars($_POST['_kodedpr2']); $_subdpr2 = @htmlspecialchars($_POST['_kodesub2']);
										$_hargabrg22 = @htmlspecialchars($_POST['_hargaet']); $_kat2 = @htmlspecialchars($_POST['_kodekat2']); $_ket2 = @htmlspecialchars($_POST['_ketbrg2']); 
										$_nilai_beli2 =  @str_replace(".", "", $_hargabrg2); $_nilai_et2 =  @str_replace(".", "", $_hargabrg22);
										if(isset($_POST['_submit-edit-brg'])){
											if($kodebrg2 === $_kode2){
												$query = @sqlsrv_query($dbconnect, "update MasterBarang set KodeBarangManual = '$_kode2', NamaBarang = '$_nama2', HargaBeli = '$_nilai_beli2',
												Keterangan = '$_ket2', KodeDepartemen = '$_dept2', KodeSubDepartemen = '$_subdpr2' where KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														@sqlsrv_query($dbconnect, "update HargaJualSatuan set HargaBeli = '$_nilai_beli2', HargaEceranTertinggi = '$_nilai_et2' where KodeBarang = '".$kodebrg."' AND KodeSatuan = 'SAT-0000001'") or die( print_r( sqlsrv_errors(), true)); 	
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
														function () { window.location.href = "mst-barang.php"; });
														</script>';
													}
													else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
													}
											}else{
												$cek = @sqlsrv_query($dbconnect, "select * from MasterBarang where replace(KodeBarangManual,' ','') = replace('$_kode2',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num = @sqlsrv_num_rows($cek);
												if($num > 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Barang Manual Sudah Ada ", "error"); </script>';
												}else{
												$query = @sqlsrv_query($dbconnect, "update MasterBarang set KodeBarangManual = '$_kode2', NamaBarang = '$_nama2', HargaBeli = '$_nilai_beli2',
												Keterangan = '$_ket2', KodeDepartemen = '$_dept2', KodeSubDepartemen = '$_subdpr2' where KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														@sqlsrv_query($dbconnect, "update HargaJualSatuan set HargaBeli = '$_nilai_beli2', HargaEceranTertinggi = '$_nilai_et2' where KodeBarang = '".$kodebrg."' AND KodeSatuan = 'SAT-0000001'") or die( print_r( sqlsrv_errors(), true)); 	
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
														function () { window.location.href = "mst-barang.php"; });
														</script>';
													}
													else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
													}
												}
											}
										}
										@sqlsrv_close;

										?>
                                
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'insert_gambar'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Insert Gambar
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Ganti Icon Gambar</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($id);?>&img=<?php echo base64_encode($fotobrg);?>&type=<?php echo base64_encode('_brng');?>&gbr=<?php echo htmlspecialchars(base64_encode('Gambar')); ?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
											</div>
											<div class="form-group">
												<?php if($fotobrg == null){
													echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" id="_oldimg" width="50%" /><br>';
												}else{
													echo '<img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$fotobrg.'" id="_oldimg" width="50%" /><br>';
												}
												?>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									<a href="mst-barang.php" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Selesai</a>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'set_gambar'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Set Gambar
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-12">
									<div class="row">
										<div class="col-lg-3 text-center">
										<div class="form-group">
											<label>Gambar 1</label><hr>
												<?php if($fotobrg !== null){
													echo '<dd><img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$fotobrg.'" width="100%" /></dd>';
												}else{ 
													echo '<dd><img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" width="100%" /></dd>'; 
												} ?>
											<hr><a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('input_gambar'))."&id=".htmlspecialchars(base64_encode($id))."&gbr=".htmlspecialchars(base64_encode('Gambar')); ?>" class="btn btn-primary">Ganti Gambar</a>
										</div>
										</div>
										<div class="col-lg-3 text-center">
										<div class="form-group">
											<label>Gambar 2</label><hr>
												<?php if($fotobrg2 !== null){
													echo '<dd><img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$fotobrg2.'" width="100%" /></dd>';
												}else{ 
													echo '<dd><img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" width="100%" /></dd>'; 
												} ?>
											<hr><a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('input_gambar'))."&id=".htmlspecialchars(base64_encode($id))."&gbr=".htmlspecialchars(base64_encode('Gambar2')); ?>" class="btn btn-primary">Ganti Gambar</a>
										</div>
										</div>
										<div class="col-lg-3 text-center">
										<div class="form-group">
											<label>Gambar 3</label><hr>
												<?php if($fotobrg3 !== null){
													echo '<dd><img class="imgl img-thumbnail" src="../img/foto-barang/thumb_'.$fotobrg3.'" width="100%" /></dd>';
												}else{ 
													echo '<dd><img class="imgl img-thumbnail" src="../img/foto-barang/thumb_no-image.png" width="100%" /></dd>'; 
												} ?>
											<hr><a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('input_gambar'))."&id=".htmlspecialchars(base64_encode($id))."&gbr=".htmlspecialchars(base64_encode('Gambar3')); ?>" class="btn btn-primary">Ganti Gambar</a>
										</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'input_gambar'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Ganti Gambar
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Ganti Icon Gambar</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($id);?>&type=<?php echo base64_encode('_brng');?>&gbr=<?php echo base64_encode($gbr);?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
											</div>
											<div class="form-group">
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									<a href="mst-barang.php?page=<?php echo htmlspecialchars(base64_encode('set_gambar'))."&id=".htmlspecialchars(base64_encode($id)); ?>" class="btn btn-primary"><i class="fa fa-refresh"></i>&nbsp;Kembali</a>
									
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-3">
									<div class="form-group">
										<div class="row">
										<ul id="thumbs" class="portfolio">
                                            <?php if($fotobrg !== null AND file_exists('../img/foto-barang/'.$fotobrg)){ $gambar = $fotobrg; ?>
											<!-- Item Project and Filter Name 1 -->
											<li class="item-thumbs col-lg-12 images" data-id="id-0" data-type="images">
											<!-- Fancybox - Gallery Enabled - Title - Full Image -->
											<a class="hover-wrap fancybox" data-fancybox-group="images" title="<?php echo ucwords($barang); ?>" href="../img/foto-barang/<?php echo $gambar; ?>">
											<span class="overlay-img"></span>
											<span class="overlay-img-thumb font-icon-plus"></span>
											</a>
											<!-- Thumb Image and Description -->
											<img class="img-thumbnail" src="../img/foto-barang/thumb_<?php echo $gambar; ?>" 
											alt="<?php echo "Harga MD : Rp. ".number_format($harga1)."<br>Harga Distributor : Rp. ".number_format($harga2)."<br>Harga Agen : Rp. ".number_format($harga3)."<br>Harga Reseller : Rp. ".number_format($harga4)."<br>Harga User : Rp. ".number_format($harga5); ?>">
											</li>
											<?php } else { echo '<li class="item-thumbs col-lg-12 images" data-id="id-0" data-type="images"><img class="img-thumbnail" src="../img/foto-barang/thumb_no-image.png" /></li>'; } ?>
										<ul/>
										</div>
                                   
										<div class="row">
										<ul id="thumbs" class="portfolio">
											<?php if($fotobrg2 !== null AND file_exists('../img/foto-barang/'.$fotobrg2)){ $gambar2 = $fotobrg2; ?>
											<!-- Item Project and Filter Name 1 -->
											<li class="item-thumbs col-lg-6 images" data-id="id-0" data-type="images">
											<!-- Fancybox - Gallery Enabled - Title - Full Image -->
											<a class="hover-wrap fancybox" data-fancybox-group="images" title="<?php echo ucwords($barang); ?>" href="../img/foto-barang/<?php echo $gambar2; ?>">
											<span class="overlay-img"></span>
											<span class="overlay-img-thumb font-icon-plus"></span>
											</a>
											<!-- Thumb Image and Description -->
											<img class="img-thumbnail" src="../img/foto-barang/thumb_<?php echo $gambar2; ?>" 
											alt="<?php echo "Harga MD : Rp. ".number_format($harga1)."<br>Harga Distributor : Rp. ".number_format($harga2)."<br>Harga Agen : Rp. ".number_format($harga3)."<br>Harga Reseller : Rp. ".number_format($harga4)."<br>Harga User : Rp. ".number_format($harga5); ?>">
											</li>
											<?php } else { echo ''; } ?>
											
											<?php if($fotobrg3 !== null AND file_exists('../img/foto-barang/'.$fotobrg3)){ $gambar3 = $fotobrg3; ?>
											<!-- Item Project and Filter Name 1 -->
											<li class="item-thumbs col-lg-6 images" data-id="id-0" data-type="images">
											<!-- Fancybox - Gallery Enabled - Title - Full Image -->
											<a class="hover-wrap fancybox" data-fancybox-group="images" title="<?php echo ucwords($barang); ?>" href="../img/foto-barang/<?php echo $gambar3; ?>">
											<span class="overlay-img"></span>
											<span class="overlay-img-thumb font-icon-plus"></span>
											</a>
											<!-- Thumb Image and Description -->
											<img class="img-thumbnail" src="../img/foto-barang/thumb_<?php echo $gambar3; ?>" 
											alt="<?php echo "Harga MD : Rp. ".number_format($harga1)."<br>Harga Distributor : Rp. ".number_format($harga2)."<br>Harga Agen : Rp. ".number_format($harga3)."<br>Harga Reseller : Rp. ".number_format($harga4)."<br>Harga User : Rp. ".number_format($harga5); ?>">
											</li>
											<?php } else { echo ''; } ?>
											
										<ul/>
											
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
                                        <label>Kode Barang</label>
                                            <dd><?php echo $kodebrg2; ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Nama Barang</label>
                                            <dd><?php echo ucwords($barang); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Harga Dasar Produsen</label>
                                            <dd><?php echo "Rp. ".number_format($hargabrg); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Harga Eceran Tertinggi</label>
                                            <dd><?php echo "Rp. ".number_format($hargabrg2); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Kategori Barang</label>
                                            <dd><?php echo ucwords($namadept); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Sub Kategori Barang</label>
                                            <dd><?php echo ucwords($namasubdept); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Keterangan</label>
                                            <dd><?php echo ucwords($ketbrg); ?></dd><br>
                                    </div>
										
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Satuan</label>
										<?php if($satbrg == null){ $satbrg2 = 'SAT-0000001'; } else { $satbrg2 = $satbrg; }
											$list = @sqlsrv_query($dbconnect, "SELECT * FROM MstSatuan Inner Join HargaJualSatuan On HargaJualSatuan.KodeSatuan = MstSatuan.KodeSatuan 
											where HargaJualSatuan.KodeBarang = '".$id."' ORDER BY MstSatuan.KodeSatuan") or die( print_r( sqlsrv_errors(), true)); 
											$jsArray_h1 = "var rekName = new Array();\n"; 
											$jsArray_h2 = "var rekName2 = new Array();\n"; 
											$jsArray_h3 = "var rekName3 = new Array();\n"; 
											$jsArray_h4 = "var rekName4 = new Array();\n";
											$jsArray_h5 = "var rekName5 = new Array();\n";
														
											echo '<select class="form-control" name="_kodesat" 
											onchange="document.getElementById(\'_h1\').value = rekName[this.value]; 
											document.getElementById(\'_h2\').value = rekName2[this.value]; 
											document.getElementById(\'_h3\').value = rekName3[this.value];
											document.getElementById(\'_h4\').value = rekName4[this.value]; 
											document.getElementById(\'_h5\').value = rekName5[this.value]" "autocomplete="off">';
											
											while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
												if($satbrg2 !== null){
													if($daftar['KodeSatuan'] === $satbrg2){
														echo "<option value=\"".$daftar['KodeSatuan']."\" selected='selected'>".ucwords($daftar['NamaSatuan'])."</option>\n";
													}else{
														echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
													}
												}else{
													echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
												}
												
												$jsArray_h1 .= "rekName['".$daftar['KodeSatuan']."'] = 'Rp. ".addslashes(ucwords(number_format($daftar['HargaJual1'])))."';\n";
												$jsArray_h2 .= "rekName2['".$daftar['KodeSatuan']."'] = 'Rp. ".addslashes(ucwords(number_format($daftar['HargaJual2'])))."';\n";
												$jsArray_h3 .= "rekName3['".$daftar['KodeSatuan']."'] = 'Rp. ".addslashes(ucwords(number_format($daftar['HargaJual3'])))."';\n";
												$jsArray_h4 .= "rekName4['".$daftar['KodeSatuan']."'] = 'Rp. ".addslashes(ucwords(number_format($daftar['HargaJual4'])))."';\n";
												$jsArray_h5 .= "rekName5['".$daftar['KodeSatuan']."'] = 'Rp. ".addslashes(ucwords(number_format($daftar['HargaJual5'])))."';\n";
											}
										?>
										</select>
									</div>
									
									
									<div class="form-group">
                                        <label>Harga Master Distributor</label><br>
											<input style="border: 0px;" type='text' name='_h1' id='_h1' value="<?php echo "Rp. ".number_format($harga1); ?>"readonly='readonly' />
											<script type="text/javascript"><?php echo $jsArray_h1; ?></script>
                                    </div>
									<div class="form-group">
                                        <label>Harga Distributor</label><br>
											<input style="border: 0px;" type='text' name='_h2' id='_h2' value="<?php echo "Rp. ".number_format($harga2); ?>"readonly='readonly' />
											<script type="text/javascript"><?php echo $jsArray_h2; ?></script>
                                    </div>
									<div class="form-group">
                                        <label>Harga Agen</label><br>
											<input style="border: 0px;" type='text' name='_h3' id='_h3' value="<?php echo "Rp. ".number_format($harga3); ?>"readonly='readonly' />
											<script type="text/javascript"><?php echo $jsArray_h3; ?></script>
                                    </div>
									<div class="form-group">
                                        <label>Harga Reseller</label><br>
											<input style="border: 0px;" type='text' name='_h4' id='_h4' value="<?php echo "Rp. ".number_format($harga4); ?>"readonly='readonly' />
											<script type="text/javascript"><?php echo $jsArray_h4; ?></script>
                                    </div>
									<div class="form-group">
                                        <label>Harga User</label><br>
											<input style="border: 0px;" type='text' name='_h5' id='_h5' value="<?php echo "Rp. ".number_format($harga5); ?>"readonly='readonly' />
											<script type="text/javascript"><?php echo $jsArray_h5; ?></script>
                                    </div>
									
								</div>
							</div>
						</div>
						
						<?php } elseif($id != null AND $page == 'set-harga'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-pencil fa-fw"></i> Set Harga
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Nama Barang</label>
													<dd><?php echo $kodebrg2." : ".ucwords($barang); ?></dd>
											</div>
											<div class="form-group">
												<label>Harga Dasar Produsen</label>
													<dd><?php echo "Rp. ".number_format($hargabrg); ?></dd>
											</div>
											<div class="form-group">
												<label>Harga Eceran Tertinggi</label>
													<dd><?php echo "Rp. ".number_format($hargabrg2); ?></dd>
											</div>
											<div class="form-group">
												<label>Harga Jual</label>
													<dd><?php echo "Rp. ".number_format($hargajualbrg); ?></dd>
											</div>
										</div>
									</div>
									
									
										<form role="form" method="post">
											<div class="form-group">
												<label>Satuan</label>
												<?php $list = @sqlsrv_query($dbconnect, "SELECT * FROM MstSatuan ORDER BY KodeSatuan") or die( print_r( sqlsrv_errors(), true)); 
													$jsArray_h1 = "var rekName = new Array();\n"; 
													
													echo '<select class="form-control" name="_kodesat" 
													onchange="document.getElementById(\'_h1\').value = rekName[this.value]" autocomplete="off">';
													echo '<option value="">-- Pilih Satuan --</option>';
													
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														if($satbrg === $daftar['KodeSatuan']){
															echo "<option value=\"".$daftar['KodeSatuan']."\" selected='selected'>".ucwords($daftar['NamaSatuan'])."</option>\n";
															$list2 = @sqlsrv_query($dbconnect, "SELECT * FROM HargaJualSatuan where KodeBarang = '".$id."' AND KodeSatuan = '".$daftar['KodeSatuan']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
															while($daftar2 = @sqlsrv_fetch_array($list2, SQLSRV_FETCH_ASSOC)){
																$jsArray_h1 .= "rekName['".$daftar2['KodeSatuan']."'] = '".addslashes($daftar2['KonversiSatuan'])."';\n";
															}
														} else {
															echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
															$list2 = @sqlsrv_query($dbconnect, "SELECT * FROM HargaJualSatuan where KodeBarang = '".$id."' AND KodeSatuan = '".$daftar['KodeSatuan']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
															while($daftar2 = @sqlsrv_fetch_array($list2, SQLSRV_FETCH_ASSOC)){
																$jsArray_h1 .= "rekName['".$daftar2['KodeSatuan']."'] = '".addslashes($daftar2['KonversiSatuan'])."';\n";
															}
														}
													}
												?>
												</select>
											</div>
											
											<div class="form-group">
												<label>Konversi (PCS)</label>
												<input class="form-control" type="text" name='_h1' id='_h1' value="<?php echo $konvbrg; ?>" autocomplete="off" required>
												<script type="text/javascript"><?php echo $jsArray_h1; ?></script>
											</div>
											
											<!-- 
											<div class="form-group">
												<label>Satuan</label>
												<?php /* $list = @sqlsrv_query($dbconnect, "SELECT * FROM MstSatuan ORDER BY KodeSatuan") or die( print_r( sqlsrv_errors(), true)); 
													echo '<select class="form-control" id="_kodesat" name="_kodesat" onchange="konversi()" autocomplete="off">';
													echo '<option value="">-- Pilih Satuan --</option>';
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
													} */
												?>
												</select>
											</div>
											
											<div class="form-group">
												<label>Konversi</label>
												<input class="form-control" type="text" name='_konversi' id='_konversi' autocomplete="off" required>
											</div>
											
											<!-- <div class="form-group">
												<label>Satuan</label>
												<select class="form-control" name="_kodesat" autocomplete="off">
													<?php /* echo '<option value="">-- Pilih Satuan --</option>';
														$list = @sqlsrv_query($dbconnect, "SELECT * FROM MstSatuan ORDER BY KodeSatuan") or die( print_r( sqlsrv_errors(), true)); 
														while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
															if($satbrg !== null){
																if($daftar['KodeSatuan'] === $satbrg){
																	echo "<option value=\"".$daftar['KodeSatuan']."\" selected='selected'>".ucwords($daftar['NamaSatuan'])."</option>\n";
																}else{
																	echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
																}
															}else{
																echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
															}
														} */
													?>
												</select>
											</div> -->
											
											<button type="submit" name="cek-satuan" class="btn btn-info">Cek Satuan</button>
											<?php if(isset($_POST['cek-satuan'])){
												$kode_sat = $_POST['_kodesat']; $konv_sat = $_POST['_h1']; 
												if($konv_sat === 'undefined' OR $konv_sat === ''){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Konversi Harus Diisi ", "error"); </script>';
												} else {
													echo '<script type="text/javascript">window.location.href = "mst-barang.php?id='.base64_encode($kodebrg).'&page='.base64_encode("set-harga").'&sat='.base64_encode($kode_sat).'&konv='.base64_encode($konv_sat).'";</script>';
												}
											}
											?>
											<hr>
											
											<?php if($satbrg == null OR $satbrg === ""){ echo "";  
											} else { 
											$list2 = @sqlsrv_query($dbconnect, "SELECT * FROM HargaJualSatuan where KodeSatuan = '".$satbrg."' AND KodeBarang = '".$id."' ORDER BY KodeSatuan", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$dapat2 = @sqlsrv_num_rows($list2); 
											while($daftar2 = @sqlsrv_fetch_array($list2, SQLSRV_FETCH_ASSOC)){
												$konv = $daftar2['KonversiSatuan']; $hrg_beli = $daftar2['HargaBeli']; $hrg_jual = $daftar2['HargaJual'];
												$hrg_jual1 = $daftar2['HargaJual1']; $hrg_jual2 = $daftar2['HargaJual2']; $hrg_jual3 = $daftar2['HargaJual3'];
												$hrg_jual4 = $daftar2['HargaJual4']; $hrg_jual5 = $daftar2['HargaJual5'];
											}
											?>
											<!-- <div class="form-group">
												<label>Konversi</label>
												<?php /* if($dapat2 === 0){
													if($satbrg === 'SAT-0000001'){
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="'.$konv.'" autocomplete="off" readonly>';
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="'.$konvbrg.'" autocomplete="off" readonly>';
														}
													} else {
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="0" autocomplete="off">';
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="'.$konvbrg.'" autocomplete="off">';
														}
													} 
												}else{
													if($satbrg === 'SAT-0000001'){
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="'.$konv.'" autocomplete="off" readonly>';
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="'.$konvbrg.'" autocomplete="off" readonly>';
														}
													} else {
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="'.$konv.'" autocomplete="off">';
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  name="_konv" value="'.$konvbrg.'" autocomplete="off">';
														}
													} 
												} */ ?>
											</div> -->
											<div class="form-group">
												<label>Harga Dasar</label>
												<?php if(@$hrg_beli == null OR @$hrg_beli === 0){
														if($satbrg === 'SAT-0000001'){
															if($konvbrg == null){
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="0" autocomplete="off" readonly>';
															}else{
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="'.number_format($konvbrg*$hargabrg,0,"",".").'" autocomplete="off" readonly >';
															}
														}else{
															if($konvbrg == null){
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="0" autocomplete="off" readonly>';
															}else{
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="'.number_format($konvbrg*$hargabrg,0,"",".").'" autocomplete="off">';
															}
														}	
													} else {
														if($satbrg === 'SAT-0000001'){
															if($konvbrg == null){
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="'.number_format($hrg_beli,0,"",".").'" autocomplete="off" readonly>';
															}else{
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="'.number_format($hrg_beli,0,"",".").'" autocomplete="off" readonly>';
															}
														}else{
															if($konvbrg == null){
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="'.number_format($hrg_beli,0,"",".").'" autocomplete="off" readonly>';
															}else{
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal" name="_hargabeli" value="'.number_format($hrg_beli,0,"",".").'" autocomplete="off">';
															}
														}	
												} ?>
											</div>
											<div class="form-group">
												<label>Harga Jual</label>
												<?php if(@$hrg_jual == null OR @$hrg_jual === 0){
													if($hj == null){
														if($konvbrg == null){
															if($satbrg === 'SAT-0000001'){
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="0" autocomplete="off">';
															}else{
																echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="0" autocomplete="off" readonly>';
															}
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="'.number_format($konvbrg*$hargajualbrg,0,"",".").'" autocomplete="off" required>';
														}
													} else {
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="'.number_format($hargajualbrg,0,"",".").'" autocomplete="off" required>';
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="'.number_format($konvbrg*$hargajualbrg,0,"",".").'" autocomplete="off" required>';
														}
													}
												} else {
													if($hj == null){
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="'.number_format($hrg_jual,0,"",".").'" autocomplete="off" required>';
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="'.number_format($hrg_jual,0,"",".").'" autocomplete="off" required>';
														}
													} else {
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="'.number_format($hj,0,"",".").'" autocomplete="off" required>';
														}else{
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal2"  name="_hargajual" value="'.number_format($hrg_jual,0,"",".").'" autocomplete="off" required>';
														}
													}
												} ?>
												<?php  ?>
											</div>
											<div class="form-group">
												<label>Laba Penjualan</label>
												<?php if($dapat2 === 0){
													if($hj == null){
														if($konvbrg == null){
															$laba = $hargajualbrg-$hargabrg;
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}else{
															$laba = $konvbrg*($hargajualbrg-$hargabrg);
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}
													} else {
														if($konvbrg == null){
															$laba = $hj-($hargabrg);
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}else{
															$laba = $hj-($hargabrg);
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}
													} 
												}else{
													if($hj == null){
														if($konvbrg == null){
															$laba = $hrg_jual-$hrg_beli;
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}else{
															$laba = $hrg_jual-$hrg_beli;
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}
													} else {
														if($konvbrg == null){
															$laba = $hj-$hrg_beli;
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}else{
															$laba = $hj-$hrg_beli;
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal3"  name="_laba" value="'.number_format($laba,0,"",".").'" autocomplete="off" readonly>';
														}
													}
												} ?>
												
											</div>
											<div class="form-group">
												<label>Laba Grosir Mart</label>&nbsp;(* 20% dari Laba Penjualan)
												<?php if($hj == null){
													$internal = $laba*(20/100);
													echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal4"  name="_labagm" value="'.number_format($internal,0,"",".").'" autocomplete="off" readonly>';
												} else {
													$internal = $laba*(20/100);
													echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal4"  name="_labagm" value="'.number_format($internal,0,"",".").'" autocomplete="off" readonly>';
												} ?>
											</div>
											<div class="form-group">
												<label>Laba Bersih</label>&nbsp;(* Setelah Laba Grosirmart)
												<?php if($hj == null){
													$lababersih = $laba-$internal;
													echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal5"  name="_labagm" value="'.number_format($lababersih,0,"",".").'" autocomplete="off" readonly>';
												} else {
													$lababersih = $laba-$internal;
													echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal5"  name="_labagm" value="'.number_format($lababersih,0,"",".").'" autocomplete="off" readonly>';
												} ?>
											</div>
											<div class="form-group">
												<label>Saving Diskon</label>&nbsp;(* 15% dari Laba Bersih)
												<?php if($hj == null){
													$saving = ($laba-$internal)*(15/100);
													echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal6"  name="_saving" value="'.number_format($saving,0,"",".").'" readonly>';
												} else {
													$saving = ($laba-$internal)*(15/100);
													echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal6"  name="_saving" value="'.number_format($saving,0,"",".").'" autocomplete="off" readonly>';
												} ?>
												
											</div>
											<button type="submit" name="cek-harga" class="btn btn-info">Hitung</button>
											<?php if(isset($_POST['cek-harga'])){
												$cek_beli = str_replace(".","",@$_POST['_hargabeli']); $cek_jual = str_replace(".","",@$_POST['_hargajual']); $cek_konv = $konvbrg;
												if($cek_jual < $cek_beli){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Harga Jual Lebih Kecil Dari Harga Beli ", "error"); </script>';
												}else{
													echo '<script type="text/javascript">window.location.href = "mst-barang.php?id='.base64_encode($kodebrg).'&page='.base64_encode("set-harga").'&sat='.base64_encode($satbrg).'&konv='.base64_encode($cek_konv).'&hj='.base64_encode($cek_jual).'";</script>';
												}	
												
												if($dapat2 === 0){
													$query = @sqlsrv_query($dbconnect, "INSERT into HargaJualSatuan(KodeBarang,KodeSatuan,KonversiSatuan,HargaBeli,HargaJual,HargaEceranTertinggi,Diskon,KodeCabang)
													values('$id','$satbrg','$cek_konv','$cek_beli','$cek_jual','$cek_jual','0','CAB-0001')") or die( print_r( sqlsrv_errors(), true)); 
												}else{
													$query = @sqlsrv_query($dbconnect, "update HargaJualSatuan set KonversiSatuan = '$cek_konv', HargaBeli = '$cek_beli', HargaJual = '$cek_jual' where KodeBarang = '".$id."' AND KodeSatuan = '".$satbrg."'") or die( print_r( sqlsrv_errors(), true)); 	 
													
												}
													
											}
											?>
										
									
										
								</div>
								
								<div class="col-lg-6">
									
										<!-- <div class="form-group">
                                            <label>Kategori</label>
                                            <?php /*$list = @sqlsrv_query($dbconnect, "SELECT * FROM MstSatuan ORDER BY KodeSatuan") or die( print_r( sqlsrv_errors(), true));
											$jsArray_id = "var satId = new Array();\n"; $jsArray_konv = "var satKonv = new Array();\n";
											$jsArray_h1 = "var satH1 = new Array();\n"; $jsArray_h2 = "var satH2 = new Array();\n"; $jsArray_h3 = "var satH3 = new Array();\n";
											$jsArray_h4 = "var satH4 = new Array();\n"; $jsArray_h5 = "var satH5 = new Array();\n";
											
											echo '<select class="form-control" name="_kode" onchange="document.getElementById(\'_kode_sat\').value = satId[this.value]; 
											document.getElementById(\'_konv_sat\').value = satKonv[this.value]; document.getElementById(\'_h1_sat\').value = satH1[this.value]; 
											document.getElementById(\'_h2_sat\').value = satH2[this.value]; document.getElementById(\'_h3_sat\').value = satH3[this.value];
											document.getElementById(\'_h4_sat\').value = satH4[this.value]; document.getElementById(\'_h5_sat\').value = satH5[this.value]">';
											echo '<option value="">-- Pilih Satuan --</option>';
											while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														if($satuan !== null){
															if($daftar['KodeSatuan'] === $satuan){
																echo "<option value=\"".$daftar['KodeSatuan']."\" selected='selected'>".$daftar['KodeSatuanManual']." : ".ucwords($daftar['NamaSatuan'])."</option>\n";
															}else{
																echo "<option value=\"".$daftar['KodeSatuan']."\">".$daftar['KodeSatuanManual']." : ".ucwords($daftar['NamaSatuan'])."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['KodeSatuan']."\">".$daftar['KodeSatuanManual']." : ".ucwords($daftar['NamaSatuan'])."</option>\n";
														}
													}
												*/?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Konversi Satuan</label>
												<input class="form-control" type="number" name="_konversi" value="<?php echo $konversi; ?>" autocomplete="off" required readonly>
                                        </div><hr> -->
										
											<div class="form-group">
												<label>Harga MD</label>&nbsp;(* Harga Dasar + Laba GM + Saving Diskon)
												<?php $MD = ($laba-$internal)*(5/100); $Dist = ($laba-$internal)*(10/100); $Agen = ($laba-$internal)*(20/100); $Resell = ($laba-$internal)*(50/100);
												if($dapat2 === 0){
													if($hj == null){
														if($konvbrg == null){
															$harga_MD = $hargajualbrg-($MD+$Dist+$Agen+$Resell);
															echo '<input class="form-control" type="hidden" value="'.$MD.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal7"  name="_internal" value="'.number_format($harga_MD,0,"",".").'" autocomplete="off" readonly>';
														} else {
															$harga_MD = ($hargajualbrg*$konvbrg)-($MD+$Dist+$Agen+$Resell);
															echo '<input class="form-control" type="hidden" value="'.$MD.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal7"  name="_internal" value="'.number_format($harga_MD,0,"",".").'" autocomplete="off" readonly>';
														}
													} else {
														$harga_MD = $hj-($MD+$Dist+$Agen+$Resell);
														echo '<input class="form-control" type="hidden" value="'.$MD.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal7"  name="_internal" value="'.number_format($harga_MD,0,"",".").'" autocomplete="off" readonly>';
													} 
												}else{
													if($hj == null){
														$harga_MD = $hrg_jual1;
														echo '<input class="form-control" type="hidden" value="'.$MD.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal7"  name="_internal" value="'.number_format($harga_MD,0,"",".").'" autocomplete="off" readonly>';
													} else {
														$harga_MD = $hj-($MD+$Dist+$Agen+$Resell);
														echo '<input class="form-control" type="hidden" value="'.$MD.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal7"  name="_internal" value="'.number_format($harga_MD,0,"",".").'" autocomplete="off" readonly>';
													} 
												} ?>
											</div>
											<div class="form-group">
												<label>Harga Distributor</label>&nbsp;(* Harga MD + 5% dari Laba Bersih)
												<?php if($dapat2 === 0){
													if($hj == null){
														if($konvbrg == null){
															$harga_Dist = $hargajualbrg-($Dist+$Agen+$Resell);
															echo '<input class="form-control" type="hidden" value="'.$Dist.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal8"  name="_dist" value="'.number_format($harga_Dist,0,"",".").'" autocomplete="off" readonly>';
														} else {
															$harga_Dist = ($hargajualbrg*$konvbrg)-($Dist+$Agen+$Resell);
															echo '<input class="form-control" type="hidden" value="'.$Dist.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal8"  name="_dist" value="'.number_format($harga_Dist,0,"",".").'" autocomplete="off" readonly>';
														}
													} else {
														$harga_Dist = $hj-($Dist+$Agen+$Resell);
														echo '<input class="form-control" type="hidden" value="'.$Dist.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal8"  name="_dist" value="'.number_format($harga_Dist,0,"",".").'" autocomplete="off" readonly>';
													}
												}else{
													if($hj == null){
														$harga_Dist = $hrg_jual2;
														echo '<input class="form-control" type="hidden" value="'.$Dist.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal8"  name="_dist" value="'.number_format($harga_Dist,0,"",".").'" autocomplete="off" readonly>';
													} else {
														$harga_Dist = $hj-($Dist+$Agen+$Resell);
														echo '<input class="form-control" type="hidden" value="'.$Dist.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal8"  name="_dist" value="'.number_format($harga_Dist,0,"",".").'" autocomplete="off" readonly>';
													} 
												} ?>
											</div>
											<div class="form-group">
												<label>Harga Agen</label>&nbsp;(* Harga Distributor + 10% dari Laba Bersih)
												<?php if($dapat2 === 0){
													if($hj == null){
														if($konvbrg == null){
															$harga_Agen = $hargajualbrg-($Agen+$Resell);
															echo '<input class="form-control" type="hidden" value="'.$Agen.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal9"  name="_agen" value="'.number_format($harga_Agen,0,"",".").'" autocomplete="off" readonly>';
														} else {
															$harga_Agen = ($hargajualbrg*$konvbrg)-($Agen+$Resell);
															echo '<input class="form-control" type="hidden" value="'.$Agen.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal9"  name="_agen" value="'.number_format($harga_Agen,0,"",".").'" autocomplete="off" readonly>';
														} 
													} else {
														$harga_Agen = $hj-($Agen+$Resell);
														echo '<input class="form-control" type="hidden" value="'.$Agen.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal9"  name="_agen" value="'.number_format($harga_Agen,0,"",".").'" autocomplete="off" readonly>';
													}
												}else{
													if($hj == null){
														$harga_Agen = $hrg_jual3;
														echo '<input class="form-control" type="hidden" value="'.$Agen.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal9"  name="_agen" value="'.number_format($harga_Agen,0,"",".").'" autocomplete="off" readonly>';
													} else {
														$harga_Agen = $hj-($Agen+$Resell);
														echo '<input class="form-control" type="hidden" value="'.$Agen.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal9"  name="_agen" value="'.number_format($harga_Agen,0,"",".").'" autocomplete="off" readonly>';
													}
												} ?>
											</div>
											<div class="form-group">
												<label>Harga Reseller</label>&nbsp;(* Harga Agen + 20% dari Laba Bersih)
												<?php if($dapat2 === 0){
													if($hj == null){
														if($konvbrg == null){
															$harga_Resell = $hargajualbrg-$Resell;
															echo '<input class="form-control" type="hidden" value="'.$Resell.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal10"  name="_resell" value="'.number_format($harga_Resell,0,"",".").'" autocomplete="off" readonly>';
														} else {
															$harga_Resell = ($hargajualbrg*$konvbrg)-$Resell;
															echo '<input class="form-control" type="hidden" value="'.$Resell.'" autocomplete="off" readonly>';
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal10"  name="_resell" value="'.number_format($harga_Resell,0,"",".").'" autocomplete="off" readonly>';
														}
													} else {
														$harga_Resell = $hj-$Resell;
														echo '<input class="form-control" type="hidden" value="'.$Resell.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal10"  name="_resell" value="'.number_format($harga_Resell,0,"",".").'" autocomplete="off" readonly>';
													}
												}else{
													if($hj == null){
														$harga_Resell = $hrg_jual4;
														echo '<input class="form-control" type="hidden" value="'.$Resell.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal10"  name="_resell" value="'.number_format($harga_Resell,0,"",".").'" autocomplete="off" readonly>';
													} else {
														$harga_Resell = $hj-$Resell;
														echo '<input class="form-control" type="hidden" value="'.$Resell.'" autocomplete="off" readonly>';
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal10"  name="_resell" value="'.number_format($harga_Resell,0,"",".").'" autocomplete="off" readonly>';
													}
												} ?>
											</div>
											<div class="form-group">
												<label>Harga User</label>&nbsp;(* Harga Reseller + 50% dari Laba Bersih)
												<?php if($dapat2 === 0){
													if($hj == null){
														if($konvbrg == null){
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal11"  name="_member" value="'.number_format($hargajualbrg,0,"",".").'" autocomplete="off" readonly>';
														} else {
															echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal11"  name="_member" value="'.number_format($hargajualbrg*$konvbrg,0,"",".").'" autocomplete="off" readonly>';
														}
													} else {
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal11"  name="_member" value="'.number_format($hj,0,"",".").'" autocomplete="off" readonly>';
													} 
												}else{
													if($hj == null){
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal11"  name="_member" value="'.number_format($hrg_jual5,0,"",".").'" autocomplete="off" readonly>';
													} else {
														echo '<input class="form-control"  style="text-align:right" type="text"  id="nominal11"  name="_member" value="'.number_format($hj,0,"",".").'" autocomplete="off" readonly>';
													} 
												} ?>
											</div>
											<button type="submit" name="cek-set-harga" class="btn btn-success">Simpan</button>
											<?php
											include "../connections/config.php";
											$cek_beli = str_replace(".","",@$_POST['_hargabeli']); $cek_jual = str_replace(".","",@$_POST['_hargajual']); $cek_konv = $konvbrg;
											$_hargajl = $hj; $_harga1 = @htmlspecialchars($_POST['_internal']); $_harga2 = @htmlspecialchars($_POST['_dist']);
											$_harga3 = @htmlspecialchars($_POST['_agen']); $_harga4 = @htmlspecialchars($_POST['_resell']); $_harga5 = @htmlspecialchars($_POST['_member']);
											
											$_nilai_jual =  @str_replace(".", "", $_hargajl);
											$_nilai_jual1 =  @str_replace(".", "", $_harga1);
											$_nilai_jual2 =  @str_replace(".", "", $_harga2);
											$_nilai_jual3 =  @str_replace(".", "", $_harga3);
											$_nilai_jual4 =  @str_replace(".", "", $_harga4);
											$_nilai_jual5 =  @str_replace(".", "", $_harga5);
											
											if(isset($_POST['cek-set-harga'])){
												if($dapat2 === 0){
													$query = @sqlsrv_query($dbconnect, "INSERT into HargaJualSatuan(KodeBarang,KodeSatuan,KonversiSatuan,HargaBeli,HargaJual,HargaEceranTertinggi,HargaJual1,HargaJual2,HargaJual3,HargaJual4,HargaJual5,Diskon,KodeCabang)
													values('$id','$satbrg','$cek_konv','$cek_beli','$cek_jual','$cek_jual','$_nilai_jual1','$_nilai_jual2','$_nilai_jual3','$_nilai_jual4','$_nilai_jual5','0','CAB-0001')") or die( print_r( sqlsrv_errors(), true)); 
												}else{
													$query = @sqlsrv_query($dbconnect, "update HargaJualSatuan set HargaBeli = '$cek_beli', HargaJual = '$_nilai_jual', HargaJual1 = '$_nilai_jual1', HargaJual2 = '$_nilai_jual2',
													HargaJual3 = '$_nilai_jual3', HargaJual4 = '$_nilai_jual4', HargaJual5 = '$_nilai_jual5' where KodeBarang = '".$id."' AND KodeSatuan = '".$satbrg."'") or die( print_r( sqlsrv_errors(), true)); 	 
													
												}
													if($query){
															/* @sqlsrv_query($dbconnect, "update HargaJualSatuan set HargaBeli = '$_hargabrg2', HargaJual1 = '$_harga12', HargaJual2 = '$_harga22', HargaJual3 = '$_harga32',
															HargaJual4 = '$_harga42', HargaJual5 = '$_harga52' where KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true)); */
															echo '<script type="text/javascript">
															sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
															function () { window.location.href = "mst-barang.php?id='.base64_encode($kodebrg).'&page='.base64_encode("set-harga").'"; });
															</script>';
														}
														else{
															echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
														}
											}
											@sqlsrv_close;
											
											}

											?>
									</form>
										
								</div>
							</div>	
						</div>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$file_path = "../img/foto-barang/".$fotobrg; $file_path2 = "../img/foto-barang/thumb_".$fotobrg;
							$cek = @sqlsrv_query($dbconnect, "select * from TrSalesOrderItem where KodeBarang = '".$kodebrg."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Data Barang Dipakai Transaksi ", type: "error" },
								function () { window.location.href = "mst-barang.php"; }); </script>';
							}else{
								$cek2 = @sqlsrv_query($dbconnect, "select * from MasterBarang where KodeBarang = '".$kodebrg."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$data2 = @sqlsrv_fetch_array($cek2);
								if($data2['IsAktif'] == TRUE){
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Non Aktifkan Data Barang Terlalu Dahulu", type: "error" },
									function () { window.location.href = "mst-barang.php"; }); </script>';
								}else{
									if(file_exists($file_path)){ unlink($file_path); }
									if(file_exists($file_path2)){ unlink($file_path2); }
									@sqlsrv_query($dbconnect, "DELETE From HargaJualSatuan WHERE KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true));
									@sqlsrv_query($dbconnect, "DELETE From MasterBarang WHERE KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true));
									echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
									function () { window.location.href = "mst-barang.php"; }); </script>';
								}
							}
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// nonaktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterBarang set IsAktif = 'False' WHERE KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-barang.php"; }); </script>';
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterBarang set IsAktif = 'True' WHERE KodeBarang = '".$kodebrg."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-barang.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<!-- membuat dropdown bertingkat klien-->
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		  $("#_kodedpr").change(function(){
			var id_departemen = $("#_kodedpr").val();
			$.ajax({
				url: "../get-data/get-departemen.php",
				data: "get_dept="+id_departemen,
				cache: false,
				success: function(msg){
					//jika data sukses diambil dari server kita tampilkan
					//di <select id=kota>
					$("#_kodesub").html(msg);
				}
			});
		  });
		  $("#_kodesub").change(function(){
			var id_subdepartemen = $("#_kodesub").val();
			$.ajax({
				url: "../get-data/get-subdepartemen.php",
				data: "get_sub="+id_subdepartemen,
				cache: false,
				success: function(msg){
					$("#_kodekat").html(msg);
				}
			});
		  });
		});
	</script>
	
	<script text="javascript">
			/* Tanpa Rupiah */
			var tanpa_rupiah = document.getElementById('nominal');
			var tanpa_rupiah2 = document.getElementById('nominal2');
			var tanpa_rupiah3 = document.getElementById('nominal3');
			var tanpa_rupiah4 = document.getElementById('nominal4');
			var tanpa_rupiah5 = document.getElementById('nominal5');
			var tanpa_rupiah6 = document.getElementById('nominal6');
			var tanpa_rupiah7 = document.getElementById('nominal7');
			var tanpa_rupiah8 = document.getElementById('nominal8');
			var tanpa_rupiah9 = document.getElementById('nominal9');
			var tanpa_rupiah10 = document.getElementById('nominal10');
			var tanpa_rupiah11 = document.getElementById('nominal11');
			
			tanpa_rupiah.addEventListener('keyup', function(e){ tanpa_rupiah.value = formatRupiah(this.value); });
			tanpa_rupiah.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah2.addEventListener('keyup', function(e){ tanpa_rupiah2.value = formatRupiah(this.value); });
			tanpa_rupiah2.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah3.addEventListener('keyup', function(e){ tanpa_rupiah3.value = formatRupiah(this.value); });
			tanpa_rupiah3.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah4.addEventListener('keyup', function(e){ tanpa_rupiah4.value = formatRupiah(this.value); });
			tanpa_rupiah4.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah5.addEventListener('keyup', function(e){ tanpa_rupiah5.value = formatRupiah(this.value); });
			tanpa_rupiah5.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah6.addEventListener('keyup', function(e){ tanpa_rupiah6.value = formatRupiah(this.value); });
			tanpa_rupiah6.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah7.addEventListener('keyup', function(e){ tanpa_rupiah7.value = formatRupiah(this.value); });
			tanpa_rupiah7.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah8.addEventListener('keyup', function(e){ tanpa_rupiah8.value = formatRupiah(this.value); });
			tanpa_rupiah8.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah9.addEventListener('keyup', function(e){ tanpa_rupiah9.value = formatRupiah(this.value); });
			tanpa_rupiah9.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah10.addEventListener('keyup', function(e){ tanpa_rupiah10.value = formatRupiah(this.value); });
			tanpa_rupiah10.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah11.addEventListener('keyup', function(e){ tanpa_rupiah11.value = formatRupiah(this.value); });
			tanpa_rupiah11.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			/* Fungsi */
			function formatRupiah(bilangan, prefix)
			{
				var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
					split	= number_string.split(','),
					sisa 	= split[0].length % 3,
					rupiah 	= split[0].substr(0, sisa),
					ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
					
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
			}
			
			function limitCharacter(event)
			{
				key = event.which || event.keyCode;
				if ( key != 188 // Comma
					 && key != 8 // Backspace
					 && key != 9 // Tab
					 && key != 116 // F5
					 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
					 && (key < 48 || key > 57) // Non digit
					 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
					) 
				{
					event.preventDefault();
					return false;
				}
			}
			</script>
	
	<!-- jQuery sticky menu -->
    <script src="../web/js/owl.carousel.min.js"></script>
    <script src="../web/js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="../web/js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="../web/js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="../web/js/bxslider.min.js"></script>
	<script type="text/javascript" src="../web/js/script.slider.js"></script>
	
	<!-- Slide -->
	<script src="../web/js/jquery.fancybox.pack.js"></script>
	<script src="../web/js/jquery.fancybox-media.js"></script>
	<script src="../web/js/google-code-prettify/prettify.js"></script>
	<script src="../web/js/portfolio/jquery.quicksand.js"></script>
	<script src="../web/js/portfolio/setting.js"></script>
	<script src="../web/js/jquery.flexslider.js"></script>
	<script src="../web/js/animate.js"></script>
	<script src="../web/js/custom.js"></script>

</body>

</html>