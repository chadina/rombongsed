<div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
							<div class="panel panel-default">
								<div class="panel-heading text-center">
									<span class="text-center">Login Sebagai <strong><?php echo strtoupper($username_aktif); ?></strong></span>
								</div>
								<!-- /.panel-heading -->
								<div class="panel-body text-center">
									<?php if($foto_aktif === null OR $foto_aktif === '' OR $foto_aktif === '-'){
										echo '<img class="img-circle" src="../img/foto-admin/ADMIN.PNG" width="60%" />';
									} else {
										echo '<img class="img-circle" src="../img/foto-admin/'.$foto_aktif.'" width="60%" />';
									} ?>
								</div>
								<div class="panel-footer text-center">
									<p><strong><?php echo strtoupper($nama_aktif); ?></strong><br><?php echo TanggalIndo(date('Y-m-d')); ?></p>
								</div>
							</div>
                        </li>
						<li><a href="default.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
						<li><a href="#"><i class="fa fa-archive fa-fw"></i> Master Data<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="mst-bank.php"><i class="fa fa-list fa-fw"></i> Master Bank</a></li>
                                <li><a href="mst-bank-penerima.php"><i class="fa fa-dollar fa-fw"></i> Master Bank Perusahaan</a></li>
								 <li><a href="mst-satuan.php"><i class="fa fa-star fa-fw"></i> Master Jenis Qurban</a></li>
                               
                            </ul>
						</li>
						<li><a href="#"><i class="fa fa-user fa-fw"></i> Data User<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
                                <li><a href="mst-user.php"><i class="fa fa-flag fa-fw"></i> Administrator</a></li>
                                <li><a href="mst-nasabah.php"><i class="fa fa-users fa-fw"></i> Data Nasabah</a></li>
								
                            </ul>
						</li>
						<li><a href="#"><i class="fa fa-check fa-fw"></i> Transaksi<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
                                <li><a href="verifikasi-dompet.php"><i class="fa fa-money fa-fw"></i> Verifikasi Tabungan</a></li>
                                <li><a href="verifikasi-investasi.php"><i class="fa fa-home fa-fw"></i> Pencairan Tabungan</a></li>
                                
                            </ul>
						</li>
						<li><a href="#"><i class="fa fa-file-o fa-fw"></i> Laporan<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="lap-verif-tabungan.php"><i class="fa fa-signal fa-fw"></i> Laporan Verifikasi Tabungan</a></li>
								<li><a href="lap-pencairan.php"><i class="fa fa-users fa-fw"></i> Laporan Pencairan Tabungan</a></li>
								<li><a href="lap-tabungan.php"><i class="fa fa-file fa-fw"></i> Laporan Tabungan</a></li> 
                                
                            </ul>
						</li> 
						<li><a href="#"><i class="fa fa-pencil fa-fw"></i> Konten Website<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="mst-terms.php"><i class="fa fa-list fa-fw"></i> Program</a></li> 
								<li><a href="mst-slider.php"><i class="fa fa-image fa-fw"></i> Set Slider</a></li>
                                <li><a href="mst-post.php"><i class="fa fa-newspaper-o fa-fw"></i> Posting Kegiatan</a></li>
                                <li><a href="mst-galeri.php"><i class="fa fa-image fa-fw"></i> Galeri Foto</a></li>
                            
                                <li><a href="mst-bukutamu.php"><i class="fa fa-book fa-fw"></i> Buku Tamu</a></li>
                                <li><a href="mst-question.php"><i class="fa fa-question fa-fw"></i> FAQ</a></li>
                            </ul>
						</li>
						<li><a href="#"><i class="fa fa-gear fa-fw"></i> Setting<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
                                <!-- <li><a href="#"><i class="fa fa-wrench fa-fw"></i> Pengaturan Dasar</a></li> -->
								<li><a href="mst-setting.php"><i class="fa fa-wrench fa-fw"></i> Pengaturan Page</a></li>
								<li><a href="edit-user.php?id=<?php echo base64_encode($id_aktif); ?>"><i class="fa fa-edit fa-fw"></i> Edit Profil</a></li>
                                <li><a href="edit-password.php?id=<?php echo base64_encode($id_aktif); ?>"><i class="fa fa-key fa-fw"></i> Ubah Password</a></li>
                            </ul>
						</li>
						<!-- <li><a href="#"><i class="fa fa-file-o fa-fw"></i> Laporan</a></li>
						<li><a href="#"><i class="fa fa-gear fa-fw"></i> Setting<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
                                <li><a href="#"><i class="fa fa-wrench fa-fw"></i> Pengaturan Dasar</a></li>
                                <li><a href="#"><i class="fa fa-list fa-fw"></i> Fitur Menu</a></li>
                                <li><a href="#"><i class="fa fa-key fa-fw"></i> Akses User</a></li>
                            </ul>
						</li> -->
						<li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Keluar</a></li>
						
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->