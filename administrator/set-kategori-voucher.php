<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 26;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
$type = isset($_GET['type']) ? base64_decode($_GET['type']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from KategoriVoucher where KodeVoucher = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodev = $cari['KodeVoucher']; $katv = $cari['KodeDepartemen']; $nominalv = $cari['NominalVoucher']; $statsv = $cari['StatusPerson']; $aktifv = $cari['IsAktif'];
}

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '13'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value_res = $cari['value']; }

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '14'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value_agn = $cari['value']; }

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '15'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value_dst = $cari['value']; }

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '16'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value16 = $cari['value']; }

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '17'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value17 = $cari['value']; }

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '18'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value18 = $cari['value']; }

@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- Slide Galeri -->
	<link href="../web/css/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="../web/css/jcarousel.css" rel="stylesheet" />
	<link href="../web/css/flexslider.css" rel="stylesheet" />
	<link href="../web/css/style.css" rel="stylesheet" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php if($id == null AND $type == null){ 
						echo "<h1 class='page-header'>Kategori Voucher Distributor&nbsp;";
						if($page === "tambah"){
							echo "<a href='set-kategori-voucher.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						} else {
							echo "<a href='set-kategori-voucher.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						}
						echo "</h1>";
					} elseif($id == null AND $type === 'Agen'){ 
						echo "<h1 class='page-header'>Kategori Voucher Agen&nbsp;";
						if($page === "tambah"){
							echo "<a href='set-kategori-voucher.php?type=".htmlspecialchars(base64_encode('Agen'))."' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						} else {
							echo "<a href='set-kategori-voucher.php?type=".htmlspecialchars(base64_encode('Agen'))."&page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						}
						echo "</h1>";
					} elseif($id == null AND $type === 'Reseller'){ 
						echo "<h1 class='page-header'>Kategori Voucher Reseller&nbsp;";
						if($page === "tambah"){
							echo "<a href='set-kategori-voucher.php?type=".htmlspecialchars(base64_encode('Reseller'))."' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						} else {
							echo "<a href='set-kategori-voucher.php?type=".htmlspecialchars(base64_encode('Reseller'))."&page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						}
						echo "</h1>";
					} elseif($id != null AND $type == null AND $page === "edit"){
						echo "<h1 class='page-header'>Kategori Voucher Distributor&nbsp;";
						echo "<a href='set-kategori-voucher.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						
					} elseif($id != null AND $type === "Agen" AND $page === "edit"){
						echo "<h1 class='page-header'>Kategori Voucher Agen&nbsp;";
						echo "<a href='set-kategori-voucher.php?type=".htmlspecialchars(base64_encode('Agen'))."' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						
					} elseif($id != null AND $type === "Reseller" AND $page === "edit"){
						echo "<h1 class='page-header'>Kategori Voucher Reseller&nbsp;";
						echo "<a href='set-kategori-voucher.php?type=".htmlspecialchars(base64_encode('Reseller'))."' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						
					} /* elseif($id == null AND ($type == null OR $page === "tambah")){
						echo "<h1 class='page-header'>Kategori Voucher Distributor&nbsp;";
						echo "<a href='set-kategori-voucher.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					} elseif($id !== null AND ($page == "edit" OR $page == "detail" OR $page == "set_gambar" OR $page == "set-harga")){ 
						echo "<a href='set-barang-voucher.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					} */
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
						<?php if($page === 'nonaktif' OR $page === 'aktif' OR $page === 'delete'){ echo ''; } else { ?>
						<ul class="nav nav-tabs">
							<li <?php if($type == null){ echo "class='active'"; } else { echo ""; } ?>><a href="set-kategori-voucher.php">Distributor</a></li>
							<li <?php if($type === 'Agen'){ echo "class='active'"; } else { echo ""; } ?>><a href="set-kategori-voucher.php?type=<?php echo htmlspecialchars(base64_encode('Agen')); ?>">Agen</a></li>
							<li <?php if($type === 'Reseller'){ echo "class='active'"; } else { echo ""; } ?>><a href="set-kategori-voucher.php?type=<?php echo htmlspecialchars(base64_encode('Reseller')); ?>">Reseller</a></li>
							
						</ul><br>
						<?php } ?>
						
						<?php if($id == null AND $type == null){ ?>
						<div class="panel panel-default">
							<?php if($page === 'tambah'){ ?>
							<div class="panel-heading">
								<i class="fa fa-plus fa-fw"></i> Tambah Data
							</div>
							<div class='panel-body'>
								<div class="row">
									<form role="form" method="post">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Kategori Voucher</label>
												<input class="form-control" type="hidden" name="_status" placeholder="0" value="DISTRIBUTOR" autocomplete="off" readonly>
												<select class="form-control" name="_kodedpr" autocomplete="off" required>
													<?php echo '<option value="">-- Pilih Kategori --</option>';
														$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterDepartemen where KodeDepartemen != 'DPT-0000001' AND IsAktif = 'TRUE' AND Keterangan = 'PRODUK' AND KodeDepartemen NOT IN (SELECT KodeDepartemen FROM KategoriVoucher where StatusPerson = 'DISTRIBUTOR') ORDER BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
														while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){	
															echo "<option value=\"".$daftar['KodeDepartemen']."\" >".ucwords($daftar['NamaDepartemen'])."</option>\n";
														}
													?>
												</select>
											</div>
											<div class="form-group">
												<label>Nilai Voucher</label>
												<input class="form-control" style="text-align:right" type="text" id="nominal"  name="_voucher" placeholder="0" autocomplete="off" required>
											</div>
											<button type="submit" class="btn btn-default" name="_submit-input-kat">Submit</button>
											<button type="reset" class="btn btn-default" name="_submit-input-kat">Reset</button>
										</div>
									</form>
									
											<?php
											include "../connections/config.php";
											$_stats = @htmlspecialchars($_POST['_status']); $_kat = @htmlspecialchars($_POST['_kodedpr']);
											$_hargaet = @htmlspecialchars($_POST['_voucher']); $_nilai_et =  @str_replace(".", "", $_hargaet);
											if(isset($_POST['_submit-input-kat'])){
												$query_cari = @sqlsrv_query($dbconnect, "select KodeDepartemen from KategoriVoucher where StatusPerson = '".$_stats."' AND KodeDepartemen = '".$_kat."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$nums_cari = @sqlsrv_num_rows($query_cari);
												if($nums_cari <> 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kategori Voucher Ini Sudah Ada ", "error"); </script>';
												} else {
													if($_stats === 'DISTRIBUTOR'){ $value_reg = $value_dst; }
													elseif($_stats === 'AGEN'){ $value_reg = $value_agn; }
													elseif($_stats === 'RESELLER'){ $value_reg = $value_res; }
													
													$query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Totalbayar from KategoriVoucher where StatusPerson = '".$_stats."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
													if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
													}
													
													if($total_bayar+$_nilai_et > $value_reg){
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Jumlah Nominal Voucher Melebihi Pagu ", "error"); </script>';
													}else{
														// membuat id otomatis
														$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeVoucher,5)) AS kode FROM KategoriVoucher WHERE LEFT(replace(KodeVoucher,' ',''),9)='KV-".date('Ym')."'") or die( print_r( sqlsrv_errors(), true)); 
														$nums = @sqlsrv_num_rows($sql); 
														while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
															if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
														}
														$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
														$kode_jadi = "KV-".date('Ym')."-".$bikin_kode;
														
														$query = @sqlsrv_query($dbconnect, "insert into KategoriVoucher(KodeVoucher,KodeDepartemen,NominalVoucher,IsAktif,StatusPerson)
														values('$kode_jadi','$_kat','$_nilai_et','1','$_stats')") or die( print_r( sqlsrv_errors(), true)); 	 
															if($query){
																echo '<script type="text/javascript">
																sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
																function () { window.location.href = "set-kategori-voucher.php"; });
																</script>';
															}
															else{
																echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
															}
													}
												}
											}
											@sqlsrv_close;

											?>
									   
								</div>
							</div>
							<!-- /.col-lg-12 -->
							<?php } else { ?>
							<div class="panel-heading">
								<i class="fa fa-list fa-fw"></i> List Data
							</div>
							<!-- /.panel-heading -->
							
							<div class="panel-body table-responsive">
								<table width="100%" class="table table-hover">
									<thead>
										<tr>
											<th width="10%">No</th>
											<th width="50%">Nama Kategori</th>
											<th width="15%">Nominal</th>
											<th width="10%">Status</th>
											<th width="15%">Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php include "../connections/config.php";
									$query = @sqlsrv_query($dbconnect, "select MasterDepartemen.NamaDepartemen, KategoriVoucher.* from KategoriVoucher inner join MasterDepartemen on MasterDepartemen.KodeDepartemen = KategoriVoucher.KodeDepartemen
									where KategoriVoucher.StatusPerson = 'DISTRIBUTOR' order by MasterDepartemen.NamaDepartemen", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
									$no = 1;
									while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
									?>
										<tr class="odd gradeX">
											<td><?php echo $no; ?></td>
											<td><?php echo "<strong>".strtoupper($cari['NamaDepartemen'])."</strong>"; ?></td>
											<td><?php echo "Rp. ".number_format($cari['NominalVoucher']); ?></td>
											<td><?php if($cari['IsAktif'] == FALSE){ echo '<a href="set-kategori-voucher.php?page='.htmlspecialchars(base64_encode("aktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeVoucher"])).'" class="btn btn-default btn-sm">Non Aktif</a>'; } 
											else { echo "<a href='set-kategori-voucher.php?page=".htmlspecialchars(base64_encode('nonaktif'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher']))."' class='btn btn-primary btn-sm'>Aktif</a>"; } ?></td>
											<td>
												<a href="set-kategori-voucher.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher'])); ?>" class="btn btn-warning btn-sm">Edit</a>
												<a href="set-kategori-voucher.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher'])); ?>" class="btn btn-danger btn-sm confirm-delete">Delete</a>
											</td>
										</tr>
									<?php $no++; } 
									@sqlsrv_close(); ?> 
									</tbody>
									<tfoot>
										<tr class="odd gradeX">
											<td colspan="5"></td>
										</tr>
										<tr class="odd gradeX">
											<td align="center" colspan="2"><strong>Total Voucher</strong></td>
											<?php $query2 = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Total from KategoriVoucher where IsAktif = 'True' AND StatusPerson = 'DISTRIBUTOR'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ 
												echo '<td colspan="3"><strong>Rp. '.number_format($cari2['Total']).'</strong></td>';
											} 
											?>
										</tr>
									</tfoot>
								</table>
							</div>
							<!-- /.panel-body -->
							<?php } ?>
							
						</div>
						<!-- /.panel -->
						
						<!-- /.panel-body -->
						
						<?php } elseif($id == null AND $type === 'Agen'){ ?>
						<div class="panel panel-default">
							<?php if($page === 'tambah'){ ?>
							<div class="panel-heading">
								<i class="fa fa-plus fa-fw"></i> Tambah Data
							</div>
							<div class='panel-body'>
								<div class="row">
									<form role="form" method="post">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Kategori Voucher</label>
												<input class="form-control" type="hidden" name="_status" placeholder="0" value="AGEN" autocomplete="off" readonly>
												<select class="form-control" name="_kodedpr" autocomplete="off" required>
													<?php echo '<option value="">-- Pilih Kategori --</option>';
														$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterDepartemen where KodeDepartemen != 'DPT-0000001' AND IsAktif = 'TRUE' AND Keterangan = 'PRODUK' AND KodeDepartemen NOT IN (SELECT KodeDepartemen FROM KategoriVoucher where StatusPerson = 'AGEN') ORDER BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
														while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){	
															echo "<option value=\"".$daftar['KodeDepartemen']."\" >".ucwords($daftar['NamaDepartemen'])."</option>\n";
														}
													?>
												</select>
											</div>
											<div class="form-group">
												<label>Nilai Voucher</label>
												<input class="form-control" style="text-align:right" type="text" id="nominal"  name="_voucher" placeholder="0" autocomplete="off" required>
											</div>
											<button type="submit" class="btn btn-default" name="_submit-input-kat">Submit</button>
											<button type="reset" class="btn btn-default" name="_submit-input-kat">Reset</button>
										</div>
									</form>
									
											<?php
											include "../connections/config.php";
											$_stats = @htmlspecialchars($_POST['_status']); $_kat = @htmlspecialchars($_POST['_kodedpr']);
											$_hargaet = @htmlspecialchars($_POST['_voucher']); $_nilai_et =  @str_replace(".", "", $_hargaet);
											if(isset($_POST['_submit-input-kat'])){
												$query_cari = @sqlsrv_query($dbconnect, "select KodeDepartemen from KategoriVoucher where StatusPerson = '".$_stats."' AND KodeDepartemen = '".$_kat."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$nums_cari = @sqlsrv_num_rows($query_cari);
												if($nums_cari <> 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kategori Voucher Ini Sudah Ada ", "error"); </script>';
												} else {
													if($_stats === 'DISTRIBUTOR'){ $value_reg = $value_dst; }
													elseif($_stats === 'AGEN'){ $value_reg = $value_agn; }
													elseif($_stats === 'RESELLER'){ $value_reg = $value_res; }
													
													$query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Totalbayar from KategoriVoucher where StatusPerson = '".$_stats."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
													if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
													}
													
													if($total_bayar+$_nilai_et > $value_reg){
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Jumlah Nominal Voucher Melebihi Pagu ", "error"); </script>';
													}else{
														// membuat id otomatis
														$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeVoucher,5)) AS kode FROM KategoriVoucher WHERE LEFT(replace(KodeVoucher,' ',''),9)='KV-".date('Ym')."'") or die( print_r( sqlsrv_errors(), true)); 
														$nums = @sqlsrv_num_rows($sql); 
														while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
															if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
														}
														$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
														$kode_jadi = "KV-".date('Ym')."-".$bikin_kode;
														
														$query = @sqlsrv_query($dbconnect, "insert into KategoriVoucher(KodeVoucher,KodeDepartemen,NominalVoucher,IsAktif,StatusPerson)
														values('$kode_jadi','$_kat','$_nilai_et','1','$_stats')") or die( print_r( sqlsrv_errors(), true)); 	 
															if($query){
																echo '<script type="text/javascript">
																sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
																function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; });
																</script>';
															}
															else{
																echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
															}
													}
												}
											}
											@sqlsrv_close;

											?>
									   
								</div>
							</div>
							<!-- /.col-lg-12 -->
							<?php } else { ?>
							<div class="panel-heading">
								<i class="fa fa-list fa-fw"></i> List Data
							</div>
							<!-- /.panel-heading -->
							
							<div class="panel-body table-responsive">
								<table width="100%" class="table table-hover">
									<thead>
										<tr>
											<th width="10%">No</th>
											<th width="50%">Nama Kategori</th>
											<th width="15%">Nominal</th>
											<th width="10%">Status</th>
											<th width="15%">Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php include "../connections/config.php";
									$query = @sqlsrv_query($dbconnect, "select MasterDepartemen.NamaDepartemen, KategoriVoucher.* from KategoriVoucher inner join MasterDepartemen on MasterDepartemen.KodeDepartemen = KategoriVoucher.KodeDepartemen
									where KategoriVoucher.StatusPerson = 'AGEN' order by MasterDepartemen.NamaDepartemen", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
									$no = 1;
									while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
									?>
										<tr class="odd gradeX">
											<td><?php echo $no; ?></td>
											<td><?php echo "<strong>".strtoupper($cari['NamaDepartemen'])."</strong>"; ?></td>
											<td><?php echo "Rp. ".number_format($cari['NominalVoucher']); ?></td>
											<td><?php if($cari['IsAktif'] == FALSE){ echo '<a href="set-kategori-voucher.php?page='.htmlspecialchars(base64_encode("aktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeVoucher"])).'&type='.htmlspecialchars(base64_encode("Agen")).'" class="btn btn-default btn-sm">Non Aktif</a>'; } 
											else { echo "<a href='set-kategori-voucher.php?page=".htmlspecialchars(base64_encode('nonaktif'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher']))."&type=".htmlspecialchars(base64_encode("Agen"))."' class='btn btn-primary btn-sm'>Aktif</a>"; } ?></td>
											<td>
												<a href="set-kategori-voucher.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher']))."&type=".htmlspecialchars(base64_encode('Agen')); ?>" class="btn btn-warning btn-sm">Edit</a>
												<a href="set-kategori-voucher.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher']))."&type=".htmlspecialchars(base64_encode('Agen')); ?>" class="btn btn-danger btn-sm confirm-delete">Delete</a>
											</td>
										</tr>
									<?php $no++; } 
									@sqlsrv_close(); ?> 
									</tbody>
									<tfoot>
										<tr class="odd gradeX">
											<td colspan="5"></td>
										</tr>
										<tr class="odd gradeX">
											<td align="center" colspan="2"><strong>Total Voucher</strong></td>
											<?php $query2 = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Total from KategoriVoucher where IsAktif = 'True' AND StatusPerson = 'AGEN'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ 
												echo '<td colspan="3"><strong>Rp. '.number_format($cari2['Total']).'</strong></td>';
											} 
											?>
										</tr>
									</tfoot>
								</table>
							</div>
							<!-- /.panel-body -->
							<?php } ?>
						</div>
						<!-- /.panel -->
						
						<!-- /.panel-body -->
						
						<?php } elseif($id == null AND $type === 'Reseller'){ ?>
						<div class="panel panel-default">
							<?php if($page === 'tambah'){ ?>
							<div class="panel-heading">
								<i class="fa fa-plus fa-fw"></i> Tambah Data
							</div>
							<div class='panel-body'>
								<div class="row">
									<form role="form" method="post">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Kategori Voucher</label>
												<input class="form-control" type="hidden" name="_status" placeholder="0" value="RESELLER" autocomplete="off" readonly>
												<select class="form-control" name="_kodedpr" autocomplete="off" required>
													<?php echo '<option value="">-- Pilih Kategori --</option>';
														$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterDepartemen where KodeDepartemen != 'DPT-0000001' AND IsAktif = 'TRUE' AND Keterangan = 'PRODUK' AND KodeDepartemen NOT IN (SELECT KodeDepartemen FROM KategoriVoucher where StatusPerson = 'RESELLER') ORDER BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
														while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){	
															echo "<option value=\"".$daftar['KodeDepartemen']."\" >".ucwords($daftar['NamaDepartemen'])."</option>\n";
														}
													?>
												</select>
											</div>
											<div class="form-group">
												<label>Nilai Voucher</label>
												<input class="form-control" style="text-align:right" type="text" id="nominal"  name="_voucher" placeholder="0" autocomplete="off" required>
											</div>
											<button type="submit" class="btn btn-default" name="_submit-input-kat">Submit</button>
											<button type="reset" class="btn btn-default" name="_submit-input-kat">Reset</button>
										</div>
									</form>
									
											<?php
											include "../connections/config.php";
											$_stats = @htmlspecialchars($_POST['_status']); $_kat = @htmlspecialchars($_POST['_kodedpr']);
											$_hargaet = @htmlspecialchars($_POST['_voucher']); $_nilai_et =  @str_replace(".", "", $_hargaet);
											if(isset($_POST['_submit-input-kat'])){
												$query_cari = @sqlsrv_query($dbconnect, "select KodeDepartemen from KategoriVoucher where StatusPerson = '".$_stats."' AND KodeDepartemen = '".$_kat."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$nums_cari = @sqlsrv_num_rows($query_cari);
												if($nums_cari <> 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kategori Voucher Ini Sudah Ada ", "error"); </script>';
												} else {
													if($_stats === 'DISTRIBUTOR'){ $value_reg = $value_dst; }
													elseif($_stats === 'AGEN'){ $value_reg = $value_agn; }
													elseif($_stats === 'RESELLER'){ $value_reg = $value_res; }
													
													$query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Totalbayar from KategoriVoucher where StatusPerson = '".$_stats."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
													if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
													}
													
													if($total_bayar+$_nilai_et > $value_reg){
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Jumlah Nominal Voucher Melebihi Pagu ", "error"); </script>';
													}else{
														// membuat id otomatis
														$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeVoucher,5)) AS kode FROM KategoriVoucher WHERE LEFT(replace(KodeVoucher,' ',''),9)='KV-".date('Ym')."'") or die( print_r( sqlsrv_errors(), true)); 
														$nums = @sqlsrv_num_rows($sql); 
														while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
															if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
														}
														$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
														$kode_jadi = "KV-".date('Ym')."-".$bikin_kode;
														
														$query = @sqlsrv_query($dbconnect, "insert into KategoriVoucher(KodeVoucher,KodeDepartemen,NominalVoucher,IsAktif,StatusPerson)
														values('$kode_jadi','$_kat','$_nilai_et','1','$_stats')") or die( print_r( sqlsrv_errors(), true)); 	 
															if($query){
																echo '<script type="text/javascript">
																sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
																function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; });
																</script>';
															}
															else{
																echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
															}
													}
												}
											}
											@sqlsrv_close;

											?>
									   
								</div>
							</div>
							<!-- /.col-lg-12 -->
							<?php } else { ?>
							<div class="panel-heading">
								<i class="fa fa-list fa-fw"></i> List Data
							</div>
							<!-- /.panel-heading -->
							
							<div class="panel-body table-responsive">
								<table width="100%" class="table table-hover">
									<thead>
										<tr>
											<th width="10%">No</th>
											<th width="50%">Nama Kategori</th>
											<th width="15%">Nominal</th>
											<th width="10%">Status</th>
											<th width="15%">Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php include "../connections/config.php";
									$query = @sqlsrv_query($dbconnect, "select MasterDepartemen.NamaDepartemen, KategoriVoucher.* from KategoriVoucher inner join MasterDepartemen on MasterDepartemen.KodeDepartemen = KategoriVoucher.KodeDepartemen
									where KategoriVoucher.StatusPerson = 'RESELLER' order by MasterDepartemen.NamaDepartemen", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
									$no = 1;
									while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
									?>
										<tr class="odd gradeX">
											<td><?php echo $no; ?></td>
											<td><?php echo "<strong>".strtoupper($cari['NamaDepartemen'])."</strong>"; ?></td>
											<td><?php echo "Rp. ".number_format($cari['NominalVoucher']); ?></td>
											<td><?php if($cari['IsAktif'] == FALSE){ echo '<a href="set-kategori-voucher.php?page='.htmlspecialchars(base64_encode("aktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeVoucher"])).'&type='.htmlspecialchars(base64_encode("Reseller")).'" class="btn btn-default btn-sm">Non Aktif</a>'; } 
											else { echo "<a href='set-kategori-voucher.php?page=".htmlspecialchars(base64_encode('nonaktif'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher']))."&type=".htmlspecialchars(base64_encode('Reseller'))."' class='btn btn-primary btn-sm'>Aktif</a>"; } ?></td>
											<td>
												<a href="set-kategori-voucher.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher']))."&type=".htmlspecialchars(base64_encode('Reseller')); ?>" class="btn btn-warning btn-sm">Edit</a>
												<a href="set-kategori-voucher.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeVoucher']))."&type=".htmlspecialchars(base64_encode('Reseller')); ?>" class="btn btn-danger btn-sm confirm-delete">Delete</a>
											</td>
										</tr>
									<?php $no++; } 
									@sqlsrv_close(); ?> 
									</tbody>
									<tfoot>
										<tr class="odd gradeX">
											<td colspan="5"></td>
										</tr>
										<tr class="odd gradeX">
											<td align="center" colspan="2"><strong>Total Voucher</strong></td>
											<?php $query2 = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Total from KategoriVoucher where IsAktif = 'True' AND StatusPerson = 'RESELLER'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ 
												echo '<td colspan="3"><strong>Rp. '.number_format($cari2['Total']).'</strong></td>';
											} 
											?>
										</tr>
									</tfoot>
								</table>
							</div>
							<!-- /.panel-body -->
							<?php } ?>
							
						</div>
						<!-- /.panel -->
						
						<!-- /.panel-body -->
						
						<?php /* } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel panel-default">
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Status User</label>
                                            <select class="form-control" name="_status" autocomplete="off" required>
                                                <option value="">-- Pilih Status User --</option>
                                                <option value="DISTRIBUTOR">Distributor</option>
                                                <option value="AGEN">Agen</option>
                                                <option value="RESELLER">Reseller</option>
											</select>
                                        </div>
										<div class="form-group">
                                            <label>Kategori Voucher</label>
                                            <select class="form-control" name="_kodedpr" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Kategori --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterDepartemen where KodeDepartemen != 'DPT-0000001' AND IsAktif = 'TRUE' AND Keterangan = 'PRODUK' ORDER BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){	
														echo "<option value=\"".$daftar['KodeDepartemen']."\" >".ucwords($daftar['NamaDepartemen'])."</option>\n";
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Nilai Voucher</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal"  name="_voucher" placeholder="0" autocomplete="off" required>
                                        </div>
									    <button type="submit" class="btn btn-default" name="_submit-input-kat">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-kat">Reset</button>
									</div>
								</form>
								
										<?php
										include "../connections/config.php";
										$_stats = @htmlspecialchars($_POST['_status']); $_kat = @htmlspecialchars($_POST['_kodedpr']);
										$_hargaet = @htmlspecialchars($_POST['_voucher']); $_nilai_et =  @str_replace(".", "", $_hargaet);
										if(isset($_POST['_submit-input-kat'])){
											$query_cari = @sqlsrv_query($dbconnect, "select KodeDepartemen from KategoriVoucher where StatusPerson = '".$_stats."' AND KodeDepartemen = '".$_kat."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$nums_cari = @sqlsrv_num_rows($query_cari);
											if($nums_cari <> 0){
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Kategori Voucher Ini Sudah Ada ", "error"); </script>';
											} else {
												while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
												if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
												}
												if($_stats === 'DISTRIBUTOR'){ $value_reg = $value_dst; }
												elseif($_stats === 'AGEN'){ $value_reg = $value_agn; }
												elseif($_stats === 'RESELLER'){ $value_reg = $value_res; }
												
												$query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Totalbayar from KategoriVoucher where StatusPerson = '".$_stats."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
												if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
												}
												
												if($total_bayar+$_nilai_et > $value_reg){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Jumlah Nominal Voucher Melebihi Pagu ", "error"); </script>';
												}else{
													// membuat id otomatis
													$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeVoucher,5)) AS kode FROM KategoriVoucher WHERE LEFT(replace(KodeVoucher,' ',''),9)='KV-".date('Ym')."'") or die( print_r( sqlsrv_errors(), true)); 
													$nums = @sqlsrv_num_rows($sql); 
													while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
														if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
													}
													$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
													$kode_jadi = "KV-".date('Ym')."-".$bikin_kode;
													
													$query = @sqlsrv_query($dbconnect, "insert into KategoriVoucher(KodeVoucher,KodeDepartemen,NominalVoucher,IsAktif,StatusPerson)
													values('$kode_jadi','$_kat','$_nilai_et','1','$_stats')") or die( print_r( sqlsrv_errors(), true)); 	 
														if($query){
															echo '<script type="text/javascript">
															sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
															function () { window.location.href = "set-kategori-voucher.php"; });
															</script>';
														}
														else{
															echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
														}
												}
											}
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
						<!-- /.col-lg-12 -->
						</div>
						<!-- /.panel-heading -->
						
						<?php */ } elseif($id !== null AND $page == 'edit' AND ($type == null OR $type === 'Agen' OR $type === 'Reseller')){ ?>
						<div class="panel panel-default">
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Status Person</label>
                                            <input class="form-control" value="<?php echo $statsv; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Kategori Voucher</label>
                                            <select class="form-control" name="_kodedpr2" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Departemen --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterDepartemen where KodeDepartemen != 'DPT-0000001' AND IsAktif = 'TRUE' AND Keterangan = 'PRODUK' ORDER BY KodeDepartemen") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
														if($katv !== null){
															if($daftar['KodeDepartemen'] === $katv){
																echo "<option value=\"".$daftar['KodeDepartemen']."\" selected='selected'>".ucwords($daftar['NamaDepartemen'])."</option>\n";
															}else{
																echo "<option value=\"".$daftar['KodeDepartemen']."\">".ucwords($daftar['NamaDepartemen'])."</option>\n";
															}
														}else{
															echo "<option value=\"".$daftar['KodeDepartemen']."\">".ucwords($daftar['NamaDepartemen'])."</option>\n";
														}
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Nilai Voucher</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal"  name="_voucher2" value="<?php echo number_format($nominalv,0,"","."); ?>"  autocomplete="off" required>
                                        </div>
									    <button type="submit" class="btn btn-default" name="_submit-edit-kat">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-kat">Reset</button>
									</div>
								</form>
								
										<?php
										include "../connections/config.php";
										$_kat2 = @htmlspecialchars($_POST['_kodedpr2']);
										$_hargaet2 = @htmlspecialchars($_POST['_voucher2']); $_nilai_et2 =  @str_replace(".", "", $_hargaet2);
										if(isset($_POST['_submit-edit-kat'])){
											if($katv === $_kat2){
												if($statsv === 'DISTRIBUTOR'){ $value_reg = $value_dst; }
												elseif($statsv === 'AGEN'){ $value_reg = $value_agn; }
												elseif($statsv === 'RESELLER'){ $value_reg = $value_res; }
													
												$query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Totalbayar from KategoriVoucher where StatusPerson = '".$statsv."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
												if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
												}
												
												if(($total_bayar-$nominalv+$_nilai_et2) > $value_reg){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Jumlah Nominal Voucher Melebihi Pagu ", "error"); </script>';
												}else{
													$query = @sqlsrv_query($dbconnect, "update KategoriVoucher set KodeDepartemen = '$_kat2', NominalVoucher = '$_nilai_et2' where KodeVoucher = '".$id."'") or die( print_r( sqlsrv_errors(), true)); 	 
														if($query){
															if($type == null){
																echo '<script type="text/javascript">
																sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
																function () { window.location.href = "set-kategori-voucher.php"; });
																</script>';
															} elseif($type === 'Agen') {
																echo '<script type="text/javascript">
																sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
																function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; });
																</script>';
															} elseif($type === 'Reseller') {
																echo '<script type="text/javascript">
																sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
																function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; });
																</script>';
															}
														}
														else{
															echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
														}
												}
											} else {
												$query_cari = @sqlsrv_query($dbconnect, "select * from KategoriVoucher where StatusPerson = '".$statsv."' AND KodeDepartemen = '".$_kat2."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$nums_cari = @sqlsrv_num_rows($query_cari);
												if($nums_cari <> 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kategori Voucher Ini Sudah Ada ", "error"); </script>';
												} else {
													if($statsv === 'DISTRIBUTOR'){ $value_reg = $value_dst; }
													elseif($statsv === 'AGEN'){ $value_reg = $value_agn; }
													elseif($statsv === 'RESELLER'){ $value_reg = $value_res; }
													
													$query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalVoucher) as Totalbayar from KategoriVoucher where StatusPerson = '".$statsv."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
													if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
													}
													
													if(($total_bayar-$nominalv+$_nilai_et2) > $value_reg){
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Jumlah Nominal Voucher Melebihi Pagu ", "error"); </script>';
													}else{
														$query = @sqlsrv_query($dbconnect, "update KategoriVoucher set KodeDepartemen = '$_kat2', NominalVoucher = '$_nilai_et2' where KodeVoucher = '".$id."'") or die( print_r( sqlsrv_errors(), true)); 	 
															if($query){
																if($type == null){
																echo '<script type="text/javascript">
																	sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
																	function () { window.location.href = "set-kategori-voucher.php"; });
																	</script>';
																} elseif($type === 'Agen') {
																	echo '<script type="text/javascript">
																	sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
																	function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; });
																	</script>';
																} elseif($type === 'Reseller') {
																	echo '<script type="text/javascript">
																	sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
																	function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; });
																	</script>';
																}
															}
															else{
																echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
															}
													}
												}
											}
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
						<!-- /.col-lg-12 -->
						</div>
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$cek = @sqlsrv_query($dbconnect, "select * from trvoucher where KodeVoucher = '".$id."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num <> 0){
								if($type == null){
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Kategori Voucher Dipakai Transaksi ", type: "error" },
									function () { window.location.href = "set-kategori-voucher.php"; }); </script>';
								} elseif($type === 'Agen'){ 
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Kategori Voucher Dipakai Transaksi ", type: "error" },
									function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; }); </script>';
								} elseif($type === 'Reseller'){ 
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Kategori Voucher Dipakai Transaksi ", type: "error" },
									function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; }); </script>';
								}
							}else{
								$cek2 = @sqlsrv_query($dbconnect, "select * from KategoriVoucher where KodeVoucher = '".$id."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$data2 = @sqlsrv_fetch_array($cek2);
								if($data2['IsAktif'] == TRUE){
									if($type == null){
										echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Non Aktifkan Kategori Voucher Terlalu Dahulu", type: "error" },
										function () { window.location.href = "set-kategori-voucher.php"; }); </script>';
									} elseif($type === 'Agen'){ 
										echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Non Aktifkan Kategori Voucher Terlalu Dahulu", type: "error" },
										function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; }); </script>';
									} elseif($type === 'Reseller'){ 
										echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Non Aktifkan Kategori Voucher Terlalu Dahulu", type: "error" },
										function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; }); </script>';
									}
									
								}else{
									@sqlsrv_query($dbconnect, "DELETE From KategoriVoucher WHERE KodeVoucher = '".$id."'") or die( print_r( sqlsrv_errors(), true));
									if($type == null){
										echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
										function () { window.location.href = "set-kategori-voucher.php"; }); </script>';
									} elseif($type === 'Agen'){ 
										echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
										function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; }); </script>';
									} elseif($type === 'Reseller'){ 
										echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
										function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; }); </script>';
									}
									
								}
							}
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// nonaktif data
							$delete = @sqlsrv_query($dbconnect, "update KategoriVoucher set IsAktif = 'False' WHERE KodeVoucher = '".$id."'") or die( print_r( sqlsrv_errors(), true));
							if($type == null){
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Voucher Berhasil ", type: "success" },
								function () { window.location.href = "set-kategori-voucher.php"; }); </script>';
							} elseif($type === 'Agen'){ 
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Voucher Berhasil ", type: "success" },
								function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; }); </script>';
							} elseif($type === 'Reseller'){ 
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Voucher Berhasil ", type: "success" },
								function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; }); </script>';
							}
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @sqlsrv_query($dbconnect, "update KategoriVoucher set IsAktif = 'True' WHERE KodeVoucher = '".$id."'") or die( print_r( sqlsrv_errors(), true));
							if($type == null){
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Voucher Berhasil ", type: "success" },
								function () { window.location.href = "set-kategori-voucher.php"; }); </script>';
							} elseif($type === 'Agen'){ 
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Voucher Berhasil ", type: "success" },
								function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Agen')).'"; }); </script>';
							} elseif($type === 'Reseller'){ 
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Voucher Berhasil ", type: "success" },
								function () { window.location.href = "set-kategori-voucher.php?type='.htmlspecialchars(base64_encode('Reseller')).'"; }); </script>';
							}
						} ?>
                    </div>
                    <!-- /.panel -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
			<?php if($page === 'nonaktif' OR $page === 'aktif' OR $page === 'delete'){ echo ''; } else { ?>
                 <?php include "footer.php"; ?>
			<?php } ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
	$(document).ready(function() {
        $('#dataTables-dist').DataTable({
            responsive: true
        });
    });
	</script>
	<script>
	$(document).ready(function() {
        $('#dataTables-agen').DataTable({
            responsive: true
        });
    });
	</script>
	<script>
	$(document).ready(function() {
        $('#dataTables-reseller').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script>
	
	<!-- <script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-dist').DataTable();

      $('#dataTables-dist tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-agen').DataTable();

      $('#dataTables-agen tbody').on('click', 'a[data-target="#delete2"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-reseller').DataTable();

      $('#dataTables-reseller tbody').on('click', 'a[data-target="#delete3"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>
	
	<!-- membuat dropdown bertingkat klien-->
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		  $("#_kodedpr").change(function(){
			var id_departemen = $("#_kodedpr").val();
			$.ajax({
				url: "../get-data/get-departemen.php",
				data: "get_dept="+id_departemen,
				cache: false,
				success: function(msg){
					//jika data sukses diambil dari server kita tampilkan
					//di <select id=kota>
					$("#_kodesub").html(msg);
				}
			});
		  });
		  $("#_kodesub").change(function(){
			var id_subdepartemen = $("#_kodesub").val();
			$.ajax({
				url: "../get-data/get-subdepartemen.php",
				data: "get_sub="+id_subdepartemen,
				cache: false,
				success: function(msg){
					$("#_kodekat").html(msg);
				}
			});
		  });
		});
	</script>
	
	<script text="javascript">
			/* Tanpa Rupiah */
			var tanpa_rupiah = document.getElementById('nominal');
			var tanpa_rupiah2 = document.getElementById('nominal2');
			var tanpa_rupiah3 = document.getElementById('nominal3');
			var tanpa_rupiah4 = document.getElementById('nominal4');
			var tanpa_rupiah5 = document.getElementById('nominal5');
			var tanpa_rupiah6 = document.getElementById('nominal6');
			var tanpa_rupiah7 = document.getElementById('nominal7');
			var tanpa_rupiah8 = document.getElementById('nominal8');
			var tanpa_rupiah9 = document.getElementById('nominal9');
			var tanpa_rupiah10 = document.getElementById('nominal10');
			var tanpa_rupiah11 = document.getElementById('nominal11');
			
			tanpa_rupiah.addEventListener('keyup', function(e){ tanpa_rupiah.value = formatRupiah(this.value); });
			tanpa_rupiah.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah2.addEventListener('keyup', function(e){ tanpa_rupiah2.value = formatRupiah(this.value); });
			tanpa_rupiah2.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah3.addEventListener('keyup', function(e){ tanpa_rupiah3.value = formatRupiah(this.value); });
			tanpa_rupiah3.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah4.addEventListener('keyup', function(e){ tanpa_rupiah4.value = formatRupiah(this.value); });
			tanpa_rupiah4.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah5.addEventListener('keyup', function(e){ tanpa_rupiah5.value = formatRupiah(this.value); });
			tanpa_rupiah5.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah6.addEventListener('keyup', function(e){ tanpa_rupiah6.value = formatRupiah(this.value); });
			tanpa_rupiah6.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah7.addEventListener('keyup', function(e){ tanpa_rupiah7.value = formatRupiah(this.value); });
			tanpa_rupiah7.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah8.addEventListener('keyup', function(e){ tanpa_rupiah8.value = formatRupiah(this.value); });
			tanpa_rupiah8.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah9.addEventListener('keyup', function(e){ tanpa_rupiah9.value = formatRupiah(this.value); });
			tanpa_rupiah9.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah10.addEventListener('keyup', function(e){ tanpa_rupiah10.value = formatRupiah(this.value); });
			tanpa_rupiah10.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			tanpa_rupiah11.addEventListener('keyup', function(e){ tanpa_rupiah11.value = formatRupiah(this.value); });
			tanpa_rupiah11.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			/* Fungsi */
			function formatRupiah(bilangan, prefix)
			{
				var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
					split	= number_string.split(','),
					sisa 	= split[0].length % 3,
					rupiah 	= split[0].substr(0, sisa),
					ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
					
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
			}
			
			function limitCharacter(event)
			{
				key = event.which || event.keyCode;
				if ( key != 188 // Comma
					 && key != 8 // Backspace
					 && key != 9 // Tab
					 && key != 116 // F5
					 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
					 && (key < 48 || key > 57) // Non digit
					 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
					) 
				{
					event.preventDefault();
					return false;
				}
			}
			</script>
	
	<!-- jQuery sticky menu -->
    <script src="../web/js/owl.carousel.min.js"></script>
    <script src="../web/js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="../web/js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="../web/js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="../web/js/bxslider.min.js"></script>
	<script type="text/javascript" src="../web/js/script.slider.js"></script>
	
	<!-- Slide -->
	<script src="../web/js/jquery.fancybox.pack.js"></script>
	<script src="../web/js/jquery.fancybox-media.js"></script>
	<script src="../web/js/google-code-prettify/prettify.js"></script>
	<script src="../web/js/portfolio/jquery.quicksand.js"></script>
	<script src="../web/js/portfolio/setting.js"></script>
	<script src="../web/js/jquery.flexslider.js"></script>
	<script src="../web/js/animate.js"></script>
	<script src="../web/js/custom.js"></script>

</body>

</html>
