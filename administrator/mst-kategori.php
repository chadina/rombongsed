<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select MasterKategory.*, MasterSubDepartemen.KodeSubDepartemen, MasterSubDepartemen.NamaSubDepartemen 
from MasterKategory inner join MasterSubDepartemen on MasterSubDepartemen.KodeSubDepartemen = MasterKategory.KodeSubDepartemen
where KodeKategory = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodetag = $cari['KodeKategory']; $kodetag2 = $cari['KodeKategoryManual']; $kategory = $cari['NamaKategory']; $kettag = $cari['Keterangan'];
	$kodesub = $cari['KodeSubDepartemen']; $namasub = $cari['NamaSubDepartemen'];
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Kategori
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-kategori.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="35%">Nama Kategori</th>
                                        <th width="30%">Sub Departemen</th>
                                        <th width="10%">Status</th>
                                        <th width="15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select MasterKategory.*, MasterSubDepartemen.NamaSubDepartemen from MasterKategory inner join MasterSubDepartemen on
								MasterSubDepartemen.KodeSubDepartemen = MasterKategory.KodeSubDepartemen order by KodeKategory", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".$cari['KodeKategoryManual']."</strong> : ".ucwords($cari['NamaKategory']); ?></td>
                                        <td><?php echo ucwords($cari['NamaSubDepartemen']); ?></td>
                                        <td><?php if($cari['IsAktif'] == TRUE){ echo '<a href="mst-kategori.php?page='.htmlspecialchars(base64_encode("delete")).'&id='.htmlspecialchars(base64_encode($cari["KodeKategory"])).'" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='mst-kategori.php?page=".htmlspecialchars(base64_encode('aktif'))."&id=".htmlspecialchars(base64_encode($cari['KodeKategory']))."' class='btn btn-danger btn-sm'>Non Aktif</a>"; } ?></td>
                                        <td>
											<a href="mst-kategori.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeKategory'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-kategori.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['KodeKategory'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<!-- <a href="mst-kategori.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeKategory'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a> -->
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Kode Kategori Manual</label>
                                            <input class="form-control" type="text" name="_kodetag" placeholder="ex : 000001" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Sub Departemen</label>
                                            <select class="form-control" id="_kodesub" name="_kodesub" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Sub Departemen --</option>';
													$list = @sqlsrv_query($dbconnect, "SELECT * FROM MasterSubDepartemen ORDER BY KodeSubDepartemen") or die( print_r( sqlsrv_errors(), true)); 
													while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){	
														echo "<option value=\"".$daftar['KodeSubDepartemen']."\" >".ucwords($daftar['NamaSubDepartemen'])."</option>\n";
													}
												?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Nama Kategori</label>
                                            <input class="form-control" type="text" name="_namatag" placeholder="ex : Nama Kategori" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" rows="8" cols="40" name="_kettag" placeholder="ex : Ini Adalah Kategori" autocomplete="off"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-input-tag">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-tag">Reset</button>
										
										<?php
										include "../connections/config.php";
										$_kode = @htmlspecialchars($_POST['_kodetag']); $_kode2 = @htmlspecialchars($_POST['_kodesub']); $_nama = @htmlspecialchars($_POST['_namatag']); $_ket = @htmlspecialchars($_POST['_kettag']); 
										if(isset($_POST['_submit-input-tag'])){
											// membuat id otomatis
											$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeKategory,7)) AS kode FROM MasterKategory WHERE LEFT(replace(KodeKategory,' ',''),4)='KTG-'") or die( print_r( sqlsrv_errors(), true)); 
											$nums = @sqlsrv_num_rows($sql); 
											while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											$bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
											$kode_jadi = "KTG-".$bikin_kode;
											
											$cek = @sqlsrv_query($dbconnect, "select * from MasterKategory where replace(KodeKategoryManual,' ','') = replace('$_kode',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$num = @sqlsrv_num_rows($cek);
											if($num > 0 ){
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Kategori Manual Sudah Ada ", "error"); </script>';
											}else{
											$query = @sqlsrv_query($dbconnect, "insert into MasterKategory(KodeKategory,KodeKategoryManual,NamaKategory,Keterangan,KodeSubDepartemen,IsAktif,IsDapatKupon,IsDapatPoint)
											values('$kode_jadi','$_kode','$_nama','$_ket','$_kode2','1','0','0')") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-kategori.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
											}
										}
										@sqlsrv_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Kode Kategori</label>
                                            <input class="form-control" type="text" value="<?php echo $kodetag; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Kode Kategori Manual</label>
                                            <input class="form-control" type="text" name="_kodetag2" value="<?php echo $kodetag2; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Departemen</label>
											<select class="form-control" id="_kodesub2" name="_kodesub2" autocomplete="off" required>
                                            <?php $menu = @sqlsrv_query($dbconnect, "SELECT * FROM MasterSubDepartemen ORDER BY KodeSubDepartemen") or die( print_r( sqlsrv_errors(), true));
											echo '<option value="">-- Pilih Sub Departemen --</option>';
											while($kode = @sqlsrv_fetch_array($menu, SQLSRV_FETCH_ASSOC)){
												if($kodesub !== NULL){
													if($kode['KodeSubDepartemen'] === $kodesub){
														echo "<option value=\"".$kode['KodeSubDepartemen']."\" selected='selected'>".$kode['NamaSubDepartemen']."</option>\n";
													}else{
														echo "<option value=\"".$kode['KodeSubDepartemen']."\" >".$kode['NamaSubDepartemen']."</option>\n";
													}
												}else{
													echo "<option value=\"".$kode['KodeSubDepartemen']."\" >".$kode['NamaSubDepartemen']."</option>\n";
												}
											}
											?>
											</select>
                                        </div>
										<div class="form-group">
                                            <label>Nama Kategori</label>
                                            <input class="form-control" type="text" name="_tag2" value="<?php echo $kategory; ?>" autocomplete="off" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" rows="8" cols="40" name="_kettag2" autocomplete="off"><?php echo $kettag; ?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-edit-tag">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-tag">Reset</button>
										
										<?php
										include "../connections/config.php";
										$_kode2 = @htmlspecialchars($_POST['_kodetag2']); $_nama2 = @htmlspecialchars($_POST['_tag2']); $_ket2 = @htmlspecialchars($_POST['_kettag2']); 
										$_kodesub2 = @htmlspecialchars($_POST['_kodesub2']);
										if(isset($_POST['_submit-edit-tag'])){
											if($kodetag2 === $_kode2){
												$query = @sqlsrv_query($dbconnect, "update MasterKategory set KodeKategoryManual = '$_kode2', NamaKategory = '$_nama2', Keterangan = '$_ket2',
												KodeSubDepartemen = '$_kodesub2' where KodeKategory = '".$kodetag."'") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
														function () { window.location.href = "mst-kategori.php"; });
														</script>';
													}
													else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
													}
											}else{
												$cek = @sqlsrv_query($dbconnect, "select * from MasterKategory where replace(KodeKategoryManual,' ','') = replace('$_kode2',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num = @sqlsrv_num_rows($cek);
												if($num > 0){
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Kategori Manual Sudah Ada ", "error"); </script>';
												}else{
												$query = @sqlsrv_query($dbconnect, "update MasterKategory set KodeKategoryManual = '$_kode2', NamaKategory = '$_nama2', Keterangan = '$_ket2', 
												KodeSubDepartemen = '$_kodesub2' where KodeKategory = '".$kodetag."'") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
														function () { window.location.href = "mst-kategori.php"; });
														</script>';
													}
													else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
													}
												}
											}
										}
										@sqlsrv_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Kode Kategori</label>
                                            <dd><?php echo $kodetag2; ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Nama Kategori</label>
                                            <dd><?php echo ucwords($kategory); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Sub Departemen</label>
                                            <dd><?php echo ucwords($namasub); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Keterangan</label>
                                            <dd><?php echo ucwords($kettag); ?></dd><br>
                                    </div>
										
								</div>
							</div>
						</div>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$cek = @sqlsrv_query($dbconnect, "select * from MasterBarang where replace(KodeKategory,' ','') = replace('$kodetag',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Kategori Dipakai Transaksi ", type: "error" },
								function () { window.location.href = "mst-kategori.php"; }); </script>';
							}else{
								// hapus data
								/*$delete = @sqlsrv_query($dbconnect, "DELETE from MasterKategory WHERE KodeKategory = '".$kodetag."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-kategori.php"; }); </script>';*/
								
								// inaktif data
								$delete = @sqlsrv_query($dbconnect, "update MasterKategory set IsAktif = 'False' WHERE KodeKategory = '".$kodetag."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-kategori.php"; }); </script>';
							}
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @sqlsrv_query($dbconnect, "update MasterKategory set IsAktif = 'True' WHERE KodeKategory = '".$kodetag."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-kategori.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>


</body>

</html>
