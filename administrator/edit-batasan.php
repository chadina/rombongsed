<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php"; 
$fitur_id = 23;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '13'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeset13 = $cari['id']; $namaset13 = $cari['nama_setting']; $value13 = number_format($cari['value'],0,"",".");
}

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '14'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeset14 = $cari['id']; $namaset14 = $cari['nama_setting']; $value14 = number_format($cari['value'],0,"",".");
}

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '15'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeset15 = $cari['id']; $namaset15 = $cari['nama_setting']; $value15 = number_format($cari['value'],0,"",".");
}

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '16'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeset16 = $cari['id']; $namaset16 = $cari['nama_setting']; $value16 = $cari['value'];
}

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '17'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeset17 = $cari['id']; $namaset17 = $cari['nama_setting']; $value17 = $cari['value'];
}

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '18'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeset18 = $cari['id']; $namaset18 = $cari['nama_setting']; $value18 = $cari['value'];
}

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Setting Administrasi dan Durasi Voucher
					<?php /*if($id == null AND $page == null){ 
						echo "<a href='mst-user.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}*/
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Batasan Administrasi Reseller</label>
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal" name="_batasan1" value="<?php echo $value13; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Batasan Administrasi Agen</label>
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal2" name="_batasan2" value="<?php echo $value14; ?>"autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Batasan Administrasi Distributor</label>
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal3" name="_batasan3" value="<?php echo $value15; ?>"autocomplete="off" required>
                                        </div>
										<hr>
										<label>Batasan Voucher Reseller</label>
										<div class="form-group input-group">
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal6" name="_batasan4" value="<?php echo $value16; ?>"autocomplete="off" required>
											<span class="input-group-addon">&nbsp;hari</span>
										</div>
										<label>Batasan Voucher Agen</label>
										<div class="form-group input-group">
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal5" name="_batasan5" value="<?php echo $value17; ?>"autocomplete="off" required>
											<span class="input-group-addon">&nbsp;hari</span>
										</div>
                                        <label>Batasan Voucher Distributor</label>
										<div class="form-group input-group">
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal4" name="_batasan6" value="<?php echo $value18; ?>" autocomplete="off" required>
											<span class="input-group-addon">&nbsp;hari</span>
										</div>
										<button type="submit" class="btn btn-default" name="_submit-edit-batasan">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-batasan">Reset</button>
									</div>
								</form>
									
									<?php
										include "../connections/config.php";
										$_value1 = @htmlspecialchars($_POST['_batasan1']); $_nilai1 =  @str_replace(".", "", $_value1);
										$_value2 = @htmlspecialchars($_POST['_batasan2']); $_nilai2 =  @str_replace(".", "", $_value2);
										$_value3 = @htmlspecialchars($_POST['_batasan3']); $_nilai3 =  @str_replace(".", "", $_value3);
										$_value4 = @htmlspecialchars($_POST['_batasan4']); 
										$_value5 = @htmlspecialchars($_POST['_batasan5']); 
										$_value6 = @htmlspecialchars($_POST['_batasan6']);
										if(isset($_POST['_submit-edit-batasan'])){
											$query1 = @sqlsrv_query($dbconnect, "update mst_setting set value = '$_nilai1' where id = '13'") or die( print_r( sqlsrv_errors(), true)); 	 
											$query2 = @sqlsrv_query($dbconnect, "update mst_setting set value = '$_nilai2' where id = '14'") or die( print_r( sqlsrv_errors(), true)); 	 
											$query3 = @sqlsrv_query($dbconnect, "update mst_setting set value = '$_nilai3' where id = '15'") or die( print_r( sqlsrv_errors(), true)); 	 
											$query4 = @sqlsrv_query($dbconnect, "update mst_setting set value = '$_value4' where id = '16'") or die( print_r( sqlsrv_errors(), true)); 	 
											$query5 = @sqlsrv_query($dbconnect, "update mst_setting set value = '$_value5' where id = '17'") or die( print_r( sqlsrv_errors(), true)); 	 
											$query6 = @sqlsrv_query($dbconnect, "update mst_setting set value = '$_value6' where id = '18'") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query1 OR $query2 OR $query3 OR $query4 OR $query5 OR $query6){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "edit-batasan.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
                        <!-- /.panel-heading -->
					
					</div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script>
	$(document).ready(function() {
		$('#datepicker-example1').Zebra_DatePicker();
	});
	</script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>

	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<script text="javascript">
			/* Tanpa Rupiah */
			var tanpa_rupiah = document.getElementById('nominal');
			var tanpa_rupiah2 = document.getElementById('nominal2');
			var tanpa_rupiah3 = document.getElementById('nominal3');
			
			tanpa_rupiah.addEventListener('keyup', function(e)
			{
				tanpa_rupiah.value = formatRupiah(this.value);
			});
			
			tanpa_rupiah.addEventListener('keydown', function(event)
			{
				limitCharacter(event);
			});
			
			tanpa_rupiah2.addEventListener('keyup', function(e)
			{
				tanpa_rupiah2.value = formatRupiah(this.value);
			});
			
			tanpa_rupiah2.addEventListener('keydown', function(event)
			{
				limitCharacter(event);
			});
			
			tanpa_rupiah3.addEventListener('keyup', function(e)
			{
				tanpa_rupiah3.value = formatRupiah(this.value);
			});
			
			tanpa_rupiah3.addEventListener('keydown', function(event)
			{
				limitCharacter(event);
			});
			
			/* Fungsi */
			function formatRupiah(bilangan, prefix)
			{
				var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
					split	= number_string.split(','),
					sisa 	= split[0].length % 3,
					rupiah 	= split[0].substr(0, sisa),
					ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
					
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
			}
			
			function limitCharacter(event)
			{
				key = event.which || event.keyCode;
				if ( key != 188 // Comma
					 && key != 8 // Backspace
					 && key != 9 // Tab
					 && key != 116 // F5
					 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
					 && (key < 48 || key > 57) // Non digit
					 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
					) 
				{
					event.preventDefault();
					return false;
				}
			}
			</script>

</body>

</html>
