<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 6;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from mst_file where id_file = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodefile = $cari['id_file']; $desc = $cari['deskripsi']; $namafile = $cari['nama_file']; $sizefile = $cari['file_size']; $doc = $cari['nama_dokumen'];
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Upload File
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-file.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}elseif($id != null AND $page != null){ 
						echo "<a href='mst-file.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="65%">Nama File</th>
                                        <th width="10%">File</th>
                                        <th width="15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select * from mst_file order by tanggal DESC", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".strtoupper($cari['nama_dokumen'])."</strong><br>Nama File : ".$cari['nama_file']."<br> Uploaded : ".TanggalIndo(DATE_FORMAT($cari['tanggal'],'Y-m-d')); ?><br>
                                        <td><?php if(file_exists('../files/'.$cari['nama_file'])){ echo "<a href='../files/".$cari['nama_file']."' class='btn btn-primary btn-sm' target='_blank'>Unduh</a>"; } else { echo "<a href='#' class='btn btn-default btn-sm'>Not Found</a>"; } ?></td>
                                        <td>
											<a href="mst-file.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['id_file'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<!-- <a href="mst-file.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['id_file'])); ?>" class="btn btn-success btn-sm">Detail</a> -->
											<a href="mst-file.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['id_file'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="well text-center">
										<h4>Masukkan File Terlebih Dahulu</h4>
										<div class="col-md-8 col-md-offset-2">
											<form id="form_upload" class="form-inline" method="post" action="">
												<div class="input-group">
													<label class="input-group-btn">
														<span class="btn btn-danger btn-md">
															Browse&hellip; <input type="file" id="media" name="media" style="display: none;" required>
														</span>
													</label>
													<input type="text" class="form-control input-md" readonly required>
												</div><br><br>
												<div class="input-group">
													<input type="submit" class="btn btn-md btn-primary" id="upload" value="Start upload">
												</div>
											</form>
											<br>
											<div class="progress" style="display:none">
												<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
													<span class="sr-only">0%</span>
												</div>
											</div>
											<div class="msg alert alert-info text-left" style="display:none"></div>
										</div>
										<div class="clearfix"></div>
									</div>
									
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Nama File</label>
                                            <input class="form-control" type="text" name="_namafile" placeholder="ex : Nama File" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Deskripsi</label>
											<textarea class="form-control" rows="5" cols="40" name="_ketfile" autocomplete="off"></textarea>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-input-file">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-file">Reset</button>
									 </form>
								</div>
										
										<?php
										include "../connections/config.php";
										$_nama = @htmlspecialchars($_POST['_namafile']); $_desc = @htmlspecialchars($_POST['_ketfile']);
										if(isset($_POST['_submit-input-file'])){
											// membuat cek id
											$edit = @sqlsrv_query($dbconnect, "SELECT TOP (1) id_file FROM mst_file ORDER BY id_file DESC") or die( print_r( sqlsrv_errors(), true)); 
											while($row = @sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){
												$_idfile = $row['id_file'];
											}
											$query = @sqlsrv_query($dbconnect, "update mst_file set nama_dokumen = '$_nama', deskripsi = '$_desc' where id_file = '".$_idfile."'") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-file.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Kode File</label>
                                            <input class="form-control" type="text" value="<?php echo $kodefile; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Detail File</label>
                                            <input class="form-control" type="text" value="<?php echo $namafile." : ".number_format(($sizefile/1000000),2)." MB"; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama File</label>
                                            <input class="form-control" type="text" name="_doc2" value="<?php echo $doc; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Deskripsi</label>
											<textarea class="form-control" rows="5" cols="40" name="_desc2" autocomplete="off"><?php echo $desc; ?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-edit-file">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-file">Reset</button>
										
										<?php
										include "../connections/config.php";
										$_doc2 = @htmlspecialchars($_POST['_doc2']); $_desc2 = @htmlspecialchars($_POST['_desc2']);
										if(isset($_POST['_submit-edit-file'])){
											$query = @sqlsrv_query($dbconnect, "update mst_file set nama_dokumen = '$_doc2', deskripsi = '$_desc2' where id_file = '".$kodefile."'") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-file.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
										}
										@sqlsrv_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php /*} elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Icon Departemen</label>
                                            <?php if($fotodpr !== null){
												echo '<dd><img class="imgl img-thumbnail" src="../img/foto-departemen/'.$fotodpr.'" width="30%" /></dd>';
											}else{ 
												echo '<dd><img class="imgl img-thumbnail" src="../img/foto-departemen/no-image.png" width="30%" /></dd>'; 
											} ?>
                                    </div>
									<div class="form-group">
                                        <label>Kode Departemen</label>
                                            <dd><?php echo $kodedpr2; ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Nama Departemen</label>
                                            <dd><?php echo ucwords($departemen); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Keterangan</label>
                                            <dd><?php echo ucwords($ketdpr); ?></dd><br>
                                    </div>
										
								</div>
							</div>
						</div> --> */ ?>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$file_path = "../files/".$namafile; 
							if(file_exists($file_path)){ unlink($file_path); }
							$delete = @sqlsrv_query($dbconnect, "DELETE from mst_file WHERE id_file = '".$kodefile."'") or die( print_r( sqlsrv_errors(), true));
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-file.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>

	<script src="../js/upload.js"></script>
    <script>
	$(function() {
	  $(document).on('change', ':file', function() {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	  });

	  $(document).ready( function() {
		  $(':file').on('fileselect', function(event, numFiles, label) {

			  var input = $(this).parents('.input-group').find(':text'),
				  log = numFiles > 1 ? numFiles + ' files selected' : label;

			  if( input.length ) {
				  input.val(log);
			  } else {
				  if( log ) alert(log);
			  }

		  });
	  });
	  
	});
	</script>
	
</body>

</html>
