<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ; 

@mysqli_close();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- CK Editor -->
	<script type="text/javascript" src="../dist/ckeditor/ckeditor.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<h1 class="page-header">Server Log&nbsp;</h1>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
							<div class="row">
								<div class="input-daterange">
									<div class="col-md-3">
										<input class="form-control date-range-filter" type="text" id="min-date" name="min-date" placeholder="Tanggal Awal" autocomplete="off">
									</div>
									<div class="col-md-3">
										<input class="form-control date-range-filter" type="text" id="max-date" name="min-date" placeholder="Tanggal Akhir" autocomplete="off">
									</div>
									<div class="col-md-3">
										<input type="button" id="search" name="search" value="Cari" class="btn btn-info">
									</div>
							    </div>      
							</div><br>
                            <table width="100%" class="table table-striped table-bordered table-hover dt-responsive" id="dataTables-server">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="20%">Tanggal</th>
                                        <th width="15%">User</th>
                                        <th width="55%">Deskripsi</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                        <!-- /.panel-body -->
						
						
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- ServerSide Master User -->
	<script>
	$( document ).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings){
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
        };
		$('#min-date').Zebra_DatePicker();
		$('#max-date').Zebra_DatePicker();
			
		fetch_data('no');
		
		function fetch_data(is_date_search, start_date='', end_date='')	{
		  var dataTable = $('#dataTables-server').DataTable({
			   "lengthMenu": [[25, 50, 100], [25, 50, 100]],
			   "bProcessing": true,
			   "serverSide" : true,
			   "order": [[1, 'asc']],
			   "ajax" : {
					url:"fetch.php",
					type:"post",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					}
					
			   },
			   "rowCallback": function (row, data, iDisplayIndex) {
					var info = this.fnPagingInfo();
					var page = info.iPage;
					var length = info.iLength;
					var index = page * length + (iDisplayIndex + 1);
					$('td:eq(0)', row).html(index);
			   }
		  });
		}

		 $('#search').click(function(){
			var start_date = $('#min-date').val();
			var end_date = $('#max-date').val();
			if(start_date != '' && end_date != '') {
				$('#dataTables-server').DataTable().destroy();
				fetch_data('yes', start_date, end_date);
			}
			else {
				swal("Maaf!", " Kedua Tanggal Harus Diisi ", "error");
			}
		 }); 
	 
	});
	</script>
	
	<!-- <script>
    $( document ).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings){
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
        };
 
		$('#dataTables-server').DataTable({
			// "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
			"lengthMenu": [[25, 50, 100], [25, 50, 100]],
			"bProcessing": true,
			"serverSide": true,
			"ajax":{
				url :"response-data.php?type=<?php echo base64_encode('_server'); ?>", // json datasource
				type: "post",  // type of method  ,GET/POST/DELETE
				error: function(){
				  $("#dataTables-server_processing").css("display","none");
				}
			},
			"order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
			}
		});   
		
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-server').DataTable();

      $('#dataTables-server tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Lanjutkan!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  window.location.href = getLink
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	  
	});
	</script>
	
</body>

</html>