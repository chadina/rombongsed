<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 9;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
$upnew = isset($_GET['upnew']) ? base64_decode($_GET['upnew']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeprs = $cari['KodePerson']; $kodeprs2 = $cari['KodePersonManual']; $namaprs = $cari['NamaPerson']; $tlahir = $cari['TempatLahir']; $tgllahir = $cari['TanggalLahir']; 
	$noktp = $cari['NoIdentitas']; $alamat = $cari['Alamat']; $kontakprs = $cari['ContactPerson']; $emailprs = $cari['Email']; $ketprs = $cari['Keterangan']; 
	$jenisk = $cari['JenisKelamin']; $fotoprs = $cari['FotoProfil']; $prov = $cari['Provinsi']; $kab = $cari['Kabupaten']; $kec = $cari['Kecamatan']; $desa = $cari['Desa'];
	$bankprs = $cari['NamaBankPerson']; $norekprs = $cari['NoRekPerson']; $banknama = $cari['AtasNamaBank']; $userprs = $cari['UserName']; $sponsprs = $cari['MemberSponsor'];
	$statsprs = $cari['StatusPerson'];
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<?php if($id == null AND $page == null){
						echo '<h1 class="page-header">Ubah Upline</h1>';
					} elseif($id != null AND $page == 'change_upline'){
						echo '<h1 class="page-header">Cari Upline&nbsp;';
						echo "<a href='ubah-upline.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo '</h1>';
					} else{
						echo '<h1 class="page-header">Ubah Upline</h1>';
					} ?>
					<?php /* if($id == null AND $page == null){ 
						echo "<a href='mst-member.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-member.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					} */
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<?php if($id == null AND $page == null){ ?>
				<div class="col-lg-6">
					<form role="form" method="post" action="ubah-upline.php">
						<div class="form-group">
						<label>Masukkan Kode, Username atau Nama Person</label>
							<input type="text" class="form-control" name="keyword" placeholder="Kode, Username atau Nama Person" autocomplete="off" value="<?php echo @$_REQUEST['keyword']; ?>">
							<br><button type="submit" name="cek" value="cek" class="btn btn-info">Cek User</button><br><br>
										
						</div>
					</form>
				</div>
				
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <!-- <th width="15%">Gambar</th> -->
                                        <th width="35%">Nama User</th>
                                        <th width="35%">Upline</th>
                                        <th width="20%">Aksi</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$keyword = @$_REQUEST['keyword'];
								if($keyword == null OR $keyword === ''){
									$query = '';
								}else{	
									$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where IsVerified = '1' AND (StatusPerson = 'USER' OR StatusPerson = 'RESELLER' OR StatusPerson = 'AGEN' OR StatusPerson = 'DISTRIBUTOR')
									AND (KodePersonManual LIKE '%".$keyword."%' OR UserName LIKE '%".$keyword."%' OR NamaPerson LIKE '%".$keyword."%') order by NamaPerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								}
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".strtoupper($cari['NamaPerson'])."</strong><br>Kode Person : ".ucwords($cari['KodePersonManual'])."<br>Status : ".ucwords($cari['StatusPerson'])."<br>UserName : ".ucwords($cari['UserName']); ?></td>
                                        <td><?php $query2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$cari['MemberSponsor']."' order by KodePerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ 
											echo "<strong>".strtoupper($cari2['NamaPerson'])."</strong><br>Kode Person : ".ucwords($cari2['KodePersonManual'])."<br>Status : ".ucwords($cari2['StatusPerson'])."<br>UserName : ".ucwords($cari2['UserName']); 
										} ?></td>
										<td>
											<a href="ubah-upline.php?page=<?php echo htmlspecialchars(base64_encode('change_upline'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson'])); ?>" class="btn btn-danger btn-sm">Ganti Upline</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
				<?php } elseif($id != null AND $page == 'change_upline'){ ?>
				<div class="col-lg-6">
					<form role="form" method="post" action="ubah-upline.php?page=<?php echo htmlspecialchars(base64_encode('change_upline'))."&id=".htmlspecialchars(base64_encode($id)); ?>">
						<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Nama Person</label>
													<dd><?php echo $kodeprs2." : ".ucwords($namaprs)." (".$statsprs.") "; ?></dd>
											</div>
											<div class="form-group">
												<label>Upline Aktif</label>
												<?php $query3 = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$sponsprs."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($cari3 = @sqlsrv_fetch_array($query3, SQLSRV_FETCH_ASSOC)){ 
													echo "<dd>".$cari3['KodePersonManual']." : ".ucwords($cari3['NamaPerson'])." (".$cari3['StatusPerson'].") </dd>"; 
												} ?>
												
											</div>
										</div>
									</div>
									
						<div class="form-group">
						<label>Masukkan Kode atau Nama Person Upline Baru</label>
							<input type="text" class="form-control" name="keyword" placeholder="Kode atau Nama Person" autocomplete="off" value="<?php echo @$_REQUEST['keyword2']; ?>">
							<br><button type="submit" name="cek" value="cek" class="btn btn-info">Cek User</button><br><br>
										
						</div>
					</form>
				</div>
				
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <!-- <th width="15%">Gambar</th> -->
                                        <th width="75%">Nama User</th>
                                        <th width="15%">Aksi</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$keyword = @$_REQUEST['keyword2']; 
								if($statsprs === 'USER'){ $upline = 'RESELLER'; }
								elseif($statsprs === 'RESELLER'){ $upline = 'AGEN'; }
								elseif($statsprs === 'AGEN'){ $upline = 'DISTRIBUTOR'; }
								elseif($statsprs === 'DISTRIBUTOR'){ $upline = 'MARKETPLACE'; }
								
								if($keyword == null OR $keyword === ''){
									$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where IsVerified = '1' AND KodePerson != '$sponsprs' AND StatusPerson = '$upline' order by NamaPerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								}else{	
									$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where IsVerified = '1' AND KodePerson != '$sponsprs' AND StatusPerson = '$upline' AND (KodePersonManual LIKE '%".$keyword."%' OR NamaPerson LIKE '%".$keyword."%') order by NamaPerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								}
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".strtoupper($cari['NamaPerson'])."</strong><br>Kode Person : ".ucwords($cari['KodePersonManual'])."<br>Status : ".ucwords($cari['StatusPerson'])."<br>UserName : ".ucwords($cari['UserName']); ?></td>
                                        <td>
											<a href="ubah-upline.php?page=<?php echo htmlspecialchars(base64_encode('set_upline')); ?>&id=<?php echo htmlspecialchars(base64_encode($id)); ?>&upnew=<?php echo htmlspecialchars(base64_encode($cari['KodePerson'])); ?>" class="btn btn-primary btn-sm" data-target="#delete" data-toggle="modal">Set Upline</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
				
				<?php } elseif($id != null AND $page == 'set_upline'){ 
					// aktif data
					$delete = @sqlsrv_query($dbconnect, "update MasterPerson set MemberSponsor = '".$upnew."' WHERE KodePerson = '".$id."'") or die( print_r( sqlsrv_errors(), true));
					echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Ubah Upline Sukses ", type: "success" },
					function () { window.location.href = "ubah-upline.php"; }); </script>';
				} ?>
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
		<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Mengubah Upline User Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Ubah Upline!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Ubah Upline Gagal ", "error");
              }
          });
      });
	});
	</script>
	
</body>

</html>
