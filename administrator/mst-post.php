<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
// $fitur_id = 17;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @mysqli_query($con, "select * from mstkegiatan where id_kegiatan = '".$id."'");
while($cari = @mysqli_fetch_array($query)){ 
	$kodepost = $cari['id_kegiatan']; $tglposting = $cari['tanggal']; $judul = $cari['judul']; $penulis = $cari['penulis']; $isipost = $cari['deskripsi']; $fotopost = $cari['foto']; $ringkasan = $cari['ringkasan']; $keterangan = $cari['Keterangan'];
}
@mysqli_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Posting Kegiatan
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-post.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					} elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-post.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="15%">Gambar</th>
                                        <th width="55%">Judul</th>
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @mysqli_query($con, "select * from mstkegiatan order by tanggal DESC");
								$no = 1;
								while($cari = @mysqli_fetch_array($query)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php if($cari['foto'] !== null){
												echo '<img class="imgl img-thumbnail" src="../android_rombongsedekah/img/kegiatan/'.$cari['foto'].'" width="100%" />';
											}else{ 
												echo '<img class="imgl img-thumbnail" src="../android_rombongsedekah/img/kegiatan/no-image.png" width="100%" />'; 
											} ?>
										</td>
                                        <td><?php echo "<strong>".strtoupper($cari['judul'])."</strong><br>Oleh : ".ucwords($cari['penulis']).", ".TanggalIndo($cari['tanggal']); ?></td>
                                        <td>
											<a href="mst-post.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['id_kegiatan'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-post.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['id_kegiatan'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<a href="mst-post.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['id_kegiatan'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@mysqli_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<?php
									// membuat id otomatis
											$sql = @mysqli_query($con, "SELECT MAX(RIGHT(id_kegiatan,5)) AS kode FROM mstkegiatan WHERE LEFT(replace(id_kegiatan,' ',''),11)='POST-".date('Ym')."'"); 
											$nums = @mysqli_num_rows($sql); 
											while($data = @mysqli_fetch_array($sql)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
											$kode_jadi = "POST-".date('Ym')."-".$bikin_kode;
									?>
								
								<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Masukkan Gambar Terlebih Dahulu</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kode_jadi);?>&type=<?php echo base64_encode('_post');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									</div>
									
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Tanggal</label>
                                            <input class="form-control" type="text" id="datepicker-example1" name="_tglpost" placeholder="ex : 2017-01-01" autocomplete="off" required>
                                            <input class="form-control" type="hidden" name="_idpost" value="<?php echo $kode_jadi; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Judul Kegiatan</label>
                                            <input class="form-control" type="text" name="_namapost" placeholder="ex : Judul Kegiatan" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Oleh</label>
                                            <input class="form-control" type="text" name="_olehpost" placeholder="ex : Nama Penulis" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <input class="form-control" type="text" name="_ket" placeholder="ex : Keterangan" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Ringkasan</label>
                                            <textarea class="form-control" rows="5" cols="20" name="_ringkas" placeholder="ex : Ringkasan Kegiatan" autocomplete="off"></textarea>
                                        </div>
										<div class="form-group">
                                            <label>Isi</label>
											<textarea class="ckeditor" name="_isipost" class="form-control" placeholder="ex : Ini Adalah Isi Kegiatan" rows="4"></textarea>
                                            
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-input-post">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-post">Reset</button>
									</div>
								</form>
									
										<?php
										include "../connections/config.php";
										$_idpost = @htmlspecialchars($_POST['_idpost']); $_tgl = @htmlspecialchars($_POST['_tglpost']); $_nama = @htmlspecialchars($_POST['_namapost']); $_nama2 = @htmlspecialchars($_POST['_olehpost']);  $_isi = $_POST['_isipost']; $_ringkasan = @htmlspecialchars($_POST['_ringkas']);  $_keterangan = @htmlspecialchars($_POST['_ket']); 
										if(isset($_POST['_submit-input-post'])){
											$cek = @mysqli_query($con, "select * from mstkegiatan where replace(id_kegiatan,' ','') = replace('$_idpost',' ','')");
											$num = @mysqli_num_rows($cek);
											if($num > 0){
											$query = @mysqli_query($con, "update mstkegiatan set Keterangan='$_keterangan', is_aktif='TRUE', tanggal = '$_tgl', judul = '$_nama', penulis = '$_nama2', deskripsi = '$_isi', ringkasan='$_ringkasan' where id_kegiatan = '".$_idpost."'"); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-post.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
											}else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Anda Belum Upload Icon Gambar ", "error"); </script>';
											}
										}
										@mysqli_close;

										?>
                                    
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
									
									<div class="col-lg-6">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Ganti Gambar</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kodepost);?>&img=<?php echo base64_encode($fotopost);?>&type=<?php echo base64_encode('_post');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
											</div>
											<div class="form-group">
											<?php if($fotopost == null){ 
													echo '';
												} else {
													echo '<img class="imgl img-thumbnail" src="../android_rombongsedekah/img/kegiatan/'.$fotopost.'" id="_oldimg" width="50%" /><br>';
												}  ?>
												
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
									</div>
									
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Kode Post</label>
                                            <input class="form-control" type="text" value="<?php echo $kodepost; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Tanggal</label>
                                            <input class="form-control" type="text" id="datepicker-example1" name="_tglpost2" value="<?php echo ($tglposting); ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Judul Kegiatan</label>
                                            <input class="form-control" type="text" name="_namapost2" value="<?php echo $judul; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Oleh</label>
                                            <input class="form-control" type="text" name="_olehpost2" value="<?php echo $penulis; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <input class="form-control" type="text" name="_ket2" value="<?php echo $keterangan; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Ringkasan</label>
                                            <textarea class="form-control" rows="5" cols="20" name="_ringkas2" autocomplete="off"><?php echo $ringkasan; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Isi Kegiatan</label>
											<textarea class="ckeditor" name="_isipost2" class="form-control" rows="4"><?php echo $isipost; ?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-edit-post">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-post">Reset</button>
									</div>
								</form>
									
										<?php
										include "../connections/config.php";
										$_tgl2 = @htmlspecialchars($_POST['_tglpost2']); $_nama21 = @htmlspecialchars($_POST['_namapost2']); $_nama22 = @htmlspecialchars($_POST['_olehpost2']);  $_isi2 = $_POST['_isipost2']; $_ringkasan2 = @htmlspecialchars($_POST['_ringkas2']);  $_keterangan2 = @htmlspecialchars($_POST['_ket2']); 
										if(isset($_POST['_submit-edit-post'])){
											$query = @mysqli_query($con, "update mstkegiatan set tanggal = '$_tgl2', judul = '$_nama21', penulis = '$_nama22', deskripsi = '$_isi2', Keterangan='$_keterangan2', ringkasan='$_ringkasan2' where id_kegiatan = '".$kodepost."'"); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-post.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
										}
										@mysqli_close;

										?>
                                    
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Gambar</label>
                                            <?php if($fotopost !== null){
												echo '<dd><img class="imgl img-thumbnail" src="../android_rombongsedekah/img/kegiatan/'.$fotopost.'" width="100%" /></dd>';
											}else{ 
												echo '<dd><img class="imgl img-thumbnail" src="../android_rombongsedekah/img/kegiatan/no-image.png" width="100%" /></dd>'; 
											} ?>
                                    </div>
									<div class="form-group">
                                        <label>Judul</label>
                                            <dd><?php echo ucwords($judul); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Oleh</label>
                                            <dd><?php echo ucwords($penulis)." Pada ".TanggalIndo($tglposting); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Isi Kegiatan</label>
                                            <dd><?php echo ucwords($isipost); ?></dd><br>
                                    </div>
									<div class="form-group">
                                        <label>Keterangan</label>
                                            <dd><?php echo ucwords($keterangan); ?></dd><br>
                                    </div>
										
								</div>
							</div>
						</div>
						
						<?php } elseif($id != null AND $page == 'delete'){ 
							$file_path = "../android_rombongsedekah/img/kegiatan/".$fotopost; $file_path2 = "../android_rombongsedekah/img/kegiatan/".$fotopost;
							if(file_exists($file_path)){ unlink($file_path); }
							if(file_exists($file_path2)){ unlink($file_path2); }
								$delete = @mysqli_query($con, "DELETE from mstkegiatan WHERE id_kegiatan = '".$kodepost."'");
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-post.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script>
	$(document).ready(function() {
		$('#datepicker-example1').Zebra_DatePicker();
	});
	</script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- ckeditor JS -->
   <script type="text/javascript" src="../web/ckeditor/ckeditor.js"></script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>

</body>

</html>
