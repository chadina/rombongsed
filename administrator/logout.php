<?php
session_start();
unset($_SESSION['_user_login']); 
session_destroy(); 
?>

<!DOCTYPE html>
<html>
<head>
<title>Rombong Sedekah</title>
<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
</head>
<body>
<?php $id = base64_decode(@$_GET['id']); 
if($id === "error"){ ?>
<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript">
	sweetAlert({
	title: "Maaf!",
	text: " Anda Harus Sign In ",
	type: "error"
	},
	function () {
	window.location.href = "../index.php";
	});
</script>
<?php } elseif($id === "timeout"){ ?>
<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript">
	sweetAlert({
	title: "Maaf!",
	text: " Anda Harus Sign In Kembali ",
	type: "error"
	},
	function () {
	window.location.href = "../index.php";
	});
</script>
<?php } else { ?>
<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript">
	sweetAlert({
	title: "Terima Kasih!",
	text: " Anda Telah Sign Out ",
	type: "info"
	},
	function () {
	window.location.href = "../index.php";
	});
</script>
<?php } ?>
</body>
</html>




