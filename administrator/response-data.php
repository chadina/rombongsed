<?php
	//include connection file 
	include_once("../connections/config.php");
	$type = isset($_GET['type']) ? base64_decode($_GET['type']) : 0 ; 
	
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();
	$params = $_REQUEST;
	
	if($type == 'desa'){
	//define index of column
	$columns = array( 
		0 => 'KodeDesa',
		1 => 'NamaDesa',
		2 => 'NamaKec',
		3 => 'NamaKab',
		4 => 'NamaProvinsi'
	);
	
	$where = $sqlTot = $sqlRec = "";

		// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" WHERE ";
		$where .=" ( KodeDesa LIKE '".$params['search']['value']."%' ";    
		$where .=" OR NamaDesa LIKE '".$params['search']['value']."%' ";
		$where .=" OR NamaKec LIKE '".$params['search']['value']."%' ";
		$where .=" OR NamaKab LIKE '".$params['search']['value']."%' ";
		$where .=" OR NamaProvinsi LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT mstdesa.KodeDesa, mstdesa.NamaDesa, mstkecamatan.NamaKec, mstkabupaten.NamaKab, mstprovinsi.NamaProvinsi FROM mstdesa 
			INNER JOIN mstkecamatan ON mstkecamatan.KodeKec = mstdesa.KodeKec
		    INNER JOIN mstkabupaten ON mstkabupaten.KodeKab = mstdesa.KodeKab 
			INNER JOIN mstprovinsi ON mstprovinsi.KodeProvinsi = mstdesa.KodeProvinsi";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = @sqlsrv_query($dbconnect, $sqlTot, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));


	$totalRecords = @sqlsrv_num_rows($queryTot);

	$queryRecords = @sqlsrv_query($dbconnect, $sqlRec) or die( print_r( sqlsrv_errors(), true));

	//iterate on results row and create new index array of data
	while( $row = @sqlsrv_fetch_array($queryRecords, SQLSRV_FETCH_ASSOC) ) { 
		$data[] = $row;
	}

	}elseif($type == 'kecamatan'){
		//define index of column
	$columns = array( 
		0 => 'KodeKec',
		1 => 'NamaKec',
		2 => 'NamaKab',
		3 => 'NamaProvinsi'
	);
	
	$where = $sqlTot = $sqlRec = "";

		// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" WHERE ";
		$where .=" ( KodeKec LIKE '".$params['search']['value']."%' ";    
		$where .=" OR NamaKec LIKE '".$params['search']['value']."%' ";
		$where .=" OR NamaKab LIKE '".$params['search']['value']."%' ";
		$where .=" OR NamaProvinsi LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT mstkecamatan.KodeKec, mstkecamatan.NamaKec, mstkabupaten.NamaKab, mstprovinsi.NamaProvinsi FROM mstkecamatan 
			INNER JOIN mstkabupaten ON mstkabupaten.KodeKab = mstkecamatan.KodeKab 
			INNER JOIN mstprovinsi ON mstprovinsi.KodeProvinsi = mstkecamatan.KodeProvinsi";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($dbconnect, $sqlTot) or die("database error:". mysqli_error($dbconnect));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($dbconnect, $sqlRec) or die("error to fetch desa data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_row($queryRecords) ) { 
		$data[] = $row;
	}
	
	}elseif($type == 'kabupaten'){
		//define index of column
	$columns = array( 
		0 => 'KodeKab',
		1 => 'NamaKab',
		2 => 'NamaProvinsi'
	);
	
	$where = $sqlTot = $sqlRec = "";

		// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" WHERE ";
		$where .=" ( KodeKab LIKE '".$params['search']['value']."%' ";    
		$where .=" OR NamaKab LIKE '".$params['search']['value']."%' ";
		$where .=" OR NamaProvinsi LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT mstkabupaten.KodeKab, mstkabupaten.NamaKab, mstprovinsi.NamaProvinsi FROM mstkabupaten 
			INNER JOIN mstprovinsi ON mstprovinsi.KodeProvinsi = mstkabupaten.KodeProvinsi";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($dbconnect, $sqlTot) or die("database error:". mysqli_error($dbconnect));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($dbconnect, $sqlRec) or die("error to fetch desa data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_row($queryRecords) ) { 
		$data[] = $row;
	}
	
	}elseif($type == 'provinsi'){
		//define index of column
	$columns = array( 
		0 => 'id',
		1 => 'provinsi'
	);
	
	$where = $sqlTot = $sqlRec = "";

		// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" WHERE ";
		$where .=" ( KodeProvinsi LIKE '".$params['search']['value']."%' ";    
		$where .=" OR NamaProvinsi LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT MstProvinsi.id, MstProvinsi.provinsi FROM MstProvinsi";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = @sqlsrv_query($dbconnect, $sqlTot, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));


	$totalRecords = @sqlsrv_num_rows($queryTot);

	$queryRecords = @sqlsrv_query($dbconnect, $sqlRec, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));

	//iterate on results row and create new index array of data
	while( $row = @sqlsrv_fetch_array($queryRecords, SQLSRV_FETCH_ASSOC) ) { 
		$data[] = $row;
	}
	
	}
	
	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval( $totalRecords ),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
	
?>
	