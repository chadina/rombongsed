<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php"; 
date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from kode_verifikasi where id_verifikasi = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeverif = $cari['id_verifikasi']; $user = $cari['id_user']; $tgl = $cari['tanggal']; $kodepin = $cari['kode_transaksi']; $status = $cari['status']; $pakai = $cari['is_dipakai'];
}
@sqlsrv_close();

function random($panjang_karakter){  
  $karakter = '1234567890abcdefghijkmnpqrstuvwxyz';  
  $string = '';  
  for($i = 0; $i < $panjang_karakter; $i++) {  
	$pos = rand(0, strlen($karakter)-1);  
	$string .= $karakter{$pos};  
  }  
return $string;  
}

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">PIN Member
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-kode.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>&nbsp;";
						echo "<a href='mst-kode.php?page=".htmlspecialchars(base64_encode('status'))."' class='btn btn-primary btn-sm'><i class='fa fa-list fa-fw'></i> Lihat Status Kode</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body">
							<form role="form" method="post">
							<div class="row col-lg-12">
								<input type="button" class="btn btn-md btn-warning" onclick="cek(this.form.cekbox)" value="Select All" />&nbsp;
								<input type="button" class="btn btn-md btn-danger" onclick="uncek(this.form.cekbox)" value="Clear All" />&nbsp;
								<input type="submit" class="btn btn-md btn-success" value="Verifikasi" name="_submit-cek" />
							</div><br><br><br>
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="50%">PIN Member</th>
                                        <th width="15%">Status</th>
                                        <th width="15%">Set</th>
                                        <th width="10%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								if(isset($_POST['_submit-cek'])){
									$cekbox = $_POST['cekbox'];
									if($cekbox) {
										foreach ($cekbox as $value) {
											@sqlsrv_query($dbconnect, "UPDATE kode_verifikasi SET status ='TRUE' WHERE id_verifikasi = '$value'") or die( print_r( sqlsrv_errors(), true));
										}
										echo '<script type="text/javascript">
										sweetAlert({ title: "Berhasil!", text: " PIN Telah Terverifikasi ", type: "success" },
										function () { window.location.href = "mst-kode.php"; });
										</script>';
									}else {
										echo '<script type="text/javascript">
										sweetAlert({ title: "Maaf!", text: " PIN Gagal Terverifikasi ", type: "error" },
										function () { window.location.href = "mst-kode.php"; });
										</script>';
									} 
								}
								
								$query = @sqlsrv_query($dbconnect, "select * from kode_verifikasi where status = 'FALSE' order by tanggal DESC", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "PIN : <strong>".$cari['kode_transaksi']."</strong><br>".TanggalIndo(DATE_FORMAT($cari['tanggal'], 'Y-m-d')); ?></td>
                                        <td><?php if($cari['status'] === 'FALSE'){ echo '<a href="#" class="btn btn-warning btn-sm">Not Verified</a>'; } ?></td>
                                        <td><span class='btn btn-primary btn-sm'><input type="checkbox" id="cekbox" name="cekbox[]" value="<?php echo $cari['id_verifikasi'] ?>"/>&nbsp;Verifikasi</span></td>
                                        <td>
											<!-- <a href="mst-kode.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['id_verifikasi'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-kode.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['id_verifikasi'])); ?>" class="btn btn-success btn-sm">Detail</a> -->
											<a href="mst-kode.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['id_verifikasi'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
							</form>
                        </div>
                        <!-- /.panel-body -->
						
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post" action="mst-kode.php?page=<?php echo htmlspecialchars(base64_encode('tambah')); ?>">
										<div class="form-group">
                                            <label>Generate PIN</label>
                                            <input class="form-control" type="number" name="_angka" value="<?php echo @$_POST['_angka']; ?>" placeholder="Masukkan Jumlah PIN Member" autocomplete="off" required>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-generate">Submit</button><br><br>
                                    </form>
								</div>
									
								<div class="row col-lg-12">
									<?php // membuat id otomatis
									$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(id_verifikasi,5)) AS kode FROM kode_verifikasi WHERE LEFT(replace(id_verifikasi,' ',''),9)='VR-".date('Ym')."'") or die( print_r( sqlsrv_errors(), true)); 
									$nums = @sqlsrv_num_rows($sql); 
									while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
									if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
									}
									// membuat kode user
									$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
									$kode_jadi = "VR-".date('Ym')."-".$bikin_kode; ?>
									
									<div class="form-group">
									<form role="form" method="post">
										<?php for($i=0;$i<@$_POST['_angka'];$i++){ $kode_generate = random(15); ?>
											<div class="col-lg-4">
												<input class="form-control" type="hidden" name="_idverif<?php echo $i; ?>" value="<?php echo $kode_jadi++; ?>" autocomplete="off" readonly>
												<input class="form-control" type="hidden" name="_iduser<?php echo $i; ?>" value="<?php echo $id_tree_aktif; ?>" autocomplete="off" readonly>
												<input class="form-control" type="text" name="_kode<?php echo $i; ?>" value="<?php echo $kode_generate; ?>" autocomplete="off" readonly><br>
											</div>
										<?php } ?>
									</div>
								</div>
								
								<div class="row col-lg-12">
									<div class="form-group">
										<div class="col-lg-4">
										<input class="form-control" type="hidden" name="_jumlah" value="<?php echo $_POST['_angka']?>"/>
										  <?php if((@$_POST['_angka'] == null) OR (@$_POST['_angka'] == '0')){
											  echo '';
										  }else{
											  echo '<button type="submit" name="_submit-pin" class="btn btn-primary">Simpan</button>';
										  } ?>
										</div>
									</form>
									
									<?php
										include "../connections/config.php";
										if(isset($_POST['_submit-pin'])){
											for($i=0; $i<@$_POST['_jumlah']; $i++){
												 $id_verifikasi 	= @$_POST['_idverif'.$i];
												 $id_user	 		= @$_POST['_iduser'.$i];
												 $pin_member		= @$_POST['_kode'.$i];
												 $tanggal	 		= date('Y-m-d');
												 $status			= 'FALSE';
												 
												 $query = "INSERT into kode_verifikasi(id_verifikasi,id_user,kode_transaksi,tanggal,status)values('$id_verifikasi','$id_user','$pin_member','$tanggal','$status')";
												 $insert = @sqlsrv_query($dbconnect, $query, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												/* if($insert){
													 echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="../generate-kode.php"; </script>';
												 }else{
													 echo '<script language="javascript">alert("Data Gagal disimpan !"); document.location="../ganerate-kode.php"; </script>';
												 }
												 */
												 if($insert){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Generate PIN Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-kode.php"; });
													</script>';
												 }else{
													echo '<script type="text/javascript">
													sweetAlert({ title: "Maaf!", text: " Generate PIN Gagal Tersimpan ", type: "error" },
													function () { window.location.href = "mst-kode.php"; });
													</script>';
												 }
											}
										}
										@sqlsrv_close;
										
										/*include "../connections/config.php";
										$_nama = @htmlspecialchars($_POST['_namabank']);  
										if(isset($_POST['_submit-input-bank'])){
											// membuat id otomatis
											$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeBank,3)) AS kode FROM mstbank WHERE LEFT(replace(KodeBank,' ',''),4)='BNK-'") or die( print_r( sqlsrv_errors(), true)); 
											$nums = @sqlsrv_num_rows($sql); 
											while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											$bikin_kode = str_pad($kode, 3, "0", STR_PAD_LEFT);
											$kode_jadi = "BNK-".$bikin_kode;
											
											$query = @sqlsrv_query($dbconnect, "insert into mstbank(KodeBank,NamaBank,IsWantrast)
											values('$kode_jadi','$_nama','0')") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													sqlsrv_query($dbconnect, "INSERT into mstbankpenerima (NoRekening,AtasNama,KodeBank)values('Belum Tersedia','Belum Tersedia','$kode_jadi')") or die( print_r( sqlsrv_errors(), true));
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-bank.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
											
										}
										@sqlsrv_close;
										*/
										
										?>
                                    </div>
								</div>
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id == null AND $page == 'status'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data Kode Available
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="55%">PIN Member</th>
                                        <th width="15%">Status</th>
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select * from kode_verifikasi where status = 'TRUE' order by tanggal DESC", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "PIN : <strong>".$cari['kode_transaksi']."</strong><br>".TanggalIndo(DATE_FORMAT($cari['tanggal'], 'Y-m-d')); ?></td>
                                        <td><?php if($cari['status'] === 'TRUE'){ echo '<a href="#" class="btn btn-primary btn-sm">Verified</a>'; } ?></td>
                                        <td><?php if($cari['is_dipakai'] === 'FALSE'){ echo '<a href="#" class="btn btn-success btn-sm">Available</a>&nbsp;';
										echo '<a href="mst-kode.php?page='.htmlspecialchars(base64_encode("delete2")).'&id='.htmlspecialchars(base64_encode($cari["id_verifikasi"])).'" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>';
										}elseif($cari['is_dipakai'] === 'TRUE') {
											$cek = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodeTransaksi = '".$cari['kode_transaksi']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$num = @sqlsrv_num_rows($cek);
											if($num > 0){
												while($num2 = @sqlsrv_fetch_array($cek, SQLSRV_FETCH_ASSOC)){
													echo "<strong>".$num2['KodePersonManual']."</strong><br>".ucwords($num2['NamaPerson']);
												}
											}else{
												echo '';
												echo '<a href="mst-kode.php?page='.htmlspecialchars(base64_encode("delete2")).'&id='.htmlspecialchars(base64_encode($cari["id_verifikasi"])).'" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a>';
											}
										}
										?>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
						</div>
                        <!-- /.panel-body -->
						
						<?php } /*elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Kode Bank</label>
                                            <input class="form-control" type="text" value="<?php echo $kodebank; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama Bank</label>
                                            <input class="form-control" type="text" name="_bank2" value="<?php echo $bank; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Status</label><br>
                                            <label class="radio-inline">
                                                <input type="radio" name="_aktif2" value="1" <?php if($iswantrast == 1){ echo 'checked="checked"'; } else { echo ''; } ?> >Aktif
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="_aktif2" value="0" <?php if($iswantrast == 0){ echo 'checked="checked"'; } else { echo ''; } ?> >Non Aktif
                                            </label>
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-edit-bank">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-bank">Reset</button>
										
										<?php
										include "../connections/config.php";
										$_bank2 = @htmlspecialchars($_POST['_bank2']); $_aktif2 = @htmlspecialchars($_POST['_aktif2']);
										if(isset($_POST['_submit-edit-bank'])){
											$query = @sqlsrv_query($dbconnect, "update mstbank set NamaBank = '$_bank2', IsWantrast = '$_aktif2' where KodeBank = '".$kodebank."'") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-bank.php"; });
													</script>';
												}
										}
										@sqlsrv_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'setting'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Set Rekening
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Nama Bank</label>
                                            <input class="form-control" type="text" value="<?php echo $bank; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>No Rekening</label>
                                            <input class="form-control" type="text" name="_norek3" value="<?php echo $norek; ?>" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Atas Nama Bank</label>
                                            <input class="form-control" type="text" name="_atasnama3" value="<?php echo $atasnama; ?>" autocomplete="off" required>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-edit-rek">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-rek">Reset</button>
										
										<?php
										include "../connections/config.php";
										$_norek3 = @htmlspecialchars($_POST['_norek3']); $_atasnama3 = @htmlspecialchars($_POST['_atasnama3']);
										if(isset($_POST['_submit-edit-rek'])){
											$query = @sqlsrv_query($dbconnect, "update mstbankpenerima set NoRekening = '$_norek3', AtasNama = '$_atasnama3' where KodeBank = '".$kodebank."'") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-bank.php"; });
													</script>';
												}
										}
										@sqlsrv_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Nama Bank</label>
                                            <dd><?php echo ucwords($bank); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Status</label>
                                            <dd><?php if($iswantrast === '1'){ echo 'Bank Perusahaan'; } else { echo 'Non Aktif'; } ?></dd>
                                    </div>
								</div>
							</div>
						</div>
						
						<?php } */ elseif($id != null AND $page == 'delete'){ 
							$cek = @sqlsrv_query($dbconnect, "select * from kode_verifikasi where status = 'TRUE' AND replace(id_verifikasi,' ','') = replace('$kodeverif',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Maaf, Kode Sudah Diverifikasi ", type: "error" },
								function () { window.location.href = "mst-kode.php"; }); </script>';
							}else{
								$delete = @sqlsrv_query($dbconnect, "DELETE from kode_verifikasi WHERE id_verifikasi = '".$kodeverif."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-kode.php"; }); </script>';
							}
						} elseif($id != null AND $page == 'delete2'){ 
							$cek = @sqlsrv_query($dbconnect, "select * from kode_verifikasi where status = 'TRUE' AND is_dipakai = 'TRUE' AND replace(id_verifikasi,' ','') = replace('$kodeverif',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Maaf, Kode Sudah Dipakai Transaksi ", type: "error" },
								function () { window.location.href = "mst-kode.php"; }); </script>';
							}else{
								$delete = @sqlsrv_query($dbconnect, "DELETE from kode_verifikasi WHERE id_verifikasi = '".$kodeverif."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-kode.php"; }); </script>';
							}
						} ?>
						
						
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>

</body>

</html>
