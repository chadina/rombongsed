<?php
include "../connections/config.php";
$set 	= base64_decode(@$_GET['set']);
$img 	= base64_decode(@$_GET['img']);
$type 	= base64_decode(@$_GET['type']);
$gbr 	= base64_decode(@$_GET['gbr']);

function random($panjang_karakter){  
  $karakter = '1234567890abcdefghijkmnpqrstuvwxyz';  
  $string = '';  
  for($i = 0; $i < $panjang_karakter; $i++) {  
	$pos = rand(0, strlen($karakter)-1);  
	$string .= $karakter{$pos};  
  }  
return $string;  
}

if(isset($_POST)){
	############ Edit settings ##############
	if($type === '_dept'){
	$ThumbSquareSize 		= 120; //Thumbnail will be 100x100
	}else{
	$ThumbSquareSize 		= 400; //Thumbnail will be 200x200	
	}
	$BigImageMaxSize 		= 500; //Image Maximum height or width
	$ThumbPrefix			= "thumb_"; //Normal thumb Prefix
	if($type === '_dept'){
		$DestinationDirectory	= '../img/foto-departemen/'; //specify upload directory ends with / (slash)
	}elseif($type === '_program'){
		$DestinationDirectory	= '../android_rombongsedekah/img/program/'; //specify upload directory ends with / (slash)
	}elseif($type === '_brng'){
		$DestinationDirectory	= '../img/foto-barang/'; //specify upload directory ends with / (slash)
	}elseif($type === '_post'){
		$DestinationDirectory	= '../android_rombongsedekah/img/kegiatan/'; //specify upload directory ends with / (slash)
	}elseif($type === '_foto'){
		$DestinationDirectory	= '../img/foto-galeri/'; //specify upload directory ends with / (slash)
	}elseif($type === '_admin'){
		$DestinationDirectory	= '../img/foto-admin/'; //specify upload directory ends with / (slash)
	}elseif($type === '_slid'){
		$DestinationDirectory	= '../android_rombongsedekah/img/slider/'; //specify upload directory ends with / (slash)
	}elseif($type === '_spons'){
		$DestinationDirectory	= '../img/foto-sponsor/'; //specify upload directory ends with / (slash)
	}elseif($type === '_user'){
		$DestinationDirectory	= '../android_rombongsedekah/img/profil/'; //specify upload directory ends with / (slash)
	}elseif($type === '_header'){
		$DestinationDirectory	= '../img/romsed/'; //specify upload directory ends with / (slash)
	}elseif($type === '_profil'){
		$DestinationDirectory	= '../img/foto-assets/'; //specify upload directory ends with / (slash)
	}elseif($type === '_sistem'){
		$DestinationDirectory	= '../img/foto-assets/'; //specify upload directory ends with / (slash)
	}
	$Quality 				= 100; //jpeg quality
	##########################################
	
	//check if this is an ajax request
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	// check $_FILES['ImageFile'] not empty
	if(!isset($_FILES['ImageFile']) || !is_uploaded_file($_FILES['ImageFile']['tmp_name'])){
		die('Upload Bermasalah, Cek Ulang Ekstensi Image Yang Diupload!'); // output error when above checks fail.
	}
	
	// Random number will be added after image name
	$RandomNumber 	= rand(0, 9999999999); 
	$ImageName 		= str_replace(' ','-',strtolower($_FILES['ImageFile']['name'])); //get image name
	$ImageSize 		= $_FILES['ImageFile']['size']; // get original image size
	$TempSrc	 	= $_FILES['ImageFile']['tmp_name']; // Temp name of image file stored in PHP tmp folder
	$ImageType	 	= $_FILES['ImageFile']['type']; //get file type, returns "image/png", image/jpeg, text/plain etc.

	//Let's check allowed $ImageType, we use PHP SWITCH statement here
	switch(strtolower($ImageType)){
		case 'image/png':
			//Create a new image from file 
			$CreatedImage = imagecreatefrompng($_FILES['ImageFile']['tmp_name']);
			break;
		case 'image/gif':
			$CreatedImage = imagecreatefromgif($_FILES['ImageFile']['tmp_name']);
			break;			
		case 'image/jpeg':
		case 'image/pjpeg':
			$CreatedImage = imagecreatefromjpeg($_FILES['ImageFile']['tmp_name']);
			break;
		default:
			die('File Tidak Support!'); //output error and exit
	}
	
	//PHP getimagesize() function returns height/width from image file stored in PHP tmp folder.
	//Get first two values from image, width and height. 
	//list assign svalues to $CurWidth,$CurHeight
	list($CurWidth,$CurHeight) = getimagesize($TempSrc);
	
	//Get file extension from Image name, this will be added after random name
	$ImageExt = substr($ImageName, strrpos($ImageName, '.'));
  	$ImageExt = str_replace('.','',$ImageExt);
	
	//remove extension from filename
	$ImageName 		= preg_replace("/\\.[^.\\s]{3,4}$/", "", $ImageName); 
	
	//Construct a new name with random number and extension.
	//$NewImageName = $ImageName.'-'.$RandomNumber.'.'.$ImageExt;
	if($type === '_dept'){
		$NewImageName = $set.'_'.random(10).'_dept.'.$ImageExt;
	}elseif($type === '_program'){
		$NewImageName = $set.'_'.random(10).'_program.'.$ImageExt;
	}elseif($type === '_brng'){
		$NewImageName = $set.'_'.random(10).'_brng.'.$ImageExt;
	}elseif($type === '_post'){
		$NewImageName = $set.'_'.random(10).'_post.'.$ImageExt;
	}elseif($type === '_foto'){
		$NewImageName = $set.'_'.random(10).'_foto.'.$ImageExt;
	}elseif($type === '_admin'){
		$NewImageName = $set.'_'.random(10).'_admin.'.$ImageExt;
	}elseif($type === '_slid'){
		$NewImageName = $set.'_'.random(10).'_slider.'.$ImageExt;
	}elseif($type === '_spons'){
		$NewImageName = $set.'_'.random(10).'_spons.'.$ImageExt;
	}elseif($type === '_user'){
		$NewImageName = $set.'_'.random(10).'_user.'.$ImageExt;
	}elseif($type === '_header'){
		$NewImageName = $set.'_'.random(10).'_header.'.$ImageExt;
	}elseif($type === '_profil'){
		$NewImageName = $set.'_'.random(10).'_profil.'.$ImageExt;
	}elseif($type === '_sistem'){
		$NewImageName = $set.'_'.random(10).'_sistem.'.$ImageExt;
	}
	
	//set the Destination Image
	$thumb_DestRandImageName 	= $DestinationDirectory.$ThumbPrefix.$NewImageName; //Thumbnail name with destination directory
	$DestRandImageName 			= $DestinationDirectory.$NewImageName; // Image with destination directory
	
	//Resize image to Specified Size by calling resizeImage function.
	if(resizeImage($CurWidth,$CurHeight,$BigImageMaxSize,$DestRandImageName,$CreatedImage,$Quality,$ImageType)){
		//Create a square Thumbnail right after, this time we are using cropImage() function
		if(!cropImage($CurWidth,$CurHeight,$ThumbSquareSize,$thumb_DestRandImageName,$CreatedImage,$Quality,$ImageType)){
			echo 'Create Thumbnail Gagal!';
		}
		/*
		We have succesfully resized and created thumbnail image
		We can now output image to user's browser or store information in the database
		*/
		if($type === '_dept'){
			echo '<img src="../img/foto-departemen/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_program'){
			echo '<img src="../android_rombongsedekah/img/program/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_brng'){
			echo '<img src="../img/foto-barang/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_post'){
			echo '<img src="../android_rombongsedekah/img/kegiatan/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_foto'){
			echo '<img src="../img/foto-galeri/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_admin'){
			echo '<img src="../img/foto-admin/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_slid'){
			echo '<img src="../android_rombongsedekah/img/slider/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_spons'){
			echo '<img src="../img/foto-sponsor/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_user'){
			echo '<img src="../android_rombongsedekah/img/profil/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_header'){
			echo '<img src="../img/romsed/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_profil'){
			echo '<img src="../img/foto-assets/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}elseif($type === '_sistem'){
			echo '<img src="../img/foto-assets/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		}
		//echo '<img src="../img/foto-campaign/'.$NewImageName.'" alt="Resized Image" title="Gambar Asli" class="img img-responsive">';
		
		// Insert info into database table!
		/* mysql_query("INSERT into mst_barang (kode_barang,foto_utama)values('$id','$NewImageName')"); */
		
		if($type === '_dept'){
			$file_path = "../img/foto-departemen/".$img; $file_path2 = "../img/foto-departemen/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM MasterDepartemen WHERE KodeDepartemen = '$set'") or die( print_r( sqlsrv_errors(), true));
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into MasterDepartemen(KodeDepartemen,GambarDepartemen) values ('$set','$NewImageName')") or die( print_r( sqlsrv_errors(), true));
				}else{
					@mysqli_query($con, "UPDATE MasterDepartemen SET GambarDepartemen = '$NewImageName' where KodeDepartemen = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE MasterDepartemen SET GambarDepartemen = '$NewImageName' where KodeDepartemen = '$set'");
			}
		}elseif($type === '_program'){
			$file_path = "../android_rombongsedekah/img/program/".$img; $file_path2 = "../android_rombongsedekah/img/program/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mstprogram WHERE id_program = '$set'");
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into mstprogram(id_program,foto) values ('$set','$NewImageName')");
				}else{
					@mysqli_query($con, "UPDATE mstprogram SET foto = '$NewImageName' where id_program = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mstprogram SET foto = '$NewImageName' where id_program = '$set'");
			}
		}elseif($type === '_brng'){
			$file_path = "../img/foto-barang/".$img; $file_path2 = "../img/foto-barang/thumb_".$img;
			if($img == null  OR $img === 'no-image.png'){
				$query = @mysqli_query($con, "SELECT * FROM MasterBarang WHERE KodeBarang = '$set'") or die( print_r( sqlsrv_errors(), true));
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					$query2 = @mysqli_query($con, "INSERT into MasterBarang(KodeBarang,Gambar) values ('$set','$NewImageName')") or die( print_r( sqlsrv_errors(), true));
					if($query2){
						@mysqli_query($con, "INSERT into HargaJualSatuan(KodeBarang,KodeSatuan,KonversiSatuan,HargaBeli,HargaJual,HargaEceranTertinggi,HargaJual1,HargaJual2,HargaJual3,HargaJual4,HargaJual5,Diskon,KodeCabang) 
						values ('$set','SAT-0000001','1','0','0','0','0','0','0','0','0','0','CAB-0001')") or die( print_r( sqlsrv_errors(), true));
					}
				}else{
					if($gbr === 'Gambar'){
						@mysqli_query($con, "UPDATE MasterBarang SET Gambar = '$NewImageName' where KodeBarang = '$set'");
					}elseif($gbr === 'Gambar2'){
						@mysqli_query($con, "UPDATE MasterBarang SET Gambar2 = '$NewImageName' where KodeBarang = '$set'");
					}elseif($gbr === 'Gambar3'){
						@mysqli_query($con, "UPDATE MasterBarang SET Gambar3 = '$NewImageName' where KodeBarang = '$set'");
					}
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				if($gbr === 'Gambar'){
						@mysqli_query($con, "UPDATE MasterBarang SET Gambar = '$NewImageName' where KodeBarang = '$set'");
					}elseif($gbr === 'Gambar2'){
						@mysqli_query($con, "UPDATE MasterBarang SET Gambar2 = '$NewImageName' where KodeBarang = '$set'");
					}elseif($gbr === 'Gambar3'){
						@mysqli_query($con, "UPDATE MasterBarang SET Gambar3 = '$NewImageName' where KodeBarang = '$set'");
					}
			}
		}elseif($type === '_post'){
			$file_path = "../android_rombongsedekah/img/kegiatan/".$img; $file_path2 = "../android_rombongsedekah/img/kegiatan/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mstkegiatan WHERE id_kegiatan = '$set'");
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into mstkegiatan(id_kegiatan,foto) values ('$set','$NewImageName')");
				}else{
					@mysqli_query($con, "UPDATE mstkegiatan SET foto = '$NewImageName' where id_kegiatan = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mstkegiatan SET foto = '$NewImageName' where id_kegiatan = '$set'");
			}
		}elseif($type === '_foto'){
			$file_path = "../img/foto-galeri/".$img; $file_path2 = "../img/foto-galeri/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mst_galeri WHERE id_foto = '$set'") ;
				$tot = @mysqli_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into mst_galeri(id_foto,gambar) values ('$set','$NewImageName')");
				}else{
					@mysqli_query($con, "UPDATE mst_galeri SET gambar = '$NewImageName' where id_foto = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mst_galeri SET gambar = '$NewImageName' where id_foto = '$set'");
			}
		}elseif($type === '_admin'){
			$file_path = "../img/foto-admin/".$img; $file_path2 = "../img/foto-admin/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mstuser WHERE id_user = '$set'") or die(mysqli_error($query));
				$tot = @mysqli_num_rows($query);
				if($tot > 0){
					@mysqli_query($con, "UPDATE mstuser SET foto = '$NewImageName' where id_user = '$set'");
				}else{
					@mysqli_query($con, "INSERT into mstuser(id_user,foto) values ('$set','$NewImageName')");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mstuser SET foto = '$NewImageName' where id_user = '$set'");
			}
		}elseif($type === '_slid'){
			$file_path = "../android_rombongsedekah/img/slider/".$img; $file_path2 = "../android_rombongsedekah/img/slider/".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mstslider WHERE id_slider = '$set'") ;
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into mstslider(id_slider,foto) values ('$set','$NewImageName')") ;
				}else{
					@mysqli_query($con, "UPDATE mstslider SET foto = '$NewImageName' where id_slider = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mstslider SET foto = '$NewImageName' where id_slider = '$set'");
			}
		}elseif($type === '_spons'){
			$file_path = "../img/foto-sponsor/".$img; $file_path2 = "../img/foto-sponsor/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mst_support WHERE id_support = '$set'") or die( print_r( sqlsrv_errors(), true));
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into mst_support(id_support,gambar) values ('$set','$NewImageName')") or die( print_r( sqlsrv_errors(), true));
				}else{
					@mysqli_query($con, "UPDATE mst_support SET gambar = '$NewImageName' where id_support = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mst_support SET gambar = '$NewImageName' where id_support = '$set'");
			}
		}elseif($type === '_user'){
			$file_path = "../android_rombongsedekah/img/profil/".$img; $file_path2 = "../android_rombongsedekah/img/profil/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mstnasabah WHERE KodeNasabah = '$set'") or die(mysqli_error($query));
				$tot = @mysqli_num_rows($query);
				if($tot > 0){
					@mysqli_query($con, "UPDATE mstnasabah SET Foto = '$NewImageName' where KodeNasabah = '$set'");
				}else{
					@mysqli_query($con, "INSERT into mstnasabah(KodeNasabah,Foto) values ('$set','$NewImageName')");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mstnasabah SET Foto = '$NewImageName' where KodeNasabah = '$set'");
			}
		}elseif($type === '_header'){
			$file_path = "../img/romsed/".$img; $file_path2 = "../img/romsed".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM systemsetting WHERE KodeSetting = '$set'");
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into systemsetting(KodeSetting,ValueData) values ('$set','$NewImageName')");
				}else{
					@mysqli_query($con, "UPDATE systemsetting SET ValueData = '$NewImageName' where KodeSetting = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE systemsetting SET ValueData = '$NewImageName' where KodeSetting = '$set'");
			}
		}elseif($type === '_profil'){
			$file_path = "../img/foto-assets/".$img; $file_path2 = "../img/foto-assets/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM systemsetting WHERE KodeSetting = '$set'") ;
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into systemsetting(KodeSetting,ValueData) values ('$set','$NewImageName')");
				}else{
					@mysqli_query($con, "UPDATE systemsetting SET ValueData = '$NewImageName' where KodeSetting = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE systemsetting SET ValueData = '$NewImageName' where KodeSetting = '$set'");
			}
		}elseif($type === '_sistem'){
			$file_path = "../img/foto-assets/".$img; $file_path2 = "../img/foto-assets/thumb_".$img;
			if($img == null){
				$query = @mysqli_query($con, "SELECT * FROM mst_setting WHERE id = '$set'") or die( print_r( sqlsrv_errors(), true));
				$tot = @sqlsrv_num_rows($query);
				if($tot == 0){
					@mysqli_query($con, "INSERT into mst_setting(id,value) values ('$set','$NewImageName')") or die( print_r( sqlsrv_errors(), true));
				}else{
					@mysqli_query($con, "UPDATE mst_setting SET value = '$NewImageName' where id = '$set'");
				}
			}else{
				if(file_exists($file_path)){ unlink($file_path); }
				if(file_exists($file_path2)){ unlink($file_path2); }
				@mysqli_query($con, "UPDATE mst_setting SET value = '$NewImageName' where id = '$set'");
			}
		}else{
			die('Resize Gagal!'); //output error
		}
	}
}


// This function will proportionally resize image 
function resizeImage($CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType){
	//Check Image size is not 0
	if($CurWidth <= 0 || $CurHeight <= 0){
		return false;
	}
	
	//Construct a proportional size of new image
	$ImageScale      	= min($MaxSize/$CurWidth, $MaxSize/$CurHeight); 
	$NewWidth  			= ceil($ImageScale*$CurWidth);
	$NewHeight 			= ceil($ImageScale*$CurHeight);
	$NewCanves 			= imagecreatetruecolor($NewWidth, $NewHeight);
	
	// Resize Image
	if(imagecopyresampled($NewCanves, $SrcImage,0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight)){
		switch(strtolower($ImageType)){
			case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
			case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;			
			case 'image/jpeg':
			case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
			default:
				return false;
	}
	
	//Destroy image, frees memory	
	if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
	return true;
	}

}

//This function corps image to create exact square images, no matter what its original size!
function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType){	 
	//Check Image size is not 0
	if($CurWidth <= 0 || $CurHeight <= 0){
		return false;
	}
	
	//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
	if($CurWidth>$CurHeight){
		$y_offset = 0;
		$x_offset = ($CurWidth - $CurHeight) / 2;
		$square_size = $CurWidth - ($x_offset * 2);
	}else{
		$x_offset = 0;
		$y_offset = ($CurHeight - $CurWidth) / 2;
		$square_size = $CurHeight - ($y_offset * 2);
	}
	
	$NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
	if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size)){
		switch(strtolower($ImageType)){
			case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
			case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;			
			case 'image/jpeg':
			case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
			default:
				return false;
		}
	//Destroy image, frees memory	
	if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
	return true;
	}
	  
}