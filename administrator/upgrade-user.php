<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 24;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;
$user = isset($_GET['user']) ? base64_decode($_GET['user']) : 0 ;
$idnota = isset($_GET['idnota']) ? base64_decode($_GET['idnota']) : 0 ; 

$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeprs = $cari['KodePerson']; $kodeprs2 = $cari['KodePersonManual']; $namaprs = $cari['NamaPerson']; $tlahir = $cari['TempatLahir']; $tgllahir = $cari['TanggalLahir']; 
	$noktp = $cari['NoIdentitas']; $alamat = $cari['Alamat']; $kontakprs = $cari['ContactPerson']; $emailprs = $cari['Email']; $ketprs = $cari['Keterangan']; 
	$jenisk = $cari['JenisKelamin']; $fotoprs = $cari['FotoProfil']; $prov = $cari['Provinsi']; $kab = $cari['Kabupaten']; $kec = $cari['Kecamatan']; $desa = $cari['Desa'];
	$bankprs = $cari['NamaBankPerson']; $norekprs = $cari['NoRekPerson']; $banknama = $cari['AtasNamaBank']; $userprs = $cari['UserName'];
	$statsprs = $cari['StatusPerson']; $spons = $cari['Sponsor']; $upline = $cari['MemberSponsor']; $sponsor = $cari['MemberSponsorAsal'];
}

$query = @sqlsrv_query($dbconnect, "select * from trdompet where NoTransaksi = '".$idnota."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$prs = $cari['KodePerson']; $gbr = $cari['Foto']; $topup = $cari['NominalDebet']; $tgl = $cari['TanggalTransaksi']; $bank = $cari['KodeBank']; 
}

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '13'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value_res = $cari['value']; }

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '14'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value_agn = $cari['value']; }

$query = @sqlsrv_query($dbconnect, "select * from mst_setting where id = '15'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $value_dst = $cari['value']; }

@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php if($id == null AND $page == null){
						echo '<h1 class="page-header">Request Upgrade</h1>';
					} elseif($id != null AND ($page == 'proses' OR $page == 'bayar_registrasi' OR $page == 'bayar_garansi')){
						echo '<h1 class="page-header">Detail Upline Baru&nbsp;';
						echo "<a href='upgrade-user.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo '</h1>';
					} elseif($id != null AND $page == 'cari_downline'){
						echo '<h1 class="page-header">Atur Downline&nbsp;';
						echo '</h1>';
					} elseif($idnota !== null AND $page == 'bukti'){ 
					    echo '<h1 class="page-header">Bukti Administrasi&nbsp;';
						echo "<a href='upgrade-user.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					} else{
						echo '<h1 class="page-header">Request Upgrade</h1>';
					} ?>
					<?php /* if($id == null AND $page == null){ 
						echo "<a href='mst-supplier.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-supplier.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					} */
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <!-- <th width="15%">Gambar</th> -->
                                        <th width="40%">Nama User</th>
                                        <th width="25%">Keterangan</th>
                                        <th width="15%">Upgrade</th>
                                        <th width="10%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where IsOnline = 'True' order by NamaPerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".strtoupper($cari['NamaPerson'])." (".$cari['UserName'].")</strong><br>Kode Person : ".ucwords($cari['KodePersonManual'])."<br>Status : ".ucwords($cari['StatusPerson']); ?></td>
										<td><?php /* $query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalDebet) as Totalbayar from trdompet where IsVerified = '1' AND Keterangan = 'Pembayaran Registrasi' AND KodePerson = '".$cari['KodePerson']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){
											if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
											echo "Registrasi : <br><strong>Rp ".number_format($total_bayar)."</strong>"; } */ ?>
										<?php $query_garansi = @sqlsrv_query($dbconnect, "select SUM(NominalDebet) as Totalbayar2 from trdompet where Keterangan = 'Administrasi Upgrade ".$cari['Sponsor']."' AND KodePerson = '".$cari['KodePerson']."' AND KodePersonDaftar = '".$cari['KodePerson']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										while($cari_garansi = @sqlsrv_fetch_array($query_garansi, SQLSRV_FETCH_ASSOC)){
											if($cari_garansi['Totalbayar2'] == null ){ $total_garansi = 0; } else { $total_garansi = $cari_garansi['Totalbayar2']; }
											echo "Administrasi : <br><strong>Rp ".number_format($total_garansi)."</strong>"; } ?>
										</td>
										<td><?php echo '<a href="#" class="btn btn-default btn-sm">'.$cari['Sponsor'].'</a><br><br>';
										if($cari['Sponsor'] !== 'User'){
											$query_foto = @sqlsrv_query($dbconnect, "select * from trdompet where Keterangan = 'Administrasi Upgrade ".$cari['Sponsor']."' AND KodePerson = '".$cari['KodePerson']."' AND KodePersonDaftar = '".$cari['KodePerson']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$cari_jumlah = @sqlsrv_num_rows($query_foto);
											if($cari_jumlah === 0){
												echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('bayar_garansi'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-primary btn-sm'>Bayar Registrasi</a>";
											} else {
												while($cari_foto = @sqlsrv_fetch_array($query_foto, SQLSRV_FETCH_ASSOC)){ 
													$foto_upload = $cari_foto['Foto'];
													if($cari_foto['Foto'] == '' OR $cari_foto['Foto'] = null OR (!file_exists('../android_grosirmart/img/bukti/'.$cari_foto['Foto']))){
														echo '<a href="#" class="btn btn-danger btn-sm">Belum Upload Bukti</a>';  
													} else { 
														echo '<a href="upgrade-user.php?page='.htmlspecialchars(base64_encode("bukti")).'&idnota='.htmlspecialchars(base64_encode($cari_foto["NoTransaksi"])).'" class="btn btn-primary btn-sm">Lihat Bukti</a>'; 
													} 
												}
											}
										} else {
											echo '';
										}
										?></td>
                                        <td>
											<?php if($cari['Sponsor'] !== 'User'){
												if(@$foto_upload == '' OR @$foto_upload = null OR (!file_exists('../android_grosirmart/img/bukti/'.$foto_upload))){
													echo "<a href='#' class='btn btn-default btn-sm' data-target='#delete' data-toggle='modal'>Administrasi Belum Selesai</a>";
												} else { 
													echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('proses'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-success btn-sm' data-target='#delete' data-toggle='modal'>Verifikasi</a>";
												} 
											} else {
												echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('proses'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-success btn-sm' data-target='#delete' data-toggle='modal'>Verifikasi</a>";
											}
											?>
											
											<?php /* if($cari['Sponsor'] === 'User' OR $cari['Sponsor'] === 'Reseller'){
												echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('proses'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-success btn-sm'>Proses</a>";
											} elseif($cari['Sponsor'] === 'Agen'){
												if($total_garansi >= $value_garansi_agn){
													echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('proses'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-success btn-sm'>Proses</a>";
												}elseif($total_garansi < $value_garansi_agn){
													echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('bayar_garansi'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-primary btn-sm'>Bayar Garansi</a>";
												}
											} elseif($cari['Sponsor'] === 'Distributor'){
												if($total_garansi >= $value_garansi_dst){
													echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('proses'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-success btn-sm'>Proses</a>";
												}elseif($total_garansi < $value_garansi_dst){
													echo "<a href='upgrade-user.php?page=".htmlspecialchars(base64_encode('bayar_garansi'))."&id=".htmlspecialchars(base64_encode($cari['KodePerson']))."' class='btn btn-primary btn-sm'>Bayar Garansi</a>";
												}
											} */ ?>
										
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						
						<?php } elseif($id != null AND $page == 'proses'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Upgrade User
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
								<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Status Person</label>
													<dd><?php echo $kodeprs2." : ".ucwords($namaprs)." (".$statsprs.") "; ?></dd>
											</div>
											<div class="form-group">
												<label>Upgrade Ke</label>
													<dd><?php echo ucwords($spons); ?></dd>
											</div>
											
										</div>
									</div>
									<form role="form" method="post">
									<?php /*	<div class="form-group">
                                            <label>Masukkan Kode Person atau Username Upline</label><br><span>( * Jika Dikosongi Maka Upline adalah Person Milik Perusahaan )</span><br><br>
											<?php
											if($user == null){
												echo '<input type="text" class="form-control" name="cek_user" placeholder="Masukkan Kode Person" autocomplete="off"/><br>'; 
												echo '<button type="submit" name="cek" value="cek" class="btn btn-info">Cek Upline</button>';
											}
                                            else{ 
											$query9 = sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$user."'") or die( print_r( sqlsrv_errors(), true));
											$hasil9 = sqlsrv_fetch_array($query9);
												echo '<input type="text" class="form-control" name="nama_dist" value="'.$hasil9['KodePersonManual'].' : '.$hasil9['NamaPerson'].'" required="required" readonly /><br>'; 
												echo '<a href="upgrade-user.php?'.base64_encode("proses").'&id='.base64_encode($id).'" name="cek" class="btn btn-info">Batal</a>';
											}
											?>
										</div>
									
									 if(isset($_POST['cek'])){
										$cek_user = @$_POST['cek_user'];
										if(empty($cek_user)){
											echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Person Upline Belum Diisi ", "error"); </script>';
										}else{
											if($statsprs === 'USER'){ $upline = 'AGEN'; }
											elseif($statsprs === 'RESELLER'){ $upline = 'DISTRIBUTOR'; }
											elseif($statsprs === 'AGEN'){ $upline = 'MARKETPLACE'; }
											elseif($statsprs === 'SUPPLIER'){ 
												if($spons === 'User'){ $upline = 'RESELLER'; }
												elseif($spons === 'Reseller'){ $upline = 'AGEN'; }
												elseif($spons === 'Agen'){ $upline = 'DISTRIBUTOR'; }
												elseif($spons === 'Distributor'){ $upline = 'MARKETPLACE'; }
											}
											
											$query = @sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE IsVerified = '1' AND StatusPerson = '$upline' AND (replace(KodePersonManual,' ','') = replace('$cek_user',' ','') OR replace(UserName,' ','') = replace('$cek_user',' ',''))", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$cari = @sqlsrv_num_rows($query);
											while($hasil = @sqlsrv_fetch_array($query)){ $kode = $hasil['KodePerson']; }
											if($cari === 0){
												echo '<script type="text/javascript">
												sweetAlert({ title: "Maaf!", text: " User Tidak Tersedia ", type: "error" },
												function () { window.location.href = "upgrade-user.php?'.base64_encode("proses").'&id='.base64_encode($id).'"; });
												</script>';
											}else{
												echo '<script type="text/javascript">window.location.href = "upgrade-user.php?page='.base64_encode("proses").'&id='.base64_encode($id).'&user='.base64_encode($kode).'";</script>';
											}

										}	
											
									} */
									?>
									</form>
										
                                    <hr>
										
								<form role="form" method="post">
									<?php
											/* if($user == null){
												echo '<input type="text" class="form-control" name="_kode_dist" autocomplete="off" required readonly />';
											}
                                            else{ 
											$query9 = sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$user."'") or die( print_r( sqlsrv_errors(), true));
											$hasil9 = sqlsrv_fetch_array($query9);
												echo '<input type="text" class="form-control" name="_kode_dist" autocomplete="off" value="'.$hasil9['KodePerson'].'" readonly />';
												select * from (select *, ROW_NUMBER() OVER (ORDER BY id_slider DESC) AS rownumber FROM mst_slider) a where a.is_aktif = 'TRUE' AND a.rownumber > 1 AND a.rownumber < 5
										} */?>
										
										
										<?php 
										if($statsprs === 'USER'){ 
											if($spons === 'Reseller'){ 
												$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_baru = $ketemu_prs['MemberSponsor']; }
											}
											elseif($spons === 'Agen'){ 
												$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_prs = $ketemu_prs['MemberSponsor']; 
													$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_prs'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline_baru = $ketemu_prs2['MemberSponsor']; }
												}
											}
											elseif($spons === 'Distributor'){
												$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_prs = $ketemu_prs['MemberSponsor']; 
													$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_prs'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline_prs2 = $ketemu_prs2['MemberSponsor']; 
														$cari_prs3 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_prs2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														while($ketemu_prs3 = @sqlsrv_fetch_array($cari_prs3, SQLSRV_FETCH_ASSOC)){ $upline_baru = $ketemu_prs3['MemberSponsor']; }
													}	
												}
											
											}	

											$cari_upline = @sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$upline_baru."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($hasil_cari = @sqlsrv_fetch_array($cari_upline)){  
													$upline_kode = $hasil_cari['KodePerson']; 
													$upline_kode2 = $hasil_cari['KodePersonManual']; 
													$upline_nama = $hasil_cari['NamaPerson']; 
													$upline_stats = $hasil_cari['StatusPerson']; 
											}
											
										}elseif($statsprs === 'RESELLER'){
											if($spons === 'Agen'){ 
												$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_baru = $ketemu_prs['MemberSponsor']; }
											}
											elseif($spons === 'Distributor'){ 
												$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_prs = $ketemu_prs['MemberSponsor']; 
													$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_prs'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline_baru = $ketemu_prs2['MemberSponsor']; }
												}
											}
											
											$cari_upline = @sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$upline_baru."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($hasil_cari = @sqlsrv_fetch_array($cari_upline)){  
													$upline_kode = $hasil_cari['KodePerson']; 
													$upline_kode2 = $hasil_cari['KodePersonManual']; 
													$upline_nama = $hasil_cari['NamaPerson']; 
													$upline_stats = $hasil_cari['StatusPerson']; 
											}
											
										}elseif($statsprs === 'AGEN'){
											if($spons === 'Distributor'){ 
												$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_baru = $ketemu_prs['MemberSponsor']; }
											}
											
											$cari_upline = @sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$upline_baru."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($hasil_cari = @sqlsrv_fetch_array($cari_upline)){  
													$upline_kode = $hasil_cari['KodePerson']; 
													$upline_kode2 = $hasil_cari['KodePersonManual']; 
													$upline_nama = $hasil_cari['NamaPerson']; 
													$upline_stats = $hasil_cari['StatusPerson']; 
											}
											
										}elseif($statsprs === 'SUPPLIER'){
											if($spons === 'User'){ $upline_prs = 'RESELLER'; }
											elseif($spons === 'Reseller'){ $upline_prs = 'AGEN'; }
											elseif($spons === 'Agen'){ $upline_prs = 'DISTRIBUTOR'; }
											elseif($spons === 'Distributor'){ $upline_prs = 'MARKETPLACE'; }
											
											$cek_upline2 = @sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE LEFT(replace(KodePerson,' ',''),5)='PRS-0' AND IsVerified = '1' AND StatusPerson = '$upline_prs'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$cek_jumlah2 = @sqlsrv_num_rows($cek_upline2); 
											$acak2 = rand(1,$cek_jumlah2);
														
											$cari_upline2 = @sqlsrv_query($dbconnect, "select * from (select *, ROW_NUMBER() OVER (ORDER BY KodePerson) AS rownumber FROM masterperson WHERE LEFT(replace(KodePerson,' ',''),5)='PRS-0' AND IsVerified = '1' AND StatusPerson = '$upline_prs') a where a.rownumber = '$acak2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											while($hasil_cari2 = @sqlsrv_fetch_array($cari_upline2)){ 
														
												$cari_upline = @sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE KodePerson = '".$hasil_cari2['KodePerson']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												while($hasil_cari = @sqlsrv_fetch_array($cari_upline)){  
													$upline_kode = $hasil_cari['KodePerson']; 
													$upline_kode2 = $hasil_cari['KodePersonManual']; 
													$upline_nama = $hasil_cari['NamaPerson']; 
													$upline_stats = $hasil_cari['StatusPerson']; 
												}
											}
										}
										
										?>
										<div class="form-group">
                                            <label>Kode Upline Baru</label>
                                            <input class="form-control" type="hidden" name="_kodeupline" value="<?php echo $upline_kode; ?>" autocomplete="off" readonly>
                                            <input class="form-control" type="text" name="_manualupline" value="<?php echo $upline_kode2; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama Upline Baru</label>
                                            <input class="form-control" type="text" name="_namaupline" value="<?php echo $upline_nama; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Status</label>
                                            <input class="form-control" type="text" name="_statsupline" value="<?php echo $upline_stats; ?>" autocomplete="off" readonly>
                                        </div>
										<?php /*if($statsprs === 'SUPPLIER' OR $statsprs === 'USER'){ ?>
										<hr><div class="form-group">
											<label>PIN Member</label>
											<input class="form-control" type="text" name="_kodepin" maxlength="18" placeholder="ex : GM20180000001" autocomplete="off" required>
										</div>
										<?php } else { echo ''; } */?> 
										<br><button type="submit" class="btn btn-default" name="_submit-input-upline">Upgrade User</button>
                                        <!-- <button type="reset" class="btn btn-default" name="_submit-input-upline">Reset</button> -->
									</div>
								</form>
									
									<?php
										include "../connections/config.php";
										$_kode = @htmlspecialchars($_POST['_kodeupline']); $_kode_pin = @htmlspecialchars($_POST['_kodepin']); 
										if(isset($_POST['_submit-input-upline'])){
											/* if($statsprs === 'SUPPLIER' OR $statsprs === 'USER'){
												$cek_kode = @sqlsrv_query($dbconnect, "select * from kode_verifikasi where replace(kode_transaksi,' ','') = replace('$_kode_pin',' ','') AND status = 'TRUE' AND is_dipakai = 'FALSE'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num_kode = @sqlsrv_num_rows($cek_kode);
											} elseif($statsprs === 'RESELLER' OR $statsprs === 'AGEN'){
												$num_kode = 1;
											}
												if($num_kode = 1){  */
													if($statsprs === 'USER'){ 
														$ganti = 'RESELLER'; 
														$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='RGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true)); 
													}
													elseif($statsprs === 'RESELLER'){ 
														$ganti = 'AGEN'; 
														$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='AGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true)); 
													}
													elseif($statsprs === 'AGEN'){ 
														$ganti = 'DISTRIBUTOR'; 
														$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='DGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
													}
													elseif($statsprs === 'SUPPLIER'){ 
														if($spons === 'User'){ 
															$ganti = 'USER'; $sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='UGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
														}
														elseif($spons === 'Reseller'){
															$ganti = 'RESELLER'; $sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='RGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
														}
														elseif($spons === 'Agen'){ 
															$ganti = 'AGEN'; $sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='AGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
														}
														elseif($spons === 'Distributor'){ 
															$ganti = 'DISTRIBUTOR'; $sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='DGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
														}
														
													}
													
													// membuat id otomatis 2
													$nums2 = @sqlsrv_num_rows($sql2); 
													while($data2 = @sqlsrv_fetch_array($sql2, SQLSRV_FETCH_ASSOC)){
														if($nums2 === 0){ $kode2 = 1; }else{ $kode2 = $data2['kode'] + 1; }
													}
													// membuat kode user 2
													$bikin_kode2 = str_pad($kode2, 10, "0", STR_PAD_LEFT);
													if($statsprs === 'USER'){ 
														$kode_jadi2 = "RGM-".date('Y')."-".$bikin_kode2;
													}
													elseif($statsprs === 'RESELLER'){
														$kode_jadi2 = "AGM-".date('Y')."-".$bikin_kode2;
													}
													elseif($statsprs === 'AGEN'){ 
														$kode_jadi2 = "DGM-".date('Y')."-".$bikin_kode2;
													}
													elseif($statsprs === 'SUPPLIER'){ 
														if($spons === 'User'){ $kode_jadi2 = "UGM-".date('Y')."-".$bikin_kode2; }
														elseif($spons === 'Reseller'){ $kode_jadi2 = "RGM-".date('Y')."-".$bikin_kode2; }
														elseif($spons === 'Agen'){ $kode_jadi2 = "AGM-".date('Y')."-".$bikin_kode2; }
														elseif($spons === 'Distributor'){ $kode_jadi2 = "DGM-".date('Y')."-".$bikin_kode2; }
														
													}
													
													if($ganti === 'USER'){
														$query = @sqlsrv_query($dbconnect, "update MasterPerson SET Referral = 'https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($id))."', IsVerified = '1', IsSupplier = 'False', KelompokHarga = 'POS', KodePersonManual = '$kode_jadi2', MemberID = '$kode_jadi2', StatusPerson = '$ganti', MemberSponsor = '$_kode', IsOnline = 'False', TglUpMember = '".date('Y-m-d H:i:s')."' where KodePerson = '$id'") or die( print_r( sqlsrv_errors(), true));
													}elseif($ganti === 'RESELLER'){ 
															$query = @sqlsrv_query($dbconnect, "update MasterPerson SET Referral = 'https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($id))."', IsVerified = '1', IsSupplier = 'False', KelompokHarga = 'UMUM', KodePersonManual = '$kode_jadi2', MemberID = '$kode_jadi2', StatusPerson = '$ganti', MemberSponsor = '$_kode', IsOnline = 'False', TglUpReseller = '".date('Y-m-d H:i:s')."' where KodePerson = '$id'") or die( print_r( sqlsrv_errors(), true)); 	 
														/* if($_kode_pin == null OR $_kode_pin === ''){
														} else {	
															$query = @sqlsrv_query($dbconnect, "update MasterPerson SET Referral = 'https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($id))."', IsVerified = '1', IsSupplier = 'False', KelompokHarga = 'UMUM', KodePersonManual = '$kode_jadi2', MemberID = '$kode_jadi2', StatusPerson = '$ganti', MemberSponsor = '$_kode', IsOnline = 'False', TglUpReseller = '".date('Y-m-d H:i:s')."', KodeTransaksi = '$_kode_pin' where KodePerson = '$id'") or die( print_r( sqlsrv_errors(), true)); 	 
														} */
													}elseif($ganti === 'AGEN'){ 
															$query = @sqlsrv_query($dbconnect, "update MasterPerson SET Referral = 'https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($id))."', IsVerified = '1', IsSupplier = 'False', KelompokHarga = 'GOL III', KodePersonManual = '$kode_jadi2', MemberID = '$kode_jadi2', StatusPerson = '$ganti', MemberSponsor = '$_kode', IsOnline = 'False', TglUpAgen = '".date('Y-m-d H:i:s')."' where KodePerson = '$id'") or die( print_r( sqlsrv_errors(), true)); 	 
														/* if($_kode_pin == null OR $_kode_pin === ''){
														} else {	
															$query = @sqlsrv_query($dbconnect, "update MasterPerson SET Referral = 'https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($id))."', IsVerified = '1', IsSupplier = 'False', KelompokHarga = 'GOL III', KodePersonManual = '$kode_jadi2', MemberID = '$kode_jadi2', StatusPerson = '$ganti', MemberSponsor = '$_kode', IsOnline = 'False', TglUpAgen = '".date('Y-m-d H:i:s')."', KodeTransaksi = '$_kode_pin' where KodePerson = '$id'") or die( print_r( sqlsrv_errors(), true)); 	 
														} */
													}elseif($ganti === 'DISTRIBUTOR'){ 
															$query = @sqlsrv_query($dbconnect, "update MasterPerson SET Referral = 'https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($id))."', IsVerified = '1', IsSupplier = 'False', KelompokHarga = 'GOL II', KodePersonManual = '$kode_jadi2', MemberID = '$kode_jadi2', StatusPerson = '$ganti', MemberSponsor = '$_kode', IsOnline = 'False', TglUpDistributor = '".date('Y-m-d H:i:s')."' where KodePerson = '$id'") or die( print_r( sqlsrv_errors(), true)); 	 
														/* if($_kode_pin == null OR $_kode_pin === ''){
														} else {	
															$query = @sqlsrv_query($dbconnect, "update MasterPerson SET Referral = 'https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($id))."', IsVerified = '1', IsSupplier = 'False', KelompokHarga = 'GOL II', KodePersonManual = '$kode_jadi2', MemberID = '$kode_jadi2', StatusPerson = '$ganti', MemberSponsor = '$_kode', IsOnline = 'False', TglUpDistributor = '".date('Y-m-d H:i:s')."', KodeTransaksi = '$_kode_pin' where KodePerson = '$id'") or die( print_r( sqlsrv_errors(), true)); 	 
														} */
													}
													
													if($query){
													// bagi hasil ulang
													$query_anyar = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$id."'") or die( print_r( sqlsrv_errors(), true));
													while($cari_anyar = @sqlsrv_fetch_array($query_anyar, SQLSRV_FETCH_ASSOC)){ 
														$upline_anyar = $cari_anyar['MemberSponsor']; $spons_anyar = $cari_anyar['MemberSponsorAsal'];
													
														// bagi hasil daftar distributor
														if($ganti === 'DISTRIBUTOR'){
															// bagi perusahaan dan sponsor
															@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi'
															where KodePersonDaftar = '".$id."' AND (Keterangan LIKE '%Administrasi Upgrade%' OR Keterangan = 'Perolehan sponsor asal') AND UserVerivication = 'Menunggu Verifikasi' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
															
															// hasil upline 1 dan 2
															if($upline_anyar === $sponsor){
																// bagi up 1
																@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = 'PRS-0000004'
																where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																// bagi up 2
																@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = 'PRS-0000004'
																where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
															} else {
																// bagi up 1
																@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																// bagi up 2
																@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = 'PRS-0000004'
																where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true));  
															}
															
															@sqlsrv_query($dbconnect, "update trvoucher set IsVerified = '1' WHERE KodePerson = '".$id."' AND Keterangan LIKE '%".strtoupper($spons)."%") or die( print_r( sqlsrv_errors(), true));
																														
														} elseif($ganti === 'AGEN'){
															// bagi perusahaan dan sponsor
															@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi'
															where KodePersonDaftar = '".$id."' AND (Keterangan LIKE '%Administrasi Upgrade%' OR Keterangan = 'Perolehan sponsor asal') AND UserVerivication = 'Menunggu Verifikasi' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
															
															if($upline_anyar === $sponsor){
																$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_anyar2 = $ketemu_prs['MemberSponsor']; 
																	if($upline_anyar2 === $sponsor){
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																	} else {
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar2."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																	}
																}
																
															} else {
																$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_anyar2 = $ketemu_prs['MemberSponsor']; 
																	if($upline_anyar2 === $sponsor){
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																	} else {
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																		where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																	}
																}
															}
															
															@sqlsrv_query($dbconnect, "update trvoucher set IsVerified = '1' WHERE KodePerson = '".$id."' AND Keterangan LIKE '%".strtoupper($spons)."%") or die( print_r( sqlsrv_errors(), true));
															
														}  elseif($ganti === 'RESELLER'){
															// bagi perusahaan dan sponsor
															@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi'
															where KodePersonDaftar = '".$id."' AND (Keterangan LIKE '%Administrasi Upgrade%' OR Keterangan = 'Perolehan sponsor asal') AND UserVerivication = 'Menunggu Verifikasi' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
															
															if($upline_anyar === $sponsor){
																$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_anyar2 = $ketemu_prs['MemberSponsor']; 
																	if($upline_anyar2 === $sponsor){
																		$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline_anyar3 = $ketemu_prs2['MemberSponsor']; 
																			if($upline_anyar3 === $sponsor){
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			} else {
																				// bagi up 1
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			}
																		}
																	} else {
																		$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline_anyar3 = $ketemu_prs2['MemberSponsor']; 
																			if($upline_anyar3 === $sponsor){
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar2."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar3'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			} else {
																				// bagi up 1
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar2."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			}
																		}
																	}
																}
																
															} else {
																$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline_anyar2 = $ketemu_prs['MemberSponsor']; 
																	if($upline_anyar2 === $sponsor){
																		$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline_anyar3 = $ketemu_prs2['MemberSponsor']; 
																			if($upline_anyar3 === $sponsor){
																				// bagi up 1
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			} else {
																				// bagi up 1
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			}
																		}
																	} else {
																		$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline_anyar3 = $ketemu_prs2['MemberSponsor']; 
																			if($upline_anyar3 === $sponsor){
																				// bagi up 1
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			} else {
																				// bagi up 1
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline satu' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																				// bagi up 2
																				$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline_anyar'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																				while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua_anyar = $ketemu_up2['MemberSponsor']; }
																				@sqlsrv_query($dbconnect, "update trdompet set IsVerified = '1', UserVerivication = 'Sudah Diverifikasi', KodePerson = '".$upline_dua_anyar."'
																				where KodePersonDaftar = '".$id."' AND Keterangan = 'Perolehan upline dua' AND IsVerified = '0'") or die( print_r( sqlsrv_errors(), true)); 
																			}
																		}
																	}
																}
															}
															
															@sqlsrv_query($dbconnect, "update trvoucher set IsVerified = '1' WHERE KodePerson = '".$id."' AND Keterangan LIKE '%".strtoupper($spons)."%") or die( print_r( sqlsrv_errors(), true));
														}
														}
															
														/*@sqlsrv_query($dbconnect, "update kode_verifikasi set is_dipakai = 'TRUE', KodePerson = '$id' where kode_transaksi = '$_kode_pin'") or die( print_r( sqlsrv_errors(), true)); */
														
														$cek_upline2 = @sqlsrv_query($dbconnect, "SELECT * FROM masterperson WHERE LEFT(replace(KodePerson,' ',''),5)='PRS-0' AND IsVerified = '1' AND StatusPerson = '$statsprs'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														$cek_jumlah2 = @sqlsrv_num_rows($cek_upline2); 
														$acak2 = rand(1,$cek_jumlah2);
														
														$cari_upline2 = @sqlsrv_query($dbconnect, "select * from (select *, ROW_NUMBER() OVER (ORDER BY KodePerson) AS rownumber FROM masterperson WHERE LEFT(replace(KodePerson,' ',''),5)='PRS-0' AND IsVerified = '1' AND StatusPerson = '$statprs') a where a.rownumber = '$acak2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														while($hasil_cari2 = @sqlsrv_fetch_array($cari_upline2)){ $upline_kode2 = $hasil_cari2['KodePerson']; }
														
														@sqlsrv_query($dbconnect, "update MasterPerson SET MemberSponsor = '$upline_kode2' where MemberSponsor = '$id'") or die( print_r( sqlsrv_errors(), true)); 	
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Upgrade User Tersimpan ", type: "success" },
														function () { window.location.href = "upgrade-user.php"; });
														</script>';
														/* function () { window.location.href = "upgrade-user.php?page='.htmlspecialchars(base64_encode("cari_downline")).'&id='.htmlspecialchars(base64_encode($id)).'"; }); */
													}
													else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Upgrade User Gagal ", "error"); </script>';
													}
												/* } else {
													echo '<script type="text/javascript">sweetAlert("Maaf!", " PIN Member Anda Belum Valid atau Sudah Terpakai ", "error"); </script>';
												} */
										
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
						
						<!-- /.panel-heading -->	
							
						<?php } elseif($id != null AND $page == 'cari_downline'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-arrow-down fa-fw"></i> Cari Downline
                        </div>
						<div class="panel-body">
							<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Status Person</label>
													<dd><?php echo $kodeprs2." : ".ucwords($namaprs)." (".$statsprs.") "; ?></dd>
											</div>
											
										</div>
									</div>
									
                            <table width="100%" class="table table-striped table-bordered table-hover" id="access-table">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="75%">Nama User</th>
                                        <th width="15%">Aksi</th>
                                    </tr>
                                </thead>
											
                                <tbody>
								<form action="upgrade-user.php?page=<?php echo htmlspecialchars(base64_encode('update_downline')); ?>&id=<?php echo htmlspecialchars(base64_encode($id)); ?>" method="post">
								<?php include "../connections/config.php";
								if($statsprs === 'RESELLER'){ $downline = 'USER'; }
								elseif($statsprs === 'AGEN'){ $downline = 'RESELLER'; }
								elseif($statsprs === 'DISTRIBUTOR'){ $downline = 'AGEN'; }
											
								$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where IsVerified = '1' AND StatusPerson = '$downline' AND LEFT(replace(MemberSponsor,' ',''),5) = 'PRS-0' AND LEFT(replace(KodePerson,' ',''),5) != 'PRS-0' order by NamaPerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											
								$no = 1;
								$hitung = @sqlsrv_num_rows($query);
								if($hitung > 0){
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
										<td><?php echo "<strong>".strtoupper($cari['NamaPerson'])."</strong><br>Kode Person : ".ucwords($cari['KodePersonManual'])."<br>UserName : ".ucwords($cari['UserName'])."<br>";
										$query2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$cari['MemberSponsor']."' order by KodePerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ echo "Sponsor : ".ucwords($cari2['NamaPerson'])." (".$cari2['KodePersonManual'].") "; } ?></td>
										<td align="center">
											<input type="checkbox" id="cekbox" name="cekbox[]" value="<?php echo $cari['KodePerson']; ?>"/>
											<input type="hidden" name="cekuser" value="<?php echo $id; ?>" readonly />
											<input type="hidden" value="<?php echo $cari['KodePerson']; ?>" readonly />
										</td>
										
                                    </tr>
								<?php $no++; }
								
								}else{
									echo '<tr class="odd gradeX"><td colspan=3>';
									echo '<div class="alert alert-info alert-dismissable"><h4><p align="center">Tidak Ada Data<br></p></h4></div><br/>';
									echo '</td></tr>';
								}
								@sqlsrv_close(); ?> 
								
                                </tbody>
								
								<input type="button" class="btn btn-md btn-warning" onclick="cek(this.form.cekbox)" value="Select All" />&nbsp;
								<input type="button" class="btn btn-md btn-danger" onclick="uncek(this.form.cekbox)" value="Clear All" />&nbsp;
								<input type="submit" class="btn btn-md btn-success" value="Simpan" name="submit" /><hr>
								
								</form>
								
                            </table>
							<a href="upgrade-user.php" class="btn btn-primary"><i class="fa fa-sign-out"></i>&nbsp;Keluar</a>
                    </div>
						
					<!-- /.panel-body -->
								
					<?php } elseif($id != null AND $page == 'bayar_registrasi'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Pembayaran Registrasi
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<?php $query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalDebet) as Totalbayar from trdompet where IsVerified = '1' AND Keterangan = 'Pembayaran Registrasi' AND KodePerson = '".$id."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
										if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
										} ?>
										
										<div class="form-group">
                                            <label>Kode User</label>
                                            <input class="form-control" type="text" value="<?php echo $kodeprs2.' : '.strtoupper($namaprs); ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Total Pembayaran Sebelumnya</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal" value="<?php echo number_format($total_bayar,0,"","."); ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nominal</label>
                                            <input class="form-control" style="text-align:right" type="text"  id="nominal2" name="_bayar" placeholder="0" autocomplete="off" required>
                                        </div><br>
										<button type="submit" class="btn btn-default" name="_submit-edit-bayar">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-bayar">Reset</button>
									</div>
								</form>
									
										<?php
										include "../connections/config.php";
										$_bayar = @htmlspecialchars($_POST['_bayar']); $_nilaibayar =  @str_replace(".", "", $_bayar);
										if(isset($_POST['_submit-edit-bayar'])){
										$jumlah = $total_bayar+$_nilaibayar;
											if($jumlah > $value_reg){
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Jumlah Pembayaran Anda Lebih ", "error"); </script>';
											}else{
												// buat id
												$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(NoTransaksi,7)) AS kode FROM trdompet WHERE LEFT(replace(NoTransaksi,' ',''),12)='TKD-".date('Ymd')."'") or die( print_r( sqlsrv_errors(), true)); 
												$nums = @sqlsrv_num_rows($sql); 
												while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
													if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
												}
												$bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
												$kode_jadi = "TKD-".date('Ymd')."-".$bikin_kode;
												
												$query = @sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,KodePerson)
												values('$kode_jadi','".date('Y-m-d H:i:s')."','$_nilaibayar','0','Pembayaran Registrasi','1','".$id."')") or die( print_r( sqlsrv_errors(), true)); 	 
													if($query){
														echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Input Pembayaran Tersimpan ", type: "success" },
														function () { window.location.href = "upgrade-user.php"; });
														</script>';
													}
												
											}
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
                        <!-- /.panel-heading -->
				
				<?php } elseif($id != null AND $page == 'bayar_garansi'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Administrasi User
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<?php $query_bayar = @sqlsrv_query($dbconnect, "select SUM(NominalDebet) as Totalbayar from trdompet where IsVerified = '1' AND Keterangan LIKE '%Administrasi Upgrade%' AND KodePerson = '".$id."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
										while($cari_bayar = @sqlsrv_fetch_array($query_bayar, SQLSRV_FETCH_ASSOC)){ 
										if($cari_bayar['Totalbayar'] == null ){ $total_bayar = 0; } else { $total_bayar = $cari_bayar['Totalbayar']; }
										} ?>
										
										<div class="form-group">
                                            <label>Kode User</label>
                                            <input class="form-control" type="text" value="<?php echo $kodeprs2.' : '.strtoupper($namaprs); ?>" autocomplete="off" readonly>
                                        </div>
										<!-- <div class="form-group">
                                            <label>Saldo Garansi Komitmen</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal" value="<?php /* echo number_format($total_bayar,0,"","."); */ ?>" autocomplete="off" readonly>
                                        </div> -->
										<div class="form-group">
                                            <label>Nominal</label>
                                            <input class="form-control" style="text-align:right" type="text" id="nominal" name="_bayar" placeholder="0" autocomplete="off" required>
                                        </div>
										<div class="form-group">
											<?php if($spons === 'Distributor'){  echo '<span>* Nominal Administrasi adalah Rp. '.number_format($value_dst).' </span>'; }
											elseif($spons === 'Agen'){ echo '<span>* Nominal Administrasi adalah Rp. '.number_format($value_agn).' </span>'; }
											elseif($spons === 'Reseller'){ echo '<span>* Nominal Administrasi adalah Rp. '.number_format($value_res).' </span>'; } ?>
                                        </div><br>
										<button type="submit" class="btn btn-default" name="_submit-edit-bayar">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-bayar">Reset</button>
									</div>
								</form>
									
										<?php
										include "../connections/config.php";
										$_bayar = @htmlspecialchars($_POST['_bayar']); $_nilaibayar =  @str_replace(".", "", $_bayar);
										if($spons === 'Distributor'){ $administrasi = $value_dst; }
										elseif($spons === 'Agen'){ $administrasi = $value_agn; }
										elseif($spons === 'Reseller'){ $administrasi = $value_res; }
										
										if(isset($_POST['_submit-edit-bayar'])){
										$jumlah = $_nilaibayar;
											if($jumlah !== $administrasi){
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Nominal Administrasi Tidak Sesuai ", "error"); </script>';
											}else{
												$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(NoTransaksi,7)) AS kode FROM trdompet WHERE LEFT(replace(NoTransaksi,' ',''),12)='TKD-".date('Ymd')."'") or die( print_r( sqlsrv_errors(), true)); 
												$nums = @sqlsrv_num_rows($sql); 
												while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
													if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
												}
												$bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
												$kode_jadi = "TKD-".date('Ymd')."-".$bikin_kode;
												
												// bagi hasil daftar distributor
												if($spons === 'Distributor'){
													$bagi_daftar = (70/100)*$_nilaibayar; $bagi_spons = (20/100)*$_nilaibayar; $bagi_up = (5/100)*$_nilaibayar;
													// voucher
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,Foto,KodeBank,KodePerson,KodePersonDaftar)
													values('$kode_jadi','".date('Y-m-d H:i:s')."','$_nilaibayar','0','Administrasi Upgrade ".strtoupper($spons)."','0','Menunggu Verifikasi','-','-','".$id."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil perusahaan
													$kode2 = $bikin_kode+1; $panjang_kode2 = (7 - (strlen($kode2)));
													$kode_baru2 = substr('0000000',0,$panjang_kode2) ;
													$bikin_kode2 = $kode_baru2.$kode2;
													$kode_jadi2 = "TKD-".date('Ymd')."-".$bikin_kode2;
																										
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
													values('$kode_jadi2','".date('Y-m-d H:i:s')."','$bagi_daftar','0','Administrasi Upgrade ".strtoupper($spons)."','0','Menunggu Verifikasi','PRS-0000004','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil sponsor
													$kode3 = $bikin_kode+2; $panjang_kode3 = (7 - (strlen($kode3)));
													$kode_baru3 = substr('0000000',0,$panjang_kode3) ;
													$bikin_kode3 = $kode_baru3.$kode3;
													$kode_jadi3 = "TKD-".date('Ymd')."-".$bikin_kode3;
													
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
													values('$kode_jadi3','".date('Y-m-d H:i:s')."','$bagi_spons','0','Perolehan sponsor asal','0','Menunggu Verifikasi','".$sponsor."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil upline 1 dan 2
													$kode4 = $bikin_kode+3; $panjang_kode4 = (7 - (strlen($kode4)));
													$kode_baru4 = substr('0000000',0,$panjang_kode4) ;
													$bikin_kode4 = $kode_baru4.$kode4;
													$kode_jadi4 = "TKD-".date('Ymd')."-".$bikin_kode4;
													
													$kode5 = $bikin_kode+4; $panjang_kode5 = (7 - (strlen($kode5)));
													$kode_baru5 = substr('0000000',0,$panjang_kode5) ;
													$bikin_kode5 = $kode_baru5.$kode5;
													$kode_jadi5 = "TKD-".date('Ymd')."-".$bikin_kode5;
													
													if($upline === $sponsor){
														// bagi up 1
														@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
														values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','PRS-0000004','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
														// bagi up 2
														@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
														values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','PRS-0000004','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													} else {
														// bagi up 1
														@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
														values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
														// bagi up 2
														@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
														values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','PRS-0000004','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													}
													
													// membuat id otomatis
													$sql_vc = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(NoTransaksi,5)) AS kode FROM trvoucher WHERE LEFT(replace(NoTransaksi,' ',''),12)='TVC-".date('Ymd')."'") or die( print_r( sqlsrv_errors(), true)); 
													$nums_vc = @sqlsrv_num_rows($sql_vc); 
													while($data_vc = @sqlsrv_fetch_array($sql_vc, SQLSRV_FETCH_ASSOC)){
														if($nums_vc === 0){ $kode_vc = 1; }else{ $kode_vc = $data_vc['kode'] + 1; }
													}
													$bikin_kode_vc = str_pad($kode_vc, 5, "0", STR_PAD_LEFT);
													$kode_jadi_vc = "TVC-".date('Ymd')."-".$bikin_kode_vc;
													
													// buat transaksi voucher
													$no = 0;
													$query_voucher = @sqlsrv_query($dbconnect, "select * from KategoriVoucher where IsAktif = 'True' AND StatusPerson = '$spons'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($cari_voucher = @sqlsrv_fetch_array($query_voucher, SQLSRV_FETCH_ASSOC)){ 
														// kode nambah
														$kode2_vc = $bikin_kode_vc+$no; $panjang_kode2_vc = (5 - (strlen($kode2_vc)));
														$kode_baru2_vc = substr('00000',0,$panjang_kode2_vc) ;
														$bikin_kode2_vc = $kode_baru2_vc.$kode2_vc;
														$kode_jadi2_vc = "TVC-".date('Ymd')."-".$bikin_kode2_vc;
														
														if($spons === 'Distributor'){ $durasi = $value18; }
														elseif($spons === 'Agen'){ $durasi = $value17; }
														elseif($spons === 'Reseller'){ $durasi = $value16; }
														else{ $durasi = 0; }
														
														$cari_tgl = 86400*$durasi;
														$tgl_baru = $tgl_vc_awal+$cari_tgl;
														$tgl_vc_akhir = date('Y-m-d H:i:s', $tgl_baru);
														
														$not_aktif = @sqlsrv_query($dbconnect, "update trvoucher set IsAktif = '0' where KodePerson = '".$id."' AND Keterangan LIKE '%".$statsprs."%'") or die( print_r( sqlsrv_errors(), true));
														if($not_aktif){
															@sqlsrv_query($dbconnect, "insert into trvoucher(NoTransaksi,KodePerson,TanggalTransaksi,Debet,Kredit,Keterangan,IsVerified,KodeVoucher,NoTransaksiDompet,MasaBerlaku,IsAktif)
															values('".$kode_jadi2_vc."','".$id."','".$tgl_daftar."','".$cari_voucher['NominalVoucher']."','0','Administrasi Upgrade ".strtoupper($spons)."','1','".$cari_voucher['KodeVoucher']."','".$kode_jadi."','".$tgl_vc_akhir."','1')") or die( print_r( sqlsrv_errors(), true)); 
														}  
													$no++;
													}
													
													echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Input Administrasi Tersimpan ", type: "success" },
														function () { window.location.href = "upgrade-user.php"; });
														</script>';
													
												} elseif($spons === 'Agen'){
													$bagi_daftar = (70/100)*$_nilaibayar; $bagi_spons = (20/100)*$_nilaibayar; $bagi_up = (5/100)*$_nilaibayar;
													// voucher
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,Foto,KodeBank,KodePerson,KodePersonDaftar)
													values('$kode_jadi','".date('Y-m-d H:i:s')."','$_nilaibayar','0','Administrasi Upgrade ".strtoupper($spons)."','0','Menunggu Verifikasi','-','-','".$id."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil perusahaan
													$kode2 = $bikin_kode+1; $panjang_kode2 = (7 - (strlen($kode2)));
													$kode_baru2 = substr('0000000',0,$panjang_kode2) ;
													$bikin_kode2 = $kode_baru2.$kode2;
													$kode_jadi2 = "TKD-".date('Ymd')."-".$bikin_kode2;
																										
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
													values('$kode_jadi2','".date('Y-m-d H:i:s')."','$bagi_daftar','0','Administrasi Upgrade ".strtoupper($spons)."','0','Menunggu Verifikasi','PRS-0000004','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil sponsor
													$kode3 = $bikin_kode+2; $panjang_kode3 = (7 - (strlen($kode3)));
													$kode_baru3 = substr('0000000',0,$panjang_kode3) ;
													$bikin_kode3 = $kode_baru3.$kode3;
													$kode_jadi3 = "TKD-".date('Ymd')."-".$bikin_kode3;
													
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
													values('$kode_jadi3','".date('Y-m-d H:i:s')."','$bagi_spons','0','Perolehan sponsor asal','0','Menunggu Verifikasi','".$sponsor."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil upline 1 dan 2
													$kode4 = $bikin_kode+3; $panjang_kode4 = (7 - (strlen($kode4)));
													$kode_baru4 = substr('0000000',0,$panjang_kode4) ;
													$bikin_kode4 = $kode_baru4.$kode4;
													$kode_jadi4 = "TKD-".date('Ymd')."-".$bikin_kode4;
													
													$kode5 = $bikin_kode+4; $panjang_kode5 = (7 - (strlen($kode5)));
													$kode_baru5 = substr('0000000',0,$panjang_kode5) ;
													$bikin_kode5 = $kode_baru5.$kode5;
													$kode_jadi5 = "TKD-".date('Ymd')."-".$bikin_kode5;
													
													if($upline === $sponsor){
														$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline2 = $ketemu_prs['MemberSponsor']; 
															if($upline2 === $sponsor){
																// bagi up 1
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																// bagi up 2
																$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
															} else {
																// bagi up 1
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline2."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																// bagi up 2
																$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
															}
														}
														
													} else {
														$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline2 = $ketemu_prs['MemberSponsor']; 
															if($upline2 === $sponsor){
																// bagi up 1
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																// bagi up 2
																$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
															} else {
																// bagi up 1
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																// bagi up 2
																$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
															}
														}
													}
													
													// membuat id otomatis
													$sql_vc = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(NoTransaksi,5)) AS kode FROM trvoucher WHERE LEFT(replace(NoTransaksi,' ',''),12)='TVC-".date('Ymd')."'") or die( print_r( sqlsrv_errors(), true)); 
													$nums_vc = @sqlsrv_num_rows($sql_vc); 
													while($data_vc = @sqlsrv_fetch_array($sql_vc, SQLSRV_FETCH_ASSOC)){
														if($nums_vc === 0){ $kode_vc = 1; }else{ $kode_vc = $data_vc['kode'] + 1; }
													}
													$bikin_kode_vc = str_pad($kode_vc, 5, "0", STR_PAD_LEFT);
													$kode_jadi_vc = "TVC-".date('Ymd')."-".$bikin_kode_vc;
													
													// buat transaksi voucher
													$no = 0;
													$query_voucher = @sqlsrv_query($dbconnect, "select * from KategoriVoucher where IsAktif = 'True' AND StatusPerson = '$spons'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($cari_voucher = @sqlsrv_fetch_array($query_voucher, SQLSRV_FETCH_ASSOC)){ 
														// kode nambah
														$kode2_vc = $bikin_kode_vc+$no; $panjang_kode2_vc = (5 - (strlen($kode2_vc)));
														$kode_baru2_vc = substr('00000',0,$panjang_kode2_vc) ;
														$bikin_kode2_vc = $kode_baru2_vc.$kode2_vc;
														$kode_jadi2_vc = "TVC-".date('Ymd')."-".$bikin_kode2_vc;
														
														if($spons === 'Distributor'){ $durasi = $value18; }
														elseif($spons === 'Agen'){ $durasi = $value17; }
														elseif($spons === 'Reseller'){ $durasi = $value16; }
														else{ $durasi = 0; }
														
														$cari_tgl = 86400*$durasi;
														$tgl_baru = $tgl_vc_awal+$cari_tgl;
														$tgl_vc_akhir = date('Y-m-d H:i:s', $tgl_baru);
														
														$not_aktif = @sqlsrv_query($dbconnect, "update trvoucher set IsAktif = '0' where KodePerson = '".$id."' AND Keterangan LIKE '%".$statsprs."%'") or die( print_r( sqlsrv_errors(), true));
														if($not_aktif){
															@sqlsrv_query($dbconnect, "insert into trvoucher(NoTransaksi,KodePerson,TanggalTransaksi,Debet,Kredit,Keterangan,IsVerified,KodeVoucher,NoTransaksiDompet,MasaBerlaku,IsAktif)
															values('".$kode_jadi2_vc."','".$id."','".$tgl_daftar."','".$cari_voucher['NominalVoucher']."','0','Administrasi Upgrade ".strtoupper($spons)."','1','".$cari_voucher['KodeVoucher']."','".$kode_jadi."','".$tgl_vc_akhir."','1')") or die( print_r( sqlsrv_errors(), true)); 
														} 
													$no++;
													}
													
													echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Input Administrasi Tersimpan ", type: "success" },
														function () { window.location.href = "upgrade-user.php"; });
														</script>';
													
												}  elseif($spons === 'Reseller'){
													$bagi_daftar = (70/100)*$_nilaibayar; $bagi_spons = (20/100)*$_nilaibayar; $bagi_up = (5/100)*$_nilaibayar;
													// voucher
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,Foto,KodeBank,KodePerson,KodePersonDaftar)
													values('$kode_jadi','".date('Y-m-d H:i:s')."','$_nilaibayar','0','Administrasi Upgrade ".strtoupper($spons)."','0','Menunggu Verifikasi','-','-','".$id."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil perusahaan
													$kode2 = $bikin_kode+1; $panjang_kode2 = (7 - (strlen($kode2)));
													$kode_baru2 = substr('0000000',0,$panjang_kode2) ;
													$bikin_kode2 = $kode_baru2.$kode2;
													$kode_jadi2 = "TKD-".date('Ymd')."-".$bikin_kode2;
																										
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
													values('$kode_jadi2','".date('Y-m-d H:i:s')."','$bagi_daftar','0','Administrasi Upgrade ".strtoupper($spons)."','0','Menunggu Verifikasi','PRS-0000004','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil sponsor
													$kode3 = $bikin_kode+2; $panjang_kode3 = (7 - (strlen($kode3)));
													$kode_baru3 = substr('0000000',0,$panjang_kode3) ;
													$bikin_kode3 = $kode_baru3.$kode3;
													$kode_jadi3 = "TKD-".date('Ymd')."-".$bikin_kode3;
													
													@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
													values('$kode_jadi3','".date('Y-m-d H:i:s')."','$bagi_spons','0','Perolehan sponsor asal','0','Menunggu Verifikasi','".$sponsor."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
													
													// hasil upline 1 dan 2
													$kode4 = $bikin_kode+3; $panjang_kode4 = (7 - (strlen($kode4)));
													$kode_baru4 = substr('0000000',0,$panjang_kode4) ;
													$bikin_kode4 = $kode_baru4.$kode4;
													$kode_jadi4 = "TKD-".date('Ymd')."-".$bikin_kode4;
													
													$kode5 = $bikin_kode+4; $panjang_kode5 = (7 - (strlen($kode5)));
													$kode_baru5 = substr('0000000',0,$panjang_kode5) ;
													$bikin_kode5 = $kode_baru5.$kode5;
													$kode_jadi5 = "TKD-".date('Ymd')."-".$bikin_kode5;
													
													if($upline === $sponsor){
														$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline2 = $ketemu_prs['MemberSponsor']; 
															if($upline2 === $sponsor){
																$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline3 = $ketemu_prs2['MemberSponsor']; 
																	if($upline3 === $sponsor){
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	} else {
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	}
																}
															} else {
																$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline3 = $ketemu_prs2['MemberSponsor']; 
																	if($upline3 === $sponsor){
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline2."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline3'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	} else {
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline2."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	}
																}
															}
														}
														
													} else {
														$cari_prs = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														while($ketemu_prs = @sqlsrv_fetch_array($cari_prs, SQLSRV_FETCH_ASSOC)){ $upline2 = $ketemu_prs['MemberSponsor']; 
															if($upline2 === $sponsor){
																$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline3 = $ketemu_prs2['MemberSponsor']; 
																	if($upline3 === $sponsor){
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	} else {
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	}
																}
															} else {
																$cari_prs2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline2'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																while($ketemu_prs2 = @sqlsrv_fetch_array($cari_prs2, SQLSRV_FETCH_ASSOC)){ $upline3 = $ketemu_prs2['MemberSponsor']; 
																	if($upline3 === $sponsor){
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	} else {
																		// bagi up 1
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi4','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline satu','0','Menunggu Verifikasi','".$upline."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																		// bagi up 2
																		$cari_up2 = @sqlsrv_query($dbconnect, "select MemberSponsor from MasterPerson where KodePerson = '$upline'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
																		while($ketemu_up2 = @sqlsrv_fetch_array($cari_up2, SQLSRV_FETCH_ASSOC)){ $upline_dua = $ketemu_up2['MemberSponsor']; }
																		@sqlsrv_query($dbconnect, "insert into trdompet(NoTransaksi,TanggalTransaksi,NominalDebet,NominalKredit,Keterangan,IsVerified,UserVerivication,KodePerson,KodePersonDaftar)
																		values('$kode_jadi5','".date('Y-m-d H:i:s')."','$bagi_up','0','Perolehan upline dua','0','Menunggu Verifikasi','".$upline_dua."','".$id."')") or die( print_r( sqlsrv_errors(), true)); 
																	}
																}
															}
														}
													}
													
													// membuat id otomatis
													$sql_vc = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(NoTransaksi,5)) AS kode FROM trvoucher WHERE LEFT(replace(NoTransaksi,' ',''),12)='TVC-".date('Ymd')."'") or die( print_r( sqlsrv_errors(), true)); 
													$nums_vc = @sqlsrv_num_rows($sql_vc); 
													while($data_vc = @sqlsrv_fetch_array($sql_vc, SQLSRV_FETCH_ASSOC)){
														if($nums_vc === 0){ $kode_vc = 1; }else{ $kode_vc = $data_vc['kode'] + 1; }
													}
													$bikin_kode_vc = str_pad($kode_vc, 5, "0", STR_PAD_LEFT);
													$kode_jadi_vc = "TVC-".date('Ymd')."-".$bikin_kode_vc;
													
													// buat transaksi voucher
													$no = 0;
													$query_voucher = @sqlsrv_query($dbconnect, "select * from KategoriVoucher where IsAktif = 'True' AND StatusPerson = '$spons'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													while($cari_voucher = @sqlsrv_fetch_array($query_voucher, SQLSRV_FETCH_ASSOC)){ 
														// kode nambah
														$kode2_vc = $bikin_kode_vc+$no; $panjang_kode2_vc = (5 - (strlen($kode2_vc)));
														$kode_baru2_vc = substr('00000',0,$panjang_kode2_vc) ;
														$bikin_kode2_vc = $kode_baru2_vc.$kode2_vc;
														$kode_jadi2_vc = "TVC-".date('Ymd')."-".$bikin_kode2_vc;
														
														if($spons === 'Distributor'){ $durasi = $value18; }
														elseif($spons === 'Agen'){ $durasi = $value17; }
														elseif($spons === 'Reseller'){ $durasi = $value16; }
														else{ $durasi = 0; }
														
														$cari_tgl = 86400*$durasi;
														$tgl_baru = $tgl_vc_awal+$cari_tgl;
														$tgl_vc_akhir = date('Y-m-d H:i:s', $tgl_baru);
														
														$not_aktif = @sqlsrv_query($dbconnect, "update trvoucher set IsAktif = '0' where KodePerson = '".$id."' AND Keterangan LIKE '%".$statsprs."%'") or die( print_r( sqlsrv_errors(), true));
														if($not_aktif){
															@sqlsrv_query($dbconnect, "insert into trvoucher(NoTransaksi,KodePerson,TanggalTransaksi,Debet,Kredit,Keterangan,IsVerified,KodeVoucher,NoTransaksiDompet,MasaBerlaku,IsAktif)
															values('".$kode_jadi2_vc."','".$id."','".$tgl_daftar."','".$cari_voucher['NominalVoucher']."','0','Administrasi Upgrade ".strtoupper($spons)."','1','".$cari_voucher['KodeVoucher']."','".$kode_jadi."','".$tgl_vc_akhir."','1')") or die( print_r( sqlsrv_errors(), true)); 
														}
													$no++;
													}
													
													echo '<script type="text/javascript">
														sweetAlert({ title: "Berhasil!", text: " Input Administrasi Tersimpan ", type: "success" },
														function () { window.location.href = "upgrade-user.php"; });
														</script>';
													
												}
												
												
											}
										}
										@sqlsrv_close;
									
										?>
									</div>
									</div>
									
									<?php } elseif($idnota != null AND $page == 'bukti'){  ?>
									<div class="panel-heading">
										<i class="fa fa-search fa-fw"></i> Bukti Transfer
									</div>
									<div class='panel-body'>
										<div class="row">
											<div class="col-lg-3">
												<?php if($gbr === '' OR $gbr === '-' OR $gbr == null){
													echo '<img class="img-thumbnail" src="../android_grosirmart/img/bukti/no-image.png" width="100%"/><br><br>';
												} else {
													echo '<img class="img-thumbnail" src="../android_grosirmart/img/bukti/'.$gbr.'" width="100%"/><br><br>';
												} ?>
											</div>
											<div class="col-lg-4">
												<?php $query2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$prs."' order by KodePerson", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC); ?>
												<div class="form-group">
													<label>Nama Person</label>
														<dd><?php echo ucwords($cari2['NamaPerson']); ?></dd>
														<dd><?php echo ucwords($cari2['KodePersonManual']); ?></dd>
														<dd><?php echo ucwords($cari2['StatusPerson']); ?></dd>
												</div>
												<div class="form-group">
													<label>Tanggal Transaksi</label>
														<dd><?php echo TanggalIndo(DATE_FORMAT($tgl,'Y-m-d'))."</strong><br>"; ?></dd>
												</div>
												<div class="form-group">
													<label>Nama Bank</label>
													<?php $query4 = @sqlsrv_query($dbconnect, "select NamaBank from mstbank where KodeBank = '".$bank."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
														if($bank == null OR $bank === '' OR $bank === '-'){ 
															echo "<dd>Pembayaran Lewat ADMIN</dd>"; 
														} else { 
															while($cari4 = @sqlsrv_fetch_array($query4, SQLSRV_FETCH_ASSOC)){ 
															echo "<dd>".ucwords($cari4['NamaBank'])."</dd>"; 
														} 
													} ?>
												
												</div>
												<div class="form-group">
													<label>Nilai Pembayaran</label>
														<dd><?php echo "Rp ".number_format($topup)."</strong><br>"; ?></dd>
												</div>
												
											</div>
										</div>
									</div>
									
									<?php } ?>
                              
						
					
					<?php if($id != null AND $page == 'update_downline'){ 
							// update akses
							$cekbox = @$_POST['cekbox']; $cekuser = @$_POST['cekuser'];
							if($cekbox) {
								foreach ($cekbox as $value) {
									@sqlsrv_query($dbconnect, "update MasterPerson SET MemberSponsor = '$cekuser' where KodePerson = '$value'") or die( print_r( sqlsrv_errors(), true));
								}
								echo '<script type="text/javascript">sweetAlert({ title: "Sukses!", text: " Set Downline Berhasil ", type: "success" },
								function () { window.location.href = "upgrade-user.php"; }); </script>';
							}else {
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Anda Belum Memilih Downline ", type: "error" },
								function () { window.location.href = "upgrade-user.php?page='.htmlspecialchars(base64_encode("cari_downline")).'&id='.htmlspecialchars(base64_encode($id)).'"; }); </script>';
							} 
					} ?>
					
					</div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
	</script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Upgrade User Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Lanjut!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
	<!-- membuat dropdown bertingkat tab I -->
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		  $("#prov").change(function(){
			var KodeProv = $("#prov option:selected").val();
			$.ajax({ url: "../get-data/get-kab.php", data: "get_prov="+KodeProv, cache: false, success: function(msg){ $("#kab").html(msg); } });
		  });
		  $("#kab").change(function(){
			var KodeProv = $("#prov option:selected").val();
			var KodeKab = $("#kab option:selected").val();
			$.ajax({ url: "../get-data/get-kec.php", data: "get_prov="+KodeProv+"&get_kab="+KodeKab, cache: false, success: function(msg){ $("#kec").html(msg); } });
		  });
		  $("#kec").change(function(){
			var KodeProv = $("#prov option:selected").val();
			var KodeKab = $("#kab option:selected").val();
			var KodeKec = $("#kec option:selected").val();
			$.ajax({ url: "../get-data/get-desa.php", data: "get_prov="+KodeProv+"&get_kab="+KodeKab+"&get_kec="+KodeKec, cache: false, success: function(msg){ $("#desa").html(msg); } });
		  });
		});
	</script>
	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>
	
	<script text="javascript">
			/* Tanpa Rupiah */
			var tanpa_rupiah = document.getElementById('nominal');
			var tanpa_rupiah2 = document.getElementById('nominal2');
			var tanpa_rupiah3 = document.getElementById('nominal3');
			
			tanpa_rupiah.addEventListener('keyup', function(e)
			{
				tanpa_rupiah.value = formatRupiah(this.value);
			});
			
			tanpa_rupiah.addEventListener('keydown', function(event)
			{
				limitCharacter(event);
			});
			
			tanpa_rupiah2.addEventListener('keyup', function(e)
			{
				tanpa_rupiah2.value = formatRupiah(this.value);
			});
			
			tanpa_rupiah2.addEventListener('keydown', function(event)
			{
				limitCharacter(event);
			});
			
			tanpa_rupiah3.addEventListener('keyup', function(e)
			{
				tanpa_rupiah3.value = formatRupiah(this.value);
			});
			
			tanpa_rupiah3.addEventListener('keydown', function(event)
			{
				limitCharacter(event);
			});
			
			/* Fungsi */
			function formatRupiah(bilangan, prefix)
			{
				var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
					split	= number_string.split(','),
					sisa 	= split[0].length % 3,
					rupiah 	= split[0].substr(0, sisa),
					ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
					
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
			}
			
			function limitCharacter(event)
			{
				key = event.which || event.keyCode;
				if ( key != 188 // Comma
					 && key != 8 // Backspace
					 && key != 9 // Tab
					 && key != 116 // F5
					 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
					 && (key < 48 || key > 57) // Non digit
					 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
					) 
				{
					event.preventDefault();
					return false;
				}
			}
			</script>

</body>

</html>