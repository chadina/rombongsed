<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
// $fitur_id = 22;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @mysqli_query($con, "select * from systemsetting where KodeSetting = '1'");
while($cari = @mysqli_fetch_array($query)){ 
	$kodeset = $cari['KodeSetting']; $namaset = $cari['nama_setting']; $value = $cari['ValueData'];
}

$query = @mysqli_query($con, "select * from systemsetting where KodeSetting = '5'");
while($cari = @mysqli_fetch_array($query)){ 
	$kodeset9 = $cari['KodeSetting']; $namaset9 = $cari['nama_setting']; $value9 = $cari['ValueData'];
}

$query = @mysqli_query($con, "select * from systemsetting where KodeSetting = '11'");
while($cari = @mysqli_fetch_array($query)){ 
	$kodeset11 = $cari['KodeSetting']; $namaset11 = $cari['nama_setting']; $value11 = $cari['ValueData'];
}

@mysqli_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- CK Editor -->
	<script type="text/javascript" src="../dist/ckeditor-full/ckeditor.js"></script>
	<link href="../dist/ckeditor-img/css/style.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Setting Page
					<?php /* if($id == null AND $page == null){ 
						echo "<a href='mst-file.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					} */
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#header" data-toggle="tab">Header</a></li>
						<li><a href="#profil" data-toggle="tab">Profil</a></li>
						
						<li><a href="#kontak" data-toggle="tab">Kontak</a>
					</ul><br>
					
					<div class="tab-content">
                    <div class="tab-pane fade in active" id="header">
					<div class="panel panel-default">
						<?php /* if($id == null AND $page == null){ */ ?>
						<div class="panel-heading">
                            <i class="fa fa-wrench fa-fw"></i> Setting Header dan Footer
                        </div>
                        <!-- /.panel-heading -->
						<div class="panel-body table-responsive">
                            <div class="row">
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Ganti Header</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kodeset);?>&img=<?php echo base64_encode($value);?>&type=<?php echo base64_encode('_header');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
													<input name="ImageFile" id="_imageInput" type="file" /><br/>
													<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output"></div></div></div>
											</div>
											<div class="form-group">
												<img class="imgl img-thumbnail" src="../img/romsed/<?php echo $value; ?>" id="_oldimg" width="50%" /><br>
												<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
											</div>
										</div>
										</div>
								</div>
								<div class="col-lg-6">		
								    <form role="form" method="post" id="edit"  enctype="multipart/form-data">
										<div class="form-group">
                                            <label>Tagline</label>
                                            <?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '2' AND nama_setting = 'tagline'");
											while($row = @mysqli_fetch_array($edit)){
												echo '<input class="form-control" type="text" name="_tagline" autocomplete="off" value="'.$row['ValueData'].'" required="required" />';
											} ?>
                                        </div>
										<div class="form-group">
                                            <label>Copyright</label>
                                            <?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '4'") ;
											while($row = @mysqli_fetch_array($edit)){
												echo '<input class="form-control" type="text" name="_copyright" autocomplete="off" value="'.$row['ValueData'].'" required="required" />';
											} ?>
                                        </div>
										<div class="form-group">
                                            <label>Design</label>
                                            <?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '10' AND nama_setting = 'design'") ;
											while($row = @mysqli_fetch_array($edit)){
												echo '<input class="form-control" type="text" name="_design" autocomplete="off" value="'.$row['ValueData'].'" required="required" />';
											} ?>
                                        </div>
										
										<button type="submit" class="btn btn-default" name="_submit-edit-head">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-head">Reset</button>
										<?php
										include "../connections/config.php";
										$_tag = @htmlspecialchars($_POST['_tagline']); $_copyright = @htmlspecialchars($_POST['_copyright']); $_design = @htmlspecialchars($_POST['_design']);
										if(isset($_POST['_submit-edit-head'])){
											$query1 = @mysqli_query($con, "update systemsetting set ValueData = '$_tag' where KodeSetting = '2'") ; 	 
											$query2 = @mysqli_query($con, "update systemsetting set ValueData = '$_copyright' where KodeSetting = '4'") ; 	 
											$query3 = @mysqli_query($con, "update systemsetting set ValueData = '$_design' where KodeSetting = '10'"); 	 
												if($query1 OR $query2 OR $query3){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-setting.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
										}
										@mysqli_close;

										?>
										
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                        </div>
                    </div>    
                    </div>
					</div>
					
					<div class="tab-pane fade" id="profil">
					<div class="panel panel-default">
						<?php /* if($id == null AND $page == null){ */ ?>
						<div class="panel-heading">
                            <i class="fa fa-wrench fa-fw"></i> Halaman Profil
                        </div>
                        <!-- /.panel-heading -->
						<div class="panel-body table-responsive">
                            <div class="row">
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
												<label>Ganti Foto Profil</label><br>
												<span>Tipe file *.jpg dengan ukuran max 1 MB</span><br><br>
												<form action="process-upload.php?set=<?php echo base64_encode($kodeset9);?>&img=<?php echo base64_encode($value9);?>&type=<?php echo base64_encode('_profil');?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm2">
													<input name="ImageFile" id="_imageInput2" type="file" /><br/>
													<!--<button type="submit" id="submit-btn" value="Upload" class="btn btn-info">Upload Foto</button>
													<button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>-->
													<input type="submit" id="_submit-btn2" name="_submit-btn" class="btn btn-default" value="Upload" />
													<img src="../img/assets/ajax-loader.gif" id="_loading-img2" style="display:none;" alt="Please Wait"/>
												</form>
												<div class="row"><div class="col-lg-6"><div id="_output2"></div></div></div>
											</div>
											<div class="form-group">
												<img class="imgl img-thumbnail" src="../img/foto-assets/<?php echo $value9; ?>" id="_oldimg2" width="50%" /><br>
												<div id="_progressbox2" style="display:none;"><div id="_progressbar2"></div><div id="_statustxt2">0%</div></div>
											</div>
										</div>
										</div>
								
								<form role="form" method="post" id="edit"  enctype="multipart/form-data">
										<div class="form-group">
                                            <label>Deskripsi</label>
											<?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '6' AND nama_setting = 'deskripsi'");
											while($row = @mysqli_fetch_array($edit)){
												echo '<textarea class="form-control" rows="15" cols="40" name="_ket" autocomplete="off">'.$row["ValueData"].'</textarea>';
											} ?>
                                        </div>
										
										<button type="submit" class="btn btn-default" name="_submit-edit-prof">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-prof">Reset</button>
										<?php
										include "../connections/config.php";
										$_ket = @htmlspecialchars($_POST['_ket']);
										if(isset($_POST['_submit-edit-prof'])){
											$query1 = @mysqli_query($con, "update systemsetting set ValueData = '$_ket' where KodeSetting = '6'"); 	 
												if($query1){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-setting.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
										}
										@mysqli_close;

										?>
										
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                        </div>
                    </div>
                        
                    </div>
					</div>
					
			
					
					<div class="tab-pane fade" id="kontak">
					<div class="panel panel-default">
						<?php /* if($id == null AND $page == null){ */ ?>
						<div class="panel-heading">
                            <i class="fa fa-wrench fa-fw"></i> Kontak dan Informasi
                        </div>
                        <!-- /.panel-heading -->
						<div class="panel-body table-responsive">
                            <div class="row">
								<div class="col-lg-6">
									<form role="form" method="post" id="edit"  enctype="multipart/form-data">
										<div class="form-group">
                                            <label>About</label>
											<?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '3' AND nama_setting = 'about'");
											while($row = @mysqli_fetch_array($edit)){
												echo '<textarea class="form-control" rows="10" cols="40" name="_about" autocomplete="off">'.$row["ValueData"].'</textarea>';
											} ?>
                                        </div>
										<div class="form-group">
                                            <label>Kontak</label>
                                            <?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '9' AND nama_setting = 'kontak'") ;
											while($row = @mysqli_fetch_array($edit)){
												echo '<input class="form-control" type="text" name="_kontak" autocomplete="off" value="'.$row['ValueData'].'" required="required" />';
											} ?>
                                        </div>
										<div class="form-group">
                                            <label>Email</label>
                                            <?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '8' AND nama_setting = 'email'");
											while($row = @mysqli_fetch_array($edit)){
												echo '<input class="form-control" type="text" name="_email" autocomplete="off" value="'.$row['ValueData'].'" required="required" />';
											} ?>
                                        </div>
										<div class="form-group">
                                            <label>Alamat</label>
											<?php $edit = @mysqli_query($con, "select * from systemsetting where KodeSetting = '7' AND nama_setting = 'alamat'");
											while($row = @mysqli_fetch_array($edit)){
												echo '<textarea class="form-control" rows="5" cols="40" name="_alamat" autocomplete="off">'.$row["ValueData"].'</textarea>';
											} ?>
                                        </div>
										
										<button type="submit" class="btn btn-default" name="_submit-edit-kontak">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-kontak">Reset</button>
										<?php
										include "../connections/config.php";
										$_about = @htmlspecialchars($_POST['_about']); $_kontak = @htmlspecialchars($_POST['_kontak']); $_email = @htmlspecialchars($_POST['_email']); $_alamat = @htmlspecialchars($_POST['_alamat']);
										if(isset($_POST['_submit-edit-kontak'])){
											$query1 = @mysqli_query($con, "update systemsetting set ValueData = '$_about' where KodeSetting = '3'") ; 	 
											$query2 = @mysqli_query($con, "update systemsetting set ValueData = '$_kontak' where KodeSetting = '7'") ; 	 
											$query3 = @mysqli_query($con, "update systemsetting set ValueData = '$_email' where KodeSetting = '9'") ; 	 
											$query3 = @mysqli_query($con, "update systemsetting set ValueData = '$_alamat' where KodeSetting = '8'") ; 	 
												if($query1 OR $query2 OR $query3 OR $query4){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-setting.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
												}
										}
										@mysqli_close;

										?>
										
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                        </div>
                    </div>
                        
                    </div>
					</div>
					
					
					
					</div>
					
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			
<!-- Start your form here --
<div class="row">
<div class="col-md-10">

  <div class="form-group">
  <label class="col-sm-2 control-label">Your Content</label>
  <div class="col-sm-10">
  <textarea id="ckeditor" name="description" class="form-control"></textarea>
  </div>
  </div>

  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
  <div class="col-sm-6"><br />
  <a href="" class="btn btn-default btn-sm" data-toggle="modal" data-target=".upload-form-modal">
  <i class="fa fa-upload"></i> Upload Image</a>
  </div>
  </div>

</div>
</div>
<!-- End of form -->


<!-- Here's your modal begin -->
<div class="modal fade upload-form-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
        <h4 class="modal-title">Upload Gambar</h4>
        </div>
        <div class="modal-body">

        <p>  
        <div id="displayImg">
        </div>
        </p>

        <form id="frm" method="post" enctype="multipart/form-data" action="../dist/ckeditor-img/processImage.php">
        <div id="imgLoading" style="display:none">
        <img src="../dist/ckeditor-img/loading.gif" alt="Uploading...."/>
        </div>
        <div id="ingLoadButton">
        <input type="file" name="deskImg" id="deskImg" />
        </div>
        <p><hr />Petunjuk : Klik untuk memasukkan Gambar ke CKEditor.</p>
        </form>
        </div>
    </div>
  </div>
</div>
<!-- End of Modal -->

			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../dist/ckeditor-img/js/jquery-1.10.2.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="../dist/ckeditor-img/js/jquery.deskform.js"></script>
	<script type="text/javascript" src="../dist/ckeditor-img/js/jquery.migrate-1.2.1.min.js"></script>
	
	<!-- CKE Image Uploader JS -->
	<script src="../dist/ckeditor-img/js/cke-upload-image.js"></script>
	
	<!-- CKEditor Adapters --> 
	<script src="../dist/ckeditor-img/js/ckeditor/adapters/jquery.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<script type="text/javascript">
		CKEDITOR.replace('ckeditor');
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>

	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox2');
		var progressbar     = $('#_progressbar2');
		var statustxt       = $('#_statustxt2');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output2',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm2').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput2').hide(); //hide input image
		$('#_submit-btn2').hide(); //hide submit button
		$('#_loading-img2').hide(); //hide loading 
		$('#_statustxt2').hide(); //hide loading 
		$('#_progressbox2').hide(); //hide loading 
		$('#_oldimg2').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput2').val()) //check empty input filed
			{
				$("#_output2").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput2')[0].files[0].size; //get file size
			var ftype = $('#_imageInput2')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output2").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output2").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn2').hide(); //hide submit button
			$('#_loading-img2').show(); //hide submit button
			$("#_output2").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output2").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox');
		var progressbar     = $('#_progressbar');
		var statustxt       = $('#_statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput').hide(); //hide input image
		$('#_submit-btn').hide(); //hide submit button
		$('#_loading-img').hide(); //hide loading 
		$('#_statustxt').hide(); //hide loading 
		$('#_progressbox').hide(); //hide loading 
		$('#_oldimg').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput').val()) //check empty input filed
			{
				$("#_output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput')[0].files[0].size; //get file size
			var ftype = $('#_imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn').hide(); //hide submit button
			$('#_loading-img').show(); //hide submit button
			$("#_output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#_progressbox3');
		var progressbar     = $('#_progressbar3');
		var statustxt       = $('#_statustxt3');
		var completed       = '0%';
		
		var options = { 
				target:   '#_output3',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm3').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#_imageInput3').hide(); //hide input image
		$('#_submit-btn3').hide(); //hide submit button
		$('#_loading-img3').hide(); //hide loading 
		$('#_statustxt3').hide(); //hide loading 
		$('#_progressbox3').hide(); //hide loading 
		$('#_oldimg3').hide(); //hide old image

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#_imageInput3').val()) //check empty input filed
			{
				$("#_output3").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#_imageInput3')[0].files[0].size; //get file size
			var ftype = $('#_imageInput3')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#_output3").html("<b>"+ftype+"</b> Tipe File Tidak Support!");
					return false
			}
			
			//Allowed file size is less than 1 MB (1048576)
			if(fsize>1048576) 
			{
				$("#_output3").html("<b>"+bytesToSize(fsize) +"</b> File Image Terlalu Besar! <br/>Perkecil Ukuran File Terlebih Dahulu");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#_submit-btn3').hide(); //hide submit button
			$('#_loading-img3').show(); //hide submit button
			$("#_output3").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#_output3").html("Upgrade Browser Anda! Browser Tidak Mendukung Fitur Ini");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 

	</script>
	
</body>

</html>
