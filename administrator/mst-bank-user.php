<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$fitur_id = 5;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @sqlsrv_query($dbconnect, "select * from mstbank where KodeBank = '".$id."'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodebank = $cari['KodeBank']; $bank = $cari['NamaBank']; 
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../dist/css/upload-img.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- ZebraDatepicker JavaScript -->
    <link rel="stylesheet" href="../dist/zebra-datepicker/css/default.css" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">
<?php include "lock-menu.php"; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Bank
					<?php if($id == null AND $page == null){ 
						echo "<a href='mst-bank.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>";
					}elseif($id !== null AND $page !== null){ 
						echo "<a href='mst-bank.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>";
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
					<?php if($id == null AND $page == null){ ?>
					<ul class="nav nav-tabs">
                        <li class="active"><a href="mst-bank.php">Master Bank</a></li>
                        <li><a href="mst-bank-penerima.php">Bank Perusahaan</a></li>
                    </ul><br>
					<?php } ?>
					
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="60%">Nama Bank</th>
                                        <th width="15%">Set</th>
                                        <th width="15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @sqlsrv_query($dbconnect, "select * from mstbank order by NamaBank", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$no = 1;
								while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".ucwords($cari['NamaBank'])."</strong>"; ?></td>
										<td><?php if($cari['IsWantrast'] == '0'){ echo '<a href="mst-bank.php?page='.htmlspecialchars(base64_encode("aktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeBank"])).'" class="btn btn-default btn-sm">Set Bank</a>'; }
										else { echo '<a href="mst-bank.php?page='.htmlspecialchars(base64_encode("nonaktif")).'&id='.htmlspecialchars(base64_encode($cari["KodeBank"])).'" class="btn btn-primary btn-sm">Bank Perusahaan</a>'; } ?>
										</td>
                                        <td>
											<a href="mst-bank.php?page=<?php echo htmlspecialchars(base64_encode('edit'))."&id=".htmlspecialchars(base64_encode($cari['KodeBank'])); ?>" class="btn btn-warning btn-sm">Edit</a>
											<a href="mst-bank.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['KodeBank'])); ?>" class="btn btn-success btn-sm">Detail</a>
											<!-- <a href="mst-bank.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['KodeBank'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a> -->
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						<?php } elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Nama Bank</label>
                                            <input class="form-control" type="text" name="_namabank" placeholder="ex : Nama Bank" autocomplete="off" required>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-input-bank">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-bank">Reset</button>
									</div>
								</form>
									
									<?php
										include "../connections/config.php";
										$_nama = @htmlspecialchars($_POST['_namabank']); 
										if(isset($_POST['_submit-input-bank'])){
											// membuat id otomatis
											$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeBank,3)) AS kode FROM mstbank WHERE LEFT(replace(KodeBank,' ',''),4)='BNK-'") or die( print_r( sqlsrv_errors(), true)); 
											$nums = @sqlsrv_num_rows($sql); 
											while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											// membuat kode user
											$bikin_kode = str_pad($kode, 3, "0", STR_PAD_LEFT);
											$kode_jadi = "BNK-".$bikin_kode;
									
											$query = @sqlsrv_query($dbconnect, "INSERT into mstbank(KodeBank,NamaBank)values('$kode_jadi','$_nama')") or die( print_r( sqlsrv_errors(), true)); 	 
											if($query){
												echo '<script type="text/javascript">
												sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
												function () { window.location.href = "mst-bank.php"; });
												</script>';
											}else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
											}
										}
										@sqlsrv_close;

										?>
                                   
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'edit'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Update Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<form role="form" method="post">
									<div class="col-lg-6">
										<div class="form-group">
                                            <label>Kode Bank</label>
                                            <input class="form-control" type="text" value="<?php echo $kodebank; ?>" autocomplete="off" readonly>
                                        </div>
										<div class="form-group">
                                            <label>Nama Bank</label>
                                            <input class="form-control" type="text" name="_namabank2" value="<?php echo $bank; ?>" autocomplete="off" required>
                                        </div>
										<button type="submit" class="btn btn-default" name="_submit-edit-bank">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-bank">Reset</button>
									</div>
								
										<?php
										include "../connections/config.php";
										$_namabank2 = @htmlspecialchars($_POST['_namabank2']); 
										if(isset($_POST['_submit-edit-bank'])){
											$query = @sqlsrv_query($dbconnect, "update mstbank set NamaBank = '$_namabank2' where KodeBank = '".$kodebank."'") or die( print_r( sqlsrv_errors(), true)); 	 
											if($query){
												echo '<script type="text/javascript">
												sweetAlert({ title: "Berhasil!", text: " Update Data Telah Tersimpan ", type: "success" },
												function () { window.location.href = "mst-bank.php"; });
												</script>';
											}
											else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Update Data Gagal ", "error"); </script>';
											}
										}
										@sqlsrv_close;

										?>
                                </form>   
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
                                        <label>Nama Bank</label>
                                            <dd><?php echo ucwords($bank); ?></dd>
                                    </div>
									
								</div>
							</div>
						</div>
						
						<?php /* } elseif($id != null AND $page == 'nonaktif'){ 
							$file_path = "../android_wantrast_sqlsrv/img/fotoprofil/".$fotoprs; $file_path2 = "../android_wantrast_sqlsrv/img/fotoprofil/thumb_".$fotoprs;
							$cek = @sqlsrv_query($dbconnect, "select * from TrPembelian where replace(KodePerson,' ','') = replace('$kodeprs',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Supplier Dipakai Transaksi ", type: "error" },
								function () { window.location.href = "mst-supplier.php"; }); </script>';
							}else{
								// hapus data 
								/*if(file_exists($file_path)){ unlink($file_path); }
								if(file_exists($file_path2)){ unlink($file_path2); }
								$delete = @sqlsrv_query($dbconnect, "DELETE from MasterPerson WHERE id_support = '".$kodespons."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-sponsor.php"; }); </script>';
								
								// inaktif data
								$delete = @sqlsrv_query($dbconnect, "update MasterPerson set IsAktif = 'False' WHERE KodePerson = '".$kodeprs."'") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-supplier.php"; }); </script>';
							} */
							
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @sqlsrv_query($dbconnect, "update mstbank set IsWantrast = '1' WHERE KodeBank = '".$id."'") or die( print_r( sqlsrv_errors(), true));
							if($delete){
								@sqlsrv_query($dbconnect, "insert into mstbankpenerima(NoRekening,AtasNama,KodeBank)values('123456789','Belum Tersedia','$id')") or die( print_r( sqlsrv_errors(), true));
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Set Bank Perusahaan Berhasil ", type: "success" },
								function () { window.location.href = "mst-bank.php"; }); </script>';
							}
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// aktif data
							$cek = @sqlsrv_query($dbconnect, "select * from mstbankpenerima where replace(KodeBank,' ','') = replace('$id',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							$num = @sqlsrv_num_rows($cek);
							if($num > 0){
								echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Data Bank Adalah Bank Perusahaan ", type: "error" },
								function () { window.location.href = "mst-bank.php"; }); </script>';
							} else {
								$cek2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where replace(NamaBankPerson,' ','') = replace('$id',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
								$num2 = @sqlsrv_num_rows($cek2);
								if($num2 > 0){
									echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Data Bank Dipakai Transaksi ", type: "error" },
									function () { window.location.href = "mst-bank.php"; }); </script>';
								} else {
									$delete = @sqlsrv_query($dbconnect, "update mstbank set IsWantrast = '0' WHERE KodeBank = '".$id."'") or die( print_r( sqlsrv_errors(), true));
									echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Bank Perusahaan Berhasil ", type: "success" },
									function () { window.location.href = "mst-bank.php"; }); </script>'; 
								}
							}
							
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- ZebraDatepicker JavaScript -->
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.js"></script>
	<script src="../dist/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script>
	$(document).ready(function() {
		$('#datepicker-example1').Zebra_DatePicker();
	});
	</script>
	
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>
	
</body>

</html>
