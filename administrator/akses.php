<?php
session_start();
include "../connections/config.php";
$user_aktif = base64_decode(htmlspecialchars(addslashes(stripslashes($_SESSION['_user_login']))));
$time_aktif = $_SESSION['_timeout'];

// Ambil nama karyawan berdasarkan username karyawan dengan mysql_fetch_assoc
$ses_sql = mysqli_query($con,  "select * from mstuser where replace(id_user,' ','') = '$user_aktif'");
$row = mysqli_fetch_array($ses_sql);
$id_aktif = $row['id_user'];
$nama_aktif = $row['NamaLengkap'];
$username_aktif = $row['UserName'];
$foto_aktif = $row['foto'];
$level_aktif = $row['level'];


$waktu    = time()+25200; //(GMT+7)
$expired  = 3000;

if(isset($_SESSION['_user_login']) AND ($_SESSION['_level_login'] === "ADMIN_WEB")){
	//jika waktu sekarang kurang dari sesi timeout
	if($waktu < $time_aktif){
	//hapus sesi timeout yang lama ,buat sesi timeout yang baru
	unset($_SESSION['_timeout']);
	$_SESSION['_timeout'] = $waktu + $expired;
	//disini konten untuk user atau admin yang berhasil login
	}
	else{
	session_destroy();
	echo '<script language="javascript">document.location="logout.php?id='.base64_encode("timeout").'"; </script>';
	}
}
else{
	echo '<script language="javascript">document.location="logout.php?id='.base64_encode("error").'"; </script>';
}

?>

