<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
// $fitur_id = 19;

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : 0 ;

$query = @mysqli_query($con, "select * from bukutamu where id_buku = '".$id."'");
while($cari = @mysqli_fetch_array($query)){ 
	$kodebuku = $cari['id_buku']; $tglbuku = $cari['tanggal']; $nama = $cari['nama']; $email = $cari['email']; $isi = $cari['isi']; $stats = $cari['status']; $kodejawab = $cari['id_jawab'];
}
@sqlsrv_close();

$post = @$_SESSION['POST'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="noBack(); "onpageshow="if(event.persisted) noBack();" onunload="">


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include "header.php"; ?>

            <div class="navbar-default sidebar" role="navigation">
                <?php include "sidebar.php"; ?>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Buku Tamu
					<?php /* if($id == null AND $page == null){ 
						echo "<a href='mst-bukutamu.php?page=".htmlspecialchars(base64_encode('tambah'))."' class='btn btn-primary btn-sm'><i class='fa fa-plus fa-fw'></i> Tambah Data</a>";
						echo "</h1>"; 
					} */ if($id != null AND ($page == 'detail' OR $page == 'balas')){ 
						echo "<a href='mst-bukutamu.php' class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-fw'></i> Kembali</a>";
						echo "</h1>"; 
					}
					?>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
						<?php if($id == null AND $page == null){ ?>
						<div class="panel-heading">
                            <i class="fa fa-list fa-fw"></i> List Data
                        </div>
                        <!-- /.panel-heading -->
						
						<div class="panel-body table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="25%">Nama</th>
                                        <th width="50%">Deskripsi</th>
                                        <th width="15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php include "../connections/config.php";
								$query = @mysqli_query($con, "select * from bukutamu where status = 'TULIS' order by tanggal DESC");
								$no = 1;
								while($cari = @mysqli_fetch_array($query)){ 
								?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo "<strong>".strtoupper($cari['nama'])."</strong><br>".$cari['email']."<br>".TanggalIndo($cari['tanggal']); ?></td>
                                        <td><?php echo ucwords($cari['isi']); ?>
										<?php $cari2 = @mysqli_query($con, "select * from bukutamu where id_jawab = '".$cari['id_buku']."' order by tanggal DESC");
										$jumlah = @mysqli_num_rows($cari2);
										if($jumlah === 0){ echo '<br><br>'; } else { echo '<br><br><a href="#" class="btn btn-default btn-sm">'.$jumlah.' balasan</a>'; }
										?>
										<a href="mst-bukutamu.php?page=<?php echo htmlspecialchars(base64_encode('detail'))."&id=".htmlspecialchars(base64_encode($cari['id_buku'])); ?>" class="btn btn-primary btn-sm">Lihat</a>
										</td>
										<td>
											<a href="mst-bukutamu.php?page=<?php echo htmlspecialchars(base64_encode('balas'))."&id=".htmlspecialchars(base64_encode($cari['id_buku'])); ?>" class="btn btn-success btn-sm">Balas</a>
											<a href="mst-bukutamu.php?page=<?php echo htmlspecialchars(base64_encode('delete'))."&id=".htmlspecialchars(base64_encode($cari['id_buku'])); ?>" class="btn btn-danger btn-sm" data-target="#delete" data-toggle="modal">Delete</a><br><br>
										<?php if($cari['is_aktif'] === '1'){ echo '<a href="mst-bukutamu.php?page='.htmlspecialchars(base64_encode("nonaktif")).'&id='.htmlspecialchars(base64_encode($cari["id_buku"])).'" class="btn btn-primary btn-sm">Aktif</a>'; } 
										else { echo "<a href='mst-bukutamu.php?page=".htmlspecialchars(base64_encode('aktif'))."&id=".htmlspecialchars(base64_encode($cari['id_buku']))."' class='btn btn-warning btn-sm'>Non Aktif</a>"; } ?>
										</td>
                                    </tr>
								<?php $no++; } 
								@sqlsrv_close(); ?> 
								
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
						
						<?php } /* elseif($id == null AND $page == 'tambah'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Tambah Data
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="form-group">
                                            <label>Kode Departemen Manual</label>
                                            <input class="form-control" type="text" name="_kodedpr" placeholder="ex : 000001" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Nama Departemen</label>
                                            <input class="form-control" type="text" name="_namadpr" placeholder="ex : Nama Departemen" autocomplete="off" required>
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" rows="8" cols="40" name="_ketdpr" placeholder="ex : Ini Adalah Departemen" autocomplete="off"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-input-dpr">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-input-dpr">Reset</button>
										
										<?php
										include "../connections/config.php";
										$_kode = @htmlspecialchars($_POST['_kodedpr']); $_nama = @htmlspecialchars($_POST['_namadpr']); $_ket = @htmlspecialchars($_POST['_ketdpr']); 
										if(isset($_POST['_submit-input-dpr'])){
											// membuat id otomatis
											$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeDepartemen,7)) AS kode FROM MasterDepartemen WHERE LEFT(replace(KodeDepartemen,' ',''),4)='DPT-'") or die( print_r( sqlsrv_errors(), true)); 
											$nums = @sqlsrv_num_rows($sql); 
											while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											$bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
											$kode_jadi = "DPT-".$bikin_kode;
											
											$cek = @sqlsrv_query($dbconnect, "select * from MasterDepartemen where replace(KodeDepartemenManual,' ','') = replace('$_kode',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
											$num = @sqlsrv_num_rows($cek);
											if($num > 0 ){
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode Departemen Manual Sudah Ada ", "error"); </script>';
											}else{
											$query = @sqlsrv_query($dbconnect, "insert into MasterDepartemen(KodeDepartemen,KodeDepartemenManual,NamaDepartemen,Keterangan,IsAktif,IsDapatKupon,IsDapatPoint)
											values('$kode_jadi','$_kode','$_nama','$_ket','1','0','0')") or die( print_r( sqlsrv_errors(), true)); 	 
												if($query){
													echo '<script type="text/javascript">
													sweetAlert({ title: "Berhasil!", text: " Input Data Telah Tersimpan ", type: "success" },
													function () { window.location.href = "mst-departemen.php"; });
													</script>';
												}
												else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Input Data Gagal ", "error"); </script>';
												}
											}
										}
										@sqlsrv_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
						
						<!-- /.panel-heading -->
						
						<?php } */ elseif($id != null AND $page == 'balas'){ ?>
						<div class="panel-heading">
                            <i class="fa fa-edit fa-fw"></i> Balas Testimoni
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-6">
									<form role="form" method="post">
										<div class="panel panel-default">
										<div class="panel-heading">
											<div class="form-group">
											<label>Pengirim</label>
												<dd><?php echo strtoupper($nama)."<br>".$email; ?></dd>
											</div>
											<div class="form-group">
											<label>Testimoni</label>
												<dd><?php echo ucwords($isi); ?></dd>
											</div>
										</div>	
										</div>
										
										<div class="form-group">
                                            <label>Isi Balasan</label>
                                            <textarea class="form-control" rows="8" cols="40" name="_balas" autocomplete="off"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default" name="_submit-edit-balas">Submit</button>
                                        <button type="reset" class="btn btn-default" name="_submit-edit-balas">Reset</button>
										
										<?php
										include "../connections/config.php";
										
										// membuat id user
										$year = date('Y')."/".date('m'); 
										$sql = @mysqli_query($con,"SELECT MAX(RIGHT(id_buku,6)) AS kode FROM bukutamu WHERE LEFT(id_buku,12)='BUKU/".date('Y')."/".date('m')."'"); 
										$nums = @mysqli_num_rows($sql); 
										while($data = @mysqli_fetch_array($sql)){
										if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; } 
										}

										// membuat kode user
										$bikin_kode = str_pad($kode, 6, "0", STR_PAD_LEFT);
										$kode_jadi = "BUKU/".$year."/".$bikin_kode;
										@mysqli_close;
										
										error_reporting(0); date_default_timezone_set("Asia/Jakarta"); // set zona waktu.							
										
										$_balas = @htmlspecialchars($_POST['_balas']); 
										if(isset($_POST['_submit-edit-balas'])){
											$simpan = @mysqli_query($con,"INSERT into bukutamu(id_buku,tanggal,isi,status,id_jawab,id_user)values('$kode_jadi','".date('Y-m-d H:i:s')."','$_balas','JAWAB','$id','$id_aktif')");
											if($simpan){
												echo '<script type="text/javascript">
												sweetAlert({ title: "Terima Kasih!", text: " Balas Pesan Terkirim ", type: "success" },
												function () { window.location.href = "mst-bukutamu.php"; });
												</script>';
											}else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " Balas Pesan Gagal", "error"); </script>';
											}
										}
										@mysqli_close;

										?>
                                    </form>
								</div>
							</div>
						</div>
                        <!-- /.panel-heading -->
						
						<?php } elseif($id != null AND $page == 'detail'){  ?>
						<div class="panel-heading">
                            <i class="fa fa-search fa-fw"></i> Detail
                        </div>
						<div class='panel-body'>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label>Rincian Buku Tamu</label>
											<dd><?php echo "Nama User : <strong>".strtoupper($nama)."</strong><br>Email : ".$email; ?></dd>
                                    </div><hr>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="form-group">
													<label><?php echo ucwords($nama)." . ".TanggalIndo($tglbuku); ?></label>
														<dd><?php echo ucwords($isi); ?></dd>
												</div>
												
											</div>
										</div>
                                    </div>
								</div>
								<div class="col-lg-2"></div>
							</div>
							<?php $query22 = @mysqli_query($con, "select mstuser.NamaLengkap, bukutamu.tanggal, bukutamu.isi from bukutamu inner join mstuser on mstuser.id_user = bukutamu.id_user where bukutamu.status = 'JAWAB' order by tanggal ASC");
							while($cari22 = @mysqli_fetch_array($query22)){ ?>
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<div class="form-group">
										<div class="panel panel-primary">
											<div class="panel-heading">
												<div class="form-group text-right">
													<label><?php echo ucwords($cari22['NamaLengkap'])." . ".TanggalIndo($cari22['tanggal']); ?></label>
														<dd><?php echo ucwords($cari22['isi']); ?></dd>
												</div>
												
											</div>
										</div>
                                    </div>
								</div>
							</div>
							<?php } ?>
						</div>
						
						<?php } 
						elseif($id != null AND $page == 'delete'){ 
							$delete = @mysqli_query($con, "DELETE from bukutamu WHERE id_buku = '".$id."'");
							if($delete){
								@mysqli_query($con, "DELETE from bukutamu WHERE id_jawab = '".$id."'");
								echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
								function () { window.location.href = "mst-bukutamu.php"; }); </script>';
							}
						} elseif($id != null AND $page == 'aktif'){ 
							// aktif data
							$delete = @mysqli_query($con, "update bukutamu set is_aktif = '1' WHERE id_buku = '".$id."'");
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-bukutamu.php"; }); </script>';
							
						} elseif($id != null AND $page == 'nonaktif'){ 
							// aktif data
							$delete = @mysqli_query($con, "update bukutamu set is_aktif = '0' WHERE id_buku = '".$id."'");
							echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
							function () { window.location.href = "mst-bukutamu.php"; }); </script>';
						} ?>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
			
			<div class="row">
                 <?php include "footer.php"; ?>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

     <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- <script type="text/javascript">
	//Override the default confirm dialog by rails
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Menghapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script> -->
	
	<script type="text/javascript">
	$(document).ready(function () {
      var table = $('#dataTables-example').DataTable();

      $('#dataTables-example tbody').on('click', 'a[data-target="#delete"]', function () {
          var btn = this;
		  var getLink = $(this).attr('href');
          swal({
              title: 'Apa Anda Yakin?',
			  text: 'Untuk Menghapus Data Ini',
			  type: 'warning',
			  html: true,
              showCancelButton: true,
              confirmButtonColor: '#d9534f',
              confirmButtonText: "Hapus Data!",
              cancelButtonText: "Tidak",
              closeOnConfirm: false,
              closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
				  /*sweetAlert({ title: "Berhasil!", text: " Delete Data Berhasil ", type: "success" },
				  function () { window.location.href = getLink; });*/
				  window.location.href = getLink
                  /* swal("Berhasil!", " Delete Data Berhasil ", "success"),
					function (){
						window.location.href = getLink
					};
                  /* table.row($(btn).parents('tr')).remove().draw(false); */
              } else {
                  swal("Maaf!", " Delete Data Gagal ", "error");
              }
          });
      });
	});
	</script>


</body>

</html>
