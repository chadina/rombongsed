<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Rombong Sedekah</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-user fa-fw"></i> Login Admin</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="_username" type="username" autocomplete="off" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="_password" type="password" autocomplete="off">
                                </div><br>
								<div class="form-group">
									 <div class="row">
									 <div class="col-lg-12"><img src="../dist/captcha/captcha_login.php" alt="gambar" /><br><br>
									 <input class="form-control" type="text" name="_captcha_login" placeholder="Masukkan Captcha" autocomplete="off" required></div>
									 </div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success" name="_submit"><i class="fa fa-sign-in fa-fw"></i> Masuk</button>&nbsp;
									<a class="btn btn-warning" href="default.php"><i class="fa fa-refresh fa-fw"></i> Kembali</a>
								</div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<!-- Sweet Alerts -->
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
		
	<?php
	@session_start();
	include "../connections/config.php";
	if(isset($_POST['_submit'])){
		if(empty($_POST['_username']) || empty ($_POST ['_password'])) {
			echo '<script type="text/javascript">
				  sweetAlert({
			    	title: "Maaf!",
					text: " Username atau Password Kosong ",
					type: "error"
				  },
				  function () {
					window.location.href = "login.php";
				  });
				  </script>';
			session_destroy();
		}
		else{		
		// Variabel username dan password
		$username = stripslashes($_POST['_username']);
		$password = stripslashes(md5($_POST['_password']));
		$waktu    = time()+25200; //(GMT+7)
		$expired  = 3000;
		
		// Mencegah MySQL injection dan XSS
		$check_user = htmlspecialchars(addslashes($username));
		$check_pass = htmlspecialchars(addslashes($password));
		
		// SQL query untuk memeriksa apakah user terdapat di database?
		$query = mysqli_query($con, "select * from mstuser where replace(UserName,' ','') = replace('$check_user',' ','') AND Password = '$check_pass' 
		AND IsAktif='1'");
		$cari = mysqli_num_rows($query); 
		if($_SESSION["_captcha_login"] == $_POST["_captcha_login"]){
			if($cari === 0){
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Login Gagal!",
						text: " Username Password Salah atau User Tidak Aktif ",
						type: "error"
					  },
					  function () {
						window.location.href = "login.php";
					  });
					  </script>';
				session_destroy();
			}else{
				$row = mysqli_fetch_array($query);
				if($row['level'] === 'ADMIN_WEB'){
					$_SESSION['_user_login'] = base64_encode($row['id_user']);
					$_SESSION['_level_login'] = $row['level'];
					$_SESSION['_timeout'] = $waktu + $expired; // Membuat Sesi Waktu
					echo '<script type="text/javascript">
					  sweetAlert({
						title: "Login Sukses!",
						text: " Anda Berhasil Login ",
						type: "success"
					  },
					  function () {
						window.location.href = "../administrator/default.php";
					  });
					  </script>';
				}else{	
					echo '<script type="text/javascript">
					  sweetAlert({
						title: "Login Gagal!",
						text: " Username atau Password Salah ",
						type: "error"
					  },
					  function () {
						window.location.href = "login.php";
					  });
					  </script>';
					session_destroy();
				}	
			}
			mysqli_close(); // Menutup koneksi
		}else{
			echo '<script type="text/javascript">sweetAlert("Maaf!", " Captcha Anda Salah ", "error"); </script>';
		}
	}
	}
	
	?>

</body>
</html>


