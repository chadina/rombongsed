<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GrosirMart</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
  
    
    
    <div class="single-product-area">
        <div class="container">
     
               
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">
                             <div id="customer_details" class="col2-set">
                                    <div class="col-md-12">
                                        <div class="woocommerce-billing-fields">
                                           
											<div class="bs-example">
												<div class="panel-group" id="accordion">
													<?php include "../connections/config.php"; 
													$edit = sqlsrv_query($dbconnect, "select Kategori from SyaratKetentuan group by Kategori") or die( print_r( sqlsrv_errors(), true));
													while($row = sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){ ?>
														
													<div class="panel panel-info">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $row['Kategori']; ?>"><?php echo ucwords($row['Kategori']); ?></a>
															</h4>
														</div>
														<div id="<?php echo $row['Kategori']; ?>" class="panel-collapse collapse">
															<div class="panel-body">
																<ol>
																<?php $edit2 = sqlsrv_query($dbconnect, "select PointSyarat from SyaratKetentuan where Kategori = '".$row['Kategori']."'") or die( print_r( sqlsrv_errors(), true));
																while($row2 = sqlsrv_fetch_array($edit2, SQLSRV_FETCH_ASSOC)){ ?>
																
																	<li><?php echo ucwords($row2['PointSyarat']); ?></li>
																
																<?php } ?>
																</ol>
															</div>
														</div>
													</div>
													
													<?php } ?>
													
												</div>
												 <p><strong>Note:</strong> Klik Pada Setiap Pertanyaan Untuk Detil Syarat dan Ketentuan</p> 
											</div>

                                        </div>
                                    </div>

                                   
                                </div>

                                
                                        
                            </form>

                        </div>                       
                    </div>                    
                </div>
				
				

                                
                                        
                            </form>

                                             
                    </div>                    
                </div>
				
            </div>
        </div>
    </div>
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>