<?php
include('../connections/config.php');
include("tgl-indo.php");
date_default_timezone_set('Asia/Jakarta');

	$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
	$edit = mysqli_query($con, "select * from mstkegiatan where id_kegiatan = '$id'");
	while($row = mysqli_fetch_array($edit)){
		$tgl		= $row['tanggal'];
		$judul		= $row['judul'];
		$jam		= $row['jam'];
		$user		= $row['penulis'];
		$isi		= $row['deskripsi'];
		$foto		= $row['foto'];
		
	}
	
date_default_timezone_set("Asia/Jakarta"); // set zona waktu.
$wib = date("H:i:s"); // waktu Indonesia bagian Barat yang telah di set mengambil zona waktu Asia/Jakarta.

$url = $_SERVER['REQUEST_URI']; 
$pecah = explode("&",$url);
$link = str_replace('/RombongSedekah/web/detail-post.php','',$pecah[0]);
$get_link = str_replace('?','',$link); 

$query = mysqli_query($con, "select * from mstuser where IsAktif = '1' AND replace(UserName, ' ','') = replace('$get_link',' ','')");  
$cari = @mysqli_num_rows($query); $dapat = @mysqli_fetch_array($query);
if($cari === 0){
	$get_user = '';
} else {
	$get_user = $dapat['UserName'];
} 

?>

<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rombong Sedekah</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area -->
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>DETAIL KEGIATAN</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="container">
            <div class="row">     
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">
                        
	    		<div class="col-sm-6">
	    			<div class="contact-form">
	    				<div class="form-group">
						<?php if($foto == null OR $foto == '' OR !file_exists('../android_rombongsedekah/img/kegiatan/'.$foto)){
							echo '<img class="img img-responsive img-thumbnail" src="../android_rombongsedekah/img/kegiatan/no-image.png" alt="" width="100%" /><br class="clear" />';
						} else {
							echo '<img class="img img-responsive img-thumbnail" src="../android_rombongsedekah/img/kegiatan/'.$foto.'" alt="" width="100%" /><br class="clear" />';
						} ?>
						</div>
						   
	    			</div>
	    		</div>
				
				<div class="col-sm-6">
	    			<div class="contact-form">
	    				<h2><?php echo ucwords($judul); ?></h2>
	    								<div class="form-group">
                                            <p><?php echo "Oleh ".strtoupper($user);?></p>
											<p><?php echo "Pada ".TanggalIndo($tgl);?></p>
                                            <p><?php echo ucfirst($isi)?></p>
                                        </div>
										
                           
	    			</div>
	    		</div>
						
                        </div>                       
                    </div>                    
                </div>
				
            </div>
        </div>
    </div>
    
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About Rombong Sedekah</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '3' AND nama_setting = 'about'");
						while($row = mysqli_fetch_array($edit)){
							echo '<p>'.ucwords($row['ValueData']).'</p>';
						} ?>
                         <div class="footer-social">
                            <a href="https://id-id.facebook.com/adminrombongsedekah/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/rombong_sedekah" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCOWTMXnIx9PDUSFRUtRRbCA" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div> 
                    </div>
                </div>
				
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>