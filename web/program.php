<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$url = $_SERVER['REQUEST_URI'];
$link = str_replace('/RombongSedekah/web/program.php','',$url);
$get_link = str_replace('?','',$link); 

$query = mysqli_query($con, "select * from mstuser where IsAktif = 'True' AND replace(UserName, ' ','') = replace('$get_link',' ','')");  
$cari = mysqli_num_rows($query); $dapat = mysqli_fetch_array($query);
if($cari === 0){
	$get_user = '';
} else {
	$get_user = $dapat['UserName'];
}

 mysqli_close();
 
?>

<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rombong Sedekah</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/js/foundation.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>	
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	<link rel="stylesheet" href="style1.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area -->
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Program Kami</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="container">
			<?php if($get_user === '' OR $get_user == null){ ?>
				<section class="section-medium">
				<?php include "../connections/config.php";
					$kolom = 3; $i=0; 
					$result = mysqli_query($con, "select id_program, nama_program, foto, detil_program from mstprogram WHERE is_aktif = 'TRUE' order by nama_program ASC") ;
					while($row = mysqli_fetch_array($result)){ 
					$id		= $row['id_program'];
					$nama	= $row['nama_program'];
					$gbr	= $row['foto'];
					$isi	= $row['detil_program'];
					
					if($i>=$kolom){
					echo "<div class='row'>";
					$i=0;
					}
					$i++; ?>
					
						<div class="columns medium-4">
							<?php
								if($gbr=='' or $gbr==null or !file_exists('../android_rombongsedekah/img/program/'.$gbr)){
									echo '<img src="../android_rombongsedekah/img/program/no-image.png" alt="" />';
								}else{
									echo '<img src="../android_rombongsedekah/img/program/'.$gbr.'" alt="" />';
								} 
							?>
							<?php echo "<strong>".ucwords($nama)."</strong><br>".ucwords($isi); ?>
							
							
						</div>
						<?php } ?> 

					</div>
				</section>
            <?php } ?>                                   
        </div>
	</div>                            
                
				
          
       
   
    
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About Rombong Sedekah</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '3' AND nama_setting = 'about'") or die( print_r( mysqli_error(), true));
						while($row = mysqli_fetch_array($edit)){
							echo '<p>'.ucwords($row['ValueData']).'</p>';
						} ?>
                         <div class="footer-social">
                            <a href="https://id-id.facebook.com/adminrombongsedekah/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/rombong_sedekah" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCOWTMXnIx9PDUSFRUtRRbCA" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div> 
                    </div>
                </div>
				
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>