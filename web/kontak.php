<?php session_start(); include "../connections/config.php"; $url = $_SERVER['REQUEST_URI']; 
$link = str_replace('/RombongSedekah/web/kontak.php','',$url);
$get_link = str_replace('?','',$link); 

$query = mysqli_query($con, "select * from mstuser where IsAktif = '1' AND replace(UserName, ' ','') = replace('$get_link',' ','')");  
$cari = @mysqli_num_rows($query); $dapat = @mysqli_fetch_array($query);
if($cari === 0){
	$get_user = '';
} else {
	$get_user = $dapat['UserName'];
} ?>

<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rombong Sedekah</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area -->
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Kontak dan Informasi</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            
                            <form enctype="multipart/form-data" action="" class="checkout" method="post" name="checkout">
							
							<?php 
							// membuat id user
							include "../connections/config.php";
							$year = date('Y')."/".date('m'); 
							$sql = @mysqli_query($con,"SELECT MAX(RIGHT(id_buku,6)) AS kode FROM bukutamu WHERE LEFT(id_buku,12)='BUKU/".date('Y')."/".date('m')."'"); 
							$nums = @mysqli_num_rows($sql); 
							while($data = @mysqli_fetch_array($sql)){
							if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; } 
							}

							// membuat kode user
							$bikin_kode = str_pad($kode, 6, "0", STR_PAD_LEFT);
							$kode_jadi = "BUKU/".$year."/".$bikin_kode;
							@mysqli_close;
							
							error_reporting(0); date_default_timezone_set("Asia/Jakarta"); // set zona waktu.							
							$wib = date("H:i:s"); // waktu Indonesia bagian Barat yang telah di set mengambil zona waktu Asia/Jakarta.
							?>
							

                                <div id="customer_details" class="col2-set">
                                    <div class="col-md-12">
                                        <div class="woocommerce-billing-fields">
                                            <h3>Tulis Komentar</h3>
											<input type="hidden" value="<?php echo $kode_jadi; ?>" placeholder="" id="billing_first_name" name="_kode" class="input-text " required="required" readonly>
                                            <input type="hidden" value="<?php echo date('Y-m-d H:i:s'); ?>" placeholder="" id="billing_first_name" name="_tgl" class="input-text " required="required" readonly>
                                            <input type="hidden" value="<?php echo $wib; ?>" placeholder="" id="billing_first_name" name="_jam" class="input-text " required="required" readonly>
                                                
                                            <p id="billing_first_name_field" class="form-row form-row-first validate-required">
                                                <label class="" for="billing_first_name">Nama <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="text" value="" placeholder="Masukkan Nama" id="billing_first_name" name="_nama" class="input-text " autocomplete="off" required="required">
												
                                            </p>

                                            <p id="billing_last_name_field" class="form-row form-row-last validate-required">
                                                <label class="" for="billing_last_name">Email <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="text" value="" placeholder="Masukkan Email" id="billing_last_name" name="_email" class="input-text " autocomplete="off" required="required">
                                            </p>
											
											<p id="billing_last_name_field" class="form-row form-row-last validate-required">
                                                <label class="" for="billing_last_name">Deskripsi <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <textarea type="text" value="" placeholder="Tulis Disini" id="billing_last_name" rows="8" name="_isi" class="input-text "  autocomplete="off" required="required"></textarea>
                                            </p><br>
											
											<p id="billing_last_name_field" class="form-row form-row-last validate-required">
												<div class="row"><div class="col-lg-12"><img src="../dist/captcha/captcha_guestbook.php" alt="gambar" /></div></div><br>
                                                <input type="text" value="" placeholder="Masukkan Captcha" id="billing_last_name" name="_captcha_guestbook" class="input-text " autocomplete="off" required="required">
                                            </p>
											
											<div class="clear"></div>

											
											<div class="form-row place-order">

                                            <input type="submit" data-value="Place order" value="Kirim" id="place_order" name="_submit" class="button alt">
											<br><br><br><br>

											</div>
											
											<?php
												$id = $_POST['_kode']; $tgl = $_POST['_tgl']; $nama = $_POST['_nama']; $email = $_POST['_email']; $isi = $_POST['_isi'];
												if(isset($_POST['_submit'])){
													if($_SESSION['_captcha_guestbook'] == $_POST['_captcha_guestbook']){
														$cari = strpos($email,"@");
														if($cari){
															$simpan = @mysqli_query($con,"INSERT into bukutamu(id_buku,tanggal,nama,email,isi,status,is_aktif)values('$id','$tgl','$nama','$email','$isi','TULIS','0')");
																if($simpan){
																	echo '<script type="text/javascript">
																	sweetAlert({ title: "Terima Kasih!", text: " Pesan Anda Telah Terkirim ", type: "success" },
																	function () { window.location.href = "index.php"; });
																	</script>';
																}else{
																	echo '<script type="text/javascript">sweetAlert("Maaf!", " Pesan Gagal Terkirim ", "error"); </script>';
																}
														}else{
															echo '<script type="text/javascript">sweetAlert("Maaf!", " Format Email Salah ", "error"); </script>';
														}
													}else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Captcha Anda Salah ", "error"); </script>';
													}	
												} 
											?>

                                        </div>
                                    </div>

                                   
                                </div>

                                
                                        
                            </form>

                        </div>                       
                    </div>                    
                </div>
				
				<div class="col-md-4">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            
                            <form enctype="multipart/form-data" action="#" class="checkout" method="post" name="checkout">

                                <div id="customer_details" class="col2-set">
                                    <div class="col-md-12">
                                        <div class="woocommerce-billing-fields">
                                            <h3>INFORMASI</h3>
											<?php if($get_user === '' OR $get_user == null){ ?>
                                            <div>
												<div class="product-main-img">
													<?php include "../connections/config.php"; 
													$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '1' AND nama_setting = 'logo'");
													while($row = mysqli_fetch_array($edit)){
														echo '<img src="../img/romsed/'.$row['ValueData'].'" alt="">';
													} ?>
													
												</div>
												<div class="clear"></div>
												
												<hr>
												<h4><i class="fa fa-globe"></i>&nbsp;&nbsp;Detail</h4>
												<?php include "../connections/config.php"; 
													$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '7' AND nama_setting = 'alamat'");
													while($row = mysqli_fetch_array($edit)){
														echo '<p>Alamat :<br>'.$row['ValueData'].'</p>';
													} ?>
												
												<?php include "../connections/config.php"; 
													$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '9' AND nama_setting = 'kontak'");
													while($row = mysqli_fetch_array($edit)){
														echo '<p>Kontak :<br>'.$row['ValueData'].'</p>';
													} ?>
												
												<?php include "../connections/config.php"; 
													$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '8' AND nama_setting = 'email'");
													while($row = mysqli_fetch_array($edit)){
														echo '<p>Email :<br>'.$row['ValueData'].'</p>';
													} ?>
												
												<div class="clear"></div>
                                            </div>
											
											<?php } else { 
											$edit1 = @mysqli_query($con, "select * from mstprovinsi where KodeProvinsi = '$prov'"); $row1 = mysqli_fetch_array($edit1);
											$edit2 = @mysqli_query($con, "select * from mstkabupaten where KodeKab = '$kab'"); $row2 = mysqli_fetch_array($edit2);
											$edit3 = @mysqli_query($con, "select * from mstkecamatan where KodeKec = '$kec'"); $row3 =mysqli_fetch_array($edit3);
											$edit4 = @mysqli_query($con, "select * from mstdesa where KodeDesa = '$desa'"); $row4 = mysqli_fetch_array($edit4); 
											?>
											<div>
												<h4><i class="fa fa-globe"></i>&nbsp;&nbsp;Detail</h4>
												<?php if($alamat == null OR $alamat === '' OR $alamat === '-'){ echo '<p>Alamat :<br>Belum Ada Data</p>'; } else { echo '<p>Alamat :<br>'.$alamat.'
												<br>'.$row4['desa'].',&nbsp;'.$row3['kecamatan'].',&nbsp;'.$row2['kabupaten'].',&nbsp;'.$row1['provinsi'].'</p>'; } ?>
												<?php if($kontakprs == null OR $kontakprs === ''){ echo '<p>Kontak :<br>Belum Ada Data</p>'; } else { echo '<p>Kontak :<br>'.$kontakprs.'</p>'; } ?>
												<?php if($emailprs == null OR $emailprs === ''){ echo '<p>Email :<br>Belum Ada Data</p>'; } else { echo '<p>Email :<br>'.$emailprs.'</p>'; } ?>
												
												<div class="clear"></div>
                                            </div>
											<?php } ?>

                                        </div>
                                    </div>

                                   
                                </div>

                                
                                        
                            </form>

                        </div>                       
                    </div>                    
                </div>
				
            </div>
        </div>
    </div>
    
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About Rombong Sedekah</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '3' AND nama_setting = 'about'");
						while($row = mysqli_fetch_array($edit)){
							echo '<p>'.ucwords($row['ValueData']).'</p>';
						} ?>
                         <div class="footer-social">
                            <a href="https://id-id.facebook.com/adminrombongsedekah/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/rombong_sedekah" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCOWTMXnIx9PDUSFRUtRRbCA" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div> 
                    </div>
                </div>
				
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>