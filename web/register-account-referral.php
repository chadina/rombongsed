<?php include "tgl-indo.php"; include "../connections/config.php";
$url = $_SERVER['REQUEST_URI'];
$link = str_replace('/grosirmart/web/register-account-referral.php','',$url);
$get_link = str_replace('?','',$link); 

date_default_timezone_set('Asia/Jakarta');
$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$stats = isset($_GET['st']) ? $_GET['st'] : 0 ; 
$md = isset($_GET['md']) ? base64_decode($_GET['md']) : 0 ; 
$dst = isset($_GET['dst']) ? base64_decode($_GET['dst']) : 0 ; 
$agn = isset($_GET['agn']) ? base64_decode($_GET['agn']) : 0 ; 
$rsl = isset($_GET['rsl']) ? base64_decode($_GET['rsl']) : 0 ; 

$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where (StatusPerson = 'DISTRIBUTOR' OR StatusPerson = 'AGEN' OR StatusPerson = 'RESELLER') 
AND IsAktif = 'True' AND replace(UserName, ' ','') = replace('$get_link',' ','')") or die( print_r( sqlsrv_errors(), true));  
$cari = @sqlsrv_num_rows($query); $dapat = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC);
if($cari === 0){
	$get_user = '';
} else {
	$get_user = $dapat['UserName'];
}

if($get_user === '' OR $get_user == null){
	$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$id."'") or die( print_r( sqlsrv_errors(), true));  
} else {
	$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where UserName = '".$get_user."'") or die( print_r( sqlsrv_errors(), true)); 
}

while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodeprs = $cari['KodePerson']; $kodeprs2 = $cari['KodePersonManual']; $namaprs = $cari['NamaPerson']; $tlahir = $cari['TempatLahir']; $tgllahir = $cari['TanggalLahir']; 
	$noktp = $cari['NoIdentitas']; $alamat = $cari['Alamat']; $kontakprs = $cari['ContactPerson']; $emailprs = $cari['Email']; $ketprs = $cari['Keterangan']; 
	$jenisk = $cari['JenisKelamin']; $fotoprs = $cari['FotoProfil']; $prov = $cari['Provinsi']; $kab = $cari['Kabupaten']; $kec = $cari['Kecamatan']; $desa = $cari['Desa'];
	$bankprs = $cari['NamaBankPerson']; $norekprs = $cari['NoRekPerson']; $banknama = $cari['AtasNamaBank']; $userprs = $cari['UserName']; $statusprs = $cari['StatusPerson'];
}
@sqlsrv_close();

?>

<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GrosirMart</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
               
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area -->
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Register Akun User</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="maincontent-area">
        <div class="container">
			<?php if(@$page == null){ ?>
            <div class="row">
				<div class="col-md-2"></div>
                <div class="col-md-8">
				    <div class="product-content-right">
                    <div class="woocommerce">
                  
					<div id="customer_details" class="col2-set">
					<?php /*if(@$statusprs === 'MEMBER'){ 
						echo '<script type="text/javascript">
						sweetAlert({ title: "Maaf!", text: " Sponsor Anda Berstatus Member, Hubungi Sponsor Anda Untuk Proses Upgrade Status Terlebih Dahulu ", type: "error" },
						function () { window.location.href = "default.php"; });
						</script>';
						
					} */ ?>
					<div class="col-md-12"><br><br>
					<form role="form" method="post">
						<div class="col-md-6">
						<h4>DATA SPONSOR</h4>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Kode Person <abbr title="required" class="required">*</abbr></label>
							<input type="hidden" value="<?php echo @$kodeprs; ?>" id="billing_first_name" name="_kode_sponsor" class="input-text " required="required" readonly>
							<input type="text" value="<?php echo @$kodeprs2.' : '.ucwords(@$namaprs); ?>" id="billing_first_name" name="_nama_sponsor" class="input-text "  autocomplete="off" required="required" readonly>
						</p>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Status <abbr title="required" class="required">*</abbr></label>
							<input type="text" value="<?php echo @$statusprs; ?>" id="billing_first_name" name="_status_sponsor" class="input-text "  autocomplete="off" required="required" readonly>
						</p>
						</div>
						
						<?php 
						if(@$stats === 'dst'){
							$query2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$dst."'") or die( print_r( sqlsrv_errors(), true));
						} elseif(@$stats === 'agn'){
							$query2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$agn."'") or die( print_r( sqlsrv_errors(), true));
						} elseif(@$stats === 'rsl'){
							$query2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where KodePerson = '".$rsl."'") or die( print_r( sqlsrv_errors(), true));
						} 
						
							if($get_user === '' OR $get_user == null){
								while($cari2 = @sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)){ 
									$kodeprs21 = $cari2['KodePerson']; $kodeprs22 = $cari2['KodePersonManual']; $namaprs2 = $cari2['NamaPerson']; $statusprs2 = $cari2['StatusPerson'];
								}
							} else {
								$kodeprs21 = $kodeprs; $kodeprs22 = $kodeprs2; $namaprs2 = $namaprs; $statusprs2 = $statusprs;
							}
							
						
						
						?>
						
						<div class="col-md-6">
						<h4>DATA UPLINE</h4>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Kode Person <abbr title="required" class="required">*</abbr></label>
							<input type="hidden" value="<?php echo @$kodeprs21; ?>" id="billing_first_name" name="_kode_sponsor2" class="input-text " required="required" readonly>
							<input type="text" value="<?php echo @$kodeprs22.' : '.ucwords(@$namaprs2); ?>" id="billing_first_name" name="_nama_sponsor" class="input-text "  autocomplete="off" required="required" readonly>
						</p>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Status <abbr title="required" class="required">*</abbr></label>
							<input type="text" value="<?php echo @$statusprs2; ?>" id="billing_first_name" name="_status_sponsor" class="input-text "  autocomplete="off" required="required" readonly>
						</p>
						</div>
					
						<div class="col-md-12"><hr>
						<h4>DATA AKUN</h4>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Status User <abbr title="required" class="required">*</abbr></label>
							<?php 
							if($get_user === '' OR $get_user == null){
								if(@$stats === 'dst'){ $statususer = 'AGEN'; }
								elseif(@$stats === 'agn'){ $statususer = 'RESELLER'; }
								elseif(@$stats === 'rsl'){ $statususer = 'USER'; } 
							} else {
								if(@$statusprs2 === 'MARKETPLACE'){ $statususer = 'DISTRIBUTOR'; }
								elseif(@$statusprs2 === 'DISTRIBUTOR'){ $statususer = 'AGEN'; }
								elseif(@$statusprs2 === 'AGEN'){ $statususer = 'RESELLER'; }
								elseif(@$statusprs2 === 'RESELLER'){ $statususer = 'USER'; }
							}
							
							
							?>
							<input type="text" value="<?php echo @$statususer; ?>" id="billing_first_name" name="_status_user" class="input-text "  autocomplete="off" required="required" readonly>
						</p>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Kode User <abbr title="required" class="required">*</abbr></label>
							<?php if(@$statususer  === 'USER'){ 
								$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='UGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
							}
							elseif(@$statususer  === 'RESELLER'){
								$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='RGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
							}
							elseif(@$statususer  === 'AGEN'){ 
								$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='AGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
							}
							/*elseif(@$statususer  === 'DISTRIBUTOR'){ 
								$sql2 = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePersonManual,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePersonManual,' ',''),8)='DGM-".date('Y')."'") or die( print_r( sqlsrv_errors(), true));
							} */
							
							// membuat id otomatis 2
							$nums2 = @sqlsrv_num_rows($sql2); 
							while($data2 = @sqlsrv_fetch_array($sql2, SQLSRV_FETCH_ASSOC)){
							if($nums2 === 0){ $kode2 = 1; }else{ $kode2 = $data2['kode'] + 1; }
							}
							// membuat kode user 2
							$bikin_kode2 = str_pad(@$kode2, 10, "0", STR_PAD_LEFT);
							if(@$statususer  === 'USER'){ 
								$kode_jadi2 = "UGM-".date('Y')."-".$bikin_kode2;
							}
							elseif(@$statususer  === 'RESELLER'){
								$kode_jadi2 = "RGM-".date('Y')."-".$bikin_kode2;
							}
							elseif(@$statususer  === 'AGEN'){
								$kode_jadi2 = "AGM-".date('Y')."-".$bikin_kode2;
							}
							/*elseif(@$statususer  === 'DISTRIBUTOR'){
								$kode_jadi2 = "DGM-".date('Y')."-".$bikin_kode2;
							}*/
												
							?>
							<input type="text" value="<?php echo @$kode_jadi2; ?>" id="billing_first_name" name="_kode_user" class="input-text "  autocomplete="off" required="required" readonly>
						</p>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Nama User <abbr title="required" class="required">*</abbr></label>
							<input type="text" value="" placeholder="Masukkan Nama User" id="billing_first_name" name="_nama_user" class="input-text "  autocomplete="off" required="required">
						</p><hr>
						
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Username <abbr title="required" class="required">*</abbr></label>
							<input type="text" value="" placeholder="Masukkan Username" id="billing_first_name" name="_username" class="input-text "  autocomplete="off" required="required">
						</p>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">Password <abbr title="required" class="required">*</abbr></label>
							<input type="password" value="" placeholder="Masukkan Password" id="billing_first_name" name="_password" class="input-text "  autocomplete="off" required="required">
						</p>
						<?php if(@$statususer  !== 'SUPPLIER' OR @$statususer  !== 'USER'){ ?>
						<p id="billing_first_name_field" class="form-row form-row-first validate-required">
							<label class="" for="billing_first_name">PIN Member <abbr title="required" class="required">*</abbr></label>
							<input type="text" value="" placeholder="Masukkan PIN" id="billing_first_name" name="_pin_user" class="input-text "  autocomplete="off" required="required">
						</p><hr>
						<?php } else { ?>
						<hr>
						<?php } ?>
						<input type="submit" data-value="Place order" value="Submit" id="place_order" name="_submit-input-user" class="button alt"><br><br><br><br>
						</div>
					
					</div>
					</form>
									
									<?php
										include "../connections/config.php";
										$_kodeuser = @htmlspecialchars($_POST['_kode_user']); $_namauser = @htmlspecialchars($_POST['_nama_user']); 
										$_username = @htmlspecialchars($_POST['_username']); $_kodepin = @htmlspecialchars($_POST['_pin_user']);
										$_pass = @htmlspecialchars(md5($_POST['_password'])); $_statususer = @htmlspecialchars($_POST['_status_user']);
										$_kodesponsor = @htmlspecialchars($_POST['_kode_sponsor']); $_kodesponsor2 = @htmlspecialchars($_POST['_kode_sponsor2']);
										
										if(isset($_POST['_submit-input-user'])){
											// membuat id otomatis
											$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePerson,10)) AS kode FROM MasterPerson WHERE LEFT(replace(KodePerson,' ',''),8)='PRS-".date('Y')."'") or die( print_r( sqlsrv_errors(), true)); 
											$nums = @sqlsrv_num_rows($sql); 
											while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
												if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
											}
											// membuat kode user
											$bikin_kode = str_pad($kode, 10, "0", STR_PAD_LEFT);
											$kode_jadi = "PRS-".date('Y')."-".$bikin_kode;
											
											if($statususer !== 'SUPPLIER' OR $statususer !== 'USER'){
												$cek_kode = @sqlsrv_query($dbconnect, "select * from kode_verifikasi where replace(kode_transaksi,' ','') = replace('$_kodepin',' ','') AND status = 'TRUE' AND is_dipakai = 'FALSE'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num_kode = @sqlsrv_num_rows($cek_kode);
											} else {
												$num_kode = 1;
											}
											if($num_kode === 1){
												$cek = @sqlsrv_query($dbconnect, "select * from MasterPerson where replace(KodePersonManual,' ','') = replace('$_kodeuser',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
												$num = @sqlsrv_num_rows($cek);
												if($num === 0){
													$cek2 = @sqlsrv_query($dbconnect, "select * from MasterPerson where replace(UserName,' ','') = replace('$_username',' ','')", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
													$num2 = @sqlsrv_num_rows($cek2);
													if($num2 === 0){
														if($_kodepin == null OR $_kodepin === ''){
															$query = @sqlsrv_query($dbconnect, "INSERT into MasterPerson(KodePerson,KodePersonManual,MemberID,NamaPerson,StatusPerson,IsAktif,
															Level,IsVerified,UserName,Password,FotoProfil,KodeCabang,IsSupplier,IsCustomer,IsSales,IsOther,PasswordTransaksi,MemberSponsor,Referral,Tgl_Reg,MemberSponsorAsal)
															values('$kode_jadi','$_kodeuser','$_kodeuser','$_namauser','$_statususer','1','USER','0','$_username','$_pass','round_account.png','CAB-0001',
															'0','1','1','1','$_pass','$_kodesponsor2','https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($kode_jadi))."',
															'".date('Y-m-d H:i:s')."','$_kodesponsor')") or die( print_r( sqlsrv_errors(), true)); 	 
														} else {
															$query = @sqlsrv_query($dbconnect, "INSERT into MasterPerson(KodePerson,KodePersonManual,MemberID,NamaPerson,StatusPerson,IsAktif,
															Level,IsVerified,UserName,Password,FotoProfil,KodeCabang,IsSupplier,IsCustomer,IsSales,IsOther,PasswordTransaksi,MemberSponsor,Referral,Tgl_Reg,MemberSponsorAsal,KodeTransaksi)
															values('$kode_jadi','$_kodeuser','$_kodeuser','$_namauser','$_statususer','1','USER','0','$_username','$_pass','round_account.png','CAB-0001',
															'0','1','1','1','$_pass','$_kodesponsor2','https://grosirmart.com/web/register-account.php?up=".htmlspecialchars(base64_encode($kode_jadi))."',
															'".date('Y-m-d H:i:s')."','$_kodesponsor','$_kodepin')") or die( print_r( sqlsrv_errors(), true)); 
														}
															if($query){
																if($get_user === '' OR $get_user == null){
																	@sqlsrv_query($dbconnect, "update kode_verifikasi set is_dipakai = 'TRUE', KodePerson = '$kode_jadi' where kode_transaksi = '$_kodepin'") or die( print_r( sqlsrv_errors(), true)); 
																	echo '<script type="text/javascript">
																	sweetAlert({ title: "Selamat!", text: " Register User Anda Berhasil ", type: "success" },
																	function () { window.location.href = "register-account.php?page='.htmlspecialchars(base64_encode("aplikasi")).'"; });
																	</script>';
																} else {
																	@sqlsrv_query($dbconnect, "update kode_verifikasi set is_dipakai = 'TRUE', KodePerson = '$kode_jadi' where kode_transaksi = '$_kodepin'") or die( print_r( sqlsrv_errors(), true)); 
																	echo '<script type="text/javascript">
																	sweetAlert({ title: "Selamat!", text: " Register User Anda Berhasil ", type: "success" },
																	function () { window.location.href = "register-account.php?'.$get_user.'&page='.htmlspecialchars(base64_encode("aplikasi")).'"; });
																	</script>';
																}
															}
															else{
																echo '<script type="text/javascript">sweetAlert("Maaf!", " Register Data Gagal ", "error"); </script>';
															}
													}else{
														echo '<script type="text/javascript">sweetAlert("Maaf!", " Username Sudah Terpakai ", "error"); </script>';
													}
												}else{
													echo '<script type="text/javascript">sweetAlert("Maaf!", " Kode User Sudah Ada ", "error"); </script>';
												}
											}else{
												echo '<script type="text/javascript">sweetAlert("Maaf!", " PIN Member Anda Belum Valid atau Sudah Terpakai ", "error"); </script>';
											}
											
										}
										@sqlsrv_close;

										?>

					                      
                </div>
				<div class="col-md-2"></div>
				
				</div>
				</div>
				
				</div>
				</div>
				
				<?php } elseif($page === 'aplikasi'){ ?>
				<div class="row">
				<div class="col-md-2"></div>
                <div class="col-md-8">
				    <div class="product-content-right">
                    <div class="woocommerce">
                  
					<div id="customer_details" class="col2-set">
					<div class="col-md-12 text-center">
					<br><br><br><br>
					<h3>SELAMAT, ANDA TELAH REGISTRASI DI GROSIRMART!</h3>
					<p>Silahkan Download Aplikasi Android GrosirMart dengan klik tombol di bawah ini :</p><hr>
					<p><a href="https://play.google.com/store/apps/details?id=app.android.grosirmart" class="btn btn-primary">https://play.google.com/store/apps/details?id=app.android.grosirmart</a></p>
					<br><br><br><br>
					</div>
					</div>
					
					</div>
					</div>
				</div>
				<div class="col-md-2"></div>
				
				</div>
				<?php } ?>
				
				
            </div>
        
    </div>
    
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About GrosirMart</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = sqlsrv_query($dbconnect, "select * from mst_setting where id = '5' AND nama_setting = 'about'") or die( print_r( sqlsrv_errors(), true));
						while($row = sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){
							echo '<p>'.ucwords($row['value']).'</p>';
						} ?>
                        <!-- <div class="footer-social">
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </div> -->
                    </div>
                </div>
                 
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>