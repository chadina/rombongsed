<?php
include('../connections/config.php');
include("tgl-indo.php");
date_default_timezone_set('Asia/Jakarta');

$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 

function TanggalIndo2($bulan){
$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
$result =  $BulanIndo[(int)$bulan-1] ;        
return($result);
}

?>

<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GrosirMart</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div><!-- End mainmenu area -->
    
    <!-- <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>DETAIL POST</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="single-product-area">
        <div class="container">
			<div class="row">
                <!-- <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Recent Posts</h2>
                        <ul>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                        </ul>
                    </div>
                </div> -->
                
					<div class="product-content-right">
                        <div class="woocommerce">
						  
						  <div class="col-lg-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Pencarian Arsip</h3>
					</div>
					<div class="panel-body">
						<div class="list-group">
						<?php $cari = @sqlsrv_query($dbconnect, "SELECT distinct Year(tanggal) as Tahun FROM mst_file Order By Tahun DESC") or die( print_r( sqlsrv_errors(), true)); 	 
					    while($row = @sqlsrv_fetch_array($cari)){	
							echo "<a class='list-group-item' href='arsip-file.php?year=".htmlspecialchars(base64_encode($row['Tahun']))."&month='>&#187;&nbsp;<strong>".$row['Tahun']."</strong></a>";	   
						$cari2 = @sqlsrv_query($dbconnect, "SELECT distinct MONTH(tanggal) as Bulan,(SELECT COUNT(*) FROM mst_file WHERE Year(tanggal) = Year(a.tanggal) AND Month(tanggal) = Month(a.tanggal)) as Jumlah					
						FROM mst_file as a WHERE Year(a.tanggal) = '".$row['Tahun']."'") or die( print_r( sqlsrv_errors(), true)); 	 
						while($row2 = @sqlsrv_fetch_array($cari2)){
							$bulanku = TanggalIndo2($row2['Bulan']);
							if($row['Tahun'] == htmlspecialchars(base64_decode(@$_GET['year']))){
								echo "<a class='list-group-item' href='arsip-file.php?year=".htmlspecialchars(base64_encode($row['Tahun']))."&month=".htmlspecialchars(base64_encode($row2['Bulan']))."'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#187;&nbsp;".$bulanku." (".$row2['Jumlah'].")</a>";	      
							}
						}
						}
						?>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-lg-9">
				<?php
				include "../connections/config.php";
				include "pagination.php";
				$kosong = null; $rowsPerPage = 10;
				if(base64_decode(@$_GET['year'])=="" AND base64_decode(@$_GET['month'])==""){
					echo '<p>Pilih navigasi <strong>"Tahun"</strong> dan <strong>"Bulan"</strong> pada kolom <strong>Arsip File</strong> untuk menampilkan Daftar File</p><hr>';
				} 	
				elseif(base64_decode(@$_GET['month'])==""){
				}
				else{
				
						
						
							
								$reload = "arsip-file.php?pagination=true&year=".base64_encode(@$_GET['year']);
								$result = sqlsrv_query($dbconnect, "select * from mst_file order by  DESC",array(),array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							}else{
								$reload = "list-barang.php?pagination=true";
								$result = sqlsrv_query($dbconnect, "select masterbarang.Gambar, masterbarang.NamaBarang, masterbarang.KodePerson, masterbarang.HargaEceran, masterbarang.KodeBarang from MasterBarang 
								INNER JOIN MasterPerson ON MasterPerson.KodePerson = MasterBarang.KodePerson 
								INNER JOIN HargaJualSatuan ON HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang
								WHERE MasterPerson.KodePerson != '' AND MasterBarang.IsAktif = 'TRUE' AND HargaJualSatuan.KodeSatuan = 'SAT-0000001' order by masterbarang.KodeBarang DESC",array(),array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
							}
							
						
						
						function getPage($result, $pageNum, $rowsPerPage){
										$offset = ($pageNum - 1) * $rowsPerPage;
										$rows = array();
										$i = 0;
										while(($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC, SQLSRV_SCROLL_ABSOLUTE, $offset + $i)) && $i < $rowsPerPage)
										{
											array_push($rows, $row);
											$i++;
										}
										return $rows;
									}
									echo '<div class = "col-lg-12"><div class="row">';
									$pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
									$no_urut = ($pageNum-1)*$rowsPerPage; 
									$page = getPage($result, $pageNum, $rowsPerPage);
									$kolom = 3;
									
									foreach($page as $row){
										if(@$i>$kolom){
										echo "</div>"; echo "<div class='row'>";
										@$i=0;
										}
										@$i++; 
										
									?>
									<div class="col-sm-3">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<?php if($id === 'DPT-0000001'){
														$edit1 = sqlsrv_query($dbconnect, "select * from MasterBarang WHERE KodeBarang = '".$row[0]."'");
														while($row1 = @sqlsrv_fetch_array($edit1, SQLSRV_FETCH_ASSOC)){ 
															if($row1['KodeBarang']=='' or $row1['KodeBarang']==null or !file_exists('../img/foto-barang/thumb_'.$row1['Gambar'])){
																echo '<img class="img-thumbnail" src="../img/foto-barang/thumb_no-image.png" alt="" /><br><br>';
															}else{
																echo '<img class="img-thumbnail" src="../img/foto-barang/thumb_'.$row1['Gambar'].'" alt="" /><br><br>';
															}
															?>
															<p><strong><a href="detail-barang.php?id=<?php echo htmlspecialchars(base64_encode($row[4]));?>"><?php echo ucwords($row1['NamaBarang']); ?></a></strong><hr>
															<?php $edit2 = @sqlsrv_query($dbconnect, "select * from HargaJualSatuan WHERE KodeBarang = '".$row1['KodeBarang']."' AND KodeSatuan = 'SAT-0000001'");
															while($row2 = @sqlsrv_fetch_array($edit2, SQLSRV_FETCH_ASSOC)){ ?>
															<!-- <font color="red">Internal : Rp <?php echo number_format($row2['HargaJual1']); ?>,-<br></font> -->
															<font color="red">Distributor : Rp <?php echo number_format($row2['HargaJual2']); ?><br></font>
															<font color="red">Agen : Rp <?php echo number_format($row2['HargaJual3']); ?><br></font>
															<font color="red">Reseller : Rp <?php echo number_format($row2['HargaJual4']); ?><br></font>
															<font color="red">Konsumen : Rp <?php echo number_format($row2['HargaJual5']); ?><br></font><hr>
															<?php } ?>
															<!-- <h2>Rp <?php /*echo $row['HargaEceran']; ?>,-</h2> -->
															<?php /* $edit2 = sqlsrv_query($dbconnect, "select NamaPerson from MasterPerson WHERE KodePerson = '".$row[2]."'");
															$row2 = sqlsrv_fetch_array($edit2, SQLSRV_FETCH_ASSOC);
															echo ucwords($row2['NamaPerson']); *?></p>
															<!-- <a href="product-details.php?id=<?php echo base64_encode($kode_barang);*/ ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a> -->
													
													<?php } 
													} else {
														if($row[0]=='' or $row[0]==null or !file_exists('../img/foto-barang/thumb_'.$row[0])){
															echo '<img class="img-thumbnail" src="../img/foto-barang/thumb_no-image.png" alt="" /><br><br>';
														}else{
															echo '<img class="img-thumbnail" src="../img/foto-barang/thumb_'.$row[0].'" alt="" /><br><br>';
														}
														?>
														<p><strong><a href="detail-barang.php?id=<?php echo htmlspecialchars(base64_encode($row[4]));?>"><?php echo ucwords($row[1]); ?></a></strong><hr>
														<?php $edit2 = sqlsrv_query($dbconnect, "select * from HargaJualSatuan WHERE KodeBarang = '".$row[4]."' AND KodeSatuan = 'SAT-0000001'");
														while($row2 = @sqlsrv_fetch_array($edit2, SQLSRV_FETCH_ASSOC)){ ?>
														<!-- <font color="red">Internal : Rp <?php echo number_format($row2['HargaJual1']); ?>,-<br></font> -->
														<font color="red">Distributor : Rp <?php echo number_format($row2['HargaJual2']); ?><br></font>
														<font color="red">Agen : Rp <?php echo number_format($row2['HargaJual3']); ?><br></font>
														<font color="red">Reseller : Rp <?php echo number_format($row2['HargaJual4']); ?><br></font>
														<font color="red">Konsumen : Rp <?php echo number_format($row2['HargaJual5']); ?><br></font><hr>
														<?php } ?>
														<!-- <h2>Rp <?php /* echo $row['HargaEceran']; ?>,-</h2> -->
														<?php /* $edit2 = sqlsrv_query($dbconnect, "select NamaPerson from MasterPerson WHERE KodePerson = '".$row[2]."'");
														$row2 = sqlsrv_fetch_array($edit2, SQLSRV_FETCH_ASSOC);
														echo ucwords($row2['NamaPerson']); *?></p>
														<!-- <a href="product-details.php?id=<?php echo base64_encode($kode_barang); */?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a> -->
													
												
													<?php } ?>
												
												
												</div>
													
											</div>
										</div>
									</div>
									<?php } 
									
									echo '</div></div><br>';
									$rowsReturned = sqlsrv_num_rows($result);
									if($rowsReturned === false)
										die( print_r( sqlsrv_errors(), true));
									elseif($rowsReturned == 0)
									{
										echo '<div class="alert alert-info alert-dismissable">
											<h4><p align="center">Tidak Ada Data<br></p></h4>
										</div><br/><br/><br/>';
									}
									else
									{     
										// Display page links.
										$numOfPages = ceil($rowsReturned/$rowsPerPage);
										for($i = 1; $i<=$numOfPages; $i++){}
										/*{
											$pageLink = "?pageNum=$i";
											print("<a href=$pageLink>$i</a>&nbsp;&nbsp;");
										}*/
											if($id === 'DPT-0000001'){
												echo '';
											}else{
												echo '<div class="clear"></div><div class="text-center col-lg-12">'.paginate_one($reload, $pageNum, $numOfPages).'</div>';
											}
										echo "<br/><br/>";
									}
									 
						}
						?>
				
			</div>
						
                        </div>                       
                    </div>                    
                
            </div>
        </div>
    </div>
    
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About GrosirMart</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = sqlsrv_query($dbconnect, "select * from mst_setting where id = '5' AND nama_setting = 'about'") or die( print_r( sqlsrv_errors(), true));
						while($row = sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){
							echo '<p>'.ucwords($row['value']).'</p>';
						} ?>
                        <!-- <div class="footer-social">
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </div> -->
                    </div>
                </div>
                 
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>