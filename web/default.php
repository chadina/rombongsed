<?php include "akses.php"; include "tgl-indo.php"; include "../connections/config.php";
$url = $_SERVER['REQUEST_URI'];
$pecah = explode("&",$url);
$link = str_replace('/RombongSedekah/web/default.php','',$pecah[0]);
$get_link = str_replace('?','',$link); 

$query = mysqli_query($con, "select * from mstuser where IsAktif = '1' AND replace(UserName, ' ','') = replace('$get_link',' ','')");  
$cari = mysqli_num_rows($query); $dapat = mysqli_fetch_array($query);
if($cari === 0){
	$get_user = '';
	$get_id = '';
} else {
	$get_user = $dapat['UserName'];
	$get_id = $dapat['KodeNasabah'];
}
 
?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rombong Sedekah</title>
    
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/stylesheet.css">

  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            
                <?php include "banner.php"; ?>
           
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area -->
    
    <div class="slider-area">
        	<!-- Slider -->
			<div class="block-slider block-slider4">
				<ul class="" id="bxslider-home4">
					<?php include "../connections/config.php";
							$edit = mysqli_query($con, "select * from mstslider where is_aktif = 'TRUE' order by id_slider");
							while($row = mysqli_fetch_array($edit)){
								$judul	= $row['judul'];
								$title	= $row['title'];
								$isi 	= $row['isi'];
								$foto 	= $row['foto'];
							?>
					<li><br><div class="col-lg-1"></div>
						<?php if($foto == null OR $foto == '' ){
							echo '<div class="col-lg-3"><div class="text-center"><img src="../android_rombongsedekah/img/slider/no-image.png" alt="Slide" /></div></div>';
						}elseif(!file_exists('../android_rombongsedekah/img/slider/'.$foto)){
							echo '<div class="col-lg-3"><div class="text-center"><img src="../android_rombongsedekah/img/slider/no-image.png" alt="Slide" /></div></div>';
						}else{
							echo '<div class="col-lg-3"><div class="text-center"><img src="../android_rombongsedekah/img/slider/'.$foto.'" alt="Slide"  /></div></div>';
						} ?>
						<div class="col-lg-1"></div>
						<div class="col-lg-7">
						<div class="caption-group">
							<br><br><h2 class="caption title">
								<span class="primary"><strong><?php echo $judul; ?></strong></span><br>
								<span class="primary"><?php echo $title; ?></span>
							</h2>
							<h4 class="caption subtitle"><?php echo $isi; ?></h4>
							<!-- <a class="caption button-radius" href="#"><span class="icon"></span>Shop now</a> -->
						</div>
						</div>
					</li>
							<?php } ?>
					
				</ul><br>
			</div>
			<!-- ./Slider -->
    </div><hr> <!-- End slider area -->
	
	<div class="maincontent-area">
        <div class="container">
            <div class="col-md-12 text-center">
					<h2 class="sidebar-title">Program Kami</h2>
                    <div class="latest-product"><br>
	
	<!-------------------------Promo Terbaru---------------------------------->
					
					<?php include "../connections/config.php";
					echo '<div class="row">';
					$kolom = 4; $i=0; 
					$result = mysqli_query($con, "select id_program, nama_program, foto from mstprogram WHERE is_aktif = 'TRUE' order by nama_program ASC") ;
					while($row = mysqli_fetch_array($result)){ 
					$id		= $row['id_program'];
					$nama	= $row['nama_program'];
					$gbr	= $row['foto'];
					
					
					if($i>=$kolom){
					echo "</div>"; echo "<div class='row'>";
					$i=0;
					}
					$i++; ?>
	
									<div class="col-sm-3">
									<div class="panel panel-info">
										<div class="panel-heading">
										<div class="row">
										<div class="col-sm-12">
												<?php
													if($gbr=='' or $gbr==null or !file_exists('../android_rombongsedekah/img/program/'.$gbr)){
														echo '<div class="col-sm-5"><img class="img-circle" src="../android_rombongsedekah/img/program/no-image.png" alt="" /></div>';
													}else{
														echo '<div class="col-sm-5"><img class="img-circle" src="../android_rombongsedekah/img/program/'.$gbr.'" alt="" /></div>';
													} 
													?>
													<div class="col-sm-7"><strong><?php echo '<a href = "list-barang.php?kode='.htmlspecialchars(base64_encode($id)).'">'.ucwords($nama).'</a>'; ?></strong></div>
													
										</div>	
										</div>
										</div>
									</div>
									</div>
								
					<?php } ?>	
					</div>
				</div>
            </div><br>
        </div>
		
        </div><hr>
    </div> <!-- End main content area -->
	
	<div class="maincontent-area">
        <div class="container">
            <div class="col-md-12">
                    <div class="latest-product">
                        <!-- <h2 class="section-title">Aktivitas Terbaru</h2> -->
						<h2 class="sidebar-title text-center">Testimoni</h2>
						<div class="product-carousel">
							<?php include "../connections/config.php";
							$result22 = mysqli_query($con, "select   * from bukutamu where status = 'TULIS' AND is_aktif = '1' order by tanggal DESC LIMIT 5") ;
							while($row22 = mysqli_fetch_array($result22)){ 
							
							?>
								<div class="single-product">
								<div class="panel panel-info">
										<div class="panel-heading">
											<div class="form-group">
												<label><?php echo ucwords($row22['nama'])."<br>(".$row22['email'].")"; ?></label>
													<dd><?php echo TanggalIndo ($row22['tanggal']); ?></dd><br>
													<dd><?php echo ucwords(substr($row22['isi'],0,250))." ... "; ?></dd>
											</div>
											<?php $cari2 = mysqli_query($con, "select * from bukutamu where id_jawab = '".$row22['id_buku']."' order by tanggal DESC");
											$jumlah = mysqli_num_rows($cari2);
											if($jumlah === 0){ echo ''; } else { echo '<a href="#" class="btn btn-default btn-sm">'.$jumlah.' balasan</a>'; }
											?>
											<a href="detail-bukutamu.php?id=<?php echo htmlspecialchars(base64_encode($row22['id_buku'])); ?>" class="btn btn-primary btn-sm">Lihat</a>
										</div>
									</div>
								</div>
							
							<?php } ?>	
							
                        </div>
                    </div><br><div class="text-center"><a href="kontak.php" class="btn btn-primary btn-md text-center">Tulis Testimoni</a></div><br><br>
			</div>
        </div>
    </div> <!-- End main content area -->
	
	
	
	<div class="footer-top-area" style="background-color:#f3f3f3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="latest-product">
                        <!-- <h2 class="section-title">Aktivitas Terbaru</h2> -->
						<h2 class="sidebar-title">Kegiatan Kami</h2>
						<div class="product-carousel">
							<?php include "../connections/config.php";
							$edit = mysqli_query($con, "select  * from mstkegiatan order by id_kegiatan DESC LIMIT 10") ;
							while($row = mysqli_fetch_array($edit)){
								$id			= $row['id_kegiatan'];
								$tgl		= $row['tanggal'];
								$judul 		= $row['judul'];
								$penulis	= $row['deskripsi'];
								$isi		= $row['Keterangan'];
								$gambar		= $row['foto'];
							
							?>
                            <div class="single-product">
                                <div class="product-f-image">
									<?php if($gambar=='' or $foto==null or !file_exists('../android_rombongsedekah/img/kegiatan/'.$gambar)){
										echo '<img class="img-thumbnail" src="../android_rombongsedekah/img/kegiatan/no-image.png" alt="" />';
									}else{
										echo '<img class="img-thumbnail" src="../android_rombongsedekah/img/kegiatan/'.$gambar.'" alt="" />';
									}
									?>
                                </div>
                                
                                <h2><a href="#"><?php echo ucwords($judul); ?></a></h2>
                                <span><?php echo ucwords(substr($isi,0,200))."....."; ?></span><br><br>
								<?php if($get_user === '' OR $get_user == null){ ?>
									<a href="detail-post.php?id=<?php echo base64_encode($id);?>" class="btn btn-default add-to-cart"><i class="fa fa-search"></i>&nbsp;Lihat Selengkapnya</a>
								<?php } else { ?>
									<a href="detail-post.php?<?php echo $get_user; ?>&id=<?php echo base64_encode($id);?>" class="btn btn-default add-to-cart"><i class="fa fa-search"></i>&nbsp;Lihat Selengkapnya</a>
								<?php } ?>
								
								<!-- <div class="product-carousel-price">
                                    <ins>$700.00</ins> <del>$100.00</del>
                                </div> -->
                            </div>
							<?php } ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->
	        
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About Rombong Sedekah</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '3' AND nama_setting = 'about'");
						while($row = mysqli_fetch_array($edit)){
							echo '<p>'.ucwords($row['ValueData']).'</p>';
						} ?>
                         <div class="footer-social">
                            <a href="https://id-id.facebook.com/adminrombongsedekah/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/rombong_sedekah" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCOWTMXnIx9PDUSFRUtRRbCA" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div> 
                    </div>
                </div>
				
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- Latest jQuery form server --
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN --
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
	
	<!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>