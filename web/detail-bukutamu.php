<?php
include('../connections/config.php');
include("tgl-indo.php");
date_default_timezone_set('Asia/Jakarta');

$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 
$query = @mysqli_query($con, "select * from bukutamu where id_buku = '".$id."'");
while($cari = @mysqli_fetch_array($query)){ 
	$kodebuku = $cari['id_buku']; $tglbuku = $cari['tanggal']; $nama = $cari['nama']; $email = $cari['email']; $isi = $cari['isi']; $stats = $cari['status']; $kodejawab = $cari['id_jawab'];
}
@mysqli_close();

$wib = date("H:i:s"); // waktu Indonesia bagian Barat yang telah di set mengambil zona waktu Asia/Jakarta.

?>

<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rombong Sedekah</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area -->
    
	<div class="single-product-area">
        <div class="container">
            <div class="row">
                               
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">
							<h2 class="sidebar-title text-center">Testimoni</h2>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<dd><?php echo "Oleh : <strong>".strtoupper($nama)."</strong><br>Email : ".$email; ?></dd>
                                    </div><hr>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="form-group">
													<label><?php echo ucwords($nama)." . ".TanggalIndo($tglbuku); ?></label>
														<dd><?php echo ucwords($isi); ?></dd>
												</div>
												
											</div>
										</div>
                                    </div>
								</div>
								<div class="col-lg-2"></div>
							</div>
							<?php $query22 = @mysqli_query($con, "select mstuser.NamaLengkap, bukutamu.tanggal,bukutamu.isi from bukutamu inner join mstuser on mstuser.id_user = bukutamu.id_user 
							where bukutamu.status = 'JAWAB' AND bukutamu.id_jawab = '".$id."'order by tanggal ASC");
							while($cari22 = @mysqli_fetch_array($query22)){ ?>
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<div class="form-group">
										<div class="panel panel-primary">
											<div class="panel-heading">
												<div class="form-group text-right">
													<label><?php echo ucwords($cari22['NamaLengkap'])." . ".TanggalIndo($cari22['tanggal']); ?></label>
														<dd><?php echo ucwords($cari22['isi']); ?></dd>
												</div>
												
											</div>
										</div>
                                    </div>
								</div>
							</div>
							<?php } ?>
				
				
						
                        </div>                       
                    </div>                    
                </div>
				
            </div>
        </div>
    </div>
    
   <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About Rombong Sedekah</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '3' AND nama_setting = 'about'");
						while($row = mysqli_fetch_array($edit)){
							echo '<p>'.ucwords($row['ValueData']).'</p>';
						} ?>
                         <div class="footer-social">
                            <a href="https://id-id.facebook.com/adminrombongsedekah/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/rombong_sedekah" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCOWTMXnIx9PDUSFRUtRRbCA" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div> 
                    </div>
                </div>
				
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>