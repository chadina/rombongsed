<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rombong Sedekah</title>
    
       <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Slide Galeri -->
	<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="css/jcarousel.css" rel="stylesheet" />
	<link href="css/flexslider.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area --> 
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Galeri Foto</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="container">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">
							<ul id="thumbs" class="portfolio">    
                            <!-------------------------Galeri Foto---------------------------------->
							<?php
							include "../connections/config.php";
							include "pagination.php";
							  // mengatur variabel reload dan sql
							  $kosong=null;
							  if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
								// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
								$keyword=$_REQUEST['keyword'];
								$reload = "MasterGaleri.php?pagination=true&keyword=$keyword";
								//sql =  "SELECT * FROM kontenweb WHERE JenisKonten='GALERI' AND Judul LIKE '%$keyword%' ORDER BY KodeKonten ASC";
								$result = mysqli_query($con,$sql);
							  }else{
							  //jika tidak ada pencarian pakai ini
								$reload = "galeri.php?pagination=true";
								$sql =  "SELECT  a.gambar, a.nama_foto, a.keterangan, b.NamaLengkap from mst_galeri a join mstuser b on a.KodeUser = b.id_user WHERE a.is_aktif = 'TRUE' ORDER BY a.id_foto";
								$result = mysqli_query($con,$sql);
							  }
							  
							  //pagination config start
							  $rpp = 8; // jumlah record per halaman
							  $page = intval(@$_GET["page"]);
							  if($page<=0) $page = 1;  
							  $tcount = mysqli_num_rows($result);
							  $tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
							  $count = 0;
							  $i = ($page-1)*$rpp;
							  $no_urut = ($page-1)*$rpp;
							  //pagination config end 
								while(($count<$rpp) && ($i<$tcount)) {
								mysqli_data_seek($result,$i);
								$data = mysqli_fetch_array($result);
							?>
							<div class="col-sm-3">
								<li class="item-thumbs col-lg-12 images" data-id="id-0" data-type="images">
									<!-- Fancybox - Gallery Enabled - Title - Full Image -->
									<a class="hover-wrap fancybox" data-fancybox-group="images" width = '20%' title="<?php echo strtoupper($data['nama_foto']); ?>" href="<?php echo '../img/foto-galeri/'.$data['gambar'];  ?>">
									<span class="overlay-img"></span>
									<span class="overlay-img-thumb font-icon-plus"></span>
									</a>
									<!-- Thumb Image and Description -->
									<img class="img-thumbnail" src="../img/foto-galeri/thumb_<?php echo $data['gambar']; ?>"  width = '20%'alt="<?php echo "Oleh : ".ucwords($data['NamaLengkap'])."<br>".ucwords($data['keterangan']); ?>">
								</li>
							</div>
								  <?php
									$i++; 
									$count++;
								  }
								  ?>
							</ul>
                        </div>                       
                    </div>                    
                </div>
				<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
            </div>
        </div>
    </div>
    
   <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About Rombong Sedekah</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = mysqli_query($con, "select * from systemsetting where KodeSetting = '3' AND nama_setting = 'about'");
						while($row = mysqli_fetch_array($edit)){
							echo '<p>'.ucwords($row['ValueData']).'</p>';
						} ?>
                         <div class="footer-social">
                            <a href="https://id-id.facebook.com/adminrombongsedekah/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/rombong_sedekah" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCOWTMXnIx9PDUSFRUtRRbCA" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div> 
                    </div>
                </div>
				
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
	
	<!-- Slide -->
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/google-code-prettify/prettify.js"></script>
	<script src="js/portfolio/jquery.quicksand.js"></script>
	<script src="js/portfolio/setting.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>
	
  </body>
</html>