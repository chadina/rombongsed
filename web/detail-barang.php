<?php
include('../connections/config.php');
include("tgl-indo.php");
date_default_timezone_set('Asia/Jakarta');

$id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ; 

$url = $_SERVER['REQUEST_URI']; 
$pecah = explode("&",$url);
$link = str_replace('/grosirmart/web/detail-barang.php','',$pecah[0]);
$get_link = str_replace('?','',$link); 

$query = @sqlsrv_query($dbconnect, "select * from MasterPerson where (StatusPerson = 'SUPPLIER' OR StatusPerson = 'DISTRIBUTOR' OR StatusPerson = 'AGEN' OR StatusPerson = 'RESELLER' OR StatusPerson = 'USER' OR StatusPerson = 'SUPPLIER') 
AND IsAktif = 'True' AND replace(UserName, ' ','') = replace('$get_link',' ','')") or die( print_r( sqlsrv_errors(), true));  
$cari = @sqlsrv_num_rows($query); $dapat = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC);
if($cari === 0){
	$get_user = '';
} else {
	$get_user = $dapat['UserName'];
} 
	
$query = @sqlsrv_query($dbconnect, "select MasterBarang.*, HargaJualSatuan.HargaBeli, HargaJualSatuan.HargaJual, HargaJualSatuan.HargaEceranTertinggi, HargaJualSatuan.HargaJual1, HargaJualSatuan.HargaJual2, HargaJualSatuan.HargaJual3, HargaJualSatuan.HargaJual4,
HargaJualSatuan.HargaJual5, MasterDepartemen.NamaDepartemen, MasterSubDepartemen.NamaSubDepartemen, MasterPerson.NamaPerson from MasterBarang 
inner join MasterPerson on MasterPerson.KodePerson = MasterBarang.KodePerson
inner join MasterDepartemen on MasterDepartemen.KodeDepartemen = MasterBarang.KodeDepartemen
inner join MasterSubDepartemen on MasterSubDepartemen.KodeSubDepartemen = MasterBarang.KodeSubDepartemen
inner join HargaJualSatuan on HargaJualSatuan.KodeBarang = MasterBarang.KodeBarang where MasterBarang.KodeBarang = '".$id."'  AND HargaJualSatuan.KodeSatuan = 'SAT-0000001'") or die( print_r( sqlsrv_errors(), true));
while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
	$kodebrg = $cari['KodeBarang']; $kodebrg2 = $cari['KodeBarangManual']; $barang = $cari['NamaBarang']; $fotobrg = $cari['Gambar']; $ketbrg = $cari['Keterangan'];
	$deptbrg = $cari['KodeDepartemen']; $subbrg = $cari['KodeSubDepartemen']; $katbrg = $cari['KodeKategory']; $hargabrg = $cari['HargaBeli']; $harga1 = $cari['HargaJual1'];
	$harga2 = $cari['HargaJual2']; $harga3 = $cari['HargaJual3']; $harga4 = $cari['HargaJual4']; $harga5 = $cari['HargaJual5']; $hargabrg2 = $cari['HargaEceranTertinggi'];
	$namadept = $cari['NamaDepartemen']; $namasubdept = $cari['NamaSubDepartemen']; $kodeprs = $cari['KodePerson']; $namaprs = $cari['NamaPerson'];
	$hargajualbrg = $cari['HargaJual']; $fotobrg2 = $cari['Gambar2']; $fotobrg3 = $cari['Gambar3']; $isvoucher = $cari['IsDapatVoucher'];
}
@sqlsrv_close();

?>

<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GrosirMart</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Slide Galeri -->
	<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="css/jcarousel.css" rel="stylesheet" />
	<link href="css/flexslider.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area" style="background-image:url(img/pattern.jpg)">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div><!-- End mainmenu area -->
    
    <!-- <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>DETAIL POST</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="single-product-area">
        <div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="sidebar-title text-center">
						<?php echo ucwords($barang); ?>
						
					</h2><hr>
				</div>
			</div>
            <div class="row">
                <!-- <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Recent Posts</h2>
                        <ul>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                        </ul>
                    </div>
                </div> -->
                
					<div class="product-content-right">
                        <div class="woocommerce">
						  
						<div class="col-sm-3">
							<div class="single-sidebar">
							<h4 class="sidebar-title">KATEGORI</h4>
							<ul>
								<?php if($get_user === '' OR $get_user == null){ ?>
									<li><a href="list-barang.php">Semua Barang</a></li>
									<li><a href="list-barang.php?kode=<?php echo htmlspecialchars(base64_encode('DPT-0000001')); ?>">Produk Terlaris</a></li>
								<?php } else { ?>
									<li><a href="list-barang.php?<?php echo $get_user; ?>">Semua Barang</a></li>
									<li><a href="list-barang.php?<?php echo $get_user; ?>&kode=<?php echo htmlspecialchars(base64_encode('DPT-0000001')); ?>">Produk Terlaris</a></li>
								<?php } ?>
							<?php $edit = sqlsrv_query($dbconnect, "select * from masterdepartemen where KodeDepartemen != 'DPT-0000001' AND Keterangan = 'PRODUK' order by NamaDepartemen") or die( print_r( sqlsrv_errors(), true));
							while($row = sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){
								if($get_user === '' OR $get_user == null){
									echo '<li><a href="list-barang.php?kode='.htmlspecialchars(base64_encode($row['KodeDepartemen'])).'">'.ucwords($row["NamaDepartemen"]).'</a></li>';
								} else {
									echo '<li><a href="list-barang.php?'.$get_user.'&kode='.htmlspecialchars(base64_encode($row['KodeDepartemen'])).'">'.ucwords($row["NamaDepartemen"]).'</a></li>';
								}
							} ?>
							</ul>
							</div>
						</div>
						
						<div class="col-sm-9">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<div class="row">
										<ul id="thumbs" class="portfolio">
                                            <?php if($fotobrg !== null AND file_exists('../img/foto-barang/'.$fotobrg)){ $gambar = $fotobrg; ?>
											<!-- Item Project and Filter Name 1 -->
											<li class="item-thumbs col-lg-12 images" data-id="id-0" data-type="images">
											<!-- Fancybox - Gallery Enabled - Title - Full Image -->
											<a class="hover-wrap fancybox" data-fancybox-group="images" title="<?php echo ucwords($barang); ?>" href="../img/foto-barang/<?php echo $gambar; ?>">
											<span class="overlay-img"></span>
											<span class="overlay-img-thumb font-icon-plus"></span>
											</a>
											<!-- Thumb Image and Description -->
											<img class="img-thumbnail" src="../img/foto-barang/thumb_<?php echo $gambar; ?>" alt="<?php echo "Harga : Rp. ".number_format($harga5); ?>">
											</li>
											<?php } else { echo '<li class="item-thumbs col-lg-12 images" data-id="id-0" data-type="images"><img class="img-thumbnail" src="../img/foto-barang/thumb_no-image.png" /></li>'; } ?>
										<ul/>
										</div>
                                   
										<div class="row">
										<ul id="thumbs" class="portfolio">
											<?php if($fotobrg2 !== null AND file_exists('../img/foto-barang/'.$fotobrg2)){ $gambar2 = $fotobrg2; ?>
											<!-- Item Project and Filter Name 1 -->
											<li class="item-thumbs col-lg-6 images" data-id="id-0" data-type="images">
											<!-- Fancybox - Gallery Enabled - Title - Full Image -->
											<a class="hover-wrap fancybox" data-fancybox-group="images" title="<?php echo ucwords($barang); ?>" href="../img/foto-barang/<?php echo $gambar2; ?>">
											<span class="overlay-img"></span>
											<span class="overlay-img-thumb font-icon-plus"></span>
											</a>
											<!-- Thumb Image and Description -->
											<img class="img-thumbnail" src="../img/foto-barang/thumb_<?php echo $gambar2; ?>" alt="<?php echo "Harga : Rp. ".number_format($harga5); ?>">
											</li>
											<?php } else { echo ''; } ?>
											
											<?php if($fotobrg3 !== null AND file_exists('../img/foto-barang/'.$fotobrg3)){ $gambar3 = $fotobrg3; ?>
											<!-- Item Project and Filter Name 1 -->
											<li class="item-thumbs col-lg-6 images" data-id="id-0" data-type="images">
											<!-- Fancybox - Gallery Enabled - Title - Full Image -->
											<a class="hover-wrap fancybox" data-fancybox-group="images" title="<?php echo ucwords($barang); ?>" href="../img/foto-barang/<?php echo $gambar3; ?>">
											<span class="overlay-img"></span>
											<span class="overlay-img-thumb font-icon-plus"></span>
											</a>
											<!-- Thumb Image and Description -->
											<img class="img-thumbnail" src="../img/foto-barang/thumb_<?php echo $gambar3; ?>" alt="<?php echo "Harga : Rp. ".number_format($harga5); ?>">
											</li>
											<?php } else { echo ''; } ?>
											
										<ul/>
											
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
                                        <label>Nama Barang</label>
                                            <dd><?php echo ucwords($barang); ?></dd>
                                    </div>
									<div class="form-group">
										<label>Satuan</label>
										<?php if(@$satbrg == null){ $satbrg2 = 'SAT-0000001'; } else { $satbrg2 = $satbrg; }
											$list = @sqlsrv_query($dbconnect, "SELECT * FROM MstSatuan Inner Join HargaJualSatuan On HargaJualSatuan.KodeSatuan = MstSatuan.KodeSatuan 
											where HargaJualSatuan.KodeBarang = '".$id."' ORDER BY MstSatuan.KodeSatuan") or die( print_r( sqlsrv_errors(), true)); 
											/*$jsArray_h1 = "var rekName = new Array();\n"; 
											$jsArray_h2 = "var rekName2 = new Array();\n"; 
											$jsArray_h3 = "var rekName3 = new Array();\n"; 
											$jsArray_h4 = "var rekName4 = new Array();\n";*/
											$jsArray_h5 = "var rekName5 = new Array();\n";
														
											echo '<select class="form-control" name="_kodesat" 
											onchange="document.getElementById(\'_h5\').value = rekName5[this.value]" "autocomplete="off">';
											
											while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
												if($satbrg2 !== null){
													if($daftar['KodeSatuan'] === $satbrg2){
														echo "<option value=\"".$daftar['KodeSatuan']."\" selected='selected'>".ucwords($daftar['NamaSatuan'])."</option>\n";
													}else{
														echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
													}
												}else{
													echo "<option value=\"".$daftar['KodeSatuan']."\">".ucwords($daftar['NamaSatuan'])."</option>\n";
												}
												
												$jsArray_h5 .= "rekName5['".$daftar['KodeSatuan']."'] = 'Rp. ".addslashes(ucwords(number_format($daftar['HargaJual5'])))."';\n";
											}
										?>
										</select>
									</div>
									
									
									<div class="form-group">
                                        <label>Harga Barang</label><br>
											<input style="border: 0px; padding: 0px;" type='text' name='_h5' id='_h5' value="<?php echo "Rp. ".number_format($harga5); ?>"readonly='readonly' />
											<script type="text/javascript"><?php echo $jsArray_h5; ?></script>
                                    </div>
									
									
									<div class="form-group">
                                        <label>Kategori Barang</label>
                                            <dd><?php echo ucwords($namadept); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Sub Kategori Barang</label>
                                            <dd><?php echo ucwords($namasubdept); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Keterangan</label>
                                            <dd><?php echo ucwords($ketbrg); ?></dd><br>
                                    </div>
										
									<?php if($isvoucher == TRUE){ echo '<div class="form-group"><dd><span class="btn btn-primary"><i class="fa fa-tag"></i> Voucher</span><br></dd></div>'; } else { echo ''; } ?>
								</div>
								
								
									
									
								</div>
								
								<!-- <div class="col-lg-4">
									<div class="form-group">
                                        <label>Harga Distributor</label>
                                            <dd><?php echo "Rp. ".number_format($harga2); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Harga Agen</label>
                                            <dd><?php echo "Rp. ".number_format($harga3); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Harga Reseller</label>
                                            <dd><?php echo "Rp. ".number_format($harga4); ?></dd>
                                    </div>
									<div class="form-group">
                                        <label>Harga Konsumen</label>
                                            <dd><?php echo "Rp. ".number_format($harga5); ?></dd>
                                    </div>
									
								</div> -->
						
						<!--features_items-->
						</div>
						</div>
                        </div>                       
                    </div>                    
                
            </div>
        </div>
    </div>
    
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>About GrosirMart</span></h2>
                        <?php include "../connections/config.php"; 
						$edit = sqlsrv_query($dbconnect, "select * from mst_setting where id = '5' AND nama_setting = 'about'") or die( print_r( sqlsrv_errors(), true));
						while($row = sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){
							echo '<p>'.ucwords($row['value']).'</p>';
						} ?>
                        <!-- <div class="footer-social">
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </div> -->
                    </div>
                </div>
                 
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- jQuery -->
    <script src="../vendor/bootstrap/js/jquery2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap2.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
	
	<!-- Slide -->
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/google-code-prettify/prettify.js"></script>
	<script src="js/portfolio/jquery.quicksand.js"></script>
	<script src="js/portfolio/setting.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>

  </body>
</html>