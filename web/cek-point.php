<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Compromise</title>
    
    <!-- Google Fonts --
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'> -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../dist/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../dist/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <?php include "header.php"; ?>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <?php include "banner.php"; ?>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <?php include "menu.php"; ?>
        </div>
    </div> <!-- End mainmenu area -->
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Cek Point Member</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <!-- <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Recent Posts</h2>
                        <ul>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                            <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                        </ul>
                    </div>
                </div> -->
				
				<?php 
						include "../connections/config.php";
						if(isset($_POST['_submit'])){
							if(empty($_POST['_username']) || empty ($_POST ['_password'])) {
								echo '<script type="text/javascript">
														sweetAlert({ title: "Maaf!", text: " Masukkan Username dan Password ", type: "error" },
														function () { window.location.href = "cek-point.php"; });
														</script>';
							}
							else{		
								// Variabel username dan password
								$username = trim(htmlspecialchars(strip_tags(stripslashes($_POST['_username']))));
								$password = htmlspecialchars(strip_tags(stripslashes(md5($_POST['_password']))));
					
								$tampil = "Select * from masterperson WHERE IsAktif='True' AND (replace(KodePersonManual,' ','')='$username' OR replace(UserName,' ','')='$username') AND Password='$password'";
								$hasil = sqlsrv_query($dbconnect, $tampil, array(),array( "Scrollable" => SQLSRV_CURSOR_KEYSET )) or die( print_r( sqlsrv_errors(), true));
								if(sqlsrv_num_rows($hasil) === 0){
									echo '<script type="text/javascript">
														sweetAlert({ title: "Data Tidak Ada!", text: " Periksa Kembali Username dan Password Anda ", type: "error" },
														function () { window.location.href = "cek-point.php"; });
														</script>';
								}else{
													
								while ($data = sqlsrv_fetch_array($hasil, SQLSRV_FETCH_ASSOC)){				
									$id = $data['KodePerson'];				
									$id2 = $data['KodePersonManual'];				
									$id3 = $data['UserName'];				
									$stats = $data['StatusPerson'];				
									$nama = $data['NamaPerson'];				
												
								}
								

								//menambahkan point masuk
								$pointMasuk = "SELECT SUM(PointMasuk) AS point_masuk FROM PointMember WHERE KodePerson = '$id' AND IsReward='1'"; //perintah untuk menjumlahkan
								$hasilPointMasuk = sqlsrv_query($dbconnect, $pointMasuk) or die( print_r( sqlsrv_errors(), true));//melakukan query dengan varibel $jumlahkan
								$tot_PointMasuk = sqlsrv_fetch_array($hasilPointMasuk); //menyimpan hasil query ke variabel $t
								$tot_PointMasuk['point_masuk'];

								//menambahkan point keluar
								$pointKeluar = "SELECT SUM(PointKeluar) AS point_keluar FROM PointMember WHERE KodePerson = '$id' AND IsReward='1'"; //perintah untuk menjumlahkan
								$hasilPointKeluar = sqlsrv_query($dbconnect, $pointKeluar) or die( print_r( sqlsrv_errors(), true));//melakukan query dengan varibel $jumlahkan
								$tot_PointKeluar = sqlsrv_fetch_array($hasilPointKeluar); //menyimpan hasil query ke variabel $t
								$tot_PointKeluar['point_keluar'];

								//cek apakah user sudah verifikasi reward, kalau belum maka point akan 0
								/* if ($is_reward=='TRUE'){
									$point_anda = (($tot_PointMasuk['point_masuk']) - ($tot_PointKeluar['point_keluar']));
								}else{
									$point_anda = 0;
								} */
								$point_anda = (($tot_PointMasuk['point_masuk']) - ($tot_PointKeluar['point_keluar']));
								}
							}
						}
						?>
                
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">
						<div class="col-lg-3"></div>
						<div class="text-center col-lg-6">
								<form method="post">
									<div class="single-sidebar">
										<h2 class="sidebar-title">Form Cek Point</h2>
										<?php if(!isset($_POST['_submit'])){ ?>
										<form action="">
											<input type="text" class="form-control" name="_username" placeholder="ID Member" value="<?php echo $_GET['keyword']; ?>" required="required">
											<input type="password" class="form-control"name="_password" placeholder="Password" value="<?php echo $_GET['keyword']; ?>" required="required"><br><br>
											<input type="submit" name="_submit" value="Cari">
										</form>
									</div>
								</form>
								
								
										
										<?php }elseif(isset($_POST['_submit'])){ ?>
										<div class="single-sidebar">
											<form action="">
												<input type="text" name="_username" value="<?php echo $id2.' : '.strtoupper($id3); ?>" readonly>
												<input type="text" name="_password" value="<?php echo ucwords($nama); ?>" readonly>
												<input type="text" name="_point" value="<?php echo number_format($point_anda,2); ?>" readonly>
												
											</form>
										</div><br/><br/><br/>
										
										<?php } ?>
									
								
						</div>
                        </div>  
                    </div>                    
                </div>
				
            </div>
        </div>
    </div>
    
    <div class="footer-top-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 text-center">
                    <div class="footer-about-us"><br>
                        <h2><span>Community Improvement System</span></h2>
                        <p>Beberapa tahun belakangan, fenomena toko online (e-commerce) seolah menjamur. Tak terhitung berapa orang yang memiliki toko online, 
						baik melalui media sosial maupun situs jual beli online. Barang yang dijual pun semakin beragam, mulai dari pakaian, aksesoris, smartphone, makanan, 
						kosmetik, bahkan layanan jasa seperti kurir juga kini disediakan dan dijual secara online. Tingginya persaingan bahkan memaksa sejumlah 
						toko online Indonesia untuk bersaing memberikan harga murah dan mengklaim bahwa bisnisnya ini merupakan toko online terpercaya. </p>
                        <div class="footer-social">
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
                 
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <?php include "footer.php"; ?>
        </div>
    </div> <!-- End footer bottom area -->
   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="js/main.js"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/script.slider.js"></script>
  </body>
</html>