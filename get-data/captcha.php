<?php
session_start();
function random($panjang_karakter){  
	$karakter = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890abcdefghijkmnopqrstuvwxyz';  
	$string = '';  
	for($i = 0; $i < $panjang_karakter; $i++) {  
		$pos = rand(0, strlen($karakter)-1);  
		$string .= $karakter{$pos};  
	}  
  return $string;  
}

$text = random(7);
$_SESSION['captcha'] = $text;
 
$image_width = 200;
$image_height = 40;
$font_size = 24;
// $characters_on_image = 6;
// $font = './monofont.ttf';
  
//The characters that can be used in the CAPTCHA code.
//avoid confusing characters (l 1 and i for example)
//$random_dots = 0;
//$random_lines = 20;
//$captcha_text_color="0x142864";
//$captcha_noise_color = "0x142864";
$image_p = imagecreate($image_width,$image_height);
$white = imagecolorallocate($image_p,0,0,0);
$black = imagecolorallocate($image_p,225,225,225);

imagestring($image_p,16,50,20,$text,$black);
imagejpeg($image_p,null,5);
?>